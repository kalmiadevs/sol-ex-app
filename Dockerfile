FROM nginx

COPY kalsoltec.conf /etc/nginx/conf.d/kalsoltec.conf
COPY ./dist/* /usr/share/nginx/html

EXPOSE 80
