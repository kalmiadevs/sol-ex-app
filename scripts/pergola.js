const _           = require('lodash');
const fs          = require('fs');
const randomColor = require('randomcolor');

"use strict";

const pt = ['SL_160_28', 'SL_170_28', 'SL_170_36'];
const it = ['1', '2', '3', '4', '5', '6'];


let codes = _.flatten(pt.map(p => it.map(i => `${p}.${i}`)));
// console.log('Codes: ', codes);

const pergola = fs.readFileSync('./pergola.svg', 'utf8');

codes.map(c => {
  const path = `../src/assets/img/products/${ c }.svg`;
  console.log(path);
  fs.writeFileSync(path, pergola.replace(/#000000/g, randomColor()));
});
