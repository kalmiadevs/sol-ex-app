import { ITranslationMessagesFile, ITransUnit, TranslationMessagesFileFactory } from 'ngx-i18nsupport-lib/dist';
import * as fs from 'fs-extra';
import * as XLSX from 'xlsx';
import {exit} from 'shelljs';


const inputExcel      = 'translations.xlsx';
const inputXliff      = '../src/locale/messages.xlf';
const outputXliffPath = '../src/locale';

const languageHeaders = [
  'fr', 'de', 'sl', 'it'
];

let workbook;
try {
  workbook = XLSX.readFile(inputExcel);
} catch (e) {
  console.error('Missing translations file, you must place the excel file with translations named "' + inputExcel + '" in this directory!');
  exit(-1);
}
const sheet = workbook.Sheets[workbook.SheetNames[0]];

const data = XLSX.utils.sheet_to_json(sheet, { header: ['id', 'en', ...languageHeaders]});
data.splice(0, 1);

const content = fs.readFileSync(inputXliff, 'UTF-8');

for (const lang of languageHeaders) {
  console.log('Translating to "' + lang + '"');
  const file: ITranslationMessagesFile = TranslationMessagesFileFactory.fromFileContent('xlf2', content, inputXliff, 'UTF-8');
  file.setTargetLanguage(lang);

  for (const translation of data) {
    const tu: ITransUnit = file.transUnitWithId((<any>translation).id);
    if (tu) { // skip missing translation units; this will happen if angular decides to change a translation unit's ID
      if (!!translation[lang]) {
        tu.translate(translation[lang]);
      } else {
        // Use the original string or angular will complain about a missing translation
        tu.translate(translation['en']);
      }
    }
  }

  const writePath = outputXliffPath + `/messages.${ lang }.xlf`;
  console.log('Saving to file "' + writePath + '"');
  fs.writeFile(writePath, file.editedContent());
}

console.log('Import complete');
