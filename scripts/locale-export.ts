import { ITranslationMessagesFile, ITransUnit, TranslationMessagesFileFactory } from 'ngx-i18nsupport-lib/dist';
import * as fs from 'fs';
import * as XLSX from 'xlsx';
import {exit} from 'shelljs';
import {WorkSheet} from 'xlsx';

const excelFile       = 'translations.xlsx';
const inputXliff      = '../src/locale/messages.xlf';


let workbook;
try {
  workbook = XLSX.readFile(excelFile);
} catch (e) {
  console.error('Missing translations file, you must place the excel file with translations named "' + excelFile + '" in this directory!');
  exit(-1);
}
const sheet: WorkSheet = workbook.Sheets[workbook.SheetNames[0]];

const sheetData = XLSX.utils.sheet_to_json(sheet, { header: ['id', 'en']});
sheetData.splice(0, 1);

const xliffContent = fs.readFileSync(inputXliff, 'UTF-8');
const inputXliffFile: ITranslationMessagesFile = TranslationMessagesFileFactory.fromFileContent('xlf2', xliffContent, inputXliff, 'UTF-8');


// Keep only new translation strings
console.log('Fetching new translation strings...');
const translationUnits: ITransUnit[] = [];
inputXliffFile.forEachTransUnit((tu: ITransUnit) => {
  translationUnits.push(tu);
});

const newUnits = translationUnits.filter((tu: ITransUnit) => sheetData.findIndex((x: any) => x.id === tu.id) < 0);

// Data to be inserted
const itemsToInsert = [];
for (const tu of newUnits) {
  itemsToInsert.push({ id: tu.id, en: tu.sourceContent() });
  console.log('\t> ' + tu.sourceContent());
}

// Insert new values
if (itemsToInsert.length) {
  console.log('Adding new strings');
  XLSX.utils.sheet_add_json(sheet, itemsToInsert, { skipHeader: true, origin: -1 });

  XLSX.writeFile(workbook, excelFile);
} else {
  console.log('No new strings found');
}

console.log('Export complete');
