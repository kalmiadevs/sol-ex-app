"use strict";

const fs         = require('fs-extra');
const _          = require('lodash');
const HTMLParser = require('fast-html-parser');

const baseLocale = '../src/app/base/core/i18n/tpl.i18n.html';
const appLocale  = '../src/app/i18n/tpl.i18n.html';

const outFile  = '../src/app/base/core/i18n/messages.ts';

const html  = fs.readFileSync(baseLocale, 'utf8') + '\n' + fs.readFileSync(appLocale, 'utf8');
const lines = html.split('\n')
  .filter(x => {
    return !!x
  })
  .map(x => {
    const n    = HTMLParser.parse(x);
    if (!n.childNodes[0]) {
      return undefined;

    }
    const i18n = n.childNodes[0].attributes['i18n'];
    return {
      key: i18n.split('@@').slice(-1)[0],
      value: n.rawText
    }
  })
  .filter(x => {
    return !!x
  });

const messages = {};
lines.forEach((v, k) => {
  messages[v.key] = v.value;
});

const out = `/// DON'T EDIT DIRECTLY!!! ///
// Command \`npm run locale\` will overwrite these file.

/* tslint:disable */

export const messages: { [key: string]: string; } = ${ JSON.stringify(messages, null, "  ") };
`;

fs.writeFileSync(outFile, out, "UTF-8");

console.log(out);
process.exit(0);
