# KalSoltec - PartnerPortal

![KalSoltec](./config/typedoc/soltec-logo.png?raw=true)

## Before start
You will have to get KalBase submodule (git@bitbucket.org:kalmiadevs/kalcorefr.git) by doing:
```bash
$ git submodule init
$ git submodule update --recursive --remote
```

Submodule is located in `./src/app/base`. Go to that folder and do git pull.

## Installation
You must have yarn installed on system and Kalmia angular cli.
If you are on Windows you also must have working `make` command.
```bash
yarn install
npm run cli:link
```

## When you need to add extra packages (if on Linux, for Windows users there is no extra steps)
```bash
npm run cli:unlink
yarn add <package_name>
npm run cli:link
npm run postinstall
```

## Development server
Run `ng serve` for a dev server. Navigate to `http://localhost:4301/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
Before running the tests make sure you are serving the app via `ng serve`.
