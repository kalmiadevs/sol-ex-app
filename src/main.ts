import { enableProdMode, Provider } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/mod.app';
import { environment } from './environments/environment';
import { getTranslationProviders } from './app/base/core/i18n/svc.i18n-provider';

if (environment.production) {
  enableProdMode();
}

if (environment.analytics) {
  const script = document.createElement('script');

  script.innerHTML = environment.analytics;
  document.head.appendChild(script);
}

getTranslationProviders().then(providers => {
  const options = { providers };
  // platformBrowserDynamic().bootstrapModule(AppModule, options);
  platformBrowserDynamic(<Provider[]>options.providers).bootstrapModule(AppModule, options);
});
