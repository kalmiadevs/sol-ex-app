import { baseEnvironment } from '../app/base/environments/environment.staging';

export const environment = {
  ...baseEnvironment,

  envName: 'staging',

  appId: 'soltec',

  raven: 'http://81973f520aad4ac3bc132a254d33cfd5@sentry.kalmia.si:9000/6',

  apiBaseProject: 'https://portal-api.kalmia.si',
  apiBaseKalBlocks: 'https://sol-api.kalmia.si',
  apiBaseIDMS: 'https://sol-idms.kalmia.si'
};
