// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
import { baseEnvironment } from '../app/base/environments/environment.local';

export const environment = {
  ...baseEnvironment,

  appId: 'soltec',

  // production: true,
  // raven: 'http://81973f520aad4ac3bc132a254d33cfd5@sentry.kalmia.si:9000/6',

  analytics: `
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
  
  ga('create', 'UA-117320488-1', 'none');
  `,

  apiBaseProject: 'http://localhost:5020/v1',
  // apiBaseProject: 'https://portal-api.kalmia.si/v1',
  // apiBaseKalBlocks: 'https://sol-api.kalmia.si/v1',
  // apiBaseIDMS: 'https://sol-idms.kalmia.si/v1'
};
