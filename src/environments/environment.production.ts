import { baseEnvironment } from '../app/base/environments/environment.production';

export const environment = {
  ...baseEnvironment,

  appId: 'soltec',

  raven: 'http://81973f520aad4ac3bc132a254d33cfd5@sentry.kalmia.si:9000/6',

  analytics: `
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
  
  ga('create', 'UA-115750431-2', 'auto');
  `,

  apiBaseProject: 'https://portal-api.soltec.si',
  apiBaseKalBlocks: 'https://api.soltec.si',
  apiBaseIDMS: 'https://idms.soltec.si'
};
