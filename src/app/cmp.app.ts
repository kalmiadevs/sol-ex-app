import { Component, HostListener } from '@angular/core';
import { Event, NavigationEnd, NavigationStart, Router } from '@angular/router';
import { SystemService } from './core/svc.system';
import { I18nService, _ } from './base/core/i18n/svc.i18n';
import { getCookie } from './base/shared/tools/cookie';
import { Angulartics2GoogleTagManager } from 'angulartics2/gtm';
import { Angulartics2GoogleAnalytics } from 'angulartics2/ga';

@Component({
  selector: 'app-root',
  templateUrl: './tpl.app.html',
  styleUrls: ['./cmp.app.scss'],
  // encapsulation: ViewEncapsulation.None
})
export class AppComponent {

  isSidebarUsed: boolean;

  showDevWarning: boolean =  window.location.hostname === 'soltec.kalmia.si' && getCookie('dev') !== 'dev';

  /**
   *
   * @param {I18nService} i18n
   * @param {Router} router
   * @param {SystemService} sys    Main purpose is to force proper initialization of all services.
   */
  constructor(
    private i18n: I18nService,
    public router: Router,
    public sys: SystemService,
    angulartics2GoogleAnalytics: Angulartics2GoogleAnalytics
  ) {

    // DEMO USAGE
    // console.info('Translation test: ' + i18n.get('appSampleTranslation', ['INJECTION WORKS']));
    // console.info('Translation test [static]: ' + _('appSampleTranslation', ['INJECTION WORKS']));
  }

  onSidebarActivate(component) {
    this.isSidebarUsed = true;
  }
  onSidebarDeactivate(component) {
    this.isSidebarUsed = false;
  }
}
