/**
 * Describes possible entity states.
 */
export enum EntityState {
  /**
   * Entity was not changed it's (same as on remote)
   */
  Pristine,

  /**
   * Entity content was modified, it should be saved to remote.
   */
  Dirty,

  /**
   * Entity was created by user and was not yet created on remote.
   */
  New,

  /**
   * Entity was removed by user, it should be also removed on remote.
   */
  Removed
}
