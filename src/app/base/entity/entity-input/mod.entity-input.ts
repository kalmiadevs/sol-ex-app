import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EntityInputComponent } from './cmp.entity-input';
import { EntityBadgeComponent } from './entity-badge/cmp.entity-badge';
import { FormsModule } from '@angular/forms';
import { UserInputComponent } from '../user/user-input/cmp.user-input';
import { PermissionsInputComponent } from '../permission/permission-input/cmp.permission-input';
import { NodeInputComponent } from '../node/node-input/cmp.node-input';
import { GroupInputComponent } from '../group/group-input/cmp.group-input';
import { TagInputModule } from '../../shared/tag-input/mod.tag-input';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    TagInputModule,
    NgbModule,
  ],
  declarations: [
    EntityInputComponent,
    EntityBadgeComponent,
    GroupInputComponent,
    NodeInputComponent,
    PermissionsInputComponent,
    UserInputComponent
  ],
  exports: [
    EntityInputComponent,
    GroupInputComponent,
    NodeInputComponent,
    PermissionsInputComponent,
    UserInputComponent
  ]
})
export class EntityInputModule { }
