import { Component, ContentChild, EventEmitter, Input, OnInit, Output, TemplateRef, ViewChild } from '@angular/core';

@Component({
  selector: 'app-entity-input',
  templateUrl: './tpl.entity-input.html',
  styleUrls: ['./cmp.entity-input.scss']
})
export class EntityInputComponent implements OnInit {

  @ContentChild(TemplateRef)
  template: any;

  @ViewChild('newEntityInputSearch') vc_tagInput;

  isAddNewInputVisible = false;

  dropdownVisible = false;

  /**
   * Used to indicate tag that is going to be removed to user (backspace).
   */
  highlightLastTag: boolean;

  // region Tags binding
  @Input() tags: any[] = [];

  @Output() tagsChange = new EventEmitter();
  // endregion

  // region Tag input binding
  _tagInput: string = '';

  @Input()
  get tagInput() { return this._tagInput; }

  @Output() tagInputChange = new EventEmitter();

  set tagInput(val) {
    this._tagInput = val;
    this.tagInputChange.emit(this._tagInput);
  }
  // endregion

  @Input() readonly: boolean;
  @Input() allowDuplicates: boolean;
  @Input() placeholderIfNoTags: string = '';
  @Input() placeholderIfTags: string = '';

  /**
   * Used only when dropdown is disabled.
   * @type {string}
   */
  @Input() suggestedTagsLabel?: string = 'Suggested tags';
  @Input() suggestedTags?: any[] = [];

  /**
   * If should suggested tags be displayed as dropdown.
   * @type {boolean}
   */
  @Input() suggestedAsDropdown: boolean;

  /**
   * Used for transforming tag obj into label used to display to user
   * @param x
   */
  @Input() tagToString: ((any) => string) = (x: string): string => x;

  /**
   * Used to convert user entered string into tag obj
   * @param x
   */
  @Input() stringToTag: ((string) => any) = (x: string): string => x;

  /**
   * If tag can be inserted.
   * @param tag
   */
  @Input() isAllowed: ((any) => boolean) = (tag): boolean => true;

  constructor() { }

  ngOnInit() {

  }

  showDropdown() {
    this.dropdownVisible = true;
  }

  hideDropdown() {
    this.dropdownVisible = false;
    this.hideInput();
  }

  tagExists(tag: string): boolean {
    return this.tags.find(x => this.tagToString(x) === tag);
  }

  addNew() {
    this.isAddNewInputVisible = true;
    this.vc_tagInput.nativeElement.focus();
  }

  onTagSubmit(tagForm) {
    const stringTag = tagForm.value.tag;
    const tag = this.stringToTag(stringTag);

    if (stringTag) {
      if (!this.allowDuplicates || !this.tagExists(stringTag)) {
        if (tag !== undefined && this.isAllowed(stringTag)) {
          // this.tags.push(tag);
          this.pushTag(tag);
        } else {
         // not on white list or couldn't convert give some feedback to user
        }
      } else {
        // already exists add shake animation
      }
    }

    this.highlightLastTag = false;
    this.hideInput();
    tagForm.reset();
  }

  hideInput() {
    this.isAddNewInputVisible = false;
    this.dropdownVisible = false;
  }

  removeTag(i) {
    this.tags.splice(i, 1);
  }

  addTag(tag, $event) {
    if ($event) {
      $event.stopPropagation();
    }
    this.pushTag(tag);
    // this.tags.push(tag);
    this.tagInput = '';
    this.hideInput();
  }

  pushTag(x) {
    this.tags.push(x);
    this.tagsChange.emit(this.tags);
  }

  tagKeyDown(event: KeyboardEvent, tagForm) {
    if (event.keyCode === 8 && !tagForm.value.tag) {
      if (this.highlightLastTag) {
        this.tags.pop();
        this.highlightLastTag = false;
      } else if (this.tags.length > 0) {
        this.highlightLastTag = true;
      }
    } else {
      this.highlightLastTag = false;
    }
  }
}
