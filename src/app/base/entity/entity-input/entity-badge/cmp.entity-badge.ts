import { Component, EventEmitter, Input, OnInit, Output, TemplateRef } from '@angular/core';

@Component({
  selector: 'app-entity-badge',
  templateUrl: './tpl.entity-badge.html',
  styleUrls: ['./cmp.entity-badge.scss']
})
export class EntityBadgeComponent implements OnInit {

  @Input()  highlight: boolean;
  @Input()  label: string;
  @Input()  tag: any;
  @Output() remove?: EventEmitter<any> = new EventEmitter();
  @Input()  readonly: boolean;
  @Input()  suggested: boolean;
  @Input()  dropdown: boolean;

  /**
   * @name template {TemplateRef<any>}
   */
  @Input() public template: TemplateRef<any>;


  constructor() { }

  ngOnInit() {
  }
}
