import { cloneDeep as _cloneDeep, each as _each, mapValues as _mapValues, reduce as _reduce, isObject  as _isObject } from 'lodash';
import { AppError } from '../../core/cls.error';
import { AmbiguousStateWarning, AppWarning } from '../../core/cls.warning';
import { ListEditActions } from '../../shared/cls.list-edit-actions';
import { Entity } from '../cls.entity';
import { NEW_ENTITY_ID_PREFIX } from '../svc.entity-with-remote';
import { EntityState } from '../cls.entity-state';
import { GroupApiResponse } from './itf.group-api-response';
import { Entities } from '../itf.entities';
import { Permission } from '../permission/cls.permission';
import { Crypt, PublicKey, SecretKey } from '../../core/crypt/cls.crypt';
import { KeyGroup, KeyRing } from '../../core/crypt/cls.key-ring';

export enum ScopeType {
  'private',
  'public'
}

export class Group extends Entity {

  public title          : string;

  public users          : string[] = [];
  public nodes          : string[] = [];
  public permissions    : Entities<Permission> = {};

  public owner          : string;

  public scope          : ScopeType;

  /**
   * @deprecated
   */
  public version        : number = 0;

  /**
   * Keys are only exposed when needed by vault.
   */
  public keyRing: Entities<KeyGroup> = {};

  public publicKey: PublicKey;

  /**
   * If we are not part of group key won't be set.
   */
  public secretKey?: SecretKey;


  public $warnings : AppWarning[];
  public $error?  : AppError;

  /**
   * List of actions applied to node.
   */
  public $nodeActions: ListEditActions<string> = new ListEditActions<string>();
  public $userActions: ListEditActions<string> = new ListEditActions<string>();

  get $private(): boolean { return this.scope === ScopeType.private; }

  public static create(data: any): Group {
    const g = new Group();
    g.id          = data.id;
    g.title       = data.title || '';
    g.users       = data.users || [];
    g.nodes       = data.nodes || [];
    g.owner       = data.owner ;
    // g.permissions = _reduce(data.permissions, (a, v) => {
    //   a[v.key] = Permission.create(v);
    //   return a;
    // }, {});

    g.permissions =  data.permissions || {};

    if (data.private) {
      g.scope = ScopeType.private;

      if (g.users.length !== 1 || g.users[0] !== g.owner) {
        g.$warnings.push(new AmbiguousStateWarning(g.clone()));
        g.users = [ g.owner ]; // auto fix
      }
    } else {
      g.scope = ScopeType.public;
    }

    g.publicKey = g.publicKey || data.publicKeyBase64 && Crypt.publicKeyParse(data.publicKeyBase64);

    // TODO WE MUST CHECK FOR THIS
    // if (!g.publicKey) {
    //   throw new Error('Group does not have public key!');
    // }

    g.setKeyRing(data.keyRing);

    return g;
  }

  static createFromApi(g: GroupApiResponse): Group {
    g.title = g.title || g.name; // TODO settle this ugly hack once and for all

    if (_isObject(g.permissions)) { // old api returned string here
      g.permissions = _reduce(g.permissions || {}, (a, v, k) => {
        a[k] = Permission.createFromApi({value: v, key: k, description: k});
        return a;
      }, {});
    } else {
      g.permissions = {};
    }

    return Group.create({...g, publicKey: undefined, publicKeyBase64: g.publicKey, permissions: g.permissions });
  }

  static createNew(): Group {
    const x = new Group();
    x.$state = EntityState.New;
    x.scope = ScopeType.private;

    const keyPair = Crypt.genKeyPair();
    x.secretKey = keyPair.sec;
    x.publicKey = keyPair.pub;

    return x;
  }

  /**
   * Apply node changes to group.
   * @param actions
   */
  public applyNodeActions(actions: ListEditActions<string>) {
    Array.from(actions.addActions).map(this.addNode);
    Array.from(actions.removeActions).map(this.removeNode);
  }

  /**
   * Apply user changes to group.
   * @param actions
   */
  public applyUserActions(actions: ListEditActions<string>) {
    // TODO this should be used like applyNodeActions if data changed, but is not yet
    Array.from(actions.addActions).map(this.addUser);
    Array.from(actions.removeActions).map(this.removeUser);
  }

  public getKeyGroup(id: string): KeyGroup {
    if (this.secretKey && this.keyRing[id]) {
      try {
        this.keyRing[id].decrypt(this.secretKey);
        return this.keyRing[id];
      } catch (e) {
        // TODO low priority
        // can't decrypt keygroup in group keyring
        console.error(e);
      }
    }
    return undefined;
  }

  public setKeyGroup(id: string, keyGroup: KeyGroup) {
    this.keyRing[id] = keyGroup;
  }

  public setKeyRing(keyRing: Entities< {
    sym?: string,
    pub?: string,
    sec?: string,
  }>) {
    if (keyRing) {
      _each(keyRing, (val, id) => {
        if (id === 'undefined') {
          // bad data detected
        }
        this.setKeyGroup(id, KeyGroup.createFromApi(<any>val));
      });
    }
  }

  /**
   * If user belongs to this group.
   * @param uid
   * @returns {boolean}
   */
  public containsUser(uid: string): boolean {
    return this.users.indexOf(uid) !== -1;
  }

  addPermission(p: Permission) {
    this.permissions[p.id] = p;
  }

  public addUser(uid: string) {
    if (this.users.indexOf(uid) === -1) {
      this.users.push(uid);
    }
    this.$userActions.add(uid);
  }

  public removeUser(uid: string) {
    const i = this.users.indexOf(uid);
    if (i !== -1) {
      this.users.splice(i, 1);
      this.$userActions.remove(uid);
    }
  }

  /**
   * Returns
   * @returns {string[]}
   */
  public getNewUsers(): string[] {
    return this.users.filter(x => x.indexOf(NEW_ENTITY_ID_PREFIX) === 0);
  }

  public addNode(nid: string) {
    this.nodes.push(nid);
    this.$nodeActions.add(nid);
  }

  /**
   * Remove node from group
   * @param nid
   * @returns {boolean}   Returns true if node was found & removed else false
   */
  public removeNode(nid: string): boolean {
    const i = this.nodes.indexOf(nid);
    if (i !== -1) {
      this.nodes.splice(i, 1);
      this.$nodeActions.remove(nid);
      return true;
    }
    return false;
  }

  /**
   * Returns
   * @returns {string[]}
   */
  public getNewNodes(): string[] {
    return this.nodes.filter(x => x.indexOf(NEW_ENTITY_ID_PREFIX) === 0);
  }

  /**
   * If user belongs to this group.
   * @param nid
   * @returns {boolean}
   */
  public containsNode(nid: string): boolean {
    return this.nodes.indexOf(nid) !== -1;
  }

  /**
   * Clone group
   * @returns {Group}
   */
  public clone(): Group {
    return _cloneDeep(this);
  }

  public niceName(): string { return this.title; }

  public getSaveObj(): any {
    return {
      id: this.id,
      title: this.title,
      name: this.title,   // TODO server expects name instead of title (fix it on server)

      users: this.users,
      nodes: this.nodes,
      permissions: _mapValues(this.permissions, (val, key) => {
        return val.getSaveObj();
      }),
      owner: this.owner,
      scope: this.scope,
      version: this.version,

      keyRing: KeyRing.encrypt(this.secretKey, this.keyRing),
      publicKey: this.publicKey && Crypt.publicKeySerialize(this.publicKey)
    };
  };
}
