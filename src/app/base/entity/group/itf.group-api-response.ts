import { Entities } from '../itf.entities';

export interface CreateGroupApiResponse {
  status: string;
  group: GroupApiResponse;
}

export interface GroupApiResponse {
  _id: string;
  deletedAt?: any;
  deleted: boolean;
  updatedAt: Date;
  createdAt: Date;
  title: string;
  name: string;
  owner: string;
  permissions: Entities<number>;
  publicKey: string;
  __v: number;
  nodes: any[];
  users: any[];
}
