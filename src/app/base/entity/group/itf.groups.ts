import { Group } from './cls.group';

export interface Groups {
  [key: string]: Group;
}
