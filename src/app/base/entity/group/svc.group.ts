import { Injectable } from '@angular/core';

import { HttpAuthService } from '../../core/http/svc.http-auth';

import { flow, keyBy, map, mapValues } from 'lodash/fp';
import { each as _each, map as _map, reduce as _reduce } from 'lodash';
import { Group } from './cls.group';
import { ListEditActions } from '../../shared/cls.list-edit-actions';
import { Groups } from './itf.groups';
import { NodeIdChange } from '../node/svc.node';
import { BaseSystemService } from '../../core/svc.system';
import { EntityWithRemoteService } from '../svc.entity-with-remote';
import { Entities } from '../itf.entities';
import { environment } from '../../../../environments/environment';
import { KeyGroup } from '../../core/crypt/cls.key-ring';
import { EntityState } from '../cls.entity-state';
import { Crypt } from '../../core/crypt/cls.crypt';

/**
 * Group service.
 *
 * Notes:
 *
 * Scenario:
 * User A edits groups.
 * User B edits groups.
 * User B saves groups (each group change should be handled separately)
 * User A saves groups (each group change should be handled separately)
 *
 * PROBLEM: how to handle already changed group?
 * SOLUTION: group change (nodes[]) should not be handled
 * as whole, but as add or remove node action.
 *
 * This should be same for node creation and update process!
 */
@Injectable()
export class BaseGroupService extends EntityWithRemoteService<Group> {

  // protected apiEntityName      = `group`;
  // protected apiEntitiesName    = `groups.json`;

  protected apiEntityName      = `groups`;
  protected apiEntitiesName    = `groups`;
  protected apiBase            = `${ environment.apiBaseKalBlocks }`;

  get groups() { return this.entities; }

  constructor(protected http: HttpAuthService) {
    super(http);
  }

  public getGroups() { return this.getEntities(); }

  public init(sys: BaseSystemService) {
    super.init(sys);

    const observable = this.sys.getNodeIdChangeObservable();
    if (observable) {
      observable.subscribe(x => {
        this.onNodeIdChange(x);
      });
    }

    // TODO same for users
  }

  /**
   * Correct groups state when id of node changes
   * @param data
   */
  private onNodeIdChange(data: NodeIdChange) {
    _each(this.store.entities, g => {
      if (g.removeNode(data.oldId)) {
        g.addNode(data.newId);
      }
    });
    this.notifyEntitiesChange();
  }

  protected onEntitiesReceiveFromRemote(groups: Entities<Group>) {
    this.onAllGroupsLoad(groups);
  }

  protected onEntityReceiveFromRemote(g: Group) {
    if (this.store.entities[g.id]) {
      g.applyNodeActions(this.store.entities[g.id].$nodeActions);
    }
    this.addEntity(g);
  }

  public groupsIdToTitles(ids: string[]): string[] {
    return ids.map(id => this.store.entities[id] ? this.store.entities[id].title : `<${ id }>` );
  }

  /**
   * Create groups from object.
   * @param data
   */
  public objToGroups(data: any[]): Group[] {
    const u = this.sys.authService.getUser();
    return flow(
      map(Group.createFromApi),
      map((g) => {
        const keyGroup = u.getKeyGroup(g.id);
        g.secretKey = keyGroup && keyGroup.secretKey;
        return g;
      }),
    )(data);
  }

  protected convertRemoteResToEntities(data: any): Promise<Group[]> {
    return new Promise((resolve, reject) => {
      resolve(this.objToGroups(data.groups));
    });
  }

  protected convertRemoteResToEntity(data: any): Promise<Group> {
    return new Promise((resolve, reject) => {
      const g = Group.createFromApi(data);
      const keyGroup = this.sys.authService.getUser().getKeyGroup(g.id);
      g.secretKey = keyGroup && keyGroup.secretKey;
      resolve(g);
    });
  }

  /**
   * Remove or add node from groups.
   * @param nid       Node id that groups apply to.
   * @param actions   Remove/add group from node.
   */
  public applyGroupChangesForNode(nid: string, actions: ListEditActions<string>) {
    Array.from(actions.addActions).map(gid => this.store.entities[gid].addNode(nid));
    Array.from(actions.removeActions).map(gid => this.store.entities[gid].removeNode(nid));
    this.notifyEntitiesChange();
  }

  /**
   * Collect all pending remove or add actions in groups for given node id
   * @param nid
   * @returns {ListEditActions<string>}
   */
  public getGroupChangesForNode(nid: string): ListEditActions<string> {
    const l = new ListEditActions<string>();
    _map(this.store.entities, g => {
      if (g.$nodeActions.addActions.has(nid)) {
        l.add(g.id);
      } else if (g.$nodeActions.removeActions.has(nid)) {
        l.remove(g.id);
      }
    });
    return l;
  }

  /**
   * Take any pending actions and reapply them to new groups.
   * @param newGroups
   */
  public onAllGroupsLoad(newGroups: Groups) {
    _map(newGroups, g => {
      if (this.store.entities[g.id]) {
        g.applyNodeActions(this.store.entities[g.id].$nodeActions);
      }
    });

    // TODO what about $new groups?

    this.setEntities(newGroups);
  }

  public getGroupUsers(gid: string): string[] {
    return this.store.entities[gid].users;
  }

  public setKeyToGroups(id: string, groupsId: string[], keyGroup: KeyGroup, markAsNew = false) {
    _each(groupsId, (gid) => {
      this.store.entities[gid].setKeyGroup(id, keyGroup);
      if (markAsNew && this.store.entities[gid].$state === EntityState.Pristine) {
        this.store.entities[gid].$state = EntityState.Dirty;
      }
    });
    this.notifyEntitiesChange();
  }

  public getBestKeyGroup(id: string, searchGroups?: string[]): KeyGroup | undefined {
    const res = _reduce(
      searchGroups || Object.keys(searchGroups),
      (a, gid) => {
        if (a && a.secretKey) {
          return a;
        }

        if (this.store.entities[gid]) {
          const x = this.store.entities[gid].getKeyGroup(id);
          a = x || a;
        }
        return a;
      },
      undefined
    );

    if (res) {
      // because we might decrypt some keys so lets save them
      this.notifyEntitiesChange();
    }
    return res;
  }

  protected hookBeforeSaveEntity(group: Group): Group {
    if (!group.secretKey) {
      throw new Error('DON\'T HAVE SECRET KEY OF GROUP');
    }

    if (group.$new) {
      group.$userActions.clear();
      group.$userActions.addActions = new Set(group.users);

      // TODO only tmp here we should add master user
      const u = this.sys.authService.getUser();
      group.addUser(u.id);

      // TODO
      // u.setKeyGroup()
    } else {
      group.$userActions = ListEditActions.createDiff(
        this.store.entities[group.id].users,
        group.users
      );
    }
    return group;
  }

  protected getEntitySavePayload(group: Group): Promise<any> {
    return new Promise((resolve, reject) => {
      if (group.$removed) {
        resolve();
      } else {
        try {
          const x = group.getSaveObj();
          const cipherSecretKey = Crypt.secretKeySerialize(group.secretKey);
          const addUsers     = Array.from(group.$userActions.addActions)
            .map(uid => {
              try {
                const u = this.sys.userService.getEntity(uid);
                return {
                  id: u.id,
                  keys: { sec: Crypt.encrypt(u.publicKey, cipherSecretKey) }
                };
              } catch (e) {
                reject(e);
              }
            });
          const removeUsers  = Array.from(group.$userActions.removeActions); // TODO need to re-encrypt all

          resolve({
            group: x,
            addUsers,
            removeUsers,
            keysForUsers: addUsers // TODO change on server side to read this, like for nodes
          });
        } catch (error) {
          reject(error);
        }
      }
    });
  }
}
