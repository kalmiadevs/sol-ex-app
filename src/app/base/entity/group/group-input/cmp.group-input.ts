import { Component, EventEmitter, forwardRef, Inject, Input, OnInit, Output } from '@angular/core';
import 'rxjs/add/operator/switchMap';
import { find as _find, reduce as _reduce } from 'lodash';
import { Group } from '../cls.group';
import { Groups } from '../itf.groups';
import { StoreService } from '../../../core/svc.store';

@Component({
  selector: 'app-group-input',
  templateUrl: './tpl.group-input.html',
  styleUrls: ['./cmp.group-input.scss']
})
export class GroupInputComponent implements OnInit {

  groups: Groups;
  groupInputText: string = '';

  @Input() readonly: boolean;

  // region node groups binding
  _nodeGroups: string[] = [];

  @Input()
  get nodeGroups() { return this._nodeGroups; }

  @Output() tagsChange = new EventEmitter();

  set nodeGroups(val: string[]) {
    this._nodeGroups = val;
    this.sort();
    this.tagsChange.emit(this._nodeGroups);
  }
  // endregion

  constructor (
    @Inject(forwardRef(() => StoreService)) sysStore: StoreService
  ) {
    sysStore.groupService.groups.subscribe(x => this.groups = x);
  }

  ngOnInit() {
    this.sort();
  }

  sort() {
    // this._nodeGroups.sort((a, b) =>
    //   this.groups[b].permissions.getScore() - this.groups[a].permissions.getScore()
    // );
  }

  /**
   * Convert group title into group id
   * @param title
   */
  stringToGroupId = (title) => (_find(this.groups, g => g.title === title) || { id: undefined }).id;

  /**
   * Convert group id into title
   * @param gid
   */
  groupIdToString = (gid: string) => this.groups[gid] ? this.groups[gid].title : gid;

  /**
   * Return suggested groups.
   */
  get suggestedGroups(): string[] {
    return _reduce(this.groups,
      (a: string[], v: Group) =>
        (!this.groupInputText || v.title.indexOf(this.groupInputText) !== -1) && this.nodeGroups.indexOf(v.id) === -1
          ? [...a, v.id] : a,
      []);
  };
}
