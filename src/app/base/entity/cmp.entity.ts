import { EventEmitter } from '@angular/core';
import { Entity } from './cls.entity';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { EntityWithRemoteService } from './svc.entity-with-remote';
import { BaseSystemService } from '../core/svc.system';
import { HttpError, NativeError } from '../core/cls.error';
import { ResponseJson } from '../core/http/cls.response-json';
import { EntityUiMessages } from './itf.entity-ui-messages';
import { EntityRemoveModalComponent } from './cmp.entity-remove-modal';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { environment } from '../../../environments/environment';
import { ISubscription } from 'rxjs/Subscription';

export abstract class EntityComponent<T extends Entity, TS extends EntityWithRemoteService<T>> {

  snapshotDataSubscription: ISubscription;
  snapshotDataSetFor: string;

  /**
   * Any warnings to display to UI.
   * @type {{}}
   */
  messages: EntityUiMessages = {};

  /**
   * Indicates if entity is in process of saving.
   */
  saving: boolean;

  /**
   * Entity copy used by component.
   */
  entity: T;

  /**
   * Parameter name used in data resolver.
   */
  abstract entityDataResolver: string = 'entity';

  /**
   * Parameter name used in route
   * For example: 'node' would be for http://kalblocks.com/node/123/edit
   */
  abstract entityRouteName: string = 'entity';

  onEditToggle: EventEmitter<any> = new EventEmitter();
  onSaveEvent: EventEmitter<any> = new EventEmitter();

  get canEdit(): boolean {
    return true;
  }

  get editEnabled(): boolean {
    return true;
  }

  get canEditBtnVisible(): boolean {
    return !this.edit && this.canEdit;
  }

  get canRemove(): boolean {
    return this.edit && !this.entity.$new;
  }

  get canRemoveBtnVisible(): boolean {
    return this.edit && this.canRemove;
  }

  _edit: boolean;

  /**
   * Indicates if component is in edit mode.
   */
  get edit(): boolean { return this._edit; }

  /**
   * Set edit mode state.
   */
  set edit(val: boolean) {
    // if (val && !this.canEdit) {
    //   val = false; // block edit
    //   console.warn('You can not edit this entity.');
    // }
    this._edit = val;
    if (this.entity && !this.entity.$new) {
      this.location.replaceState(`${ this.entityRouteName }/${ this.entity.id }${ val ? '/edit' : '' }`);
    }
    this.onEditToggle.emit(this.edit);
  }

  get entityCmp(): EntityComponent<T, TS> { return this; }

  /**
   * Confirm dialog before remove.
   */
  removeModalDialog: any = EntityRemoveModalComponent;

  constructor (
    protected location: Location,
    protected route: ActivatedRoute,
    protected router: Router,
    protected sys: BaseSystemService,
    protected entityService: TS, // Service responsible for @entity.
    protected modalService: NgbModal
  ) {}

  onInit() {
    if (this.route.snapshot.data[this.entityDataResolver]) {
      this.onInitExistingEntity();
    } else {
      this.onInitNewEntity();
    }
  }

  abstract createNewEntity(): T;

  /**
   * If entity object can be updated.
   * @return {boolean}
   */
  canUpdateEntity(x: T): boolean { return true; }

  onInitNewEntity() {
    this.route.params.subscribe(params => {
      this.setEntity(this.createNewEntity());
      this.edit = true;
    });
  }

  // hook_onEntitySetFromSnapshot(e: T, data: any) {}

  onInitExistingEntity() {
    this.snapshotDataSubscription = this.route.snapshot.data[this.entityDataResolver].subscribe((x: T) => {
      if (this.canUpdateEntity(x)) {
        if (x.id !== this.snapshotDataSetFor && this.route.snapshot.data['edit']) {
          this.edit = true;
          this.snapshotDataSetFor = x.id;
        }
        // this.hook_onEntitySetFromSnapshot(x, this.route.snapshot.data);
        this.setEntity(x);
      }
    });
  }

  onDestroy() {
    if (this.snapshotDataSubscription) {
      this.snapshotDataSubscription.unsubscribe();
    }
  }

  clearMessages() {
    this.messages = {};
  }

  setEntity(e: T) {
    if (e) {
      this.entity = e;
      this.onEntityChange();
    } else {
      if (!environment.production) {
        console.warn('Warning! Function setEntity called with undefined entity.');
      }
    }
  }

  onEntityChange() {
    if (this.entity) {
      if (this.entity.$error && this.entity.$error instanceof NativeError) {
        this.messages.errorMessage = this.entity.$error.error.message;
      }
    } else {
      if (!environment.production) {
        console.warn('Warning! this.entity is undefined.');
      }
    }
  }

  cancelEdit() {
    if (this.entity.$new) {
      // node creation was canceled, lets go back from wherever user came
      // Note about behaviour: if user came from external source it will go back to that
      this.location.back();
    } else {
      this.edit                     = false;
      this.clearMessages();
      this.setEntity(this.entityService.getEntity(this.entity.id)); // get original from store
    }
  }

  toggleEdit() {
    if (this.edit) {
      this.cancelEdit();
    } else {
      this.edit = true;
    }
  }

  handleEntityError (e: {error?: any, lockedEntity: T}) {
    if (e.error instanceof HttpError) {
      if (ResponseJson.isResponseJson(e.error.httpResponse) && e.error.httpResponse.data && e.error.httpResponse.data.errorMessage) {
        return this.messages.errorMessage = e.error.httpResponse.data.errorMessage;
      }

      if (!this.sys.tryHandleHttpError(e.error)) {
        return this.messages.httpError = e.error;
      }
    }

    this.messages.error = e.error;
  }

  onSave() {
    this.saving = false;
    this.edit   = false;
    this.onSaveEvent.emit();
  }

  save(form) {
    if (this.edit) {
      this.clearMessages();
      this.saving = true;
      this.entityService.save(this.entity)
        .then(x => {
          const id = x.lockedEntity.$new ? x.res.data.id : this.entity.id;
          this.setEntity(this.entityService.getEntity(id));
          this.onSave();
        })
        .catch(e => {
          this.onSaveFail(e);
        });
    }
  }

  onSaveFail(e) {
    this.saving = false;
    this.handleEntityError(e);
  }

  remove() {
    this.entityService.remove(this.entity.id)
      .then(x => {
        this.back();
      })
      .catch(e => {
        this.handleEntityError(e);
      });
  }

  removeWithConfirmDialog() {
    const modalRef = this.modalService.open(this.removeModalDialog);
    modalRef.componentInstance.titles = [ this.entity.niceName() ];
    modalRef.result
      .then(modalRes => {
        this.remove();
      })
      .catch(e => {});
  }

  back() {
    this.router.navigate(['/']);
  }
}
