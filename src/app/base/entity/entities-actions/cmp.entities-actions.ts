import { Component, Input } from '@angular/core';
import { EntitiesComponent } from '../cmp.entities';

@Component({
  selector: 'app-entities-actions',
  templateUrl: './tpl.entities-actions.html',
  styleUrls: ['./cmp.entities-actions.scss']
})
export class EntitiesActionsComponent {

  @Input() addLink: string[];
  @Input() customAdd: boolean;
  @Input() entitiesCmp?: EntitiesComponent<any, any>;
  @Input() bulkActionsEnabled: boolean = true;

  constructor() { }

  selectAll() {
    this.entitiesCmp.displayedEntities.map(id => {
      this.entitiesCmp.selectEntity(id);
    });
  }

  deselectAll() {
    this.entitiesCmp.deselectAll();
  }
}
