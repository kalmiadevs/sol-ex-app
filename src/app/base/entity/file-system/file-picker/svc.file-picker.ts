import { Injectable } from '@angular/core';
import { BaseFileSystemService } from '../svc.file-system';
import { formGroupNameProvider } from '@angular/forms/src/directives/reactive_directives/form_group_name';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FileManagerComponent } from '../file-manager/cmp.file-manager';
import { FilePickerComponent } from './cmp.file-picker';
import { FsFile } from '../cls.fs-node';

@Injectable()
export class FilePickerService {

  constructor(
    protected modalService: NgbModal
  ) { }

  pick(fs: BaseFileSystemService, path: string[] = []): Promise<FsFile | undefined> {
    const x = this.modalService.open(FilePickerComponent, { backdrop: 'static', size: 'lg' });
    x.componentInstance.fs = fs;
    x.componentInstance.lockedPath = [];
    x.componentInstance.path = path;
    return x.result;

    // return new Promise(() => {
    //
    // });
  }
}
