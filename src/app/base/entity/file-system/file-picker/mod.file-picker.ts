import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FilePickerComponent } from './cmp.file-picker';
import { FilePickerService } from './svc.file-picker';
import { FileManagerModule } from '../file-manager/mod.file-manager';

@NgModule({
  imports: [
    CommonModule,
    FileManagerModule
  ],
  declarations: [
    FilePickerComponent
  ],
  providers: [
    FilePickerService
  ],
  entryComponents: [
    FilePickerComponent
  ],
  exports: [
  ]
})
export class FilePickerModule { }
