import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { BaseFileSystemService } from '../svc.file-system';
import { FsItem } from '../cls.fs-node';

@Component({
  selector: 'app-file-picker',
  templateUrl: './tpl.file-picker.html',
  styleUrls: ['./cmp.file-picker.scss']
})
export class FilePickerComponent implements OnInit {

  @Input() fs: BaseFileSystemService;
  @Input() lockedPath?: string[] = [];
  @Input() path: string[] = [];

  constructor(
    public activeModal: NgbActiveModal
  ) {

  }

  ngOnInit() {
  }

  open(item: FsItem) {
    if (item.isFile()) {
      this.activeModal.close(item);
    }
  }
}
