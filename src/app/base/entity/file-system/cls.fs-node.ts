import { Entity } from '../cls.entity';
import { isString as _isString, reduce as _reduce, cloneDeep as _cloneDeep, size as _size } from 'lodash';
import { EntityState } from '../cls.entity-state';
import { Entities } from '../itf.entities';
import { ResponseToEntityError } from '../../core/cls.error';

export type FsItem = FsNode | FsDir | FsFile;

export abstract class FsNode extends Entity {
  $parent?: FsDir;
  _size: number = undefined;

  // region MetaData
  modify?: Date;
  birth?: Date;
  meta: any;
  // endregion

  get size(): number {
    return this._size;
  }

  set size(value: number) {
    this._size = value;
  }

  get name(): string {
    return this.id;
  }

  get pathArray(): string[] {
    return this.$parent ? [...this.$parent.pathArray, this.id] : [this.id];
  }

  get path(): string {
    return `/${ this.pathArray.join('/') }`;
  }

  static createFromApi(json: any): FsFile | FsDir {
    return {
      'file': FsFile.createFromApi,
      'dir': FsDir.createFromApi,
      undefined: () => new Error('Server returned unknown FsItem type.')
    }[json.type](json);
  }

  isFile(): this is FsFile {
    return this instanceof FsFile;
  }

  isDir(): this is FsDir {
    return this instanceof FsDir;
  }
}


export class FsDir extends FsNode {
  /**
   * Content of folder.
   * If undefined it wasn't loaded yet.
   * If [] folder is empty.
   */
  children?: Entities<FsItem>;

  get size() {
    return this._size !== undefined ? this._size : _reduce(this.children, (a, v) => a + v.size, 0);
  }

  get childrenLoaded(): boolean {
    return this.children !== undefined;
  }

  get childrenCount(): number {
    return _size(this.children);
  }


  static createFromApi(json: any): FsDir {
    const x = new FsDir();
    x.id = json.name;
    return x;
  }

  static createNew(name: string = '') {
    const x    = new FsDir();
    x.birth    = new Date();
    x.id       = name;
    x.children = {};
    return x;
  }

  addChild(...item: FsItem[]) {
    this.children = this.children || {};
    item.forEach(x => {
      x.$parent           = this;
      this.children[x.id] = x;
    });
  }

  clone(): FsDir {
    return _cloneDeep(this);
  }

  public niceName(): string {
    return this.name;
  }
}

export class FsRootDir extends FsDir {
  get pathArray(): string[] {
    return [];
  }

  static create(name: string = '<root>') {
    const x    = new FsRootDir();
    x.id       = name;
    return x;
  }

  clone(): FsRootDir {
    return _cloneDeep(this);
  }
}

export class FsFile extends FsNode {

  /**
   * Used when uploading to remote.
   */
  $file?: File;

  /**
   * Used when uploading to remote.
   */
  $content?: string;

  $progress?: ProgressEvent;

  get progressPercentage(): number {
    // return 30;
    return this.$progress ? (this.$progress.loaded * 100) / this.$progress.total : (this.$pristine ? 100 : 0);
  }

  get ext(): string {
    const x = this.name.split('.');
    return x.length > 1 ? x.pop() : '';
  }

  static createFromApi(json: any): FsFile {
    const x = new FsFile();
    x.id     = json.name;
    x.size   = json.size;
    x.modify = json.lastModified;
    return x;
  }

  static createNew(name: string, content?: string | File) {
    const x = new FsFile();
    x.$state = EntityState.New;
    x.size = 0;
    x.birth = new Date();
    x.id = name;
    if (content) {
      if (_isString(x)) {
        x.$content = <string>content;
      } else {
        x.$file = <File>content;
      }
    }
    return x;
  }

  static createFromFile(file: File) {
    const x = FsFile.createNew(file.name);
    x.size   = file.size;
    x.modify = new Date(file.lastModifiedDate);
    x.$file  = file;
    return x;
  }

  clone(): FsFile {
    return _cloneDeep(this);
  }

  public niceName(): string {
    return this.name;
  }
}

export class FsStandaloneFile extends FsFile {

  $path: string[];

  static createFromApi(json: any): FsStandaloneFile {
    const x = new FsStandaloneFile();
    x.id     = json.name;
    x.size   = json.size;
    x.modify = json.lastModified;
    x.$path  = (json.path || '').split('/');

    // if path was /a/b   remove first ""
    if (!x.$path[0]) {
      x.$path.shift();
    }
    return x;
  }

  get pathArray(): string[] {
    return this.$path;
  }

  clone(): FsStandaloneFile {
    return _cloneDeep(this);
  }
}
