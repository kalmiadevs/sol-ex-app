import { Injectable } from '@angular/core';
import { HttpAuthService } from '../../core/http/svc.http-auth';
import { FsDir } from './cls.fs-dir';
import { FsFile } from './cls.fs-file';
import { each as _each } from 'lodash';
import { FsItem, FsNode, FsRootDir, FsStandaloneFile } from './cls.fs-node';
import { environment } from 'environments/environment';
import { RequestOptions, Headers, Http } from '@angular/http';
import { AuthService as Ng2UiAuthService } from 'ng2-ui-auth';
import { Subject } from 'rxjs/Subject';
import { ResponseJson } from '../../core/http/cls.response-json';
import { EntityState } from '../cls.entity-state';


/**
 * We are not extending from generic entities svc, because
 * this is service is providing mutable tree structure.
 * Again, MUTABLE!!! so don't forget to make copy if you wan't
 * to mess with obj.
 */
@Injectable()
export class BaseFileSystemService {

  change: Subject<string[]> = new Subject<string[]>();

  root: FsRootDir = FsRootDir.create();

  bucket: string;

  static rebuildTree(x: FsDir) {
    if (x.childrenLoaded) {
      _each(x.children, c => {
        c.$parent = x;
        if (c.isDir()) {
          BaseFileSystemService.rebuildTree(c);
        }
      });
    }
  }

  constructor(protected http: HttpAuthService,
              protected ng2UiAuthService: Ng2UiAuthService) {

  }

  get(path: string[] = []): FsItem {
    return this.getItemFrom(path, this.root);
  }

  getItemFrom(path: string[] = [], item: FsDir): FsItem {
    const x = path.shift();
    return x && item.children[x] && item.children[x].isDir() ? this.getItemFrom(path, <FsDir>item.children[x]) : item;
  }

  getAsyn(path: string[] = []): Promise<FsItem> {
    return this.getItemFromAsyn(path, this.root);
  }

  getItemFromAsyn(path: string[] = [], item: FsDir): Promise<FsItem> {
    const x = path.shift();
    if (!x) {
      return Promise.resolve(item);
    }

    if (item.childrenLoaded) {
      if (item.children[x] && item.children[x].isDir()) {
        return this.getItemFromAsyn(path, <FsDir>item.children[x]);
      }
    }

    return new Promise((resolve, reject) => {
      if (item.childrenLoaded) {
        resolve(item);
      } else {
        this.list(item.pathArray)
          .then(resItem => {
            if (resItem.children[x] && resItem.children[x].isDir()) {
              this.getItemFromAsyn(path, <FsDir>resItem.children[x])
                .then(resolve)
                .catch(reject);
            } else {
              resolve(item);
            }
          })
          .catch(reject)
      }
    });
  }

  beforeUpload(x: FsItem) {
    x.$locked = true;
    this.change.next(x.pathArray);
  }

  onUploadState(x: FsFile, event: ProgressEvent) {
    x.$progress = event;
    if (!environment.production) {
      console.log(`Uploading ${ x.name } ${ event.loaded }/${ event.total }`);
    }

    this.change.next(x.pathArray);
  }

  onUploadSuccessful(x: FsItem, res: any) {
    x.$locked = false;
    x.$state = EntityState.Pristine;

    if (x.isFile()) {
      x.$progress = undefined;
    }
  }

  onUploadFail(x: FsItem, error: any) {
    x.$locked   = false;
    x.setError(error);

    if (x.isFile()) {
      x.$progress = undefined;
    }
  }

  onUploadFinish(x: FsItem) {
    this.change.next(x.pathArray);
  }

  // region API
  upload(x: FsItem, options: { onProgress?: any } = {}): Promise<any> {
    return new Promise((reject, resolve) => {
      if (x.isFile()) {
        const formData: FormData = new FormData();
        formData.append('file', x.$file, x.name);

        const httpOptions = {
          onProgress: (event: ProgressEvent) => {
            this.onUploadState(x, event);
            if (options.onProgress) {
              options.onProgress(event);
            }
          }
        };

        this.beforeUpload(x);

        this.http.sendMultipartFormData('POST', `${ environment.apiBaseProject }/${ this.bucket }?filePath=${ encodeURIComponent(x.path) }`, formData, httpOptions)
          .then(res => {
            this.onUploadSuccessful(x, res.data);
            this.onUploadFinish(x);
            resolve(x);
          })
          .catch(e => {
            this.onUploadFail(x, e);
            this.onUploadFinish(x);
            reject(e);
          });
      } else {
        throw new Error('Folder upload not implemented');
      }
    });
  }

  downloadText(x: FsFile) {
    const myHeaders = new Headers();
    myHeaders.set('X-Access-Token', this.ng2UiAuthService.getToken());

    const options: RequestInit = {
      method: 'GET',
      headers: myHeaders.toJSON(),
      mode: 'cors',
      cache: 'no-cache'
    };

    return fetch(`${ environment.apiBaseProject }/${ this.bucket }?filePath=${ encodeURIComponent(x.path) }`, options).then(response => response.text())
  }

  list(path: string[] = []): Promise<FsDir> {
    return new Promise((resolve, reject) => {
      this.http.get(`${ environment.apiBaseProject }/${ this.bucket }/list?filePath=${ encodeURIComponent(path.join('/')) }`)
        .subscribe((res: ResponseJson) => {
          const children = res.data.files.map(FsNode.createFromApi);
          const node     = this.get(path);
          if (node.isDir()) {
            node.addChild(...children);
            // No need addChild does it
            // BaseFileSystemService.rebuildTree(node);
            resolve(node);
          } else {
            reject('Should be dir: ' + path.join('/'));
          }
        }, e => {
          reject(e);
        });
    });
  }

  search(dir: FsDir, name: string): Promise<FsDir> {
    return new Promise((resolve, reject) => {
      this.http.get(`${ environment.apiBaseProject }/${ this.bucket }/search?dir=${ encodeURIComponent(dir.path) }&name=${ encodeURIComponent(name) }`)
        .subscribe((res: ResponseJson) => {
          const fakeDir = new FsRootDir();
          const children = res.data.files.map(FsStandaloneFile.createFromApi);
          fakeDir.addChild(...children);
          resolve(fakeDir);
        }, e => {
          reject(e);
        });
    });
  }

  copy() {

  }

  rename() {

  }

  create() {
    // const formData: FormData = new FormData();
    // const headers = new Headers();
    // formData.append('uploadFile', entity.file, entity.file.name);
    // // headers.append('Accept', 'application/json');
    // // let options = new RequestOptions({ headers: headers });
    // resolve(formData);
  }

  // endregion


  test() {
    // const a = FsDir.createNew('test');
    // a.addChild(
    //   FsDir.createNew('sample'),
    //   FsFile.createNew('test2.txt'),
    // );
    // this.root.addChild(
    //   a,
    //   FsDir.createNew('home'),
    //   FsDir.createNew('documents'),
    //   FsDir.createNew('pictures'),
    //   FsDir.createNew('music'),
    //   FsDir.createNew('videos'),
    //   FsDir.createNew('sample'),
    //   FsFile.createNew('test2'),
    //   FsFile.createNew('test3'),
    //   FsFile.createNew('test3.pdf'),
    //   FsFile.createNew('sd.doc'),
    //   FsFile.createNew('test3.png'),
    //   FsFile.createNew('32323.png'),
    //   FsFile.createNew('sd.png'),
    //   FsFile.createNew('sds.png'),
    //   FsFile.createNew('sdds.png'),
    //   FsFile.createNew('ccc.png'),
    // );
    // BaseFileSystemService.rebuildTree(this.root);
  }
}
