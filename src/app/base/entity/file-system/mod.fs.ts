import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BaseFileSystemService } from './svc.file-system';

@NgModule({
  imports: [
    CommonModule
  ],
  providers: [

  ],
  entryComponents: [
  ]
})
export class BaseFileSystemModule { }
