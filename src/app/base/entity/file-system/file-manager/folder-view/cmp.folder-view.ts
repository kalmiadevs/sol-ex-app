import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FsDir, FsItem } from '../../cls.fs-node';
import { sortBy } from 'lodash';

@Component({
  selector: 'app-folder-view',
  templateUrl: './tpl.folder-view.html',
  styleUrls: ['./cmp.folder-view.scss']
})
export class FolderViewComponent implements OnInit {

  @Input() dir: FsDir;
  @Output() itemClick: EventEmitter<FsItem> = new EventEmitter();
  @Input() sort: false;

  get childrenIds(): string[] {
    return Object.keys(this.dir.children);
  }

  get children(): FsItem[] {
    return sortBy(Object.keys(this.dir.children).map(x => this.dir.children[x]), 'name');
  }


  constructor() { }

  ngOnInit() {
  }

  onItemClick(item: FsItem) {
    this.itemClick.emit(item);
  }

}
