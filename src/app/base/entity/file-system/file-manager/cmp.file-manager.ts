import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { BaseFileSystemService } from '../svc.file-system';
import { FsDir, FsFile, FsItem, FsNode } from '../cls.fs-node';

@Component({
  selector: 'app-file-manager',
  templateUrl: './tpl.file-manager.html',
  styleUrls: ['./cmp.file-manager.scss']
})
export class FileManagerComponent implements OnInit {

  /**
   * Idea here is that you can bring your own provider (provider extended from generic fs one)
   */
  @Input() fs: BaseFileSystemService;

  @Input() lockedPath?: string[] = [];
  @Input() path: string[] = [];

  @Input() canUpload: boolean = true;

  @Input() sort: false;

  @Output('open') onSelect: EventEmitter<FsItem> = new EventEmitter();

  cDir: FsDir;

  /**
   * Used to show search results.
   */
  fakeDir: FsDir;

  search: string = '';

  constructor() {

  }

  ngOnInit() {
    this.fs.getAsyn(this.path)
      .then(dir => {
        if (!dir.isDir()) {
          dir = dir.$parent;
        }
        this.open(<FsDir>dir);
      });

    // let dir = this.fs.get(this.path);
    // if (!dir.isDir()) {
    //   dir = dir.$parent;
    // }
    // this.open(<FsDir>dir);

    this.fs.change.subscribe((path) => {
      console.log('change: ', path);
      if (this.cDir) { // wait until loaded
        this.open(<FsDir>this.fs.get(this.path));
      }
    });
  }

  searchChange() {
    if (this.search) {
      this.fs.search(this.cDir, this.search)
        .then(list => {
          this.fakeDir = list;
          console.log(list);
        })
        .catch(e => {

        });
    } else {
      this.fakeDir = undefined;
    }
  }

  selectPathItem(index: number) {
    this.open(<FsDir>this.fs.get(this.path.slice(0, index + 1)));
  }

  uploadFile(event: { target: HTMLInputElement}) {
    if (event.target.files) {
      for (let i = 0; i < event.target.files.length; i++) {
        const f = event.target.files.item(i);
        const x = FsFile.createFromFile(f);
        this.cDir.addChild(x);
        this.fs.upload(x)
          .then(res => {
            // nothing to do
          })
          .catch(e => {
            // nothing to do
          })
      }
      // event.target.files. map(f => {
        // const fileReader = new FileReader();
        // fileReader.onload = () => {
        //   // fileReader.result
        // };
        // fileReader.readAsDataURL(event.target.files[0]);
      // });
    }
  }

  back() {
    this.path.pop();
  }

  forward() {

  }

  up() {
    this.open(this.cDir.$parent);
  }

  itemClick(item: FsNode) {
    if (item.isDir()) {
      this.open(item);
    } else {

    }

    this.onSelect.next(item);
  }

  open(dir: FsDir) {
    this.search  = '';
    this.fakeDir = undefined;

    this.path = dir.pathArray;
    this.cDir = dir;

    if (!this.cDir.childrenLoaded) {
      this.fs.list(this.cDir.pathArray);
    }
  }
}
