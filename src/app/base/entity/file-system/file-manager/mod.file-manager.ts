import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FileManagerComponent } from './cmp.file-manager';
import { FolderViewComponent } from './folder-view/cmp.folder-view';
import { NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { BaseSharedModule } from '../../../shared/mod.shared';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    BaseSharedModule,
    NgbTooltipModule,
    FormsModule
  ],
  declarations: [
    FileManagerComponent,
    FolderViewComponent
  ],
  entryComponents: [
    FileManagerComponent,
  ],
  exports: [
    FileManagerComponent,
    FolderViewComponent
  ]
})
export class FileManagerModule { }
