export interface EntityIdChange {
  oldId: string;
  newId: string;
}
