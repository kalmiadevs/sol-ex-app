import {Component, Input} from '@angular/core';

import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Entity } from './cls.entity';

@Component({
  selector: 'app-entity-remove-modal',
  template: `
    <div class="modal-header">
      <h4 class="modal-title"><i class="fa fa-exclamation-circle"></i><ng-container i18n>Remove confirmation</ng-container></h4>
      <button type="button" class="close" aria-label="Close" (click)="activeModal.dismiss()">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
      <ng-container i18n>Are you sure you want to remove:</ng-container>
      <ul>
        <li *ngFor='let x of titles'>{{ x }}</li>
      </ul>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-secondary" (click)="activeModal.dismiss()" i18n>Cancel</button>
      <button type="button" class="btn btn-primary" (click)="activeModal.close()" i18n>Remove</button>
    </div>
  `
})
export class EntityRemoveModalComponent {
  @Input() titles: string[];

  constructor(public activeModal: NgbActiveModal) {

  }
}
