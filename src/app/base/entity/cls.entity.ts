import { AppWarning } from '../core/cls.warning';
import { AppError, NativeError, UnknownError } from '../core/cls.error';
import { EntityState } from './cls.entity-state';

export abstract class Entity {
  public id        : string;

  /**
   * Used for creating automatic messages. Like for unhandled error notifications.
   * It should be written in lowercase.
   * @type {string}
   */
  public objName   : string = 'entity';
  get objDescription(): string { return this.objName + ' ' + this.id; }

  // region Dynamic data
  /**
   * If true entity is locked so other processes know not to touch it.
   * Note: This is just a flag, and it doesn't really protect from changes. That must be enforced by other processes themselves.
   */
  public $locked: boolean;

  /**
   * Indicates node state. You can also use ´$dirty´, ´$new´, ´$removed´ flags but be aware that are mutually exclusive!
   */
  public $state: EntityState = EntityState.Pristine;
  get $pristine():    boolean { return this.$state === EntityState.Pristine;}
  get $dirty():       boolean { return this.$state === EntityState.Dirty;   }
  get $new():         boolean { return this.$state === EntityState.New;     }
  get $removed():     boolean { return this.$state === EntityState.Removed; }

  /**
   * Can have multiple warnings.
   * In contrast entity can have only one $error.
   */
  public $warnings? : AppWarning[];

  /**
   * Can have only one error.
   * Limitation to one error only is here because error is fatal and should be resolved.
   */
  public $error?    : AppError;

  get hasError(): boolean {
    return !!this.$error;
  }

  get hasWarnings(): boolean {
    return this.$warnings && !!this.$warnings.length;
  }
  // endregion


  public static isEntity(x: any): x is Entity {
    return x instanceof Entity;
  }

  setError(error: any) {
    if (error instanceof AppError) {
      this.$error = error;
    } else if (error instanceof Error) {
      this.$error = new NativeError(error);
    } else {
      this.$error = new UnknownError(error);
    }
  }

  addWarning(warning: AppWarning) {
    this.$warnings.push(warning);
  }

  clearError() {
    delete this.$error;
  }

  abstract clone(): Entity;

  /**
   * Human readable name of entity.
   * For example user entity would return: John Snow
   */
  public abstract niceName(): string;

  public getSaveObj(): any { return { id: this.id }; };
}
