import { Component, EventEmitter, HostListener } from '@angular/core';
import { Entity } from './cls.entity';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { EntityWithRemoteService } from './svc.entity-with-remote';
import { BaseSystemService } from '../core/svc.system';
import { HttpError } from '../core/cls.error';
import { ResponseJson } from '../core/http/cls.response-json';
import { EntityUiMessages } from './itf.entity-ui-messages';
import { Entities } from './itf.entities';
import { EntityState } from './cls.entity-state';
import { isEmpty as _isEmpty, difference as _difference, without as _without } from 'lodash';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { PromiseHelper } from '../shared/cls.promise-helper';
import { Subscription } from 'rxjs/Subscription';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { EntityRemoveModalComponent } from './cmp.entity-remove-modal';

/**
 * Abstract component for helping with showing and managing list of items.
 *
 * By default component will load all entities on start.
 * If you wan't to prevent that overwrite subscribeToEntities.
 */
export abstract class EntitiesComponent<T extends Entity, TS extends EntityWithRemoteService<T>> {

  /**
   * Any warnings to display to UI.
   * @type {{}}
   */
  messages: EntityUiMessages = {};

  /**
   * Indicates if bulk operation is in process of saving.
   */
  bulkActionInProgress: boolean;

  // NOT IN USE RIGHT NOW
  // /**
  //  * On bulk action complete.
  //  * @type {EventEmitter}
  //  */
  // onBulkActionCompleteEvent: EventEmitter<any> = new EventEmitter();

  // NOT IN USE RIGHT NOW
  // /**
  //  * On bulk action fail.
  //  * @type {EventEmitter}
  //  */
  // onBulkActionFailEvent: EventEmitter<any> = new EventEmitter();

  /**
   * All entities id's. If you need sorting this is place to do it.
   * @type {Array}
   */
  allEntities: string[] = [];

  /**
   * Displayed entities.
   * @type {Array}
   */
  displayedEntities: string[] = [];

  /**
   * Indicates if ctrl key is pressed. Used for selecting entities.
   */
  ctrlPressed: boolean;

  /**
   * Selected entities.
   * @type {Array}
   */
  selectedEntities: Set<string> = new Set();

  /**
   * Always up to date entities store.
   * @type {{}}
   */
  entities: Entities<T> = {};

  /**
   * If pagination is enabled.
   * This has only effect on performance.
   * @type {boolean}
   */
  paginationEnabled: boolean = false;

  /**
   * Entities per page to show.
   * @type {number}
   */
  public pageSize: number = 10;

  /**
   * Current selected page.
   * We start counting with 0 obviously :)
   * @type {number}
   */
  selectedPageIndex: number = 0;

  /**
   * Subscription to entities.
   */
  providerSubscription?: Subscription;

  /**
   * Confirm dialog before remove.
   */
  removeModalDialog: any = EntityRemoveModalComponent;

  get selectedPage(): number {
    return this.selectedPageIndex + 1;
  }

  set selectedPage(val: number) {
    this.selectedPageIndex = val - 1;
    if (this.selectedPageIndex < 0) {
      this.selectedPageIndex = 0;
    }
    this.onSelectedPageChange();
  }

  /**
   * Provider for template.
   * @return {EntitiesComponent}
   */
  public get entitiesCmp(): EntitiesComponent<T, TS> {
    return this;
  }

  /**
   * If we don't have any entities yet.
   * @return {boolean}
   */
  get entitiesEmpty(): boolean {
    return _isEmpty(this.entities);
  }

  get totalEntitiesCount(): number {
    return this.allEntities.length;
  }

  constructor(protected location: Location,
              protected route: ActivatedRoute,
              protected router: Router,
              protected sys: BaseSystemService,
              protected entityService: TS, // Service responsible for @entity.
              protected modalService: NgbModal
  ) {}

  /**
   * This is not automatically done in constructor because of AOT problems,
   * also we might trigger it after ngOnInit.
   */
  init() {
    this.subscribeToEntities();
  }

  subscribeToEntities(provider = this.entityService.entities) {
    this.providerSubscription = provider.subscribe(x => {
      this.onEntitiesChange(x);
    });
  }

  /**
   * Entry point when entities change.
   * @param entities
   */
  onEntitiesChange(entities: Entities<T>) {
    this.entities    = entities;
    this.allEntities = Object.keys(entities);

    this.filter();
    this.sort();

    this.updateDisplayedEntities();
  }

  setPageSize(size: number) {
    this.pageSize = size;
    this.updateDisplayedEntities();
  }

  /**
   * Only displayedEntities, it doesn't do anything else like sorting, ...
   */
  updateDisplayedEntities() {
    if (this.paginationEnabled) {
      this.onSelectedPageChange();
    } else {
      this.displayedEntities = this.allEntities;
    }
  }

  // region Entities selection handlers
  onEntitySelectChange(id, selected) {
    if (selected) {
      this.selectEntity(id);
    } else {
      this.deselectEntity(id);
    }
  }

  toggleEntitySelect(id: string) {
    if (this.selectedEntities.has(id)) {
      this.deselectEntity(id);
    } else {
      this.selectEntity(id);
    }
  }

  selectEntity(id: string) {
    this.selectedEntities.add(id);
  }

  deselectEntity(id: string) {
    this.selectedEntities.delete(id);
  }

  deselectAll() {
    this.selectedEntities.clear();
  }

  /**
   * If ctrl key pressed select entity and stop propagation else do nothing.
   * @param event
   * @param id
   */
  trySelectEntityOnCtrl(event, id) {
    if (this.ctrlPressed) {
      this.toggleEntitySelect(id);
      event.stopPropagation();
    }
  }

  // endregion

  /**
   * Sort allEntities.
   */
  sort() {
    // should be implemented by parent.
  }

  /**
   * Filter allEntities.
   */
  filter() {
    // should be implemented by parent.
  }

  // region Bulk operations and REST helpers
  handleEntityError(e: { error?: any, lockedEntity: T }) {
    if (e.error instanceof HttpError) {
      if (ResponseJson.isResponseJson(e.error.httpResponse) && e.error.httpResponse.data && e.error.httpResponse.data.errorMessage) {
        return this.messages.errorMessage = e.error.httpResponse.data.errorMessage;
      }

      if (!this.sys.tryHandleHttpError(e.error)) {
        return this.messages.httpError = e.error;
      }
    }

    this.messages.error = e.error;
  }

  remove(id: string) {
    const modalRef = this.modalService.open(this.removeModalDialog);
    modalRef.componentInstance.titles = [(this.entities[id] || this.entityService.getEntitySoft(id)).niceName()];
    modalRef.result
      .then(modalRes => {
        this.markForRemove(id); // We need this for now, since some projects don't listen to observables

        this.entityService.remove(id)
          .then(x => {
            this.displayedEntities = _without(this.displayedEntities, id);
          })
          .catch(e => {
            this.entities[e.lockedEntity.id].setError(e.error); // is this really needed?
          });
      })
      .catch(e => {});
  }

  markForRemove(id: string): T {
    const e: T  = this.entities[id] || this.entityService.getEntitySoft(id);
    if (!e) {
      console.warn(`Entity ${ id } doesn't exist in memory. Possible leftovers?`);
      return undefined;
    }
    e.$state = EntityState.Removed;
    return e;
  }

  public hook_filterRemoveSelected(x: string[]): string[] {
    return x;
  }

  public removeSelected() {
    const toRemove = this.hook_filterRemoveSelected([...Array.from(this.selectedEntities)]);

    if (!toRemove.length) {
      this.deselectAll();
      return;
    }

    const modalRef = this.modalService.open(this.removeModalDialog);
    modalRef.componentInstance.titles = toRemove.map(id => {
      const e: T  = this.entities[id] || this.entityService.getEntitySoft(id);
      if (!e) {
        console.warn(`Entity ${ id } doesn't exist in memory. Possible leftovers?`);
        return ``;
      }
      return e.niceName();
    });

    modalRef.result
      .then(modalRes => {

        const entitiesList = toRemove
          .map(id => {
            return this.markForRemove(id);
          })
          .filter(Boolean);

        this.deselectAll();

        const promises = this.entityService.applyToRemote(entitiesList);
        promises.map(p => {
          p
            .then(x => {
              this.displayedEntities = _without(this.displayedEntities, x.lockedEntity.id);
            })
            .catch(e => {
              // no need for save bg service should already set error
              // TODO check if bg svc registers? or if it should
              this.entities[e.lockedEntity.id].setError(e.error);
            });
        });
        PromiseHelper.allSettled(promises).then(this.onRemoveSelectedFinish);
      })
      .catch(e => {});
  }

  onRemoveSelectedFinish(res: ({ error?: any } | { value?: any })[]) {
    // hook for parent to use if needed
  }

  // endregion

  @HostListener('document:keydown', ['$event'])
  handleKeyDownEvent(event: KeyboardEvent) {
    const which = event.which || event.keyCode || 0;

    if (event.ctrlKey) {
      this.ctrlPressed = true;
    }
  }

  @HostListener('document:keyup', ['$event'])
  handleKeyUpEvent(event: KeyboardEvent) {
    const which = event.which || event.keyCode || 0;

    if (which === 17) {
      this.ctrlPressed = false;
    }
  }

  // region Pagination
  nextPage() {
    ++this.selectedPage;
  }

  previousPage() {
    --this.selectedPage;
  }

  firstPage() {
    this.selectedPage = 1;
  }

  onSelectedPageChange() {
    const startIndex       = this.selectedPageIndex * this.pageSize;
    const endIndex         = startIndex + this.pageSize;
    this.displayedEntities = this.allEntities.slice(startIndex, endIndex);
  }
  // endregion
}
