import { ResponseJson } from '../core/http/cls.response-json';
import { AppError } from '../core/cls.error';

export interface EntityApplyRemoteResponse<T> {
  res?: ResponseJson;
  error?: any | AppError;
  lockedEntity: T;
}
