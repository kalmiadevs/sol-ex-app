import { Entities } from './itf.entities';

export interface EntityStore<T> {
  entities: Entities<T>;
}
