import { Injectable } from '@angular/core';

import { filter as _filter, mapValues as _mapValues } from 'lodash';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import { Entity } from './cls.entity';
import { Entities } from './itf.entities';
import { BaseSystemService } from '../core/svc.system';
import { EntityIdChange } from './itf.entity-id-change';
import { Subject } from 'rxjs/Subject';
import { EntityStore } from './itf.entity-store';
import { InvalidEntityEnteredSystemError } from '../core/cls.error';

@Injectable()
export class EntityService<T extends Entity> {

  public serviceId?: string;

  protected _entities: BehaviorSubject<Entities<T>> = new BehaviorSubject({});
  public entities: Observable<Entities<T>>          = this._entities.asObservable();

  protected _entityIdChange: Subject<EntityIdChange> = new Subject<EntityIdChange>();
  public entityIdChange: Observable<EntityIdChange> = this._entityIdChange.asObservable();

  protected sys: BaseSystemService;

  /**
   * All stored data.
   * Don't make this public or else ... I will look for you, I will find you, and I will tell you why is that bad.
   */
  protected store: EntityStore<T>;

  /**
   * Make copy and notify BehaviorSubject about entities change.
   * Note: Copy is made to prevent accidental changes from outside.
   */
  public notifyEntitiesChange() {
    this._entities.next(this.getEntities());
  }

  public init(sys: BaseSystemService) {
    this.sys = sys;
  }

  constructor() {
    this.clean();
  }

  /**
   * Destroy data.
   */
  clean(notify = false) {
    this.store = {
      entities: {}
    };

    if (notify) {
      this.notifyEntitiesChange();
    }
  }

  // region Common entity operations
  public isEntityValid(entity: T) {
    return entity && entity.id !== undefined;
  }

  public onInvalidEntity(entity: T) {
    return this.sys.onError(
      new InvalidEntityEnteredSystemError(entity, this.serviceId)
    );
  }

  /**
   * Set entities and notify BehaviorSubject.
   * @param entities
   */
  public setEntities(entities: Entities<T>) {
    if ('undefined' in this.entities && !this.isEntityValid(entities['undefined'])) {
      this.onInvalidEntity(entities['undefined']);
      delete entities['undefined'];
    }

    this.store.entities = entities;
    this.notifyEntitiesChange();
  }

  /**
   * Add entity and notify BehaviorSubject. If entity already exists it will be replaced.
   * @param entity
   */
  public addEntity(entity: T) {
    if (!this.isEntityValid(entity)) {
      this.onInvalidEntity(entity);
      return;
    }

    this.store.entities = { ...this.store.entities, [entity.id]: entity };
    this.notifyEntitiesChange();
  }

  /**
   * Add entities and notify BehaviorSubject. If entity already exists it will be replaced.
   * @param newEntities
   */
  public addEntities(newEntities: Entities<T>) {
    if ('undefined' in newEntities && !this.isEntityValid(newEntities['undefined'])) {
      this.onInvalidEntity(newEntities['undefined']);
      delete newEntities['undefined'];
    }

    this.store.entities = { ...this.store.entities, ...newEntities };
    this.notifyEntitiesChange();
  }

  /**
   * Check if entity exists in store.
   * @param id
   * @returns {boolean}
   */
  public entityExists(id): boolean { return !!this.store.entities[id]; }

  /**
   * Return entity copy.
   * @param id
   * @throws Error
   * @returns {T}
   */
  public getEntity(id: string): T {
    if (!this.entityExists(id)) {
      throw new Error('Entity ' + id + ' doesn\'t exist.');
    }
    return <T>this.store.entities[id].clone();
  }

  public getEntitySoft(id: string): T {
    return this.entityExists(id) && <T>this.store.entities[id].clone() || undefined;
  }

  /**
   * Return entities copy.
   * @returns {Entities<T>}
   */
  public getEntities(): Entities<T> {
    return _mapValues(this.store.entities, x => <T>x.clone());
  }

  /**
   * Return entity as observable.
   * @param id
   * @returns {Observable<Entity>}
   */
  public getEntityObservable(id: string): Observable<T> {
    return this.entities.map(entities => entities[id]);
  }

  /**
   * Get nodes marked as dirty.
   * @returns {Entities<T>}
   */
  public getDirtyEntitiesArray(): T[] { return _filter(this.store.entities, x => x.$dirty); }

  /**
   * Get nodes marked as removed.
   * @returns {Entities<T>}
   */
  public getRemovedEntitiesArray(): T[] { return _filter(this.store.entities, x => x.$removed); }

  /**
   * Get nodes marked as new.
   * @returns {Entities<T>}
   */
  public getNewEntitiesArray(): T[] { return _filter(this.store.entities, x => x.$new); }

  /**
   * Let others know about entity id change.
   * @param oldId
   * @param newId
   */
  public onEntityIdChange(oldId: string, newId: string) {
    // Lets notify others like: group, vault, ... so they can correct entity id
    this._entityIdChange.next({
      oldId:  oldId,
      newId:  newId
    });
  }
  // endregion
}
