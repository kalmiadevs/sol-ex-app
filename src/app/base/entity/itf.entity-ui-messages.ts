export interface EntityUiMessages {
  originalChanged?: boolean;
  httpError?: any & { handled: boolean };
  error?: any;
  errorMessage?: string;
  warningMessage?: string;
}
