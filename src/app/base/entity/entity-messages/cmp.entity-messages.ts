import { Component, Input } from '@angular/core';
import { EntityComponent } from '../cmp.entity';
import { EntityUiMessages } from '../itf.entity-ui-messages';

@Component({
  selector: 'app-entity-messages',
  templateUrl: './tpl.entity-messages.html',
  styleUrls: ['./cmp.entity-messages.scss']
})
export class EntityMessagesComponent {

  @Input() entityCmp?: EntityComponent<any, any>;
  @Input() messages?: EntityUiMessages;

  get m(): EntityUiMessages {
    return this.messages || this.entityCmp.messages;
  }
  constructor() { }

  getMessageJson(data) {
    try {
      return JSON.stringify(data);
    } catch (e) {
      return data.toString();
    }
  }
}
