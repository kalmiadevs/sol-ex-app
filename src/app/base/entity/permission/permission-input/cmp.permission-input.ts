import { Component, EventEmitter, forwardRef, Inject, Input, OnInit, Output } from '@angular/core';
import 'rxjs/add/operator/switchMap';
import { findKey as _findKey, reduce as _reduce } from 'lodash';
import { StoreService } from '../../../core/svc.store';
import { Entities } from '../../itf.entities';
import { Permission } from '../cls.permission';

@Component({
  selector: 'app-permission-input',
  templateUrl: './tpl.permission-input.html',
  styleUrls: ['./cmp.permission-input.scss']
})
export class PermissionsInputComponent implements OnInit {

  permissions: Entities<Permission>;
  permissionInputText: string = '';

  @Input() readonly: boolean;

  // region permissions binding
  @Input()
  groupPermissions: Entities<Permission> = {};


  @Output() groupPermissionsChange = new EventEmitter();

  get inputPermissions() { return  Object.keys(this.groupPermissions); }
  set inputPermissions(val: string[]) {
    this.groupPermissions = _reduce(val, (a, v) => {
      a[v] = this.groupPermissions[v];

      if (!a[v]) {
        a[v] = this.permissions[v];
      }

      return a;
    }, {});

    this.groupPermissionsChange.emit(this.groupPermissions);
  }
  // endregion

  constructor (
    @Inject(forwardRef(() => StoreService)) sysStore: StoreService
  ) {
    sysStore.permissionService.permissions.subscribe(x => this.permissions = x);
  }

  ngOnInit() {}

  /**
   * Convert permission title into permission id
   * @param title
   */
  stringToPermissionId = (title) => (this.permissions[_findKey(this.permissions, (p: Permission) => p.niceName() === title)] || { id: undefined }).id;

  /**
   * Convert permission id into title
   * @param gid
   */
  permissionIdToString = (gid: string) => this.permissions[gid] ? this.permissions[gid].niceName() : gid;

  /**
   * Return suggested permissions.
   */
  get suggestedPermissions(): string[] {
    return _reduce(this.permissions,
      (a: string[], v: Permission) =>
        (!this.permissionInputText || v.niceName().indexOf(this.permissionInputText) !== -1) && this.inputPermissions.indexOf(v.id) === -1
          ? [...a, v.id] : a,
      []);
  };
}
