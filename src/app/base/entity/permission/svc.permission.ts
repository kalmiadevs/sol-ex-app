import { Injectable } from '@angular/core';
import { HttpAuthService } from '../../core/http/svc.http-auth';
import { chain as _chain } from 'lodash';
import { EntityWithRemoteService } from '../svc.entity-with-remote';
import { Entities } from '../itf.entities';
import { environment } from '../../../../environments/environment';
import { Permission } from './cls.permission';

/**
 * Users service
 */
@Injectable()
export class BasePermissionService extends EntityWithRemoteService<Permission>  {

  protected apiEntityName      = `permissions`;
  protected apiEntitiesName    = `permissions`;
  protected apiBase            = `${ environment.apiBaseKalBlocks }`;

  get permissions() { return this.entities; }

  constructor(protected http: HttpAuthService) {
    super(http);

    this.setEntities(
      {
        'kp_node'   : Permission.createFromApi({ value: 0b111, key: 'kp_node'   , description: 'KalPass passwords'}),
        'kb_admin'   : Permission.createFromApi({ value: 0b111, key: 'kb_admin'   , description: 'Admin rights'}),
        'kb_groups'  : Permission.createFromApi({ value: 0b111, key: 'kb_groups'  , description: 'KalBlock groups management'}),
        'kb_users'  : Permission.createFromApi({ value: 0b111, key: 'kb_users'  , description: 'KalBlock users management'}),
      }
    );
  }

  protected convertRemoteResToEntities(data: any): Promise<Permission[]> {
    return new Promise((resolve, reject) => {
      resolve(this.objToPermission(data.users));
    });
  }

  protected convertRemoteResToEntity(data: any): Promise<Permission> {
    return new Promise((resolve, reject) => {
      resolve(Permission.createFromApi(data));
    });
  }

  /**
   * Convert remote object into Users
   * @param data
   */
  private objToPermission(data: any): Permission[] {
    return _chain(data)
      .map(x => Permission.createFromApi(x))
      .value();
  }
}
