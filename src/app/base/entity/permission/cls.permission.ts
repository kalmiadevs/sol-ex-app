import { cloneDeep as _cloneDeep, mapValues as _mapValues } from 'lodash';
import { Entity } from '../cls.entity';
import { Entities } from '../itf.entities';

/* tslint:disable:no-bitwise */

export const PermissionIsAdmin = 'kb_admin';
export const PermissionKalPassNode = 'kp_node';

export class Permission extends Entity {

  public description : string;
  public value: number;

  get read(): boolean {
    return !!(this.value & 0b100);
  }
  set read(val: boolean) {
    if (val) {
      this.value |= 0b100;
    } else {
      this.value &= ~0b100;
    }
  }
  get write(): boolean {
    return !!(this.value & 0b10);
  }
  set write(val: boolean) {
    if (val) {
      this.value |= 0b10;
    } else {
      this.value &= ~0b10;
    }
  }
  get remove(): boolean {
    return !!(this.value & 0b1);
  }
  set remove(val: boolean) {
    if (val) {
      this.value |= 0b1;
    } else {
      this.value &= ~0b1;
    }
  }

  public static create(data: {id, description, value}): Permission {
    const x       = new Permission();
    x.id          = data.id;
    x.description = data.description;
    x.value       = + data.value || 0;
    return x;
  }

  static createFromApi(data: any): Permission {
    return Permission.create({...data, id: data.key});
  }

  static createFromToken(p: Entities<number>): Entities<Permission> {
    return _mapValues(p, (v, k) => Permission.create({id: k, description: '', value: v}));
  }

  public getSaveObj(): any {
    return this.value;
  };

  clone(): Entity {
    return _cloneDeep(this);
  }

  public niceName(): string {
    return this.description;
  }

  /**
   * Usefully for sorting permissions.
   * @return {number}
   */
  public getAccessScore(): number {
    return this.value;
  }

  public toString(): string {
    const canDo: string[] = [];
    if (this.read)    { canDo.push('read'); }
    if (this.write)   { canDo.push('write'); }
    if (this.remove)  { canDo.push('remove'); }

    return canDo.length ? `Can ${canDo.join(', ')}` : 'Has no permissions.';
  }
}
