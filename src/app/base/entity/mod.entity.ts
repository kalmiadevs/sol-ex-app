import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EntityActionsComponent } from './entity-actions/cmp.entity-actions';
import { EntityMessagesComponent } from './entity-messages/cmp.entity-messages';
import { EntitiesActionsComponent } from './entities-actions/cmp.entities-actions';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { EntityRemoveModalComponent } from './cmp.entity-remove-modal';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    NgbModule
  ],
  declarations: [
    EntityActionsComponent,
    EntityMessagesComponent,
    EntitiesActionsComponent,
    EntityRemoveModalComponent
  ],
  entryComponents: [
    EntityRemoveModalComponent
  ],
  exports: [
    EntityActionsComponent,
    EntityMessagesComponent,
    EntitiesActionsComponent,
    EntityRemoveModalComponent
  ]
})
export class EntityModule { }
