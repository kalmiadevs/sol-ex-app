import { Entity } from './cls.entity';
import { HttpAuthService } from '../core/http/svc.http-auth';
import { Observable } from 'rxjs/Observable';
import { EntityService } from './svc.entity';
import { EntityState } from './cls.entity-state';
import { EntityApplyRemoteResponse } from './itf.entity-remote-response';
import {
  CanActivateError,
  HttpError,
  InvalidEntitiesReceivedError,
  InvalidEntityReceivedError,
  ResolveError,
  ResponseToEntityError
} from '../core/cls.error';
import { Entities } from './itf.entities';
import { flow, keyBy, map, mapValues } from 'lodash/fp';
import { environment } from '../../../environments/environment';
import { isString as _isString, template as _template, mapValues as _mapValues, keyBy as _keyBy, reduce as _reduce } from 'lodash';
import { PromiseHelper } from '../shared/cls.promise-helper';
import { ActivatedRouteSnapshot, CanActivate, Resolve } from '@angular/router';
import { ResponseJson } from '../core/http/cls.response-json';
import { URLSearchParams } from '@angular/http';

export const NEW_ENTITY_ID_PREFIX = 'APP_NEW_ENTITY_';

export abstract class EntityWithRemoteService<T extends Entity> extends EntityService<T> implements Resolve<Observable<T>>, CanActivate {

  /**
   * Prefix used when creating new node so it doesn't collide with remote id's.
   * @type {string}
   */
  public new_entity_id_prefix = NEW_ENTITY_ID_PREFIX;

  protected apiEntityName      = `entity`;
  protected apiEntitiesName    = `entities`;
  protected apiBase            = `${ environment.apiBaseKalBlocks }`;

  protected apiEntityGet       = `<%= apiBase %>/<%= apiEntityName %>/<%= id %>`;
  protected apiEntityPost      = `<%= apiBase %>/<%= apiEntityName %>`;
  protected apiEntityPut       = `<%= apiBase %>/<%= apiEntityName %>/<%= id %>`;
  protected apiEntityDelete    = `<%= apiBase %>/<%= apiEntityName %>/<%= id %>`;
  protected apiEntities        = `<%= apiBase %>/<%= apiEntitiesName %>`;

  public lastSync: number; // TODO
  public lastLoad: number;
  public lastSave: number; // TODO

  public lastSuccessfulSync: number; // TODO
  public lastSuccessfulLoad: number;
  public lastSuccessfulSave: number; // TODO

  constructor(
    protected http: HttpAuthService
  ) {
    super();
    this.serviceId = this.serviceId || (this.apiEntityName + '&' + this.apiEntitiesName);
  }

  public clean(notify = false) {
    this.lastSync = 0;
    this.lastLoad = 0;
    this.lastSave = 0;

    this.lastSuccessfulSync = 0;
    this.lastSuccessfulLoad = 0;
    this.lastSuccessfulSave = 0;
    super.clean(notify);
  }

  /**
   * Generate new entity id.
   * @param c You can skip that it's used for recursion if id is already taken.
   * @returns {string}
   */
  public genNewEntityId(c: number = 0): string {
    const id = `${this.new_entity_id_prefix}${+ new Date()}-${c}`;
    return this.store.entities[id] ? this.genNewEntityId(++c) : id;
  }

  /**
   * Check if entity is fully loaded.
   *
   * Diff isEntityLoaded vs entityExists:
   *  - isEntityLoaded - is fully loaded
   *  - entityExists - can be only partially loaded (metadata)
   * @param id
   * @return {boolean|any}
   */
  public isEntityLoaded(id: string): boolean {
    return this.entityExists(id);
  }

  /**
   * Save & load
   * @param options - { throttle: milliseconds }
   */
  public sync(options?: { throttle: number }) {
    if (options) {
      if (options.throttle && this.lastSync > (+ new Date()) - options.throttle ) {
        console.log(`Sync in ${ this.constructor.name } prevented not enough time passed.`);
        return;
      }
    }

    PromiseHelper.allSettled(this.applyToRemote()).then(res => {
      this.loadEntitiesFromRemote().catch();
    });

    this.lastSync = + new Date();
  }

  // region Load

  /**
   * Load entities from remote and replace existing.
   * @param options - { throttle: milliseconds }
   */
  public loadEntitiesFromRemote(options: {
    throttle?: number,
    pagination?: {
      start: number,
      items: number
    },
    sort?: {
      order: 'asc' | 'desc',
      by: string,
    },
    filter?: any | string,
    search?: any | string,
    aggregate?: boolean,
    params?: any
  } = {
    params: {}
  }): Promise<{entities: Entities<T>, keys: string[], data: any}> {
    return new Promise((resolve, reject) => {
      const params = this.hookProcessLoadEntitiesParams(options);

      if (options.throttle && this.lastLoad > (+ new Date()) - options.throttle ) {
          reject(`Load in ${ this.constructor.name } prevented not enough time passed.`);
          return;
      }

      this.lastLoad = + new Date();
      this.http.get(_template(this.apiEntities)(this.hookGetApiParams()), { search: params }).subscribe(
        (res: ResponseJson) => {
          this.convertRemoteResToEntities(res.data)
            .then(entitiesArray => {
              const keys = [];
              const entities = _reduce(entitiesArray, (a, v, k) => {
                keys.push(v.id);
                a[v.id] = v;
                return a;
              }, {});

              this.onEntitiesReceiveFromRemote(entities);
              this.lastSuccessfulLoad  = + new Date();
              resolve({keys, entities, data: res.data});
            })
            .catch(error => {
              reject(new InvalidEntitiesReceivedError(error, res, this.serviceId));
          });
        },
        (error) => {
          reject(new HttpError(error));
        });
    });
  }

  /**
   * Convert options for loading entities from remote into HTTP GET params.
   * @param options
   * @return {{}}
   */
  hookProcessLoadEntitiesParams(options: {
    pagination?: {
      start: number,
      items: number
    },
    sort?: {
      order: 'asc' | 'desc',
      by: string,
    },
    filter?: any,
    search?: any | string,
    aggregate?: boolean,
    params?: any
  } = {
    params: {}
  }): URLSearchParams {
    const params: URLSearchParams = new URLSearchParams();

    if (options.pagination) {
      params.set('offset', (options.pagination.start || 0) + '');
      params.set('limit', (options.pagination.items || 10)  + '');
    }

    if (options.sort) {
      params.set('sort', options.sort.by);
      params.set('sortOrder', options.sort.order || 'desc');
    }

    if (options.filter) {
      params.set('filter', _isString(options.filter) ? options.filter : JSON.stringify(options.filter));
    }

    if (options.search) {
      params.set('search', _isString(options.search) ? options.search : JSON.stringify(options.search));
    }

    if (options.aggregate) {
      params.set('aggregate', '1');
    }

    _mapValues(options.params, (v, k) => {
      params.set(k, v);
    });

    return params;
  };

  /**
   * Load specific entity from remote.
   * @param id
   * @returns {Promise<T>}
   */
  public loadEntityFromRemote(id: string): Promise<{entity: T, data: any}> {
    return new Promise((resolve, reject) => {
      this.makeHttpCall(id).subscribe(
        (res: ResponseJson) => {
          this.convertRemoteResToEntity(res.data)
            .then(entity => {
              if (!this.isEntityValid(entity)) {
                return reject(new InvalidEntityReceivedError(entity, id, res, this.apiEntityName));
              }

              this.onEntityReceiveFromRemote(entity);
              resolve({entity, data: res.data});
            })
            .catch(error => {
              return reject(new ResponseToEntityError(error, id, res, this.apiEntityName));
            });
        },
        (error) => {
          reject(new HttpError(error));
        });
    });
  }

  /**
   * This overwrites currently saved entities.
   * @param el
   */
  protected onEntitiesReceiveFromRemote(el: Entities<T>) {
    this.setEntities(el);
  }

  /**
   * This overwrites currently saved entity.
   * @param e
   */
  protected onEntityReceiveFromRemote(e: T) {
    this.addEntity(e);
  }

  protected abstract convertRemoteResToEntities(data: any): Promise<T[]>;
  protected abstract convertRemoteResToEntity(data: any): Promise<T>;
  // endregion

  // region Save

  /**
   * Hook triggered before saving entity.
   * @param entity
   * @returns {T}
   */
  protected hookBeforeSaveEntity(entity: T): T { return entity; }

  /**
   * Hook triggered after saving entity.
   * @param entity
   * @returns {T}
   */
  protected hookAfterSaveEntity(entity: T): T { return entity; }

  /**
   * Mark entity as changed and apply changes on remote.
   * @param entity
   */
  public save(entity: T): Promise<EntityApplyRemoteResponse<T>> {
    if (entity.$new) {
      entity.id = entity.id || this.genNewEntityId();
    } else {
      entity.$state = EntityState.Dirty;
    }
    entity = this.hookBeforeSaveEntity(entity);
    this.store.entities[entity.id] = entity;
    this.notifyEntitiesChange();
    entity = this.hookAfterSaveEntity(entity);

    return this.applyToRemote([entity])[0];
  }

  /**
   * Mark entity as removed and try to apply changes on remote.
   * @param id
   */
  public remove(id: string): Promise<EntityApplyRemoteResponse<T>> {
    // Mark as removed. So it will be detected by sync operation and removed from remote.
    this.store.entities[id].$state = EntityState.Removed;

    this.notifyEntitiesChange();
    return this.applyToRemote([this.store.entities[id]])[0];
  }

  /**
   * Lock entities and notify
   * @param entities
   */
  protected beforeApplyToRemote(entities: T[]) {
    entities.map(x => {
      this.store.entities[x.id].$locked = true;
      this.store.entities[x.id].clearError();
    });
    this.notifyEntitiesChange();
  }

  /**
   * Get payload to be send to remote.
   * Note: payload will change depending on the state of entity.
   * @param entity
   * @returns {Promise<any>}
   */
  protected getEntitySavePayload(entity: T): Promise<any> {
    return new Promise((resolve, reject) => {
      if (entity.$removed) {
        resolve();
      } else {
        try {
          resolve(entity.getSaveObj());
        } catch (error) {
          reject(error);
        }
      }
    });
  }

  /**
   * Called when insert action to remote was successful.
   * @param data
   * @param lockedEntity
   */
  protected onEntityInsertSuccess(data: any, lockedEntity: T) {
    const entity   = this.store.entities[lockedEntity.id].clone();
    entity.id      = data.id;
    entity.$state  = EntityState.Pristine;
    entity.$locked = false;

    delete this.store.entities[lockedEntity.id];

    this.addEntity(<T>entity);
    this.onEntityIdChange(lockedEntity.id, entity.id);
  }

  /**
   * Called when update action to remote was successful.
   * @param data
   * @param lockedEntity
   */
  protected onEntityUpdateSuccess(data: any, lockedEntity: T) {
    this.store.entities[lockedEntity.id].$state = EntityState.Pristine;
  }

  /**
   * Called when remove action to remote was successful.
   * @param data
   * @param lockedEntity
   */
  protected onEntityRemoveSuccess(data: any, lockedEntity: T) {
    delete this.store.entities[lockedEntity.id];
  }

  /**
   * Called when insert action to remote failed.
   * @param data
   * @param lockedEntity
   */
  protected onEntityInsertFail(error: HttpError, lockedEntity: T) {
    delete this.store.entities[lockedEntity.id];
  }

  /**
   * Called when update action to remote failed.
   * @param data
   * @param lockedEntity
   */
  protected onEntityUpdateFail(error: HttpError, lockedEntity: T) {
    this.store.entities[lockedEntity.id].setError(error);
  }

  /**
   * Called when remove action to remote failed.
   * @param data
   * @param lockedEntity
   */
  protected onEntityRemoveFail(error: HttpError, lockedEntity: T) {
    this.store.entities[lockedEntity.id].setError(error);
  }

  /**
   * Called when applying entity changes to remote returned successful http response.
   * Note: No need for calling notify this will be handled by onEntityHttpComplete
   * @param res
   * @param lockedEntity
   */
  protected onEntityHttpResponse(res: ResponseJson, lockedEntity: T) {
    if (lockedEntity.$new) {
      this.onEntityInsertSuccess(res.data, lockedEntity);
    } else if (lockedEntity.$dirty) {
      this.onEntityUpdateSuccess(res.data, lockedEntity);
    } else if (lockedEntity.$removed) {
      this.onEntityRemoveSuccess(res.data, lockedEntity);
    }
  }

  /**
   * Called when applying entity changes to remote returned failed http response.
   * Note: No need for calling notify this will be handled by onEntityHttpComplete
   * @param error
   * @param lockedEntity
   */
  protected onEntityHttpError(error: HttpError, lockedEntity: T) {
    if (lockedEntity.$new) {
      this.onEntityInsertFail(error, lockedEntity);
    } else if (lockedEntity.$dirty) {
      this.onEntityUpdateFail(error, lockedEntity);
    } else if (lockedEntity.$removed) {
      this.onEntityRemoveFail(error, lockedEntity);
    }
  }

  /**
   * Called when applying entity changes to remote completed, successful or failed.
   * Note: No need for calling notify this will be handled by onEntityHttpComplete
   * @param lockedEntity
   */
  protected onEntityHttpComplete(lockedEntity: T) {
    if (this.store.entities[lockedEntity.id]) {
      this.store.entities[lockedEntity.id].$locked = false;
    }

    this.notifyEntitiesChange();
  }

  /**
   * Prepare parameters for api template.
   * @param e
   * @returns {*}
   */
  protected hookGetApiParams(e?: T | string): any {
    const common = {
      apiBase: this.apiBase,
      apiEntityName: this.apiEntityName,
      apiEntitiesName: this.apiEntitiesName
    };
    if (!e) {
      return common;
    }
    if (Entity.isEntity(e)) {
      return {
        id: e.id,
        ...common
      };
    } else {
      return {
        id: e,
        ...common
      };
    }
  }

  /**
   * Make http call to remote.
   * This is helper function for applyToRemote. You can overwrite this if
   * you would like to change communication protocol with remote.
   * @param e
   * @param payload
   * @returns {any}
   */
  protected makeHttpCall(e: T | string, payload?: any): Observable<ResponseJson> {
    if (Entity.isEntity(e)) {
      if (e.$new) {
        return <any>this.http.post(_template(this.apiEntityPost)(this.hookGetApiParams(e)), payload);
      } else if (e.$dirty) {
        return <any>this.http.put(_template(this.apiEntityPut)(this.hookGetApiParams(e)), payload);
      } else if (e.$removed) {
        return <any>this.http.delete(_template(this.apiEntityDelete)(this.hookGetApiParams(e)));
      } else {
        throw Error('Nothing to do.');
      }
    } else {
      return <any>this.http.get(_template(this.apiEntityGet)(this.hookGetApiParams(e)));
    }
  }

  /**
   * Apply entity changes to remote.
   * @param lockedEntity
   * @returns {Promise<EntityApplyRemoteResponse<T>>}
   * @private
   */
  protected _applyToRemote(lockedEntity: T): Promise<EntityApplyRemoteResponse<T>> {
    return new Promise((resolve, reject) => {
      this.getEntitySavePayload(lockedEntity)
        .then(payload => {
          this.makeHttpCall(lockedEntity, payload)
            .subscribe(
              (res) => {
                this.onEntityHttpResponse(res, lockedEntity);
                this.onEntityHttpComplete(lockedEntity);
                resolve({res, lockedEntity});
              },
              (error) => {
                error = new HttpError(error);
                this.onEntityHttpError(error, lockedEntity);
                this.onEntityHttpComplete(lockedEntity);
                reject({error, lockedEntity});
              },
              () => {
                this.onEntityHttpComplete(lockedEntity);
                reject({lockedEntity});
              }
            );
        })
        .catch(error => {
          // Handle exception while processing.
          lockedEntity.setError(error);
          this.store.entities[lockedEntity.id].$locked = false;
          this.notifyEntitiesChange();
          reject({error, lockedEntity});
        });
    });
  }

  /**
   * Lock & apply entity changes to remote.
   * If no entities given it will automatically collect all of them.
   * @param entities
   * @returns {Promise<EntityApplyRemoteResponse<T>>[]}
   */
  public applyToRemote(
    entities: T[] = [
      ...this.getNewEntitiesArray(),
      ...this.getDirtyEntitiesArray(),
      ...this.getRemovedEntitiesArray()
    ]
  ): Promise<EntityApplyRemoteResponse<T>>[] {
    entities = entities.filter(x => !x.$locked);
    this.beforeApplyToRemote(entities);
    return entities.map(entity => this._applyToRemote(entity));
  }

  // endregion


  // region Data resolve & canActive
  public canActivate(): boolean | Promise<boolean> {
    if (this.lastSuccessfulLoad) {
      return true;
    }

    return new Promise((resolve, reject) => {
      this.loadEntitiesFromRemote()
        .then(x => {
          resolve(true);
        })
        .catch(e => {
          if (e instanceof HttpError) {
            if (e.httpResponse.status === 401) {
              return resolve(false);
            }
          }
          this.sys.onError(new CanActivateError(e, this.serviceId));
          reject(e);
        });
    });
  }

  resolve(route: ActivatedRouteSnapshot): Promise<Observable<T>> {
    return this.getEntityObservableWhenLoaded(route.params['id'])
      .catch(e => {
        this.sys.onError(new ResolveError(e, this.serviceId));
        throw e;
      });
  }

  /**
   * Return node as observable but only when it is  fully loaded.
   * @param id
   * @returns {Promise<Observable<Node>>}
   */
  public getEntityObservableWhenLoaded(id: string): Promise<Observable<T>> {
    return new Promise((resolve, reject) => {
      if (this.isEntityLoaded(id)) {
        resolve(this.getEntityObservable(id));
      } else {
        this.loadEntityFromRemote(id)
          .then(x => {
            resolve(this.getEntityObservable(id));
          })
          .catch(e => {
            reject(e);
          });
      }
    });
  }
  // endregion
}
