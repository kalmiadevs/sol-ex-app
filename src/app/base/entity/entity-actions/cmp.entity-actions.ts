import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { EntityComponent } from '../cmp.entity';
import { FormGroup } from '@angular/forms';
import { NgbTooltip } from '@ng-bootstrap/ng-bootstrap';
import { I18nService, _ } from '../../core/i18n/svc.i18n';

@Component({
  selector: 'app-entity-actions',
  templateUrl: './tpl.entity-actions.html',
  styleUrls: ['./cmp.entity-actions.scss']
})
export class EntityActionsComponent implements OnInit {

  @ViewChild('savedTooltip')
  savedTooltip: NgbTooltip;

  @Input()
  entityCmp: EntityComponent<any, any>;
  @Input()
  form?: FormGroup;

  @Input()
  removeString?: string;
  @Input()
  cancelString?: string;

  @Input()
  closeEnabled: boolean  = true;

  @Input()
  closeButtonVisible: boolean = true;

  constructor(
    private i18n: I18nService,
  ) {}

  ngOnInit() {
    const self = this;
    this.entityCmp.onSaveEvent.subscribe(() => {
      self.savedTooltip.open();
      setTimeout(() => {
        self.savedTooltip.close();
      }, 5000);
    });
  }
}
