import { Injectable } from '@angular/core';
import { flow, keyBy, map, mapValues } from 'lodash/fp';
import { BaseNode as BaseNode } from './cls.node';
import { map as _map } from 'lodash';
import { EntityWithRemoteService } from '../svc.entity-with-remote';
import { HttpAuthService } from '../../core/http/svc.http-auth';
import { Groups } from '../group/itf.groups';
import { ListEditActions } from '../../shared/cls.list-edit-actions';
import { NodeApiResponse } from './itf.node-api-response';
import { Entities } from '../itf.entities';
import { BaseSystemService } from '../../core/svc.system';

export interface NodeIdChange {
  oldId: string;
  newId: string;
}

/**
 * Main service for handling everything related to nodes.
 *
 * Note on terminology:
 * - partial node <- node without content
 * - node dynamic data <- node flags not directly saved in node like $favorite, $categoryId, ...
 */
export abstract class BaseNodeService<T extends BaseNode> extends EntityWithRemoteService<T> {

  protected apiEntityName      = `node`;
  protected apiEntitiesName    = `nodes`;

  get nodes() { return this.entities; }
  get nodeIdChange() { return this.entityIdChange; }

  public getNodes(): Entities<T> {
    return this.getEntities();
  }

  public init(sys: BaseSystemService) {
    super.init(sys);

    // Subscribe to groups changes and update nodes accordingly
    this.sys.groupService.groups.subscribe(gl => {
      this.updateNodesGroups(this.store.entities, gl);
      this.notifyEntitiesChange();
    });
  }

  constructor(
    protected http: HttpAuthService
  ) {
    super(http);
    this.clean();
  }

  // region Common node operations
  /**
   * Return new Node instance.
   */
  public abstract newNodeInstance(): T;

  /**
   * Get list of users witch are in same groups as node.
   * @param node
   * @returns {any[]}
   */
  public getNodeUsers(node: T): string[] {
    return node.$groups.reduce((a, gid) => {
      return [...a, ...this.sys.groupService.getGroupUsers(gid)];
    }, []);
  }
  // endregion

  // region Update nodes dynamic data
  /**
   * Set $groups to nodes.
   *
   * Warning! This function expects $groups flag to be empty. Use @clearGroupsFromNodes function if you can't guarantee that.
   * @param nl
   * @param gl
   */
  public setGroupsFlag(nl: Entities<T>, gl: Groups = this.sys.groupService.getGroups()): void {
    const u = this.sys.authService.getUser();
    _map(gl, g => {
      const hasAccess = g.containsUser(u.id);
      g.nodes.map(id => {
        if (nl[id]) {
          nl[id].$groups.push(g.id);
          nl[id].$hasAccess =  nl[id].$hasAccess || hasAccess;
        }
      });
    });
  }

  /**
   * Update $groups flag of nodes based on groups list.
   * @param nl  Nodes list to be modified.
   * @param gl  Categories list.
   */
  public updateNodesGroups(nl: Entities<T>, gl?: Groups) {
    this.clearGroupsFromNodes(nl);
    this.setGroupsFlag(nl, gl);
  }

  /**
   * Set $groups flag to empty for given nodes.
   * @param ln  Nodes list to be modified.
   */
  public clearGroupsFromNodes(ln: Entities<T>): void { _map(ln, x => {
    x.$groups = [];
    x.$hasAccess = false;
  }); }
  // endregion

  // region Save
  protected hookBeforeSaveEntity(n: T): T {
    // Note: here gets little tricky but idea is to offload work to group svc,
    // because you can also edit nodes inside group, so we only have one place in
    // app where we remember with group-node relation changes.
    //
    // When comes time to apply changes to remote, node svc takes all the work from group svc and handles creation process
    // since we must resolve every change per node.
    //
    // Once you are done trying to 'figure out' better solution,
    // and have realized what a terrible mistake that was,
    // please increment the following counter as a warning
    // for next time you wan't to make improvements.
    //
    // total_hours_wasted = 6
    const groupsActions = ListEditActions.createDiff(
      this.store.entities[n.id] ? this.store.entities[n.id].$groups || [] : [], // TODO KALPASS specific when is  : []?
      n.$groups || []
    );
    this.sys.groupService.applyGroupChangesForNode(n.id, groupsActions);
    return n;
  }

  protected onEntityUpdateSuccess(data: any, lockedEntity: T) {
    if (data[this.apiEntityName] && data[this.apiEntityName].version !== undefined) {
      this.store.entities[lockedEntity.id].version = data[this.apiEntityName].version;
    }

    super.onEntityUpdateSuccess(data, lockedEntity);
  }
  // endregion

  // region Load
  /**
   * Convert object to Node
   * @param data
   * @param options
   * @returns {Node}
   */
  protected objToNode(data: NodeApiResponse, options: { onlyMetaData: boolean } = { onlyMetaData: false }): T {
    const res = this.objToNodes([data], options);
    return res[Object.keys(res)[0]];
  }

  /**
   * Convert object into node without setting dynamic data.
   * @param data
   * @param options
   */
  protected objToNodeNoDynamicData(data: NodeApiResponse, options: { onlyMetaData: boolean } = { onlyMetaData: false }): T {
    const n = this.newNodeInstance();
    options.onlyMetaData ? n.setOnlyMetaData(data) : n.setData(data);
    return n;
  }

  /**
   * Convert object[] to Nodes
   * @param data
   * @param options
   * @returns {Dictionary<Node>}
   */
  public objToNodes(data: NodeApiResponse[], options: { onlyMetaData: boolean } = { onlyMetaData: false }): Entities<T> {
    return flow(
      map( x => this.objToNodeNoDynamicData(x, options)),
      keyBy('id'),
      (x) => { this.setDynamicData(x); return x; }
    )(data);
  }

  /**
   * Set right dynamic data on nodes.
   * @param nl  Nodes list to be modified.
   */
  public setDynamicData(nl: Entities<T>) {
    this.setGroupsFlag(nl);
  }

  /**
   * Insert existing content of already loaded nodes if version code was not changed.
   * @param newNodes
   * @returns {Node[]}  Return rejected nodes (remote content changed, existing node has $dirty status)
   */
  public insertAlreadyLoaded(newNodes: Entities<T>): T[] {
    const rejected: T[] = [];
    _map(newNodes, (n) => {
      const oldNode = this.store.entities[n.id];
      if (oldNode && oldNode.isLoaded()) {
        if (n.version === oldNode.version) {
          // just use old node since nothing changed on remote
          newNodes[n.id] = oldNode;
        } else {
          if (oldNode.$dirty) {
            // return lost nodes data
            rejected.push(oldNode);
          }
        }
      }
    });
    return rejected;
  }
  // endregion
}
