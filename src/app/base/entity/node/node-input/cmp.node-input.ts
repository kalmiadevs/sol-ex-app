import { Component, EventEmitter, forwardRef, Inject, Input, OnInit, Output } from '@angular/core';
import 'rxjs/add/operator/switchMap';
import { find as _find, reduce as _reduce } from 'lodash';
import { BaseNode } from '../cls.node';
import { StoreService } from '../../../core/svc.store';
import { Entities } from '../../itf.entities';


@Component({
  selector: 'app-node-input',
  templateUrl: './tpl.node-input.html',
  styleUrls: ['./cmp.node-input.scss']
})
export class NodeInputComponent implements OnInit {


  nodes: Entities<BaseNode>;
  nodeInputText: string = '';

  @Input() readonly: boolean;

  // region node groups binding
  _inputNodes: string[] = [];

  @Input()
  get inputNodes() { return this._inputNodes; }

  @Output() inputNodesChange = new EventEmitter();

  set inputNodes(val: string[]) {
    this._inputNodes = val;
    this.inputNodesChange.emit(this._inputNodes);
  }
  // endregion

  constructor (
    @Inject(forwardRef(() => StoreService)) sysStore: StoreService
  ) {
    if (sysStore.nodeService) {
      sysStore.nodeService.nodes.subscribe(x => this.nodes = x);
    }
  }

  ngOnInit() {
  }

  /**
   * Convert node title into node id
   * @param title
   */
  stringToNodeId = (title) => (_find(this.nodes, g => g.niceName() === title) || { id: undefined }).id;

  /**
   * Convert node id into title
   * @param gid
   */
  nodeIdToString = (gid: string) => this.nodes[gid] ? this.nodes[gid].niceName() : gid;

  /**
   * Return suggested nodes.
   */
  get suggestedNodes(): string[] {
    return _reduce(this.nodes,
      (a: string[], v: BaseNode) =>
        (!this.nodeInputText || v.niceName().indexOf(this.nodeInputText) !== -1) && this.inputNodes.indexOf(v.id) === -1
          ? [...a, v.id] : a,
      []);
  };
}
