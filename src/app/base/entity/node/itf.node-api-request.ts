import { NodeApiResponse } from './itf.node-api-response';
export interface NodeApiRequest {
  /**
   * Node payload
   */
  node: NodeApiResponse;

  /**
   * Groups to remove or add to node
   */
  groups: {
    add: string[];
    remove: string[];
  };

  /**
   * Users list with encrypted node key to update their vault
   */
  users: {
    uid: string;
    chipperNodeKey: string;
  }[];
}
