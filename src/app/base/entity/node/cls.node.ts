import { cloneDeep as _cloneDeep } from 'lodash';
import { Entity } from '../cls.entity';
import { NodeApiResponse } from './itf.node-api-response';

export class BaseNode extends Entity {

  public version : number = 0;

  // region Dynamic data
  /**
   * List of group id's that node belongs to.
   * @type {Array}
   */
  public $groups: string[] = [];

  /**
   * Indicates if user can open node.
   * It's based on groups.
   */
  public $hasAccess: boolean = false;
  // endregion

  /**
   * Create entity with only metadata.
   * @param data
   * @returns {Node}
   */
  public setOnlyMetaData(data: NodeApiResponse) {
    this.id      = data.id;
    this.version = data.version;
  }

  /**
   * Create fully loaded entity.
   * @param data
   * @returns {Node}
   */
  public setData(data: NodeApiResponse) {
    this.setOnlyMetaData(data);
  }

  /**
   * Is node content loaded or do we have only node with metadata.
   * @returns {boolean}
   */
  isLoaded(): boolean { return true; }

  public niceName(): string { return this.id; }

  /**
   * Clone node.
   * @returns {Node}
   */
  clone(): BaseNode {
    return _cloneDeep(this);
  }
}
