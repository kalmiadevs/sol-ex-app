export interface NodeApiResponse {
  id             : string;
  version        : number;
}
