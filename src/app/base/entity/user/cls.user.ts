import { cloneDeep as _cloneDeep } from 'lodash';
import { Md5 } from 'ts-md5/dist/md5';
import { Entity } from '../cls.entity';
import { EntityState } from '../cls.entity-state';
import { UserApiResponse } from 'app/base/entity/user/itf.user-api-response';
import { Crypt, PublicKey } from '../../core/crypt/cls.crypt';
import { environment } from '../../../../environments/environment';
import { appConfNoDep } from '../../../conf.app-no-dep';

/**
 * User instance.
 */
export class User extends Entity {

  public id: string;
  public accountId: string;
  public email: string;
  public firstName: string;
  public lastName: string;
  public meta: any;

  /**
   * Only used for upload.
   */
  public avatarBase64: string;

  /**
   * Should be ISO 639-2
   */
  public language: 'af' | 'sq' | 'ar-SA' | 'ar-IQ' | 'ar-EG' | 'ar-LY' |
    'ar-DZ' | 'ar-MA' | 'ar-TN' | 'ar-OM' | 'ar-YE' | 'ar-SY' | 'ar-JO' |
    'ar-LB' | 'ar-KW' | 'ar-AE' | 'ar-BH' | 'ar-QA' | 'eu' | 'bg' | 'be' |
    'ca' | 'zh-TW' | 'zh-CN' | 'zh-HK' | 'zh-SG' | 'hr' | 'cs' | 'da' |
    'nl' | 'nl-BE' | 'en' | 'en-US' | 'en-EG' | 'en-AU' | 'en-GB' | 'en-CA' |
    'en-NZ' | 'en-IE' | 'en-ZA' | 'en-JM' | 'en-BZ' | 'en-TT' | 'et' | 'fo' |
    'fa' | 'fi' | 'fr' | 'fr-BE' | 'fr-CA' | 'fr-CH' | 'fr-LU' | 'gd' | 'gd-IE' |
    'de' | 'de-CH' | 'de-AT' | 'de-LU' | 'de-LI' | 'el' | 'he' | 'hi' | 'hu' |
    'is' | 'id' | 'it' | 'it-CH' | 'ja' | 'ko' | 'lv' | 'lt' | 'mk' | 'mt' |
    'no' | 'pl' | 'pt-BR' | 'pt' | 'rm' | 'ro' | 'ro-MO' | 'ru' | 'ru-MI' |
    'sz' | 'sr' | 'sk' | 'sl' | 'sb' | 'es' | 'es-AR' | 'es-GT' | 'es-CR' |
    'es-PA' | 'es-DO' | 'es-MX' | 'es-VE' | 'es-CO' | 'es-PE' | 'es-EC' |
    'es-CL' | 'es-UY' | 'es-PY' | 'es-BO' | 'es-SV' | 'es-HN' | 'es-NI' |
    'es-PR' | 'sx' | 'sv' | 'sv-FI' | 'th' | 'ts' | 'tn' | 'tr' | 'uk' | 'ur' |
    've' | 'vi' | 'xh' | 'ji' | 'zu';

  /**
   * Describes user role in system.
   * admin, superadmin, customer, partner, qa, ...
   */
  public role: string;

  /**
   * Note for not Kal* project this might not be used.
   * Check appConfNoDep config to see if it's enabled for this project.
   */
  public _publicKey: string | PublicKey;

  public get publicKey(): PublicKey {
    if (typeof this._publicKey ===  'string') {
      this._publicKey = Crypt.publicKeyParse(this._publicKey);
    }
    return this._publicKey;
  }

  public get avatar(): string { return this.getGravatarUrl(); }

  // region Dynamic data
  /**
   * Contains all groups that user belongs to.
   * @returns {string[]}
   */
  public get $groups(): string[] {
    return this.$privateGroup ? [ ...this.$publicGroups, this.$privateGroup ] : this.$publicGroups;
  }

  /**
   * Public groups list that user belongs to.
   * This list doesn't contain users private group!
   */
  public $publicGroups: string[] = [];

  /**
   * Users private group, if he has it.
   *
   * Private group is used for directly linking user to node.
   */
  public $privateGroup?: string;
  // endregion

  /**
   * Create new empty user.
   * @returns {Node}
   */
  static createNew(): User {
    const x = new User();
    x.$state = EntityState.New;
    return x;
  }

  static create(data: any): User {
    return new User(data);
  }

  static createFromApi(u: UserApiResponse): User {
    const x =  new User();
    x.id         = u.id;
    x.firstName  = u.firstName;
    x.lastName   = u.lastName;
    x.accountId   = u.account;
    x.email      = u.email;
    x.role       = u.role;
    x.language   = u.language as any || 'en';
    x._publicKey = u.publicKey;
    x.meta       = u.meta;
    return x;
  }

  constructor(data?: any) {
    super();

    if (data) {
      this.id         = data.id;
      this.firstName  = data.firstName;
      this.lastName   = data.lastName;
      this.accountId   = data.accountId;
      this.language   = data.language;
      this.email      = data.email;
      this.role       = data.role;
      if (appConfNoDep.userCertEnabled) {
        this._publicKey = data.publicKey;
      }
      this.meta = data.meta;
    }
  }

  clone(): User {
    return _cloneDeep(this);
  }

  getFullName(): string {
    return this.firstName + ' ' + this.lastName;
  }

  /**
   * https://en.gravatar.com/site/implement/images/
   * @returns {string}
   */
  getGravatarUrl(): string {
    // if (this.avatarBase64) {
    //   return this.avatarBase64;
    // }
    return `${ environment.apiBaseIDMS }/avatar/${ this.accountId }/${ this.id }?email=${
      this.email ? <string>Md5.hashStr(this.email.trim().toLowerCase()) : ''
      }?size=256`;
    // return `https://www.gravatar.com/avatar/${
    //   this.email ? <string>Md5.hashStr(this.email.trim().toLowerCase()) : ''
    // }?s=256&d=${ this.email ? 'identicon' : 'mm' }&r=g`;
  }

  public niceName(): string { return this.firstName + ' ' + this.lastName; }

  public getSaveObj(): any {
    return {
      id          : this.id,
      email       : this.email,
      firstName   : this.firstName,
      lastName    : this.lastName,
      avatarBase64: this.avatarBase64,
    };
  };
}
