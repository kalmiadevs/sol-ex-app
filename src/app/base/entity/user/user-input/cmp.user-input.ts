import { Component, EventEmitter, forwardRef, Inject, Input, OnInit, Output } from '@angular/core';
import 'rxjs/add/operator/switchMap';
import { find as _find, reduce as _reduce } from 'lodash';
import { Entities } from '../../itf.entities';
import { User } from '../cls.user';
import { StoreService } from '../../../core/svc.store';

@Component({
  selector: 'app-user-input',
  templateUrl: './tpl.user-input.html',
  styleUrls: ['./cmp.user-input.scss']
})
export class UserInputComponent implements OnInit {


  users: Entities<User>;
  userInputText: string = '';

  @Input() readonly: boolean;

  // region binding
  _inputUsers: string[] = [];

  @Input()
  get inputUsers() { return this._inputUsers; }

  @Output() inputUsersChange = new EventEmitter();

  set inputUsers(val: string[]) {
    this._inputUsers = val;
    this.inputUsersChange.emit(this._inputUsers);
  }
  // endregion

  constructor (
    @Inject(forwardRef(() => StoreService)) sysStore: StoreService
  ) {
    sysStore.userService.users.subscribe(x => this.users = x);
  }

  ngOnInit() {
  }

  /**
   * Convert user name into user id
   * @param val
   */
  stringToUserId = (val) => (_find(this.users, x => x.niceName().toLowerCase()  === val.toLowerCase()) || { id: undefined }).id;

  /**
   * Convert user id into name
   * @param id
   */
  userIdToString = (id: string) => this.users[id] ? this.users[id].niceName() : id;

  /**
   * Return suggested users.
   */
  get suggestedUsers(): string[] {
    return _reduce(this.users,
      (a: string[], v: User) =>
        (
          !this.userInputText ||
          v.firstName.indexOf(this.userInputText) !== -1 ||
          v.lastName.indexOf(this.userInputText) !== -1
        )
        &&
        this.inputUsers.indexOf(v.id) === -1 // is not already added
          ? [...a, v.id] : a,
      []);
  };
}
