import { Injectable } from '@angular/core';
import { User } from './cls.user';
import { HttpAuthService } from '../../core/http/svc.http-auth';
import { chain as _chain, map as _map } from 'lodash';
import { EntityWithRemoteService } from '../svc.entity-with-remote';
import { Entities } from '../itf.entities';
import { Groups } from '../group/itf.groups';
import { BaseSystemService } from '../../core/svc.system';
import { environment } from '../../../../environments/environment';

/**
 * Users service
 */
@Injectable()
export class BaseUserService extends EntityWithRemoteService<User>  {

  // protected apiEntityName      = `user`;
  // protected apiEntitiesName    = `users.json`;

  protected apiEntityName      = `users`;
  protected apiEntitiesName    = `users`;
  protected apiBase            = `${ environment.apiBaseKalBlocks }`;

  get users() { return this.entities; }

  constructor(protected http: HttpAuthService) {
    super(http);
  }

  public init(sys: BaseSystemService) {
    this.sys = sys;

    this.sys.groupService.groups.subscribe(x => {
      this.updateUsersGroups(this.store.entities, x);
      this.notifyEntitiesChange();
    });
  }

  /**
   * Update $groups flag of users based on groups list.
   * @param ul  Users list to be modified.
   * @param gl  Categories list.
   */
  private updateUsersGroups(ul: Entities<User>, gl?: Groups) {
    this.clearGroupsFromUsers(ul);
    this.setGroupsFlag(ul, gl);
  }

  /**
   * Set $groups to users.
   *
   * Warning! This function expects $groups flag to be empty. Use @clearGroupsFromUsers function if you can't guarantee that.
   * @param ul
   * @param gl
   */
  public setGroupsFlag(ul: Entities<User>, gl = this.sys.groupService.getEntities()) {
    _map(gl, g => {
      if (g.$private) {
        if (ul[g.owner]) {
          ul[g.owner].$privateGroup = g.id;
        }
        // else nothing to do we don't know user
      } else {
        g.users.map(uid => {
          if (ul[uid]) {
            ul[uid].$publicGroups.push(g.id);
          }
        });
      }
    });
  }

  /**
   * Set $favorite flag to false for given nodes.
   * @param ul  Users list to be modified.
   */
  private clearGroupsFromUsers(ul: Entities<User>): void {
    _map(ul, x => {
      x.$publicGroups = [];
      x.$privateGroup = undefined;
    });
  }

  protected onEntitiesReceiveFromRemote(users: Entities<User>) {
    this.updateUsersGroups(users); // mutate!
    this.setEntities(users);
  }

  protected onEntityReceiveFromRemote(u: User) {
    this.updateUsersGroups({ [u.id]: u }); // mutate!
    this.addEntity(u);
  }

  protected convertRemoteResToEntities(data: any): Promise<User[]> {
    return new Promise((resolve, reject) => {
      resolve(this.objToUsers(data.users));
    });
  }

  protected convertRemoteResToEntity(data: any): Promise<User> {
    return new Promise((resolve, reject) => {
      resolve(User.createFromApi(data));
    });
  }

  /**
   * Convert remote object into Users
   * @param data
   */
  private objToUsers(data: any): User[] {
    return _chain(data)
      .map(x => User.createFromApi(x))
      .value();
  }

  protected onEntityInsertSuccess(data: any, lockedEntity: User) {
    const u = User.createFromApi(data.user);
    this.store.entities[lockedEntity.id]._publicKey = u.publicKey;
    super.onEntityInsertSuccess(data, lockedEntity);
  }
}
