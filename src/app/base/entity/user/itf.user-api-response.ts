import { Entities } from '../itf.entities';
export interface CreateUserApiResponse {
  status: string;
  user: UserApiResponse;
}

export interface Meta {
  devices: number;
  cases: number;
}

export interface UserApiResponse {
  __v: number;
  deletedAt?: any;
  deleted: boolean;
  updatedAt: Date;
  createdAt: Date;
  account: string;
  firstName: string;
  lastName: string;
  email: string;
  meta: Meta;
  password: string;
  id: string;
  _id: string;
  role: string;
  groups: any[];
  locked: boolean;
  active: boolean;
  language: string;
  publicKey: string;
  privateKey: string;
  keyRing: Entities< {
    sym?: string,
    pub?: string,
    sec?: string,
  }>;
}
