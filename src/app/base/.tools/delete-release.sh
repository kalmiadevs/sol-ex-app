#!/bin/bash

#
#  Upload source maps to Sentry and remove them after.

# start from this file location
parent_path=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )
cd "$parent_path"

# move up to app root
cd '../../../../'


export SENTRY_AUTH_TOKEN="0826a898f8e54db5bb9390de121ca9f8df9782ad41834dd5874deb4771fd4ce1"
./node_modules/.bin/sentry-cli releases files $1 delete --all
