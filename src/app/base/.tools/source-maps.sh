#!/bin/bash

#
#  Upload source maps to Sentry and remove them after.
# TODO remove hardcoded app name

# start from this file location
parent_path=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )
cd "$parent_path"

# move up to app root
cd '../../../../'

COMMIT_HASH=`git rev-parse HEAD`
PACKAGE_VERSION=$(cat package.json | grep version | head -1 | awk -F= "{ print $2 }" | sed 's/[version:,\",]//g' | tr -d '[[:space:]]')
# RELEASE="$PACKAGE_VERSION-$COMMIT_HASH"
RELEASE="$PACKAGE_VERSION"
# RELEASE="0.0.0"

export SENTRY_AUTH_TOKEN="77b62e50fc8643cfa3773109dc930b1cdea0f880f0464901b798c27eda0282d0"

# Upload
./node_modules/.bin/sentry-cli releases -o kalmia -p soltecapp new "$RELEASE"

# Move source maps to tmp folder
mv -rf tmp
mkdir tmp
cp ./dist/*.js.map tmp

# Upload source maps
./node_modules/.bin/sentry-cli releases -o kalmia -p soltecapp files "$RELEASE" upload-sourcemaps ./tmp

# Remove source maps
rm -rf tmp

