#!/bin/bash

#
#  Version Update Script

# Check First Param
if [ -z "$1" ]
then
  printf "\n!!! ERROR: Env parameter is required!\n\n"
  printf "Version script usage: \n-----------------------\n./version.sh stage \n\n"
  exit 0
fi

# start from this file location
parent_path=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )
cd "$parent_path"

# move up to app root
cd '../../../../'


# gather info
ENV_TAG="$1"
DATE=`date +%Y-%m-%d:%H:%M:%S`
COMMIT_HASH=`git rev-parse HEAD`
PACKAGE_VERSION=$(cat package.json | grep version | head -1 | awk -F= "{ print $2 }" | sed 's/[version:,\",]//g' | tr -d '[[:space:]]')
# RELEASE="$PACKAGE_VERSION-$COMMIT_HASH"
RELEASE="$PACKAGE_VERSION"
# RELEASE="0.0.0"

echo "ENV_TAG: $ENV_TAG"
echo "DATE: $DATE"
echo "COMMIT_HASH: $COMMIT_HASH"
echo "PACKAGE_VERSION: $PACKAGE_VERSION"

#    sed -i.bak "s/\"version\": \".*\"/\"version\": \"$VERSION\"/g" ./mobile/package.json
sed -i -e "s/\${version}/$PACKAGE_VERSION/" -e "s/\${release}/$RELEASE/" -e "s/\${commit}/$COMMIT_HASH/" -e "s/\${buildDate}/$DATE/" ./src/app/base/environments/environment.common.ts


# If angular is failing this could be helpful:
# sed -i -e "s/\${version}/$PACKAGE_VERSION/" -e "s/\${commit}/$COMMIT_HASH/" -e "s/\${buildDate}/$DATE/" -e "s/\${env}/$ENV_TAG/" ./src/assets/version.json
#
# {"version": "${version}", "release": "${commit}", "date": "${buildDate}", "env": "${env}"}
