import { commonBaseEnvironment } from './environment.common';

export const baseEnvironment = {
  ...commonBaseEnvironment,

  production: true,
  env       : 'stage',
  isEnvProd : false,

  appsLocation: {
    admin: 'https://admin-stage.kalblocks.com',
    pass: 'https://pass-stage.kalblocks.com'
  },

  apiBaseKalBlocks: 'https://api-dev.kalblocks.com/v1',
  apiBaseIDMS: 'https://idms-dev.kalblocks.com/v1'
};
