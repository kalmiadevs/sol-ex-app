import { commonBaseEnvironment } from './environment.common';

export const baseEnvironment = {
  ...commonBaseEnvironment,

  production: true,
  env       : 'prod',
  isEnvProd : true,

  appsLocation: {
    admin: 'https://admin.kalblocks.com',
    pass: 'https://pass.kalblocks.com'
  },

  apiBaseKalBlocks: 'https://api-dev.kalblocks.com/v1',
  apiBaseIDMS: 'https://idms-dev.kalblocks.com/v1'
};
