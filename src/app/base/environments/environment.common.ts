export const commonBaseEnvironment = {
  enableRegistration: true,
  enableForgotPass: true,

  raven: undefined,

  version: '${version}',
  commit: '${commit}',
  buildDate: '${buildDate}',
  release: '${release}',

  /**
   * Id of app. Example: pass
   * Should match key in appsLocation.
   */
  appId: ''
};
