import { commonBaseEnvironment } from './environment.common';

export const baseEnvironment = {
  ...commonBaseEnvironment,

  production: true,
  env       : 'dev',
  isEnvProd : false,


  appsLocation: {
    admin: 'https://admin-dev.kalblocks.com',
    pass: 'https://pass-dev.kalblocks.com'
  },

  apiBaseKalBlocks: 'https://api-dev.kalblocks.com/v1',
  apiBaseIDMS: 'https://idms-dev.kalblocks.com/v1'
};
