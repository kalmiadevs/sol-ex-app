import { commonBaseEnvironment } from './environment.common';

export const baseEnvironment = {
  ...commonBaseEnvironment,

  production: false,
  env       : 'local',
  isEnvProd : false,

  /**
   * List of all URL for each app.
   */
  appsLocation: {
    admin: 'http://localhost:4203',
    pass: 'http://localhost:4200',
  },

  apiBaseKalBlocks: 'http://localhost:5001/v1',
  apiBaseIDMS: 'http://localhost:5000/v1'
};
