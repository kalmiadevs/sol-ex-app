import { Pipe, PipeTransform } from '@angular/core';
import { environment } from '../../../environments/environment';

@Pipe({
  name: 'filterEntities'
})
export class FilterEntitiesPipe implements PipeTransform {

  transform(value: any, args?: any): any[] {
    if (!value) {
      return [];
    }
    // args.search = args.search && args.search.toLowerCase() || false;

    const out = [];
    for (const key in value) {
      if (value.hasOwnProperty(key)) {
        const n = value[key];
        // marked for removal
        // if(n.removed) continue;

        // search
        // if(args.search && n.title.toLowerCase().indexOf(args.search) < 0) continue;

        if (n) {
          out.push(n);
        } else {
          if (environment.env !== 'prod') {
            console.error(`Warning! Entity ${ key } was empty.`);
          }
        }
      }
    }
    return out;
  }

}
