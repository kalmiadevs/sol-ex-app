import { CapitalizePipe } from './pip.capitalize';

describe('CapitalizePipe', () => {
  it('capitalize word', () => {
    const pipe = new CapitalizePipe();
    expect(pipe.transform('abc')).toBe('Abc');
  });
});
