export class PromiseHelper {
  public static allSettled(promises): Promise<({ error?: any } | { value?: any })[]> {
    const wrappedPromises = promises.map(p => Promise.resolve(p)
      .then(
        val => ({value: val}),
        err => ({error: err})));
    return Promise.all(wrappedPromises);
  }
}
