import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-sort-controls',
  templateUrl: './tpl.sort-controls.html',
  styleUrls: ['./cmp.sort-controls.scss']
})
export class SortControlsComponent {

  @Input() activeSortBy: string;
  @Input() activeReverseSort: boolean;

  @Output() sort?: EventEmitter<{
    key: string,
    asc: boolean,
    desc: boolean
  }> = new EventEmitter();
  @Input() key: string;

  asc() {
    this.sort.emit({
      asc: true,
      desc: false,
      key: this.key
    });
  }

  desc() {
    this.sort.emit({
      asc: false,
      desc: true,
      key: this.key
    });
  }
}
