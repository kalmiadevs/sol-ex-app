import { ValuesPipe } from './pip.values';

describe('ValuesPipe', () => {
  it('create an instance', () => {
    const pipe = new ValuesPipe();
    expect(pipe.transform(['a'], { a: 123, b: 321})[0] === 123).toBeTruthy();
  });
});
