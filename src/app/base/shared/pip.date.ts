import { Pipe, PipeTransform } from '@angular/core';
import { map as _map } from 'lodash';
import { DatePipe } from '@angular/common';

@Pipe({
  name: 'safeDate'
})
export class SafeDatePipe extends DatePipe implements PipeTransform {

  transform(value: any, pattern?: string): string | null {
    try {
      return super.transform(value, pattern);
    } catch (e) {
      return value;
    }
  }
}
