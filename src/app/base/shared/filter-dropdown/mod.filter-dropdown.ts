import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FilterDropdownComponent } from './cmp.filter-dropdown';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FilterDropdownTextComponent } from './filter-dropdown-text/cmp.filter-dropdown-text';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NgbModule
  ],
  declarations: [
    FilterDropdownComponent,
    FilterDropdownTextComponent
  ],
  exports: [
    FilterDropdownComponent,
    FilterDropdownTextComponent
  ]
})
export class FilterDropdownModule { }
