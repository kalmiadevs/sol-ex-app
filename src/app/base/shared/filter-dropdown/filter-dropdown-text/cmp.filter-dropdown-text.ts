import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-filter-dropdown-text',
  templateUrl: './tpl.filter-dropdown-text.html',
  styleUrls: ['./cmp.filter-dropdown-text.scss']
})
export class FilterDropdownTextComponent implements OnInit {

  _value: string = '';

  @Input()
  typeaheadFunc?: any;

  @Input()
  showTypeaheadOnFocus: boolean = false;

  @Input()
  clearButton: boolean = false;

  @Input()
  get value() {
    return this._value;
  }
  set value(val) {
    if (val !== this._value) {
      this._value = val;
      this.valueChange.emit(this._value);
    }
  }
  @Output() valueChange = new EventEmitter();

  @Input() label = '';
  @Output() change = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  onFocus(e: Event) {
    if (this.showTypeaheadOnFocus) {
      if (!this.value) {
        e.stopPropagation();
        setTimeout(() => {
          const inputEvent: Event = new Event('input');
          e.target.dispatchEvent(inputEvent);
        }, 0);
      }
    }
  }

  onTypeaheadSelect(e: any) {
    if (this.value !== e.item) {
      this.value = e.item;
    }
  }

  clear() {
    this.value = '';
    this.change.emit(this._value);
  }
}
