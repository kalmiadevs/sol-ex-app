import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-filter-dropdown',
  templateUrl: './tpl.filter-dropdown.html',
  styleUrls: ['./cmp.filter-dropdown.scss']
})
export class FilterDropdownComponent implements OnInit {

  @Input() enabled: boolean;
  @Output() close = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  onClose() {
    this.close.emit(undefined);
  }

}
