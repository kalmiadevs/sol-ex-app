import { Pipe, PipeTransform } from '@angular/core';
import { map as _map } from 'lodash';

@Pipe({
  name: 'values'
})
export class ValuesPipe implements PipeTransform {

  transform<T>(keys: string[], obj: {[key: string]: T}): T[] {
    if (!keys) { return []; }
    return _map(keys, (k) => obj[k]);
  }
}
