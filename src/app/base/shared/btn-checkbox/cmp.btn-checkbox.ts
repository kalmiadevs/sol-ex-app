import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-btn-checkbox',
  templateUrl: './tpl.btn-checkbox.html',
  styleUrls: ['./cmp.btn-checkbox.scss']
})
export class BtnCheckboxComponent {

  @Input() checked: boolean;
  @Input() disabled: boolean;

  @Output() checkedChange: EventEmitter<boolean> = new EventEmitter();

  constructor() { }

  toggle() {
    if (!this.disabled) {
      this.checked = !this.checked;
      this.checkedChange.emit(this.checked);
    }
  }
}
