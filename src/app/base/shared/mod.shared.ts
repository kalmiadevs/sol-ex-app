import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CapitalizePipe } from './pip.capitalize';
import { BtnCheckboxComponent } from './btn-checkbox/cmp.btn-checkbox';
import { FilterEntitiesPipe } from './pip.filter-entities';
import { ValidationComponent } from './form/cmp.validation';
import { ValuesPipe } from './pip.values';
import { SortControlsComponent } from './sort-controls/cmp.sort-controls';
import { FilterDropdownModule } from './filter-dropdown/mod.filter-dropdown';
import { DisableFormControlDirective } from './drc.disable-cond';
import { SafeDatePipe } from './pip.date';
import { SafePipe } from './pip.safe';
import { RoundPipe } from './pip.round';

@NgModule({
  imports: [
    CommonModule,
    FilterDropdownModule
  ],
  declarations: [
    CapitalizePipe,
    BtnCheckboxComponent,
    FilterEntitiesPipe,
    ValidationComponent,
    ValuesPipe,
    SortControlsComponent,
    DisableFormControlDirective,
    SafeDatePipe,
    SafePipe,
    RoundPipe,
  ],
  exports: [
    CapitalizePipe,
    BtnCheckboxComponent,
    FilterEntitiesPipe,
    ValidationComponent,
    ValuesPipe,
    SortControlsComponent,
    FilterDropdownModule,
    DisableFormControlDirective,
    SafeDatePipe,
    SafePipe,
    RoundPipe,
  ]
})
export class BaseSharedModule {
  static forRoot() {
    return {
      ngModule: BaseSharedModule,
      providers: [],
    };
  }
}
