/**
 * Class for maneging remove and add instructions.
 *
 * For example if you add action it will automatically remove it from remove list.
 */
export class ListEditActions <T> {

  public removeActions : Set<T>;
  public addActions    : Set<T>;

  public static createDiff<A>(original: A[], modified: A[]): ListEditActions<A> {
    const l = new ListEditActions<A>();
    original.filter(x => modified.indexOf(x) === -1).map(x => l.remove(x));
    modified.filter(x => original.indexOf(x) === -1).map(x => l.add(x));
    return l;
  }

  constructor() {
    this.removeActions = new Set<T>();
    this.addActions    = new Set<T>();
  }

  public remove(val: T) {
    this.removeActions.add(val);
    if (this.addActions.has(val)) {
      this.addActions.delete(val);
    }
  }

  public add(val: T) {
    this.addActions.add(val);
    if (this.removeActions.has(val)) {
      this.removeActions.delete(val);
    }
  }

  public clear() {
    this.removeActions = new Set<T>();
    this.addActions = new Set<T>();
  }
}
