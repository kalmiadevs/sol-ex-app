/**
 * Get cookie.
 * @param cookieName
 * @return {any}
 */
export function getCookie(cookieName) {
  const name = cookieName + '=';
  const decodedCookie = decodeURIComponent(document.cookie);
  const ca = decodedCookie.split(';');
  for (let i = 0; i < ca.length; i++) {
    let c = ca[i];
    while (c.charAt(0) === ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) === 0) {
      return c.substring(name.length, c.length);
    }
  }
  return '';
}

export function setCookie(name: string, value: string, days?: number) {
  if (!days) {
    days = 365 * 20;
  }

  const date = new Date();
  date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));

  const expires = '; expires=' + date.toUTCString();

  document.cookie = name + '=' + value + expires + '; path=/';
}

export function removeCookie(name: string) {
  const date = new Date();
  date.setTime(0);
  const expires = '; expires=' + date.toUTCString();
  document.cookie = name + '=' + expires + '; path=/';
}
