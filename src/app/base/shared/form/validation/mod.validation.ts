import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ControlMessagesComponent } from './cmp.control-messages';
import { ValidationService } from './svc.validation';

@NgModule({
  imports: [
    CommonModule
  ],
  providers: [
    ValidationService
  ],
  declarations: [ControlMessagesComponent],
  exports: [
    ControlMessagesComponent
  ]
})
export class ValidationModule { }
