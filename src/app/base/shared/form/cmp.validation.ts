import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-validation',
  template: `
    <ul class="validation">
      <li *ngFor="let message of messages" class="text-danger">{{message}}</li>
    </ul>
  `,
  styles: [`
    .validation {
      list-style: none;
    }`
  ]
})
export class ValidationComponent {
  @Input() messages: Array<string>;
}
