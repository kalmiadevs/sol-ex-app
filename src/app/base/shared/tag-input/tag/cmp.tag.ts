import { Component, EventEmitter, Input, OnInit, Output, TemplateRef } from '@angular/core';

@Component({
  selector: 'app-tag',
  templateUrl: './tpl.tag.html',
  styleUrls: ['./cmp.tag.scss']
})
export class TagComponent implements OnInit {

  @Input()  highlight: boolean;
  @Input()  label: string;
  @Input()  tag: any;
  @Output() remove?: EventEmitter<any> = new EventEmitter();
  @Input()  readonly: boolean;
  @Input()  suggested: boolean;
  @Input()  dropdown: boolean;

  /**
   * @name template {TemplateRef<any>}
   */
  @Input() public template: TemplateRef<any>;


  constructor() { }

  ngOnInit() {
  }
}
