import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TagInputComponent } from './cmp.tag-input';
import { TagComponent } from './tag/cmp.tag';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    FormsModule
  ],
  declarations: [TagInputComponent, TagComponent],
  exports: [TagInputComponent]
})
export class TagInputModule { }
