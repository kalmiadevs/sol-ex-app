import { Injectable } from '@angular/core';
import { BaseSystemService } from './svc.system';
import { BaseGroupService } from '../entity/group/svc.group';
import { BaseUserService } from '../entity/user/svc.user';
import { BaseNotificationService } from '../app/notifications/svc.notification';
import { AuthService } from './auth/svc.auth';
import { BaseNodeService } from '../entity/node/svc.node';
import { BaseNode as BaseNode } from '../entity/node/cls.node';
import { BasePermissionService } from '../entity/permission/svc.permission';

/**
 * Purpose of store is to help base components get base services without
 * need to make providers for base service, since they are not used directly.
 */
@Injectable()
export class StoreService {

  public sys: BaseSystemService;


  get authService(): AuthService {
    return this.sys.authService;
  }
  get notificationService(): BaseNotificationService {
    return this.sys.notificationService;
  }
  get userService(): BaseUserService {
    return this.sys.userService;
  }
  get groupService(): BaseGroupService {
    return this.sys.groupService;
  }
  get permissionService(): BasePermissionService {
    return this.sys.permissionService;
  }

  /**
   * Can return undefined if parent project doesn't use node service.
   * @return {BaseNodeService<BaseNode>|NodeService}
   */
  get nodeService(): BaseNodeService<BaseNode> {
    return (<BaseSystemService & {nodeService: BaseNodeService<BaseNode>}>this.sys).nodeService;
  }
}
