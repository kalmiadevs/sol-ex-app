import { forwardRef, Inject, Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { StoreService } from './svc.store';
import { User } from '../entity/user/cls.user';
import { BaseUserService } from '../entity/user/svc.user';
import { BaseGroupService } from '../entity/group/svc.group';
import { Group } from '../entity/group/cls.group';

@Injectable()
export class BaseGroupResolver implements Resolve<Observable<Group>> {

  groupService: BaseGroupService;

  constructor(
    @Inject(forwardRef(() => StoreService)) sysStore: StoreService
  ) {
    this.groupService = sysStore.groupService;
  }

  resolve(route: ActivatedRouteSnapshot) {
    return this.groupService.resolve(route);
  }
}


@Injectable()
export class BaseUserResolver implements Resolve<Observable<User>> {
  userService: BaseUserService;

  constructor(
    @Inject(forwardRef(() => StoreService)) sysStore: StoreService
  ) {
    this.userService = sysStore.userService;
  }

  resolve(route: ActivatedRouteSnapshot) {
    return this.userService.resolve(route);
  }
}
