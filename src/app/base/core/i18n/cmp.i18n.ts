import {Component} from '@angular/core';

@Component({
  selector: 'app-i18n-holder',
  moduleId: module.id,
  templateUrl: 'tpl.i18n.html'
})
export class BaseI18nHolderComponent {
}
