import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BaseI18nHolderComponent } from './cmp.i18n';
import { I18nService } from './svc.i18n';

@NgModule({
  imports: [
    CommonModule
  ],
  providers: [
    I18nService
  ],
  declarations: [
    BaseI18nHolderComponent
  ]
})
export class I18nModule { }
