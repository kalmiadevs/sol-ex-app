import {Injectable, Inject, TRANSLATIONS} from '@angular/core';
import { I18NHtmlParser, HtmlParser, Xliff, Xliff2 } from '@angular/compiler';
import { loadedTranslations } from './svc.i18n-provider';
import { messages } from './messages';

let i18nServiceInstance: I18nService;

export function _(key: string, interpolation: any[] = []): string {
  if (!i18nServiceInstance) {
    console.error(new Error('i18nServiceInstance not ready. Requested translation for ' + key));
    return messages[key];
  }
  return i18nServiceInstance.get(key, interpolation);
}

/**
 *  Example usage:
 *   constructor(private i18n: I18nService) {
 *         console.info('Translation test: ' + i18n.get('appSampleTranslation', ['INJECTION WORKS']));
 *         console.info('Translation test [static]: ' + _('appSampleTranslation', ['INJECTION WORKS']));
 *   }
 */
@Injectable()
export class I18nService {
  private _source: string;
  private _translations: { [name: string]: any };

  private cache: any = {};
  private parser: I18NHtmlParser;

  constructor(
    // @Inject(TRANSLATIONS) source: string
  ) {
    const xliff = new Xliff2();
    this._source = loadedTranslations;
    if (this._source) {
      const loaded       = xliff.load(this._source, ''); // NOTE somewhere in angular core this is already done
      this._translations = loaded['i18nNodesByMsgId'] ? loaded['i18nNodesByMsgId'] : {};
      // console.info('I18nService: TRANSLATIONS: ', this._translations);

    } else {
      // console.log('I18nService: No TRANSLATIONS using default.');

      this._translations = {};
    }

    this.parser = new I18NHtmlParser(new HtmlParser(), this._source, 'xlf2');
    i18nServiceInstance = this;
  }

  _(key: string, interpolation: any[] = []): string {
    return this.get(key, interpolation);
  }

  get(key: string, interpolation: any[] = [], placeholders: string[] = []): string {
    if (messages[key] === undefined) {
      console.warn('No translation found for: ' + key);
      return '';
    }

    if (!interpolation.length && this.cache[key]) {
      // console.info('serving from cache' + key);
      return this.cache[key];
    }

    // const parser = new I18NHtmlParser(new HtmlParser(), this._source, 'xlf2');
    // const placeholders = this._getPlaceholders(this._translations[key] ? this._translations[key] : []);
    placeholders = placeholders.length ? placeholders : this._getPlaceholders(messages[key]);

    // console.log(`<div i18n="@@${key}">${ messages[key] }</div>`);
    const parseTree = this.parser.parse(`<div i18n="@@${key}">${ messages[key] }</div>`, 'someI18NUrl');
    const interName = this._interpolationWithName(placeholders, interpolation);
    const value = parseTree.rootNodes[0]['children'][0].value;

    const out = this._interpolate(value, interName);
    if (!interpolation.length && this.cache[key]) {
      this.cache[key] = out;
    }

    return out;
  }

  private _getPlaceholders(msg: string): string[] {
    return (msg.match(/{{ (\w+) }}/g) || []).map(key => key.substring(3, key.length - 3))
  }

  /* private _getPlaceholders(nodes: any[]): string[] {
    return nodes
      .filter((node) => node.hasOwnProperty('name'))
      .map((node) => `${node.name}`);
  } */

  private _interpolationWithName(placeholders: string[], interpolation: any[]): {[name: string]: any} {
    const asObj = {};

    placeholders.forEach((name, index) => {
      asObj[name] = interpolation[index];
    });

    return asObj;
  }

  private _interpolate(pattern: string, interpolation: {[name: string]: any}) {
    let compiled = '';
    compiled += pattern.replace(/{{ (\w+) }}/g, function (match, key) {
      if (interpolation[key] && typeof interpolation[key] === 'string') {
        match = match.replace(`{{ ${key} }}`, interpolation[key]);
      }
      return match;
    });

    return compiled;
  }
}
