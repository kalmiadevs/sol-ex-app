import { TRANSLATIONS, TRANSLATIONS_FORMAT, LOCALE_ID, MissingTranslationStrategy } from '@angular/core';
import { CompilerConfig } from '@angular/compiler';
import { getCookie, setCookie } from '../../shared/tools/cookie';
import { environment } from '../../../../environments/environment';

export let loadedTranslations: string;
export let failedLocale: string;
export let loadedLocale: string;

function getParameterByName(name, url = window.location.href) {
  name = name.replace(/[\[\]]/g, '\\$&');
  const regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
      results = regex.exec(url);
  if (!results) { return undefined; }
  if (!results[2]) { return ''; }
  return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

export function getTranslationProviders(): Promise<Object[]> {

  const forceLocale = getParameterByName('locale');
  if (forceLocale) {
    setCookie('locale', forceLocale);
  }

  // Get the locale id from the global
  const locale: string = getCookie('locale') || navigator.language || navigator['userLanguage'];
  // const locale = 'tmp' as any;

  if (!environment.isEnvProd) {
    console.info(`Language: ${ locale }`); // tslint:disable-line
  }

  // return no providers if fail to get translation file for locale
  const noProviders: Object[] = [];

  // No locale or U.S. English: no translation providers
  if (!locale || locale === 'en' || locale.startsWith('en-')) {
    return Promise.resolve(noProviders);
  }

  const translationFile = `./locale/messages.${locale}.xlf`;

  return getTranslations(translationFile)
    .then(x => {
      loadedTranslations = x;
      loadedLocale = locale;
      return x;
    })
    .then( (translations: string ) => [
      { provide: TRANSLATIONS, useValue: translations },
      { provide: TRANSLATIONS_FORMAT, useValue: 'xlf2' },
      { provide: LOCALE_ID, useValue: locale },
    ])
    .catch(() => {
      failedLocale = locale;
      if (!environment.isEnvProd) {
        console.info(`Language <${ locale }> not found! Fallback to default.`); // tslint:disable-line
      }
      return noProviders;
    }); // ignore if file not found
}

function getTranslations(file: string): Promise<string> {
  return fetch(file).then(response => {
    if (!response.ok) {
      throw Error('No translation.')
    }
    return response.text()
  })
}
