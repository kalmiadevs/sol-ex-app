/// DON'T EDIT DIRECTLY!!! ///
// Command `npm run locale` will overwrite these file.

/* tslint:disable */

export const messages: { [key: string]: string; } = {
  "mainTitle": "test {{something}}",
  "appSampleTranslation": "Sample of app translation {{ variable }}",
  "sysOops": "Oops!",
  "sysOopsBadData": "Oops! Bad data detected.",
  "sysErrorMessageGeneric": "Error message",
  "sysErrorMessageServer": "There was a server error.",
  "sysErrorUnhandledHTTP": "Unhandled HTTP error happened.",
  "sysErrorDataFixed": "Data was fixed, but if this keeps happening please send us bug report.",
  "sysErrorUnknown": "Unknown error happened.",
  "sysErrorNoResponse": "Didn't get any response, do you have internet?",
  "sysErrorTooManyRequests": "Too many requests",
  "sysErrorAccessDenied": "Access denied",
  "substepOrderAccepted": "Accepted",
  "substepOrderCuttingAndCNC": "Cutting & CNC",
  "substepOrderPainting": "Painting",
  "substepOrderAssembly": "Assembly",
  "substepOrderReadyForDispatch": "Ready for Dispatch",
  "substepOrderDefault": "Order",
  "stepOrderCancelled": "Cancelled",
  "stepOrderOffer": "Offer",
  "stepOrderOrder": "Order",
  "stepOrderCompleted": "Completed",
  "orderFailedWarning": "Order processing failed. Please contact administrator.",
  "orderConfirmModalTitle": "Confirm order {{ orderNumber }}",
  "orderConfirmModalContent": "Are you sure you want to confirm this order?",
  "orderConfirmModalButtonText": "Confirm",
  "orderConfirmNotificationSuccess": "Confirmed.",
  "orderCancelModalTitle": "Cancel order {{ orderNumber }}",
  "orderCancelModalContent": "Are you sure you want to cancel this order?",
  "orderCancelModalButtonText": "Cancel order",
  "orderCancelNotificationSuccess": "Cancelled.",
  "orderTransferNotificationTitle": "Transfer",
  "orderTransferNotificationSuccess": "Order has been transferred.",
  "orderTransferBulkNotificationSuccess": "Selected public orders have been transferred to the selected user.",
  "orderTransferNotificationError": "Failed to transfer order.",
  "searchExportNotificationTitle": "Export search results",
  "searchExportNotificationSuccess": "Results have been successfully exported. Your download should start soon.",
  "orderSaveNotificationSuccessTitle": "Order {{ orderNumber }} saved",
  "orderSaveNotificationSuccessContent": "It may take up to 5 minutes to calculate the cost and create all the documents.",
  "orderSaveNotificationConflictTitle": "Error saving order {{ orderNumber }}",
  "orderSaveNotificationConflictContent": "Documents and price calculation is currently in progress, please try again later.",
  "orderNavigateWarningTitle": "Confirm navigation",
  "orderNavigateWarningContent": "Are you sure you wish to navigate away from this page? All unsaved changes will be lost!",
  "orderNavigateWarningButtonText": "Continue",
  "articleSaveNotificationSuccessTitle": "News article saved",
  "articleSaveNotificationSuccessContent": "The article has been successfully added.",
  "articleSaveNotificationErrorTitle": "Error saving article",
  "articleSaveNotificationErrorContent": "Could not save news article, please try again.",
  "articleDeleteNotificationSuccessTitle": "News article removed",
  "articleDeleteNotificationSuccessContent": "The article has been successfully removed.",
  "articleDeleteNotificationErrorTitle": "Error removing article",
  "articleDeleteNotificationErrorContent": "Could not remove news article, please try again.",
  "orderPublicSaveNotificationSuccessTitle": "Order saved",
  "orderPublicSaveNotificationSuccessContent": "Your order has been successfully saved.",
  "orderBtnLabelCreate": "Create",
  "orderBtnLabelSave": "Save",
  "orderLoaderLine1": "Saving order...",
  "orderLoaderLine2": "Creating documents...",
  "orderLoaderLine3": "Calculating prices...",
  "orderLoaderLine4": "Syncing to SharePoint...",
  "kbBarLinkLabelProfile": "Profile",
  "kbBarLinkLabelAcount": "Account",
  "kbBarLinkLabelLogs": "Logs",
  "userBarLinkLabelProfile": "Profile",
  "userBarLinkLabelAcount": "Account",
  "entityActionCancel": "Cancel",
  "entityActionRemove": "Remove",
  "entityActionCreate": "Create",
  "entityActionSave": "Save",
  "validationFeedbackRequired": "Required",
  "validationFeedbackCreditCard": "Invalid credit card number",
  "validationFeedbackEmail": "Invalid email address",
  "validationFeedbackPassword": "Invalid password (must be at least 6 characters long and contain a number)",
  "validationFeedbackMinLength": "Minimum length is {{ length }}",
  "validationFeedbackMin": "Minimum value is {{ value }}",
  "validationFeedbackMax": "Maximum value is {{ value }}",
  "accessoryBetaName": "Beta Kit / 8 spot light + driver",
  "accessoryLedStrip": "LED Light RGB tape, IP 68",
  "accessoryAdditionalRCName": "Additional remote control",
  "accessoryHeaterName": "Heater Heliosa infra IPX5 1.5kW + controller - White",
  "accessoryHeaterAnthraciteName": "Heater Heliosa infra IPX5 1.5kW + controller - Anthracite",
  "accessoryBluetoothSystemName": "Bluetooth system",
  "accessorySpeakersName": "Speakers",
  "accessoryRCAudioSystemName": "Remote control audio system",
  "accessoryWindSensorName": "Wind Sensor",
  "accessoryRainSensorName": "Rain Sensor",
  "accessoryTemperatureSensorName": "Temperature Sensor",
  "accessoryPresenceSensorName": "Presence Sensor",
  "accessoryExternalAntennaName": "External antenna",
  "accessorySomfyMotorName": "Somfy Motor for screen ZIP 230/20 Nm - Altus 50 RTS receiver",
  "accessorySomfyRTST1TBName": "Somfy RTS remote control Telis 1 / Teleco Bridge",
  "accessorySomfyRTST4TBName": "Somfy RTS remote control Telis 4 / Teleco Bridge",
  "accessorySomfyRTST16TBName": "Somfy RTS remote control Telis 16 / Teleco Bridge",
  "accessorySomfyBridge1Name": "Teleco Bridge 1",
  "accessorySomfyBridge7Name": "Teleco Bridge 7",
  "accessorySomfyRTST1Name": "Somfy RTS remote control Telis 1",
  "accessorySomfyRTST4Name": "Somfy RTS remote control Telis 4",
  "accessorySomfyRTST16Name": "Somfy RTS remote control Telis 16",
  "accessoryDoubleMotorName": "Additional Linak motor + Mechanism",
  "accessoryTransportBox1Name": "Transport wood box up to 4,1m",
  "accessoryTransportBox2Name": "Transport wood box up to 6,1m",
  "accessoryTransportBox3Name": "Transport wood box up to 7,1m",
  "accessorySparePartsKitSLName": "Spare part kit SL Pergola",
  "accessoryMerbenitGlueName": "Merbenit Glue",
  "accessoryButylTapeRollName": "Butyl Tape Roll (22,5m)",
  "mediaEmailSendNotificationSuccessTitle": "Email sent",
  "mediaEmailSendNotificationSuccessContent": "Your email message has been sent",
  "mediaEmailSendNotificationFailTitle": "Error",
  "mediaEmailSendNotificationFailContent": "An error occured while trying to send the email, please try again later or contact support if this continues to happen",
  "profileImageWrongSizeTitle": "Invalid image",
  "profileImageWrongSizeContent": "Image size must be equal to {{ width }} x {{ height }} pixels.",
  "externalServiceErrorTitle": "An error occured",
  "externalServiceErrorContent": "There was an error while trying to redirect you to your content. Please try again."
};
