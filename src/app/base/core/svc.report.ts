import { environment } from '../../../environments/environment';
import * as Raven from 'raven-js';
import { isString, map } from 'lodash';
import { ErrorHandler } from '@angular/core';

if (environment.production && environment.raven) {
  Raven
    .config(environment.raven, {
      release: `${environment.release}`,
      environment: environment.env,
      autoBreadcrumbs: {
        'xhr'     : true,   // XMLHttpRequest
        'console' : false,  // console logging
        'dom'     : false,  // DOM interactions, i.e. clicks/typing
        'location': true    // url changes, including pushState/popState
      }
    })
    .install();
}

export function reportError(message: string, e: any, data: any) {
  console.log('ReportError received error:', message, data);
  console.log(e);

  if (environment.production && environment.raven) {

    const rawError: any = new Error();
    rawError.message      = isString(e.message) ? e.message : `Unknown message: ${ e.message }`;
    rawError.stack        = e.stack;
    rawError.extraData    = data;
    rawError.extraMessage = message;

    try {
      Raven.captureException(rawError);
    } catch (re) {
      console.log('Raven failed to report error. RE Error:');
      console.log(re);
    }
  }
}

export class BaseErrorHandler implements ErrorHandler {
  handleError(err: any): void {
    console.log('ErrorHandler received error:');
    console.log(err);

    if (true || environment.production && environment.raven) {
      try {
        err = err.originalError || err;

        const rawError = new Error();
        rawError.message = isString(err.message) ? err.message : `Unknown message: ${ err.message }`;
        rawError.stack   = err.stack;

        Raven.captureException(rawError);
      } catch (re) {
        console.log('Raven failed to report exception. RE Error:');
        console.log(re);
      }
    }
  }
}
