import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { AuthService, SignInData } from './auth/svc.auth';
import { BaseNotificationService } from '../app/notifications/svc.notification';
import { ModalLoginComponent } from '../app/notifications/modal/modal-login/cmp.modal-login';
import { BaseGroupService } from '../entity/group/svc.group';
import { BaseUserService } from '../entity/user/svc.user';
import { StoreService } from './svc.store';
import { Entity } from '../entity/cls.entity';
import { HttpAuthService } from './http/svc.http-auth';
import { CanActivateError, HandledError, HttpError, InvalidEntityEnteredSystemError, ResolveError } from './cls.error';
import { AuthUser } from './auth/cls.auth-user';
import { Observable } from 'rxjs/Observable';
import { ResponseJson } from './http/cls.response-json';
import { BasePermissionService } from '../entity/permission/svc.permission';
import { EntityIdChange } from '../entity/itf.entity-id-change';
import { reportError } from './svc.report';
import { I18nService, _ } from './i18n/svc.i18n';

@Injectable()
export class BaseSystemService {

  private loginModal?: NgbModalRef;

  constructor(
    protected router: Router,
    protected modalService: NgbModal,
    public authService: AuthService,
    public httpAuthService: HttpAuthService,
    public notificationService: BaseNotificationService,
    public userService: BaseUserService,
    public groupService: BaseGroupService,
    public permissionService: BasePermissionService,
    public storeService: StoreService
  ) {
  }

  /**
   * Override if you are extending in order to get right 'this' instance.
   */
  protected initServices(i: BaseSystemService = this) {
    this.storeService.sys = i;

    this.authService.init(i);
    this.userService.init(i);
    this.groupService.init(i);
    this.permissionService.init(i);
  }

  onAuthExpired() {
    // Show login dialog if not already
    if (!this.loginModal) {
      this.loginModal = this.modalService.open(ModalLoginComponent, { backdrop: 'static', windowClass: 'modal-danger modal-login' });
    }
  }

  /**
   * Primarily used by group service, to dynamical update node id's.
   * @return {undefined}
   */
  public getNodeIdChangeObservable(): Observable<EntityIdChange> | undefined { return undefined; };

  askForPassword(): Observable<string> {
    throw new Error('askForPassword not implemented');
  }

  closeLoginModal() {
    if (this.loginModal) {
      this.loginModal.close();
      delete this.loginModal;
    }
  }

  /**
   * Trigger data load needed at start. If rejected it's callers job to clean any set user.
   */
  login(user: AuthUser, signInData?: SignInData): Promise<undefined> {
    this.closeLoginModal();
    this.authService.setUser(user);
    return Promise.resolve(undefined);
  }

  /**
   * Handle error.
   * errorGroupId is useful for cleaning error notifications or preventing duplicated errors.
   * @param error
   * @param errorGroupId - used to identify error from specific part of system
   */
  onError(error: any, errorGroupId?: string) {
    switch (error.constructor) {
      case CanActivateError:
      case ResolveError:
        // TODO redirect to error page
        this.onError(error.error, errorGroupId);
        return;
      case HttpError:
      case Response:
        if (this.tryHandleHttpError(error)) {
          return;
        }
        this.notificationService.add('danger', _('sysOops'), _('sysErrorUnhandledHTTP'), error, 'HTTP-' + error.httpResponse.status);
        return;
    }

    if (error instanceof HandledError) {
      this.notificationService.add('danger', error.title, error.details, error);
    } else if (error instanceof InvalidEntityEnteredSystemError) {
      this.notificationService.add('danger', _('sysOopsBadData'), _('sysErrorDataFixed'), error, typeof error);
    } else {
      this.report('unknown error', error, error);
      this.notificationService.add('danger', _('sysOops'), _('sysErrorUnknown'), error);
    }
  }

  /**
   * Try to handle HTTP error, if it couldn't handle it will return false.
   * This is intended for handling common http errors like 500, 429, 401, ...
   * @param e
   * @return {boolean} Returns true if system handled error else false
   */
  tryHandleHttpError(e: Response | ResponseJson | HttpError): boolean {
    if (!e) {
      return false;
    }

    if (e instanceof HttpError) {
      e = e.httpResponse;
    }

    const errorCodes = {
      0: _('sysErrorNoResponse'),
      429: _('sysErrorTooManyRequests'),
      403: _('sysErrorAccessDenied'),
    };

    if (errorCodes[e.status]) {
      this.notificationService.add('danger', _('sysOops'), errorCodes[e.status], e, 'HTTP-' + e.status);
      return true;

    }
    if (e.status >= 500 && e.status < 600) {
      let msg = _('sysErrorMessageServer');
      if ((<ResponseJson>e).data && (<ResponseJson>e).data.errorMessage) {
        msg = (<ResponseJson>e).data.errorMessage;
      }

      this.notificationService.add('danger', _('sysOops'), msg, e, 'HTTP-' + e.status);
      return true;
    }

    if ((<ResponseJson>e).data && (<ResponseJson>e).data.errorMessage) {
      this.notificationService.add('danger', _('sysErrorMessageGeneric'), (<ResponseJson>e).data.errorMessage, e);
      return true;
    }

    return false;
  }

  report(message: string, e: any, data: any) {
    reportError(message, e, data);
  }

  /**
   * Remove all data.
   */
  logout() {
    this.closeLoginModal();
    this.authService.logout();

    this.userService.clean();
    this.groupService.clean();
  }

  logoutAndRedirect() {
    this.logout();
    this.router.navigate(['/user/login']);
  }

  addEntityToNotifications(e: Entity) {
    this.notificationService.addEntity(e);
  }

  addEntitiesToNotifications(e: Entity[]) {
    e.map(x => this.addEntityToNotifications(x));
  }

  hasPendingActions(): boolean {
    return false;
  }
}
