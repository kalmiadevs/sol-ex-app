import { Response } from '@angular/http';

export class ResponseJson extends Response {

  /**
   * Converted body to json.
   */
  data?: any;

  /**
   * Indicates if response was already handled or not by someone.
   */
  handled?: boolean;

  public static isResponseJson(x): x is ResponseJson {
    return !!x.data || x.handled;
  }

  /**
   * Warning! this will modify existing object!
   * @param res
   * @return {Response}
   */
  public static transform(res: Response)/*: ResponseJson*/ {
    try {
      (<ResponseJson>res).data = res.json();
    } catch (ignore) {}
    // Object.setPrototypeOf(res, ResponseJson);
    // res.constructor.prototype.ResponseJson = 'ResponseJson';
    // return res;
  }
}
