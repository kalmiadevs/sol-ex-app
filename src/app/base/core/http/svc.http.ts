import { Injectable } from '@angular/core';
import { Http, Request, RequestOptions, RequestOptionsArgs, Response, XHRBackend } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { ResponseJson } from './cls.response-json';
import { StoreService } from '../svc.store';
import { BaseNotificationService } from '../../app/notifications/svc.notification';

@Injectable()
export class HttpService extends Http {

  notificationService: BaseNotificationService;

  constructor (backend: XHRBackend, options: RequestOptions, private storeService: StoreService) {
    super(backend, options);
  }

  public request(url: string|Request, options?: RequestOptionsArgs): Observable<ResponseJson> {
    if (this.storeService.notificationService) { this.storeService.notificationService.remove('HTTP-0'); }
    return super
      .request(url, options)
      .map((res: Response) => {
        if (res.ok) {
          ResponseJson.transform(res);
          return res.json();
        }

        return Observable.throw(res);
      })
      .catch(this.handleError(this));
  }

  private handleError(self: HttpService) {
    // we have to pass HttpService's own instance here as `self`
    return (res: Response) => {
      ResponseJson.transform(res);
      return Observable.throw(res);
    };
  }
}
