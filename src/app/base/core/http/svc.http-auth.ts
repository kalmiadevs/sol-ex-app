import { Injectable } from '@angular/core';
import { RequestOptions, Headers, Http, Request, RequestOptionsArgs, Response, ResponseContentType } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { AuthService } from '../auth/svc.auth';
import { ConfigService, JwtHttp, SharedService } from 'ng2-ui-auth';
import { ResponseJson } from './cls.response-json';
import { StoreService } from '../svc.store';
import * as FileSaver from 'file-saver';

@Injectable()
export class HttpAuthService extends JwtHttp {

  constructor(
    _http: Http,
    _shared: SharedService,
    _config: ConfigService,
    private authService: AuthService,
    private storeService: StoreService
  ) {
    super(_http, _shared, _config);
  }

  public request(url: string|Request, options?: RequestOptionsArgs): Observable<ResponseJson> {
    if (this.storeService.notificationService) { this.storeService.notificationService.remove('HTTP-0'); }
    return super
      .request(url, options)
      .map((res: Response) => {
        if (options && options.responseType === ResponseContentType.Blob) {
          return res;
        }
        if (res.ok) {
          ResponseJson.transform(res);
          // if (!environment.production && ResponseJson.isResponseJson(res)) {
          //   console.log('=============================================');
          //   console.log(url + ' ' + res.status);
          //   console.log(JSON.stringify(res.data, null, 1));
          //   console.log('=============================================');
          // }

          if (res.status === 260) {
            const newToken = res.headers.get('X-Access-Token');
            if (newToken) {
              this.authService.setToken(newToken);
            }
          }
          return res;
        }
        return Observable.throw(res);
      })
      .catch(this.handleError(this));
  }

  private handleError(self: HttpAuthService) {
    // we have to pass HttpService's own instance here as `self`
    return (res: Response) => {
      ResponseJson.transform(res);

      if (res.status === 401) { // if not authenticated
        (<ResponseJson>res).handled = true;
        this.authService.sys.onAuthExpired();
      }

      return Observable.throw(res);
    };
  }

  /**
   * Created because at the time of writing there is still
   * bug in Angular 4.3 https://github.com/angular/angular/issues/18096
   * @param {"POST" | "DELETE" | "PUT"} method
   * @param {string} url
   * @param {FormData} formData
   * @param options
   * @return {Promise<{xhr: any; data: any}>}
   */
  sendMultipartFormData(method: 'POST' | 'DELETE' | 'PUT', url: string, formData: FormData, options: {
    headers?: Headers,
    onProgress?: any
  } = {}): Promise<{ xhr: any, data: any}> {
    return new Promise((resolve, reject) => {
      const xhr: XMLHttpRequest = new XMLHttpRequest();
      if (options.onProgress) {
        xhr.onprogress = options.onProgress;
      }

      xhr.onreadystatechange = () => {
        if (xhr.readyState === 4) {
          if (xhr.status === 200) {
            let data;
            try {
              data = JSON.parse(xhr.response);
            } catch (ignore) {}
            resolve({ xhr: false, data });
          } else {
            reject(xhr.response);
          }
        }
      };
      xhr.open(method, url, true);
      xhr.setRequestHeader('X-Access-Token', this.authService.getToken());

      // let browser set boundary
      // xhr.setRequestHeader('Content-Type', 'multipart/form-data');

      xhr.setRequestHeader('Accept', 'application/json');

      if (options.headers) {
        options.headers.keys().map(k => {
          xhr.setRequestHeader(k, options.headers.get(k));
        });
      }

      xhr.send(formData);
    });
  }

  downloadFile(url, fileName: string) {
    this.get(url,  { responseType: ResponseContentType.Blob })
      .map((res: Response) => res.blob())
      .subscribe(
        data => {
          const blob = new Blob([data], { type: data.type });
          FileSaver.saveAs(blob, fileName);
        },
        err => console.error(err),
        () => {}
      );
  }
}
