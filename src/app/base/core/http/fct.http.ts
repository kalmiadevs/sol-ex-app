import { HttpService } from './svc.http';
import { Http, RequestOptions, XHRBackend } from '@angular/http';
import { AuthService } from '../auth/svc.auth';
import { HttpAuthService } from './svc.http-auth';
import { ConfigService, SharedService } from 'ng2-ui-auth';
import { StoreService } from '../svc.store';

export function HttpServiceFactory(backend: XHRBackend, options: RequestOptions, storeService: StoreService) {
  return new HttpService(backend, options, storeService);
}

// export function HttpAuthServiceFactory(backend: XHRBackend, options: RequestOptions, authService: AuthService) {
//   return new HttpAuthService(backend, options, authService);
// }

export function HttpAuthServiceFactory(_http: Http, _shared: SharedService, _config: ConfigService, authService: AuthService, storeService: StoreService) {
  return new HttpAuthService(_http, _shared, _config, authService, storeService);
}

