import { UserApiResponse } from '../../entity/user/itf.user-api-response';
export interface License {
  users: number;
  active: number;
  admins: number;
  monthlyPrice: number;
  apps: string[];
}

export interface Account {
  _id: string;
  updatedAt: Date;
  createdAt: Date;
  company: string;
  address: string;
  city: string;
  post: string;
  country: string;
  vadId: string;
  phone: string;
  email: string;
  database: string;
  dateFormat: string;
  __v: number;
  lastDBUpdate: Date;
  locked: boolean;
  active: boolean;
  language: string;
  license: License;
  payment: string;
}

export interface AuthResponse {
  token: string;
  created: Date;
  expires: number;
  user: UserApiResponse;
}
