import { Injectable } from '@angular/core';
import {CanActivate, Router} from '@angular/router';
import { AuthService } from './svc.auth';

@Injectable()
export class NoPublicGuard implements CanActivate {

  constructor(
    private router: Router,
    private authService: AuthService,
  ) {}

  public canActivate(): Promise<boolean> | boolean {
    if (!this.authService.isAuthenticated() || (this.authService.getUser() && this.authService.getUser().isPublic())) {
      this.router.navigate(['/user/login']);
      return false;
    }

    return true;
  }
}
