import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AuthService } from './svc.auth';
import { AuthGuard } from './guard.auth';

@Injectable()
export class AdminGuard implements CanActivate {

  constructor(
    private authService: AuthService,
    private authGuard: AuthGuard,
    private router: Router
  ) {}

  public canActivate(): Promise<boolean> | boolean {
    const authRes = this.authGuard.canActivate();
    if (typeof authRes === 'boolean') {
      return authRes && this.authService.getUser().isAdmin();
    } else {
      return new Promise((resolve, reject) => {
        authRes
          .then(isAuth => {
            resolve(isAuth && this.authService.getUser().isAdmin());
          })
          .catch(e => {
            reject(e);
          });
      });
    }
  }
}
