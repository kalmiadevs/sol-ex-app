import { forwardRef, Inject, Injectable } from '@angular/core';
import { CanActivate, Resolve, Router } from '@angular/router';
import { AuthService } from './svc.auth';
import { AuthUser } from './cls.auth-user';
import { StoreService } from '../svc.store';
import { BaseSystemService } from '../svc.system';
import { appConfNoDep } from '../../../conf.app-no-dep';

@Injectable()
export class AuthGuard implements CanActivate, Resolve<AuthUser>  {

  sys: BaseSystemService;

  constructor(
    private authService: AuthService,
    private router: Router,
    @Inject(forwardRef(() => StoreService)) sysStore: StoreService,
  ) {
    this.sys = sysStore.sys;
  }

  public logout() {
    this.authService.logout();
    this.router.navigate(['/user/login']);
  }

  /**
   * Check if user auth exists.
   * @return {any}
   */
  public canActivate() {
    if (this.authService.isAuthenticated()) {

      // we might not have user data loaded
      if (this.authService.getUser()) {
        return true;
      } else  {
        return new Promise<boolean>((resolve, reject) => {
          this.authService.loadUser()
            .then(u => {
              resolve(true);
            })
            .catch(e => {
              resolve(false);
            });
        });
      }

    } else {
      this.logout();
      return false;
    }
  }


  resolve(): Promise<AuthUser> | AuthUser {
    const u = this.authService.getUser();
    if (u && (!appConfNoDep.userCertEnabled || u.secretKey))  {
      return u;
    }

    return new Promise<AuthUser>((resolve, reject) => {

      this.sys.askForPassword()
        .subscribe((password) => {
          if (u.decryptSecretKey(password)) {
            resolve(u);
          } else {
            reject();
          }
        }, () => {
          reject();
        });
    });
  }

}
