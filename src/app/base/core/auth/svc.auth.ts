import { Injectable } from '@angular/core';
import { HttpService } from '../http/svc.http';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import { AuthService as Ng2UiAuthService } from 'ng2-ui-auth';
import { Observable } from 'rxjs/Observable';
import { Response } from '@angular/http';
import { BaseSystemService } from '../svc.system';
import { User } from '../../entity/user/cls.user';
import { AuthResponse } from './itf.auth-response';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { environment } from '../../../../environments/environment';
import { AuthUser } from './cls.auth-user';
import { SecretKey } from '../crypt/cls.crypt';
import { ResponseJson } from '../http/cls.response-json';
import { HttpError } from '../cls.error';
import { Entities } from '../../entity/itf.entities';
import { Permission } from '../../entity/permission/cls.permission';
import { removeCookie, setCookie } from '../../shared/tools/cookie';
import { EntityApplyRemoteResponse } from '../../entity/itf.entity-remote-response';

export interface SignInData {
  email: string;
  password: string;
}

export interface SignUpData {
  name: string;
  email: string;
  company: string;
  password: string;
  address: string;
  city: string;
  post: string;
  country: string;
  vadId: string;
}

export interface ForgotData {
  email: string;
}

export interface ResetPasswordData {
  newPassword: string;
}

@Injectable()
export class AuthService {

  public sys: BaseSystemService;

  protected _user: BehaviorSubject<AuthUser | undefined> = new BehaviorSubject(undefined);
  public user: Observable<AuthUser | undefined>          = this._user.asObservable(); // .map(x => x.withoutSecretKey());

  // Currently not needed. AuthService should have latest user data.
  // /**
  //  * Watch user service for any change on our user so we can notify _user.
  //  */
  // protected userSubscription: Subscription;

  /**
   * Used when user token expires & @user is not set
   * So login popup
   */
  public coldUserInstance?: User;

  constructor(
    private ng2UiAuthService: Ng2UiAuthService,
    private httpService: HttpService
  ) { }

  public init(sys: BaseSystemService) {
    this.sys = sys;
  }

  clean() {

  }

  /**
   * Can be used for getting email of token owner in case if user token expired.
   * For all other cases you shouldn't call this.
   * @return {any}
   */
  getTokenEmailFromMemory(): string {
    const tokenData = this.getJwtPayload();
    // const uid = tokenData.uid;
    // if (this.coldUserInstance && this.coldUserInstance.id === uid) {
    //   return this.coldUserInstance.email;
    // }
    return tokenData.sub;
  }

  saveUser(user): Promise<EntityApplyRemoteResponse<User>>  {
    return this.sys.userService.save(user)
      .then(x => {
        this.setUser(user);
        return x;
      })
  }

  setUser(user: AuthUser | undefined) {
    // // first unsubscribe from user service
    // if (this.userSubscription) {
    //   this.userSubscription.unsubscribe();
    // }

    // update user service
    if (user) {

      this.sys.userService.addEntity(user.toUser());

      // notify our user instance
      this._user.next(user);

      // // subscribe back to user service
      // this.userSubscription = this.sys.userService.getEntityObservable(user.id)
      //   .subscribe(x => {
      //     if (!x) {
      //       // this._user.next(x);
      //     } else {
      //       this.sys.onError('There was error in stored user data. Please re-login.');
      //     }
      //   });
    } else {
      this._user.next(undefined);
    }
  }

  getUser(): AuthUser {
    return this._user.getValue();
  }

  getSecretKey(): SecretKey {
    const u = this._user.getValue();
    if (!u) {
      throw new Error('Can\'t get secret key. NOT LOGGED IN');
    }
    return u.secretKey;
  }

  public getJwtPayload(): {
    exp: number;
    acc: string;
    sub: string;
    uid: string;
    db: string;
    per: Entities<number>;
    grp: string;
    iss: string;
    meta: any;
  } {
    return this.ng2UiAuthService.getPayload();
  }

  /**
   * Get permissions for current token.
   * @return {Entities<Permission>}
   */
  public getPermissions(): Entities<Permission> {
    return Permission.createFromToken((this.getJwtPayload() || { per: {} }).per || {});
  }

  /**
   * Load user based on JWT payload if exists.
   * @return {Promise<T>}
   */
  public loadUser(): Promise<User> {
    return new Promise((resolve, reject) => {
      const tokenData = this.getJwtPayload();
      const aid = tokenData.acc;
      const uid = tokenData.uid;

      this.sys.httpAuthService.get(`${ environment.apiBaseIDMS }/users/${ aid }/${ uid }`)
        .subscribe((x: ResponseJson) => {
          let user;

          if (!<any>x.data) {
            return reject('Bad data');
          }
          const resUser = (<any>x.data);

          try {
            user = AuthUser.createFromApi(resUser);
          } catch (e) {
            // TODO can handle better?
            console.error(e);

            this.coldUserInstance = User.createFromApi(resUser);

            this.sys.onAuthExpired();
            return reject(e);
          }
          user.permissions = this.getPermissions();
          user.saveToSessionStorage();
          this.sys.login(user)
            .then(() => {
              resolve(user);
            })
            .catch(e => {
              this.sys.onAuthExpired(); // FIXME  if sessionStorage.setItem('KalPass-vault', 'dfdf'); than guard is prematurely resolved
              reject(e);
            });
        }, error => {
          reject(new HttpError(error));
        });
    });
  }


  /**
   * Is user logged-in.
   * @return {boolean}
   */
  public isAuthenticated(): boolean {
    return this.ng2UiAuthService.isAuthenticated();
  }

  /**
   * Return stored token.
   * @return {string}
   */
  public getToken(): string {
    return this.ng2UiAuthService.getToken();
  }

  /**
   * @param data {Object}
   */
  public setToken(token: string | Response) {
    this.ng2UiAuthService.setToken(token);
  }

  public logout() {
    sessionStorage.removeItem(AuthUser.sessionStorageKey);
    this.coldUserInstance = undefined;
    this.ng2UiAuthService.logout();
    this.ng2UiAuthService.removeToken();

    removeCookie('locale');

    // todo trigger sys clean
    this.setUser(undefined);
  }

  /**
   * Try to sign in.
   * First part is observable so it can be canceled, second is promise (handles after login to-do logic).
   * @param data
   * @returns {Observable<R>}
   */
  public signIn(data: SignInData): Observable<Promise<{} | undefined>> {
    return this.ng2UiAuthService.login(data)
      .map(res => {
        const authRes: AuthResponse = res.json();
        let user;
        try {
          user = AuthUser.createFromApi(authRes.user, data.password);
        } catch (e) {
          // TODO can handle better?
          console.error(e);
          this.logout();
          return Promise.reject(e);
        }

        this.setToken(res);
        user.permissions = this.getPermissions();
        user.saveToSessionStorage();

        return new Promise((resolve, reject) => {
          this.sys.login(user, data)
            .then(resolve)
            .catch(e => {
              console.error(e);
              this.logout();
              reject(e);
            });
        });
      });
  }

  public signUp(data: SignUpData): Observable<any> {
    return this.httpService.post(`${ environment.apiBaseIDMS }/account`, data);
  }

  public forgotPassword(data: ForgotData): Observable<any> {
    return this.httpService.post(`${ environment.apiBaseIDMS }/reset`, data);
  }

  public resetPasswordWithToken(token: string, data: ResetPasswordData): Observable<any> {
    return this.httpService.post(`${ environment.apiBaseIDMS }/set`, { token, ...data });
  }

  public updatePassword(newPassword: string): Observable<any> {
    const uid = this.getUser().id;
    const aid = this.getJwtPayload().acc;
    return this.httpService.put(`${ environment.apiBaseIDMS }/users/${ aid }/${ uid }`, {newPassword} );
  }

}
