import { cloneDeep as _cloneDeep, each as _each } from 'lodash';
import { UserApiResponse } from 'app/base/entity/user/itf.user-api-response';
import { Crypt, KeyPair, PublicKey, SecretKey, SymmetricKey } from '../../core/crypt/cls.crypt';
import { User } from '../../entity/user/cls.user';
import { Entities } from '../../entity/itf.entities';
import { KeyGroup } from '../crypt/cls.key-ring';
import { Permission, PermissionIsAdmin } from '../../entity/permission/cls.permission';
import { appConfNoDep } from '../../../conf.app-no-dep';


export class VaultExternalApp {

  /**
   * Fingerprint of app on that device.
   *
   * More than one external app can have same fingerprint.
   */
  fingerPrint?: string;

  /**
   * Public key of external app. We use it to encrypt data and send it via kalpass:// protocol.
   */
  publicKey: PublicKey;
}


/**
 * User instance.
 */
export class AuthUser extends User {

  static sessionStorageKey = 'AuthUser';

  public externalApps: VaultExternalApp[] = [];

  /**
   * We might not always need key.
   * If you need key add gurad to route to insure it's set.
   */
  private _secretKey?: SecretKey;

  /**
   * Only set when there is no password to decrypt it.
   * Ss let's store it in case we need it, we can request for password latter.
   */
  private cipherSecretKey?: string;

  public keyRing: Entities<KeyGroup> = {};

  public permissions: Entities<Permission> = {};

  public get secretKey(): SecretKey {
      return this._secretKey;
  }

  static createFromSessionStorage(): AuthUser | undefined {
    return this.parse(sessionStorage.getItem(this.sessionStorageKey));
  }

  static createFromApi(data: UserApiResponse, password?: string): AuthUser {
    data.id = data.id || data._id;
    const u =  User.createFromApi(data);

    if (appConfNoDep.userCertEnabled) {
      let secretKey;
      let cipherSecretKey;
      if (!password) {
        const su        = AuthUser.createFromSessionStorage();
        secretKey       = su && su.secretKey;
        u._publicKey    = su && su.publicKey;
        cipherSecretKey = data.privateKey || su && su.cipherSecretKey;
      } else {
        try {
          secretKey = Crypt.userSecretKeyDecrypt(password, data.privateKey);
        } catch (e) {
          throw new Error('INVALID PRIVATE KEY');
        }
      }

      const au = new AuthUser(u, {
        pub: u.publicKey,
        sec: secretKey,
      });
      if (!secretKey) {
        au.cipherSecretKey = cipherSecretKey;
      }

      if (!au.publicKey) {
        throw new Error('NO PUBLIC KEY');
      }

      if (secretKey && !au.keyPairValid()) {
        throw new Error('KEY PAIR IS NOT VALID');
      }

      if (data.keyRing) {
        _each(data.keyRing, (val, id) => {
          au.setKeyGroup(id, KeyGroup.createFromApi(<any>val));
        });
      }

      return au;
    } else {
      return new AuthUser(u, {
        pub: undefined,
        sec: undefined,
      });
    }
  }

  public static parse(json: string): AuthUser | undefined {
    if (json) {
      try {
        const data = JSON.parse(json);
        if ((data.secretKey || data.cipherSecretKey) && data.publicKey && data.id) {
          const secretKey =  data.secretKey && Crypt.secretKeyParse(data.secretKey);
          const publicKey =  Crypt.publicKeyParse(data.publicKey);
          const au = new AuthUser(data, {
            sec: secretKey,
            pub: publicKey
          });
          au.cipherSecretKey = data.cipherSecretKey;
          return au;
        }
      } catch (e) {
        // ignore
      }
    }
    return undefined;
  }

  static createNew(): AuthUser {
    return new AuthUser(User.createNew(), Crypt.genKeyPair());
  }

  constructor(user: User, keyPair: KeyPair) {
    super(user);
    this._secretKey = keyPair.sec;
    this._publicKey = keyPair.pub;
  }

  clone(): AuthUser {
    return _cloneDeep(this);
  }

  isAdmin() {
    return this.permissions[PermissionIsAdmin] && this.permissions[PermissionIsAdmin].read;
  }

  isPublic() {
    return this.meta && !!this.meta.public;
  }

  public getKeyGroup(id: string): KeyGroup {
    if (this.secretKey && this.keyRing[id]) {
      this.keyRing[id].decrypt(this.secretKey);
      return this.keyRing[id];
    }
    return undefined;
  }

  public setKeyGroup(id: string, keyGroup: KeyGroup) {
    this.keyRing[id] = keyGroup;
  }

  public toUser(): User {
    const x = this.clone();
    delete x._secretKey;
    return x;
  }

  public decryptSecretKey(password: string): boolean {
    this._secretKey = Crypt.userSecretKeyDecrypt(password, this.cipherSecretKey);
    return this.keyPairValid();
  }

  public saveToSessionStorage() {
    sessionStorage.setItem(AuthUser.sessionStorageKey, this.stringify());
  }

  /**
   * quick test if we have matching pair
   * @return {undefined}
   */
  public keyPairValid(): boolean {
    const randomWord = Array(32 + 1).join((Math.random().toString(36) + '00000000000000000').slice(2, 18)).slice(0, 32);
    return Crypt.decrypt(<any>this.secretKey, Crypt.encrypt(this.publicKey, randomWord)) === randomWord;
  }

  stringify(): string {
    return JSON.stringify({
      id: this.id,
      secretKey: this.secretKey && Crypt.secretKeySerialize(this.secretKey),
      cipherSecretKey: this.cipherSecretKey,
      publicKey: this.publicKey && Crypt.publicKeySerialize(this.publicKey),
    });
  }

  /**
   * Decrypt node key with user secret key.
   * @param cipherKey   Node key encrypted wit user public key.
   * @returns {string}
   */
  decryptNodeKey(cipherKey: string): SymmetricKey {
    const base64key = Crypt.decrypt(this.secretKey, cipherKey);
    return Crypt.symmetricKeyParse(base64key);
  }

  getCipherSecretKey(password: string): string {
    return Crypt.secretKeyEncrypt(password, this.secretKey);
  }

  /**
   * @todo
   */
  initOneClick() {
    // TODO One click lunch test demo.
    // (can remove)
    const x = new VaultExternalApp();
    x.publicKey = Crypt.publicKeyParse('6U7WYdbzYIEp5155jAwkskyoxjyWzhgYaN6Q5KmlSrTwIQw5cX0eH1prZqDc2WkuEMW463nicerDFPBiUQYDXg==');
    this.externalApps.push(x);
    // END
  }

  getExternalAppPubKeys(): PublicKey[] {
    throw Error('Not implemented');
  }
}
