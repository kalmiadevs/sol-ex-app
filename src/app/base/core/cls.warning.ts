import { Group } from '../entity/group/cls.group';

export class AppWarning {
  public timestamp = + new Date();

  constructor() {}
}

export class AmbiguousStateWarning extends AppWarning {
  public timestamp = + new Date();

  constructor(
    public originalGroup: Group
  ) {
    super();
  }
}

export class SimpleWarning extends AppWarning {
  public timestamp = + new Date();

  constructor(
    public msg: string
  ) {
    super();
  }
}
