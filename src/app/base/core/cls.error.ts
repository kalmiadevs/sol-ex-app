import { Response } from '@angular/http';
import { BaseNode } from '../entity/node/cls.node';
import { ResponseJson } from './http/cls.response-json';
import { Entity } from 'app/base/entity/cls.entity';

export enum KnownErrors {
  InvalidKey,
  DecryptError,
}

export class AppError {
  public timestamp = + new Date();

  public preventNotification: boolean;

  isNativeError(e: any): e is NativeError {
    return e instanceof NativeError;
  }

  isHttpError(): this is HttpError {
    return this instanceof HttpError;
  }

  isNodeOverwrittenError(): this is NodeOverwrittenError {
    return this instanceof NodeOverwrittenError;
  }

  constructor() {
    // super();
  }
}

/**
 * Used when angular resolve fails. It should indicate critical error. If passed to system
 * it will redirect to error page with details (Notification is not used because we don't know
 * when we can remove it).
 */
export class ResolveError extends AppError {
  constructor(
    public error: AppError,
    public serviceId: string
  ) {
    super();
  }
}


/**
 * Used when angular canActivate fails. It should indicate critical error. If passed to system
 * it will redirect to error page with details (Notification is not used because we don't know
 * when we can remove it).
 */
export class CanActivateError extends AppError {
  constructor(
    public error: AppError,
    public serviceId: string
  ) {
    super();
  }
}

/**
 * Can happen in lot of scenarios. It can handle basic responses.
 */
export class HttpError extends AppError {
  constructor(
    public httpResponse: ResponseJson | Response
  ) {
    super();
  }

  get message(): string {
    return this.httpResponse.statusText;
  }
}

// region Entity errors
export class InvalidEntityEnteredSystemError extends AppError {
  constructor(
    public badEntity: any,
    public serviceId: string
  ) {
    super();
  }
}

export class ResponseToEntityError extends AppError {
  constructor(
    public error: any,
    public requestId: string,
    public httpRes: ResponseJson,
    public serviceId: string
  ) {
    super();
  }

  get message(): string {
    return `Couldn't read server response.`;
  }
}

export class InvalidEntityReceivedError extends AppError {
  constructor(
    public badEntity: Entity,
    public requestId: string,
    public httpRes: ResponseJson,
    public serviceId: string
  ) {
    super();
  }

  get message(): string {
    return `Server returned bad response.`;
  }
}

export class InvalidEntitiesReceivedError extends AppError {
  constructor(
    public badEntity: Entity,
    public httpRes: ResponseJson,
    public serviceId: string
  ) {
    super();
  }

  get message(): string {
    return `Couldn't read server response.`;
  }
}
// endregion

/**
 * Wrapper for Error
 */
export class NativeError extends AppError {
  constructor(
    public error: any
  ) {
    super();
    // tslint:disable-next-line
    console.error(error);
  }
}

/**
 * Unknown error type it's not instance of Error.
 */
export class UnknownError extends AppError {
  constructor(
    public error: any
  ) {
    super();
    // tslint:disable-next-line
    console.error(error);
  }
}

// TODO check and test bottom errors:

export class InvalidKeyError extends AppError {
  constructor(
    public error: any
  ) {
    super();
  }
}

export class KnownError extends AppError {
  constructor(
    public type: KnownErrors,
    public error: any,
    public debug: any,
    public message: any
  ) {
    super();
  }
}

export class HandledError extends AppError {
  constructor(
    public title: string,
    public details: string,
    public debug: any
  ) {
    super();
  }
}

export class IncompleteServerDataError extends AppError {
  constructor(
    public debug: any
  ) {
    super();
  }
}


/**
 * Sjcl random is not ready
 */
export class SjclRandomError extends AppError {
  constructor() {
    super();
  }
}

export class NodeOverwrittenError extends AppError {
  constructor(
    public oldNode: BaseNode
  ) {
    super();
  }
}
