import { Crypt } from './cls.crypt';

describe('Crypt', () => {
  it('gen key encrypt & decrypt node with it', () => {
    const content = 'Test123';
    const key = Crypt.genSymmetricKey();
    const cipher = <any>Crypt.nodeEncrypt(key, content);

    expect(Crypt.nodeDecrypt(key, cipher)).toEqual(content);
  });

  it('full node exchange complete test', () => {
    const node = '{fields:[{label:"username",value:"tilen"},{label:"password",value:"tomakic"}]}';
    const nodeKey = Crypt.genSymmetricKey();
    const nodeKeyTxt = Crypt.symmetricKeySerialize(nodeKey);
    const cipherNode = <any>Crypt.nodeEncrypt(nodeKey, node);

    const otherUser = Crypt.genKeyPair();
    const cipherNodeKeyForOtherUser = Crypt.encrypt(otherUser.pub, nodeKeyTxt);

    const userCipherSecTxt = Crypt.secretKeySerialize(otherUser.sec);
    const userCipherSec = Crypt.userSecretKeyEncrypt('kalmia', otherUser.sec);
    const userDecSec = Crypt.userSecretKeyDecrypt('kalmia', userCipherSec);
    expect(userCipherSecTxt).toEqual(userDecSec as any);

    const txtNodeKeyForOtherUser = Crypt.decrypt(userDecSec, cipherNodeKeyForOtherUser);
    const nodeKeyForOtherUser = Crypt.symmetricKeyParse(txtNodeKeyForOtherUser);
    expect(nodeKey).toEqual(nodeKeyForOtherUser);

    const decNode = Crypt.nodeDecrypt(nodeKeyForOtherUser, cipherNode);
    expect(node).toEqual(decNode);
  });

  it('Symmetric key serialize & parse', () => {
    const key = Crypt.genSymmetricKey();
    const text = Crypt.symmetricKeySerialize(key);
    const parsedKey = Crypt.symmetricKeyParse(text);
    expect(parsedKey).toEqual(key);
  });

  it('Private key serialize & parse', () => {
    const key = Crypt.genKeyPair().sec;
    const bin = Crypt.secretKeySerialize(key);
    const parsedKey = Crypt.secretKeyParse(bin).get();
    expect(parsedKey).toEqual(key.get());
  });

  it('Public key serialize & parse', () => {
    const key = Crypt.genKeyPair().pub;
    const text = Crypt.publicKeySerialize(key);
    const parsedKey = Crypt.publicKeyParse(text).get();
    expect(parsedKey.x).toEqual(key.get().x);
    expect(parsedKey.y).toEqual(key.get().y);
  });
});
