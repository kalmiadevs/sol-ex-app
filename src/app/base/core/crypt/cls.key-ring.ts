import { Crypt, PublicKey, SecretKey, SymmetricKey } from '../../core/crypt/cls.crypt';
import { Entities } from '../../entity/itf.entities';
import { mapValues as _mapValues } from 'lodash';
import { KnownError, KnownErrors } from 'app/base/core/cls.error';

/**
 * Idea here is that we decrypt keys on the fly.
 * So when we set tru key we remove cipher one since we don't need it.
 */
export class KeyGroup {
  cipherSecretKey?: string;
  base64PublicKey?: string;
  cipherSymmetricKey?: string;

  private _secretKey?:    SecretKey;
  private _publicKey?:    PublicKey;
  private _symmetricKey?: SymmetricKey;

  get secretKey() {
    return this._secretKey;
  }

  get publicKey() {
    return this._publicKey;
  }

  get symmetricKey() {
    return this._symmetricKey;
  }

  set secretKey(x: SecretKey) {
    delete this.cipherSecretKey;
    this._secretKey = x;
  }

  set publicKey(x: PublicKey) {
    delete this.base64PublicKey;
    this._publicKey = x;
  }

  set symmetricKey(x: SymmetricKey) {
    delete this.cipherSymmetricKey;
    this._symmetricKey = x;
  }

  public static createNew(): KeyGroup {
    const x        = new KeyGroup();
    const keyPair  = Crypt.genKeyPair();
    x.secretKey    = keyPair.sec;
    x.publicKey    = keyPair.pub;
    x.symmetricKey = Crypt.genSymmetricKey();
    return x;
  }

  public static createFromApi(data: {sec: string, pub: string, sym: string}): KeyGroup {
    const x        = new KeyGroup();
    x.cipherSecretKey    = data.sec;
    x.base64PublicKey    = data.pub;
    x.cipherSymmetricKey = data.sym;
    return x;
  }

  public decrypt(decryptKey: SecretKey) {
    if (!this.secretKey && this.cipherSecretKey) {
      this.secretKey = Crypt.vaultSecretKeyDecrypt(decryptKey, this.cipherSecretKey);
    }

    if (!this.publicKey && this.base64PublicKey) {
      this.publicKey = Crypt.publicKeyParse(this.base64PublicKey);
    }

    if (!this.symmetricKey && this.cipherSymmetricKey) {
      try {
        if (this.cipherSymmetricKey.startsWith('{')) {
          this.symmetricKey = Crypt.symmetricKeyDecrypt(decryptKey, this.cipherSymmetricKey);
        } else { // TODO FIXME just TMP so we can rescue data from devlopment instance WILL BE REMOVED ON 2.07.2017
          this.symmetricKey = Crypt.symmetricKeyParse(this.cipherSymmetricKey);
        }
      } catch (e) {
        throw new KnownError(KnownErrors.DecryptError, e, 'KeyGroup > decrypt > symmetricKey', 'Can\' decrypt symmetric key.');
      }
    }
  }

  public encrypt(secretKey: SecretKey): {
    sym?: string,
    pub?: string,
    sec?: string,
  } {
    // TODO not very efficient algo
    this.decrypt(secretKey);

    return {
      sym: this.symmetricKey && Crypt.symmetricKeyEncrypt(secretKey, this.symmetricKey),
      pub: this.publicKey && Crypt.publicKeyEncrypt(secretKey, this.publicKey),
      sec: this.secretKey && Crypt.secretKeyEncrypt(secretKey, this.secretKey),
    };
  }
}

export class KeyRing {
  public static encrypt(secretKey: SecretKey, keyRing: Entities<KeyGroup>): Entities<{
    sym?: string,
    pub?: string,
    sec?: string,
  }> {
    return _mapValues(keyRing, x => {
      return x.encrypt(secretKey);
    });
  }
}
