import * as sjcl from 'sjcl';
import { SjclRandomError } from '../cls.error';

/**
 * Interface for cryptographic lib.
 *
 * Crypt lib (currently used sjcl) should be called only here. So it can be easily replaced.
 * It also provides better security overview since all is gathered here (used protocols, ...).
 */

export type SecretKey    = sjcl.SjclElGamalSecretKey;
export type PublicKey    = sjcl.SjclElGamalPublicKey;
export type SymmetricKey = sjcl.BitArray;
export interface KeyPair {
  pub: PublicKey;
  sec: SecretKey;
}

export class Crypt {

  /**
   * Method to be used for node encryption.
   * @param key
   * @param content
   * @param metadata
   * @returns {any}
   */
  public static nodeEncrypt(key: SymmetricKey, content: string, metadata?: string): string {
    return metadata ? <any>sjcl.json.encrypt(<any>key, content, <any>{ adata: metadata }) : <any>sjcl.json.encrypt(key, content);
  }

  /**
   * Method to be used for node decryption.
   * @param key
   * @param cipherContent
   * @returns {string}
   */
  public static nodeDecrypt(key: SymmetricKey, cipherContent: string): string {
    return sjcl.decrypt(key, <any>cipherContent);
  }

  /**
   * Method to be used for node decryption.
   * @param key
   * @param cipherContent
   * @returns {any}
   */
  public static nodeDecryptJson(key: SymmetricKey, cipherContent: string): any {
    return JSON.parse(Crypt.nodeDecrypt(key, cipherContent));
  }

  /**
   * Method to be used for generating hash from password field.
   * @param text
   * @returns {string}
   */
  public static nodePasswordHash(text: string): string {
    const hash = sjcl.hash.sha256.hash(text);
    return sjcl.codec.hex.fromBits(hash);
  }

  /**
   * Method to be used for encrypting user secret key.
   * @param key
   * @param content
   * @returns {any}
   */
  public static secretKeyEncrypt(key: SecretKey | string, content: SecretKey): string {
    return <any>sjcl.encrypt(<any>key, Crypt.secretKeySerialize(content));
  }

  public static publicKeyEncrypt(key: SecretKey, content: PublicKey): string {
    return <any>sjcl.encrypt(<any>key, Crypt.publicKeySerialize(content));
  }

  public static symmetricKeyEncrypt(key: SecretKey, content: SymmetricKey): string {
    return <any>sjcl.encrypt(<any>key, Crypt.symmetricKeySerialize(content));
  }

  public static symmetricKeyDecrypt(key: SecretKey, cipherContent: string): SymmetricKey {
    return Crypt.symmetricKeyParse(sjcl.decrypt(key, <any>cipherContent));
  }


  /**
   * Method to be used for encrypting node key.
   * @param key
   * @param content
   * @returns {any}
   */
  public static encrypt(key: PublicKey, content: string): string {
    return <any>sjcl.encrypt(key, content);
  }

  /**
   * Method to be used for decryption of encrypted node key.
   * @param key
   * @param cipherContent
   * @returns {string}
   */
  public static decrypt(key: SecretKey, cipherContent: string): string {
    return sjcl.decrypt(key, <any>cipherContent);
  }

  /**
   * Method to be used for decryption of encrypted node key.
   * @param key
   * @param cipherContent
   * @returns {string}
   */
  public static vaultSecretKeyDecrypt(key: SecretKey, cipherContent: string): SecretKey {
    return Crypt.secretKeyParse(Crypt.decrypt(key, cipherContent));
  }

  // /**
  //  * Method to be used for decryption of encrypted node key.
  //  * @param key
  //  * @param cipherContent
  //  * @returns {any}
  //  */
  // public static vaultKeyDecryptJson(key: PublicKey, cipherContent: string): string {
  //   return JSON.parse(this.vaultKeyDecrypt(key, cipherContent));
  // }

  /**
   * Generate new symmetric key
   * @returns {any}
   */
  public static genSymmetricKey(): SymmetricKey {
    if (!sjcl.random.isReady()) {
      throw new SjclRandomError();
    }

    const keys = sjcl.ecc.elGamal.generateKeys(256, 6);
    return keys.sec.get();
    // const privateKey: sjcl.BitArray = keys.sec.get();
    // return sjcl.codec.base64.fromBits(privateKey);
  }

  public static publicKeySerialize(pubKey: PublicKey): string {
    return sjcl.codec.base64.fromBits(pubKey.get().x.concat(pubKey.get().y));
  }

  public static secretKeySerialize(key: SecretKey): string {
    return sjcl.codec.base64.fromBits(key.get());
  }

  public static publicKeyParse(pubKey: string): PublicKey {
    return new sjcl.ecc.elGamal.publicKey(
      sjcl.ecc.curves.c256,
      sjcl.codec.base64.toBits(pubKey)
    );
  }

  public static symmetricKeySerialize(key: SymmetricKey): string {
    return sjcl.codec.base64.fromBits(key);
  }

  public static symmetricKeyParse(key: string): SymmetricKey {
    return sjcl.codec.base64.toBits(key);
  }

  public static secretKeyParse(key: string): SecretKey {
    return new sjcl.ecc.elGamal.secretKey(
      sjcl.ecc.curves.c256,
      (<any>sjcl.ecc.curves.c256).field.fromBits(sjcl.codec.base64.toBits(key))
    );
  }

  /**
   * Generate private and public key.
   * @returns {KeyPair}
   */
  public static genKeyPair(): KeyPair {
    if (!sjcl.random.isReady()) {
      throw new SjclRandomError();
    }

    return sjcl.ecc.elGamal.generateKeys(256, 6);
  }

  public static userSecretKeyEncrypt(password: string, secretKey: SecretKey): string {
    return <any>sjcl.encrypt(password, Crypt.secretKeySerialize(secretKey));
  }

  public static userSecretKeyDecrypt(password: string, cipherContent: string): SecretKey {
    return Crypt.secretKeyParse(sjcl.decrypt(password, cipherContent));
  }

  public static hashPassword(password: string): string {
    const out = sjcl.hash.sha1.hash(password);
    return sjcl.codec.hex.fromBits(out);
  }


  ///      TODO: We can handle SjclRandomError by  COLLECTING RANDOM STUFF
  ///
  ///      var cont = function () {
  ///      sjcl.random.removeEventListener('seeded', cont)
  ///      // The collectors can be turned off if no more entopy is needed.
  ///      // sjcl.random.stopCollectors()
  ///
  ///      // Continue doing whatever.
  ///      sjcl.random.randomWords(...)
  ///      }
  ///
  ///      sjcl.random.addEventListener('seeded', cont)
  ///      sjcl.random.startCollectors()
  ///
  ///
}
