# KalBlocks - base
This is collection of base and shared components for KalBlocks projects like KalPass. 

It's **not standalone** app and it should be included in other project as git submodule in `src/app/base`.
