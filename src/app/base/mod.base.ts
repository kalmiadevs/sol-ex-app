import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreService } from './core/svc.store';
import { AuthService } from './core/auth/svc.auth';
import { RouterModule } from '@angular/router';
import { NotificationsModule } from './app/notifications/mod.notifications';
import { AppContainerComponent } from './app/cmp.app-container';
import { TopBarModule } from './app/top-bar/mod.top-bar';
import { Http, RequestOptions, XHRBackend } from '@angular/http';

/* Core */
import { AuthGuard } from './core/auth/guard.auth';
import { AdminGuard } from './core/auth/guard.admin';
import { HttpService } from './core/http/svc.http';
import { HttpAuthServiceFactory, HttpServiceFactory } from './core/http/fct.http';
import { HttpAuthService } from './core/http/svc.http-auth';
import { AppModule } from './app/mod.app';
import { ConfigService, SharedService } from 'ng2-ui-auth';
import { BaseSharedModule } from './shared/mod.shared';
import { I18nModule } from './core/i18n/mod.i18n';
import { EntityModule } from './entity/mod.entity';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    NotificationsModule,
    TopBarModule,
    AppModule,
    BaseSharedModule.forRoot(),
    I18nModule,
    EntityModule // not needed but let's try it since prod build is reporting error
  ],
  providers: [
    {
      provide: HttpService,
      useFactory: HttpServiceFactory,
      deps: [XHRBackend, RequestOptions, StoreService]
    },
    {
      provide: HttpAuthService,
      useFactory: HttpAuthServiceFactory,
      deps: [Http, SharedService, ConfigService, AuthService, StoreService]
      // deps: [XHRBackend, RequestOptions, AuthService]
    },
    AuthService,
    AuthGuard,
    AdminGuard,
    StoreService
  ],
  declarations: [
    AppContainerComponent
  ],
  exports: [
    AppContainerComponent,
    NotificationsModule
  ]
})
export class BaseModule { }
