import { Component, HostListener, Input } from '@angular/core';
import { Event, NavigationCancel, NavigationEnd, NavigationError, NavigationStart, Router } from '@angular/router';
import { BaseSystemService } from '../core/svc.system';
import { environment } from '../../../environments/environment';
import { appConf } from '../../conf.app';

@Component({
  selector: 'app-container',
  templateUrl: './tpl.app-container.html',
  styleUrls: ['./cmp.app-container.scss']
})
export class AppContainerComponent {

  @Input() sys: BaseSystemService;

  loading: boolean;

  footerText = appConf.footerText;

  printMode = typeof (window as any).callPhantom === 'function';

  // TODO add safeHtml to footer copyright https://gist.github.com/klihelp/4dcac910124409fa7bd20f230818c8d1#file-pipe-safehtml-ts

  /**
   *
   * @param router
   * @param sys     Main purpose is to force proper initialization of all services.
   */
  constructor(public router: Router) {
    router.events.subscribe( (event: Event) => {
      if (event instanceof NavigationStart) {
        this.loading = true;
      } else if (
        event instanceof NavigationEnd ||
        event instanceof NavigationCancel ||
        event instanceof NavigationError
      ) {
        this.loading = false;
      }

      // RoutesRecognized
    });
  }

  @HostListener('window:beforeunload')
  onLeave(): boolean {
    return !this.sys.hasPendingActions();
  }
}
