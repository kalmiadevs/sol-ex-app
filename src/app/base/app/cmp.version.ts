import { Component, OnInit } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpService } from '../core/http/svc.http';

/**
 * Empty component often used for overwriting kalBlocks outlets.
 */
@Component({
  selector: 'app-version',
  template: `
    <div class="row">
      <div class="m-auto col-md-5">
        <h2>App build info</h2>
        <hr>
        <p>
          Currently running version:<br><strong>{{version}}</strong>
        </p>
        <p>
          Release:<br><strong>{{release}}</strong>
        </p>
        <p>
          Build:<br><strong>{{commit}}</strong>
        </p>
        <p>
          Build date:<br>{{buildDate}}
        </p>
        <p>
          App ID:<br>{{appId}}
        </p>
        <p>
          IDMS Environment:<br>{{apiBaseIDMS}}
          <ng-container *ngIf="apiBaseIdmsState !== undefined">
            <span *ngIf="apiBaseIdmsState"  class="ml-3 badge badge-success">Online</span>
            <span *ngIf="!apiBaseIdmsState" class="ml-3 badge badge-danger">Offline</span>
          </ng-container>
        </p>
        <p>
          Blocks Api Environment:<br>{{apiBaseKalBlocks}}
          <ng-container *ngIf="apiBaseKalBlocksState !== undefined">
            <span *ngIf="apiBaseKalBlocksState"  class="ml-3 badge badge-success">Online</span>
            <span *ngIf="!apiBaseKalBlocksState" class="ml-3 badge badge-danger">Offline</span>
          </ng-container>
        </p>
        <p>
          Project Api Environment:<br>{{apiBaseProject}}
          <ng-container *ngIf="apiBaseProjectState !== undefined">
            <span *ngIf="apiBaseProjectState"  class="ml-3 badge badge-success">Online</span>
            <span *ngIf="!apiBaseProjectState" class="ml-3 badge badge-danger">Offline</span>
          </ng-container>
        </p>
        <hr>
      </div>
    </div>
  `,
  styles: []
})
export class VersionComponent {
  version = environment.version;
  buildDate = environment.buildDate;
  commit = environment.commit;
  release = environment.release;

  appId = environment.appId;

  apiBaseIDMS = environment.apiBaseIDMS;
  apiBaseKalBlocks = environment.apiBaseKalBlocks;
  apiBaseProject = environment['apiBaseProject'];

  apiBaseIdmsState        = undefined;
  apiBaseKalBlocksState   = undefined;
  apiBaseProjectState     = undefined;

  constructor(
    private http: HttpService
  ) {
    this.isOnline(`${this.apiBaseIDMS}/test`)
      .then(x => this.apiBaseIdmsState = x);
    this.isOnline(`${this.apiBaseKalBlocks}/test`)
      .then(x => this.apiBaseKalBlocksState = x);
    this.isOnline(`${this.apiBaseProject}/test`)
      .then(x => this.apiBaseProjectState = x);
  }

  isOnline(uri): Promise<boolean> {
    return new Promise((resolve, reject) => {
      this.http.get(uri)
        .subscribe(res => {
          resolve(true);
        }, e => {
          resolve(e.status >= 400 && e.status < 500);
        });
    });
  }
}
