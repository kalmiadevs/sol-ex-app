import { LoginHeaderComponent } from './user/login/cmp.login-header';
import { KalblocksBarComponent } from './kalblocks-bar/cmp.kalblocks-bar';
import { KalblocksBarLogoComponent } from './kalblocks-bar/kalblocks-bar-logo/cmp.kalblocks-bar-logo';

/**
 * Default config of kalBase.
 * @type {{}}
 */
export const baseAppConf = {
  enableKalBlocks: true,
  topBarHomeLink: '/blocks/',
  footerText: 'Copyright &copy; 2017 KalBlocks by Kalmia LTD',

  userAllowRegistration: true,
  userPublicProjectBar: KalblocksBarComponent,
  userAuthProjectBar: KalblocksBarComponent,
  userLoginHeader: LoginHeaderComponent,

  customLoginFields: []
};
