import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BaseProjectBarComponent } from './cmp.project-bar';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    CommonModule,
    RouterModule
  ],
  providers: [

  ],
  declarations: [
    BaseProjectBarComponent
  ],
  entryComponents: [

  ],
  exports: [
    BaseProjectBarComponent
  ]
})
export class ProjectBarModule { }
