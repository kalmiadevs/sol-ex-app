import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-base-project-bar',
  templateUrl: './tpl.project-bar.html',
  styleUrls: ['./cmp.project-bar.scss']
})
export class BaseProjectBarComponent {

  @Input() links?: {label: string, routerLink: string}[];
  @Input() homeLink = ['/'];

  constructor() { }

}
