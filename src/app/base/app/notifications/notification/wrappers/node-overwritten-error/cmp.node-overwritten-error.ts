import { Component, Input, OnInit } from '@angular/core';
import { NodeOverwrittenError } from '../../../../../core/cls.error';
import { Entity } from '../../../../../entity/cls.entity';

@Component({
  selector: 'app-node-overwritten-error',
  templateUrl: './tpl.node-overwritten-error.html',
  styles: []
})
export class NodeOverwrittenErrorComponent implements OnInit {

  @Input() node: Entity & { $error: NodeOverwrittenError };

  constructor() { }

  ngOnInit() {
  }

}
