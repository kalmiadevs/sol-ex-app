import { Component, Input, OnInit } from '@angular/core';
import { EntityNotification } from 'app/base/app/notifications/cls.notification';
import { HttpError } from '../../../../../core/cls.error';


@Component({
  selector: 'app-http-error',
  templateUrl: './tpl.http-error.html',
  styles: []
})
export class HttpErrorComponent implements OnInit {

  @Input() notification: EntityNotification & { entity: { $error: HttpError } };

  get entity() { return this.notification.entity; }
  get httpResponse() { return this.notification.entity.$error.httpResponse; }

  constructor() {
  }

  ngOnInit() {
  }

  triggerSync() {
    console.log(this);
  }
}
