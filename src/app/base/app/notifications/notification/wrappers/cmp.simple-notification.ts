import { Component, Input } from '@angular/core';
import { Notification } from '../../cls.notification';

@Component({
  selector: 'app-simple-notification',
  template: `
    <app-notification [type]="notification.type" [debug]="notification">
      {{ notification.title }}
      <p>
        {{ notification.content }}
      </p>
    </app-notification>
  `
})
export class SimpleNotificationComponent {

  @Input() notification: SimpleNotification;

}

export class SimpleNotification extends Notification {
  constructor(
    public type: string,
    public title: string,
    public content: string,
    public debug?: any,
    public _id?: string
  ) {
    super();
    this.id = _id || this.id;
  }

  public getComponent() {
    return SimpleNotificationComponent;
  }
}
