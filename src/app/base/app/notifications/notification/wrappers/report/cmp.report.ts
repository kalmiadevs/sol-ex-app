import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { EntityNotification, ErrorNotification } from '../../../cls.notification';

@Component({
  selector: 'app-report-error',
  templateUrl: './tpl.report.html',
  styles: []
})
export class ReportErrorComponent implements OnInit {

  @Input() notification: ErrorNotification | EntityNotification;
  @Output() remove: EventEmitter<ErrorNotification | EntityNotification> = new EventEmitter();

  constructor() {
  }

  ngOnInit() {
  }

}
