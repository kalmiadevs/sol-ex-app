import { Component, Input, OnInit } from '@angular/core';
import { NativeError } from '../../../../../core/cls.error';

@Component({
  selector: 'app-native-error',
  templateUrl: './tpl.native-error.html',
  styles: []
})
export class NativeErrorComponent implements OnInit {

  @Input() node: Node & { $error: NativeError };
  error: Error;

  constructor() { }

  ngOnInit() {
    this.error = this.node.$error.error;
  }

}
