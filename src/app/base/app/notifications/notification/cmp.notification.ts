import { Component, Input, OnInit } from '@angular/core';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  animations: [
    trigger('fadeIn', [
      transition(':enter', [
        style({ opacity: 0 }),
        animate(100)
      ]),
      transition(':leave', [
        animate(100, style({ opacity: 0 }))
      ])
    ]),
    trigger('slideIn', [
      transition(':enter', [
        style({ height: 0, opacity: .5  }),
        animate(100)
      ]),
      transition(':leave', [
        animate(100, style({ width: 0 }))
      ])
    ]),
    trigger('flyInOut', [
      state('in', style({transform: 'translateX(0)'})),
      transition('void => *', [
        style({transform: 'translateX(-100%)'}),
        animate(100)
      ]),
      transition('* => void', [
        animate(100, style({transform: 'translateX(100%)'}))
      ])
    ])
  ],
  selector: 'app-notification',
  templateUrl: './tpl.notification.html',
  styleUrls: ['./cmp.notification.scss']
})
export class NotificationComponent implements OnInit {

  @Input() type: string;
  @Input() title: string;
  @Input() debug?: any;
  timestamp: number;

  constructor(
    private modalService: NgbModal
  ) { }

  ngOnInit() {
    this.timestamp = + new Date();
    if (!this.type) {
      this.type = 'warning';
      this.title = 'Oops! Unknown notification.';
    }
  }

  moreInfo($event, content) {
    if ($event.ctrlKey) {
      // tslint:disable-next-line
      if (console && console.log) { console.log(this.debug); }

      this.modalService.open(content, { windowClass: 'modal-danger' });
    }
  }
}
