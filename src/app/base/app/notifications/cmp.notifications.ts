import {
  ChangeDetectionStrategy,
  Component,
  ComponentFactoryResolver,
  ComponentRef,
  forwardRef,
  Inject,
  OnInit,
  ViewChild,
  ViewContainerRef
} from '@angular/core';
import { Notification } from './cls.notification';
import { StoreService } from '../../core/svc.store';
import { Entities } from '../../entity/itf.entities';
import { without as _without } from 'lodash';
import { BaseNotificationService } from './svc.notification';

@Component({
  selector       : 'app-notifications',
  template       : `
    <div id="notifications-wrapper"><div #notificationsContainer></div></div>`,
  styles: [`
    #notifications-wrapper {
      position: fixed;
      top: 2rem;

      max-width: 600px;

      left: 0;
      right: 0;
      margin: auto;

      z-index: 1000;
    }
  `],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NotificationsComponent implements OnInit {

  @ViewChild('notificationsContainer', { read: ViewContainerRef }) childContainer: ViewContainerRef;

  notificationService: BaseNotificationService;
  notificationsRef: Entities<ComponentRef<any>>;

  constructor(@Inject(forwardRef(() => StoreService)) sysStore: StoreService,
              private componentFactoryResolver: ComponentFactoryResolver,
              private viewContainerRef: ViewContainerRef) {
    this.notificationService = sysStore.sys.notificationService;
    this.notificationsRef    = {};
  }

  ngOnInit() {
    this.notificationService.notifications.subscribe(this.onNotificationsChange.bind(this));
  }

  private onNotificationsChange(nl: Notification[]) {
    let unprocessed = Object.keys(this.notificationsRef);
    nl.map(n => {
      if (n.isVisible()) {
        if (this.notificationsRef[n.id]) {
          unprocessed = _without(unprocessed, n.id);
        } else {
          this.notificationsRef[n.id] = this.createNotificationCmp(n);
        }
      }
    });
    unprocessed.map(this.removeNotificationCmp.bind(this));
  }

  private removeNotification(id: string) {
    this.removeNotificationCmp(id);
    this.notificationService.remove(id);
  }

  private removeNotificationCmp(id: string) {
    if (this.notificationsRef[id]) {
      this.notificationsRef[id].destroy();
      delete this.notificationsRef[id];
    }
  }

  private createNotificationCmp(n: Notification): ComponentRef<any> {
    const cmp           = n.getComponent();
    const factory       = this.componentFactoryResolver.resolveComponentFactory(cmp);
    const ref           = this.childContainer.createComponent(factory);
    const instance: any = ref.instance;

    instance.notification = n;

    if (!!instance.remove) {
      instance.remove.subscribe(() => {
        this.removeNotification(n.id);
      });
    }

    // TODO just TMP we shouldn't auto hide stuff this is against kalmia style guide
    if (n.timeout !== -1) {
      setTimeout(() => {
        this.removeNotification(n.id);
      }, n.timeout || 5000);
    }

    ref.changeDetectorRef.detectChanges();
    return ref;
  }
}
