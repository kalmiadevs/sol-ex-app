import {
  ChangeDetectionStrategy,
  Component,
  ComponentFactoryResolver,
  ComponentRef,
  Input,
  OnChanges,
  OnInit,
  ViewContainerRef
} from '@angular/core';
import { Notification } from './cls.notification';
import { Entities } from '../../entity/itf.entities';
import { without as _without } from 'lodash';

/**
 * Currently not in use. Idea was to replace UI messages component, but that newer
 * happened because I had to much other work and I forgot about this.
 * @deprecated
 */
@Component({
  selector       : 'app-entity-notifications',
  template       : `
    <div></div>`,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EntityNotificationsComponent implements OnInit, OnChanges {

  @Input() notifications: Notification[];
  notificationsRef: Entities<ComponentRef<any>>;

  constructor(
              private componentFactoryResolver: ComponentFactoryResolver,
              private viewContainerRef: ViewContainerRef
  ) {
    this.notificationsRef    = {};
  }

  ngOnInit() {}

  ngOnChanges(changes: any) {
    console.log(changes.myInput.currentValue);
  }

  private onNotificationsChange(nl: Notification[]) {
    let unprocessed = Object.keys(this.notificationsRef);
    nl.map(n => {
      if (n.isVisible()) {
        if (this.notificationsRef[n.id]) {
          unprocessed = _without(unprocessed, n.id);
        } else {
          this.notificationsRef[n.id] = this.createNotificationCmp(n);
        }
      }
    });
    unprocessed.map(this.removeNotificationCmp.bind(this));
  }

  private removeNotificationCmp(id: string) {
    this.notificationsRef[id].destroy();
    delete this.notificationsRef[id];
  }

  private createNotificationCmp(n: Notification): ComponentRef<any> {
    const cmp                        = n.getComponent();
    const factory                    = this.componentFactoryResolver.resolveComponentFactory(cmp);
    const ref                        = this.viewContainerRef.createComponent(factory);
    (<any>ref.instance).notification = n;
    ref.changeDetectorRef.detectChanges();
    // this.renderComponent();
    return ref;
  }
}
