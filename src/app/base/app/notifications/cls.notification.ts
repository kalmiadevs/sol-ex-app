import { Entity } from '../../entity/cls.entity';
import { AppError } from '../../core/cls.error';
import { HttpErrorComponent } from './notification/wrappers/http-error/cmp.http-error';
import { ReportErrorComponent } from './notification/wrappers/report/cmp.report';

/**
 * Base notification class.
 */
export abstract class Notification {

  /**
   * Unique id of notification.
   */
  public id: string;

  /**
   * Number of milliseconds to show notification.
   * If -1 it won't auto hide.
   */
  public timeout?: number;

  constructor() {
    if (!this.id) {
      // Math.random should be unique because of its seeding algorithm.
      this.id = +new Date() + '-' + Math.random().toString(36).substr(2, 9);
    }
  }

  /**
   * Should be notification visible.
   * @returns {boolean}
   */
  public isVisible(): boolean {
    return true;
  }

  /**
   * Return component to be used for rendering notification.
   * Component will receive notifications as input under name 'notification'.
   *
   *  Example for SimpleNotificationComponent:
   *    @Input() notification: SimpleNotification;
   */
  public abstract getComponent(): any;

  setTimeout(milis: number): Notification {
    this.timeout = milis;
    return this;
  }
}

export class ErrorNotification extends Notification {
  constructor(
    public error: AppError,
  ) {
    super();
  }

  public getComponent() {
    return ReportErrorComponent;
  }
}

export class ErrorReportNotification extends Notification {
  constructor(
    public type: string,
    public title: string,
    public content: string,
    public debug?: any,
    public _id?: string
  ) {
    super();
    this.id = _id || this.id;
  }

  public getComponent() {
    return ReportErrorComponent;
  }
}

export class EntityNotification extends Notification {

  public static isEntityNotification(x: any): x is EntityNotification {
    return x instanceof EntityNotification;
  }

  constructor(
    public entity: Entity
  ) {
    super();
    this.id = entity.id;
  }

  public isVisible(): boolean {
    return !!this.entity.$error || !!this.entity.$warnings && !!this.entity.$warnings.length;
  }

  public getComponent() {
    if (this.entity.$error) {
      if (this.entity.$error.isHttpError()) {
        return HttpErrorComponent;
      }
    }
    return ReportErrorComponent;
  }
}
