import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotificationsComponent } from './cmp.notifications';
import { BaseNotificationService } from './svc.notification';
import { NotificationComponent } from './notification/cmp.notification';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpErrorComponent } from './notification/wrappers/http-error/cmp.http-error';
import { NodeOverwrittenErrorComponent } from './notification/wrappers/node-overwritten-error/cmp.node-overwritten-error';
import { NativeErrorComponent } from './notification/wrappers/native-error/cmp.native-error';
import { ModalLoginComponent } from './modal/modal-login/cmp.modal-login';
import { FormsModule } from '@angular/forms';
import { SimpleNotificationComponent } from './notification/wrappers/cmp.simple-notification';
import { EntityNotificationsComponent } from './cmp.entity-notifications';
import { ReportErrorComponent } from './notification/wrappers/report/cmp.report';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    NgbModule,
    FormsModule
  ],
  providers: [
    BaseNotificationService
  ],
  declarations: [
    SimpleNotificationComponent,
    NotificationsComponent,
    NotificationComponent,
    EntityNotificationsComponent,
    HttpErrorComponent,
    NodeOverwrittenErrorComponent,
    NativeErrorComponent,
    ModalLoginComponent,
    ReportErrorComponent
  ],
  entryComponents: [
    ModalLoginComponent,
    NotificationComponent,
    SimpleNotificationComponent,
    HttpErrorComponent,
    ReportErrorComponent
  ],
  exports: [
    SimpleNotificationComponent,
    NotificationsComponent,
    EntityNotificationsComponent,
    HttpErrorComponent,
    NodeOverwrittenErrorComponent,
    NativeErrorComponent,
    ModalLoginComponent,
    ReportErrorComponent
  ]
})
export class NotificationsModule { }
