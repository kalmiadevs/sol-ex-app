import { Component, forwardRef, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { User } from 'app/base/entity/user/cls.user';
import { AuthService, SignInData } from '../../../../core/auth/svc.auth';
import { BaseSystemService } from '../../../../core/svc.system';
import { StoreService } from '../../../../core/svc.store';

@Component({
  selector: 'app-modal-login',
  templateUrl: './tpl.modal-login.html',
  styleUrls: ['./cmp.modal-login.scss']
})
export class ModalLoginComponent {

  loading: boolean;
  user: User;
  signInData: SignInData;

  private sys: BaseSystemService;

  constructor(
    private router: Router,
    private authService: AuthService,
    @Inject(forwardRef(() => StoreService)) sysStore: StoreService
  ) {
    this.sys  = sysStore.sys;
    this.user = authService.getUser();

    const email = this.authService.getTokenEmailFromMemory();
    if (!email) {
      this.router.navigate(['/user/login']);
    }

    this.signInData = {
      email: email || '',
      password: ''
    };
  }

  login() {
    this.loading = true;
    this.authService.signIn(this.signInData)
      .subscribe((promise) => {
          promise
            .then(x => {
              this.router.navigate(['/']);
            })
            .catch(error => {
              this.loading = false;
            });
        },
        (error) => {
          this.loading = false;
        });
  }

  logout() {
    this.sys.logoutAndRedirect();
  }
}
