import { Injectable } from '@angular/core';
import { EntityNotification, Notification, SimpleNotification } from './index';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Entity } from '../../entity/cls.entity';


/**
 * Notification service.
 *
 * New concept because of refactoring:
 * Service must push notification. System will resolve it automatically.
 *
 * Notification guide: notification should be self aware of context and environment.
 * It should automatically remove itself when not relevant anymore.
 * User action should required only when it provides some action and not "click to close" concept.
 *
 * For example node notifications:
 * NotificationService automatically creates notifications for nodes and when error report for node
 * is not relevant anymore it's removed. Until than it should be visible to user.
 */
@Injectable()
export class BaseNotificationService {

  private _notifications: BehaviorSubject<Notification[]> = new BehaviorSubject([]);
  public notifications: Observable<Notification[]> = this._notifications.asObservable();

  private store: {
    notifications: Notification[]
  };

  constructor() {
    this.store = {
      notifications: []
    };
  }

  test() {
    this.store.notifications.push(new SimpleNotification('info', 'Title here', 'Content here.').setTimeout(-1));
    this.store.notifications.push(new SimpleNotification('success', 'Title here', 'Content here.').setTimeout(-1));
    this.store.notifications.push(new SimpleNotification('danger', 'Title here', 'Content here.').setTimeout(-1));
    this.store.notifications.push(new SimpleNotification('warning', 'Title here', 'Content here.').setTimeout(-1));
    this.notify();
  }

  clear() {
    this.store.notifications = [];
    this.notify();
  }

  notify() {
    this._notifications.next(this.store.notifications);
  }

  removeSilent(id: string) {
    this.store.notifications = this.store.notifications.filter(x => x.id !== id);
    this.notify();
  }

  remove(id: string) {
    this.removeSilent(id);
    this.notify();
  }

  push(n: Notification) {
    this.store.notifications.push(n);
    this.notify();
  }

  /**
   * @param {string} type
   * @param {string} title
   * @param {string} body
   * @param debug
   * @param {string} id
   * @return {string} id
   */
  add(type: string, title: string, body: string, debug?: any, id?: string): string {
    const n = new SimpleNotification(type, title, body, debug, id);
    this.store.notifications.push(n);
    // this.store.notifications.push(new ErrorReportNotification(type, title, body, debug, id));
    this._notifications.next(this.store.notifications);
    this.notify();
    return n.id;
  }

  addReport(type: string, title: string, body: string, debug?: any, id?: string) {
    this.store.notifications.push(new SimpleNotification(type, title, body, debug, id));
    this._notifications.next(this.store.notifications);
    this.notify();
  }

  removeAllEntities() {
    this.store.notifications = this.store.notifications.filter(x => !EntityNotification.isEntityNotification(x));
  }

  addEntity(e: Entity) {
    const en = new EntityNotification(e);
    this.removeSilent(en.id);
    this.store.notifications.push(en);
    this.notify();
  }
}
