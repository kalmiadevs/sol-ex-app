// This file contains references to other routes, modules, components that can be overwritten
// by parent app. It also serves functional purpose if parent wants do keep everything as it is in base.

export { UserRoutingModule } from '../app/user/rut.user';
