import { Component, forwardRef, Inject, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService, ForgotData, ResetPasswordData } from '../../../core/auth/svc.auth';
import { StoreService } from '../../../core/svc.store';
import { BaseSystemService } from '../../../core/svc.system';
import { HttpError } from '../../../core/cls.error';
import { ResponseJson } from '../../../core/http/cls.response-json';
import { SimpleNotification } from '../../notifications';

@Component({
  selector: 'app-user-reset-password',
  templateUrl: './tpl.user-reset-password.html',
  styleUrls: ['./cmp.user-reset-password.scss']
})
export class UserResetPasswordComponent implements OnInit {

  resetData: ResetPasswordData = {
    newPassword: '',
  };

  error: any;
  loading = false;
  sys: BaseSystemService;
  errorMsg: string;
  resetSuccessful: boolean = false;

  token: string;

  constructor(
    private authService: AuthService,
    @Inject(forwardRef(() => StoreService)) sysStore: StoreService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.sys  = sysStore.sys;
    this.sys.logout();
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.token = params['token'];
    });
  }

  reset() {
    this.loading = true;
    this.sys.notificationService.clear();

    this.authService.resetPasswordWithToken(this.token, this.resetData)
      .subscribe( (promise) => {
          this.sys.notificationService.push(new SimpleNotification('success', 'Password changed', 'Your password has been changed').setTimeout(5000));
          this.resetSuccessful = true;
        },
        (error: ResponseJson) => {
          this.loading = false;
          this.error = error;
          this.resetSuccessful = false;
          try {
            const x = <any>error.json();
            if (x.message) {
              this.errorMsg = x.message;
            }
          } catch (e) {
            // ignore
          }
          this.sys.notificationService.push(new SimpleNotification('danger', 'An error occured', this.errorMsg).setTimeout(5000));
        });
  }
}
