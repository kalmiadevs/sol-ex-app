import { Component, forwardRef, Inject } from '@angular/core';
import { StoreService } from '../../../core/svc.store';
import { BaseSystemService } from '../../../core/svc.system';
import { AuthService, SignUpData } from '../../../core/auth/svc.auth';
import { Router } from '@angular/router';
import { HttpError } from '../../../core/cls.error';
import { ResponseJson } from '../../../core/http/cls.response-json';

@Component({
  selector: 'app-registration',
  templateUrl: './tpl.user-registration.html',
  styleUrls: ['./cmp.user-registration.scss']
})
export class RegistrationComponent {
  error: any;
  loading = false;
  sys: BaseSystemService;
  errorMsg: string;
  errorCompany: boolean;
  errorUser: boolean;

  signUpData: SignUpData = {
    name: '',
    email: '',
    company: '',
    password: '',
    address: '',
    city: '',
    post: '',
    country: 'unknown',
    vadId: ''
  };

  constructor(
    private authService: AuthService,
    @Inject(forwardRef(() => StoreService)) sysStore: StoreService,
    private router: Router
  ) {
    this.sys  = sysStore.sys;
    this.sys.logout();
  }

  register() {
    this.loading = true;
    const password = this.signUpData.password;
    const email = this.signUpData.email;
    this.sys.notificationService.clear();
    this.errorCompany = false;
    this.errorUser = false;

    this.authService.signUp(this.signUpData)
      .subscribe( (res) => {
          this.login(email, password);
        },
        (error: ResponseJson) => {
          this.loading = false;
          // this.error = error;

          if (error.status === 400) {
            if (error.data.errorCode === 'company_exists') {
              this.errorCompany = true;
              this.errorMsg = 'Company name is already in use.';
              return;
            } else if (error.data.errorCode === 'email_exists') {
              this.errorUser = true;
              this.errorMsg = 'Email address is already in use.';
              return;
            }
          }
          try {
            const x = <any>error.json();
            if (x.message) {
              return this.errorMsg = x.message;
            }
          } catch (e) {
            // ignore
          }
          this.sys.onError(new HttpError(error));
        });
  }

  login(email: string, password: string) {
    this.authService.signIn({email, password})
      .subscribe( (promise) => {
          promise
            .then(() => {
              this.router.navigate(['/']);
            })
            .catch(error => {
              this.loading = false;
            });
        },
        (errorRes: Response) => {
          this.loading = false;
          this.errorMsg = 'Login error: Please go to our Login page.';
        });
  }
}
