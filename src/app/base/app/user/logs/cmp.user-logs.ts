import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../core/auth/svc.auth';
import { User } from '../../../entity/user/cls.user';

@Component({
  selector: 'app-user-logs',
  templateUrl: './tpl.user-logs.html',
  styleUrls: ['./cmp.user-logs.scss']
})
export class UserLogsComponent implements OnInit {

  user: User;
  lastActivity: any;

  constructor(
    private authService: AuthService,
  ) {

    this.lastActivity = [
      {
        type: 'load',
        time: 'YESTERDAY 15:00',
        label: 'Read gmail credential.'
      },
      {
        type: 'load',
        time: '1.3.2017 13:30',
        label: 'Read dropbox credentials.'
      },
      {
        type: 'wrongpass',
        time: '1.3.2017 13:30',
        label: 'Someone tried to enter into your account.'
      },
      {
        type: 'signin',
        time: '1.3.2017 13:30',
        label: 'Login'
      },
      {
        type: 'passrec',
        time: '1.3.2017 13:30',
        label: 'Password recover'
      },
      {
        type: 'signup',
        time: '1.3.2017 13:30',
        label: 'Signup'
      }
    ];
  }

  ngOnInit() {

  }

}
