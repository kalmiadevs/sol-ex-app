import { Component, forwardRef, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService, ForgotData } from '../../../core/auth/svc.auth';
import { StoreService } from '../../../core/svc.store';
import { BaseSystemService } from '../../../core/svc.system';
import { HttpError } from '../../../core/cls.error';
import { ResponseJson } from '../../../core/http/cls.response-json';

@Component({
  selector: 'app-user-forgot-password',
  templateUrl: './tpl.user-forgot-password.html',
  styleUrls: ['./cmp.user-forgot-password.scss']
})
export class UserForgotPasswordComponent {

  forgotData: ForgotData = {
    email: ''
  };
  error: any;
  loading = false;
  sys: BaseSystemService;
  errorMsg: string;
  resetSuccessful: boolean;

  constructor(
    private authService: AuthService,
    @Inject(forwardRef(() => StoreService)) sysStore: StoreService,
    private router: Router
  ) {
    this.sys  = sysStore.sys;
    this.sys.logout();
  }

  reset() {
    this.loading = true;
    this.sys.notificationService.clear();

    this.authService.forgotPassword(this.forgotData)
      .subscribe( (promise) => {
        this.resetSuccessful = true;
        },
        (error: ResponseJson) => {
          this.loading = false;
          this.error = error;
          this.resetSuccessful = false;
          try {
            const x = <any>error.json();
            if (x.message) {
              return this.errorMsg = x.message;
            }
          } catch (e) {
            // ignore
          }
          this.sys.onError(new HttpError(error));
        });
  }
}
