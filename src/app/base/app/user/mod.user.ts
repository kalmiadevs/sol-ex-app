import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ProfileComponent } from './profile/cmp.profile';
import { UserLoginComponent } from './login/cmp.user-login';
import { UserForgotPasswordComponent } from './forgot-password/cmp.user-forgot-password';
import { RegistrationComponent } from './registration/cmp.user-registration';
import { ValidationModule } from '../../shared/form/validation/mod.validation';
import { UserLogsComponent } from './logs/cmp.user-logs';
import { UserAccountComponent } from './account/cmp.user-account';
// import { UserRoutingModule } from '../../../conf.app';
import { LoginHeaderComponent } from './login/cmp.login-header';
import { UserRoutingModule } from './rut.user';
import { BaseSharedModule } from '../../shared/mod.shared';
import { UserResetPasswordComponent } from './reset-password/cmp.user-reset-password';

@NgModule({
  imports: [
    CommonModule,
    UserRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    ValidationModule,
    BaseSharedModule
  ],
  providers: [

  ],
  declarations: [
    ProfileComponent,
    UserLoginComponent,
    RegistrationComponent,
    UserForgotPasswordComponent,
    UserResetPasswordComponent,
    UserAccountComponent,
    UserLogsComponent,
    LoginHeaderComponent
  ],
  entryComponents: [
    LoginHeaderComponent
  ],
  exports: [
    UserRoutingModule,
    ProfileComponent,
    UserLoginComponent,
    LoginHeaderComponent
  ]
})
export class UserModule { }
