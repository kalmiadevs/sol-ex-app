import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login-header',
  template: `
    <div class="login-header-wrapper">
      <h2 class="font-weight-bold">
        Hey, log in
      </h2>
    </div>
  `,
  styleUrls: ['./cmp.user-login.scss']
})
export class LoginHeaderComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
