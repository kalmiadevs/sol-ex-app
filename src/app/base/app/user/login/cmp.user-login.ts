import { Component, ComponentFactoryResolver, ComponentRef, forwardRef, Inject, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService, SignInData } from '../../../core/auth/svc.auth';
import { StoreService } from '../../../core/svc.store';
import { BaseSystemService } from '../../../core/svc.system';
import { HttpError } from '../../../core/cls.error';
import { ResponseJson } from '../../../core/http/cls.response-json';
import { appConf } from '../../../../conf.app';
import { LoginHeaderComponent } from './cmp.login-header';
import { environment } from '../../../../../environments/environment';
import { failedLocale, loadedLocale } from '../../../core/i18n/svc.i18n-provider';
import { setCookie } from '../../../shared/tools/cookie';

@Component({
  selector: 'app-user-login',
  templateUrl: './tpl.user-login.html',
  styleUrls: ['./cmp.user-login.scss']
})
export class UserLoginComponent implements OnInit {
  @ViewChild('headerContainer', { read: ViewContainerRef }) headerContainer: ViewContainerRef;

  signInData: SignInData = {
    email: '',
    password: ''
  };

  enableRegistration = appConf.userAllowRegistration;
  customLoginFields = appConf.customLoginFields;

  error: any;
  errorMsg: string;

  loading = false;
  sys: BaseSystemService;

  constructor(
    private authService: AuthService,
    @Inject(forwardRef(() => StoreService)) sysStore: StoreService,
    private componentFactoryResolver: ComponentFactoryResolver,
    private router: Router
  ) {
    this.sys  = sysStore.sys;

    this.sys.logout();
  }

  ngOnInit() {
    this.injectHeader();
  }

  login() {
    this.loading = true;
    this.errorMsg = '';
    this.sys.notificationService.clear();

    this.authService.signIn(this.signInData)
      .subscribe( (promise) => {
          promise
            .then(() => {

              const user = this.authService.getUser();

              let ulg = user.language;
              const lgmap = {
                si: 'sl',
                che: 'ch',
                atu: 'de'
              };
              if (lgmap[ulg]) {
                ulg = lgmap[ulg];
              }
              console.log(`Setting user language to ${ ulg }`);


              if (ulg !== undefined && failedLocale !== ulg && loadedLocale !== ulg) {
                setCookie('locale', ulg);
                return  window.location.href = '/';
              }

              this.router.navigate(['/']);
            })
            .catch(error => {
              this.loading = false;
            });
        },
        (errorRes: ResponseJson) => {
          this.loading = false;
          // Note we must check for 401: we can't use tryHandleHttpError for that
          if (errorRes.status === 401) {
            this.errorMsg = 'Invalid credentials';
            return;
          }
          try {
            const x = <any>errorRes.json();
            if (x.message) {
              return this.errorMsg = x.message;
            }
          } catch (e) {
            // ignore
          }
          this.sys.onError(new HttpError(errorRes));
        });
  }

  private injectHeader() {
    // const cmp = LoginHeaderComponent;
    // const factory = this.componentFactoryResolver.resolveComponentFactory(cmp);
    // const ref     = this.headerContainer.createComponent(factory);
    // ref.changeDetectorRef.detectChanges();

    if (appConf.userLoginHeader) {
      const cmp = appConf.userLoginHeader;
      const factory = this.componentFactoryResolver.resolveComponentFactory(cmp);
      const ref     = this.headerContainer.createComponent(factory);
      ref.changeDetectorRef.detectChanges();
    }
  }
}
