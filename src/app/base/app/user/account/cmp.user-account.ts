import { Component, forwardRef, Inject, OnInit } from '@angular/core';
import { AuthService } from '../../../core/auth/svc.auth';
import { BaseSystemService } from '../../../core/svc.system';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { User } from '../../../entity/user/cls.user';
import { HttpError } from '../../../core/cls.error';
import { ResponseJson } from '../../../core/http/cls.response-json';
import { StoreService } from '../../../core/svc.store';
import { HttpAuthService } from '../../../core/http/svc.http-auth';
import { environment } from '../../../../../environments/environment';
import { Observable } from 'rxjs/Observable';
import { SimpleNotification } from '../../notifications/notification/wrappers/cmp.simple-notification';

@Component({
  selector: 'app-user-account',
  templateUrl: './tpl.user-account.html',
  styleUrls: ['./cmp.user-account.scss']
})
export class UserAccountComponent implements OnInit {

  passFormGroup: FormGroup;
  user: User;
  loading = false;
  error: any;
  sys: BaseSystemService;
  errorMsg: string;
  updateSuccessful: boolean;

  errorPassNoMatch: boolean;
  errorBadPass: boolean;
  errorWrongPass: boolean;

  constructor(
    @Inject(forwardRef(() => StoreService)) sysStore: StoreService,
    private authService: AuthService,
    public http: HttpAuthService,
    private fb: FormBuilder
  ) {
    this.sys  = sysStore.sys;
    authService.user.subscribe(x => this.user = x);
  }

  ngOnInit() {
    this.passFormGroup = this.fb.group({
      oldPass: ['', Validators.required],
      'new': this.fb.group({
        'newPass': ['', Validators.required],
        confirmPass: ['', Validators.required],
      })
    });
  }

  update() {
    this.loading = true;
    this.sys.notificationService.clear(); // TODO this is to strong for here, we should only clear our notifications

    this.errorBadPass = false;
    this.errorPassNoMatch = false;
    this.errorWrongPass = false;

    if (this.passFormGroup.value.new.newPass !== this.passFormGroup.value.new.confirmPass) {
      this.errorPassNoMatch = true;
      return;
    }

    if (this.passFormGroup.value.new.newPass.length < 5) {
      this.errorBadPass = true;
      return;
    }

    this.updatePassword()
      .subscribe( (promise) => {
          this.updateSuccessful = true;
          this.passFormGroup.reset();
          this.sys.notificationService.push(new SimpleNotification('success', 'Saved', '').setTimeout(5000));
        },
        (error: ResponseJson) => {
          this.loading = false;
          this.error = error;
          this.updateSuccessful = false;

          try {
            const x = <any>error.json();

            if (x.errCode === 'wrongPass') {
              this.errorWrongPass = true;
              return;
            }
            if (x.message) {
              return this.errorMsg = x.message;
            }
          } catch (e) {
            // ignore
            console.error(e);
          }
          this.sys.onError(new HttpError(error));
        });
  }

  public updatePassword(): Observable<any> {
    const uid = this.authService.getUser().id;
    const aid = this.authService.getJwtPayload().acc;
    return this.http.put(`${ environment.apiBaseIDMS }/users/${ aid }/${ uid }?preventEmail=true&passc=T`, {
      oldPass: this.passFormGroup.value.oldPass,
      newPassword: this.passFormGroup.value.new.newPass,
      confirmNewPassword: this.passFormGroup.value.new.confirmPass
    } );
  }

  pfgHasError(key: string): boolean {
    return this.passFormGroup.get(key).hasError('required') && this.passFormGroup.get(key).touched;
  }
}

