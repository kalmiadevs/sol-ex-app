import { NgModule } from '@angular/core';
import { Route, RouterModule, Routes } from '@angular/router';
import { UserLoginComponent } from './login/cmp.user-login';
import { ProfileComponent } from './profile/cmp.profile';
import { AuthGuard } from '../../core/auth/guard.auth';
import { KalblocksBarComponent } from '../kalblocks-bar/cmp.kalblocks-bar';
import { RegistrationComponent } from './registration/cmp.user-registration';
import { UserForgotPasswordComponent } from './forgot-password/cmp.user-forgot-password';
import { UserAccountComponent } from './account/cmp.user-account';
import { UserLogsComponent } from './logs/cmp.user-logs';
import { appConf } from '../../../conf.app';
import {NoPublicGuard} from '../../core/auth/guard.nopublic';
import { UserResetPasswordComponent } from './reset-password/cmp.user-reset-password';

export const publicRoutes = [
  {
    path: 'login', children: [
    {
      path     : '',
      component: UserLoginComponent
    },
    {
      path     : '',
      outlet   : 'project-bar',
      component: appConf.userPublicProjectBar
    }
  ]
  },

  {
    path      : 'logout',
    redirectTo: 'login',
    pathMatch : 'full'
  },

  (appConf.userAllowRegistration
    ?
      {
        path: 'registration', children: [
          {
            path     : '',
            component: RegistrationComponent
          },
          {
            path     : '',
            outlet   : 'project-bar',
            component: appConf.userPublicProjectBar
          }
        ]
      }
    :
      {
       path      : 'registration',
       redirectTo: 'login',
       pathMatch : 'full'
      }
  ),

  {
    path: 'password', children: [
    {
      path     : '',
      component: UserForgotPasswordComponent
    },
    {
      path     : '',
      outlet   : 'project-bar',
      component: appConf.userPublicProjectBar
    }
  ]
  },

  {
    path: 'reset-password/:token', children: [
    {
      path     : '',
      component: UserResetPasswordComponent
    },
    {
      path     : '',
      outlet   : 'project-bar',
      component: appConf.userPublicProjectBar
    }
  ]
  },
];

export const authRoutes: Routes = [
  {path: '', redirectTo: 'profile', pathMatch: 'full'},
  {path: '', redirectTo: 'account', pathMatch: 'full'},
  {path: '', redirectTo: 'logs', pathMatch: 'full'},

  {
    path: 'profile', children: [
    {
      path     : '',
      canActivate: [NoPublicGuard],
      component: appConf.userProfileComponent
    },
    {
      path     : '',
      outlet   : 'project-bar',
      component: appConf.userAuthProjectBar
    }
  ]
  },

  {
    path: 'account', children: [
    {
      path     : '',
      canActivate: [NoPublicGuard],
      component: UserAccountComponent
    },
    {
      path     : '',
      outlet   : 'project-bar',
      component: appConf.userAuthProjectBar
    }
  ]
  },

  {
    path: 'logs', children: [
    {
      path     : '',
      canActivate: [NoPublicGuard],
      component: UserLogsComponent
    },
    {
      path     : '',
      outlet   : 'project-bar',
      component: appConf.userAuthProjectBar
    }
  ]
  },
];

export const routes: Routes = [
  {path: 'user', children: publicRoutes},
  {path: 'user', canActivate: [AuthGuard, NoPublicGuard], children: authRoutes},
];


@NgModule({
  imports  : [RouterModule.forChild(routes)],
  exports  : [RouterModule],
  providers: []
})
export class UserRoutingModule {
}
