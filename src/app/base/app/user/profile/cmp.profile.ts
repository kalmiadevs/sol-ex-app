import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../../core/auth/svc.auth';
import { User } from '../../../entity/user/cls.user';
import { EntityState } from '../../../entity/cls.entity-state';
import { StoreService } from '../../../core/svc.store';
import { SimpleNotification } from '../../notifications/';

@Component({
  selector: 'app-profile',
  templateUrl: './tpl.profile.html',
  styleUrls: ['./cmp.profile.scss'],
})
export class ProfileComponent implements OnInit {

  userFormGroup: FormGroup;
  passFormGroup: FormGroup;
  user: User;
  lastActivity: any;

  constructor(
    private authService: AuthService,
    private store: StoreService,
    private fb: FormBuilder
  ) {
    authService.user.subscribe(x => this.user = x);
  }

  ngOnInit() {
    this.userFormGroup    = this.fb.group({
      firstName           : [ this.user.firstName, Validators.required],
      lastName            : [ this.user.lastName, Validators.required],
      email               : [ this.user.email, [ Validators.required, Validators.email ]],
    });

    this.passFormGroup         = this.fb.group({
      oldPass: ['', Validators.required],
      'new'            : this.fb.group({
        'newPass'             : ['', Validators.required],
        confirmPass           : ['', Validators.required],
      })
    });
  }

  ufgHasError(key: string): boolean {
    return this.userFormGroup.get(key).hasError('required') && this.userFormGroup.get(key).touched;
  }

  pfgHasError(key: string): boolean {
    return this.passFormGroup.get(key).hasError('required') && this.passFormGroup.get(key).touched;
  }

  onSubmit({ value, valid }: { value: User, valid: boolean }) {
    if (valid) { // note this could be done better
      this.user.firstName = value.lastName;
      this.user.lastName  = value.lastName;
      this.user.email     = value.email;
      this.user.$state = EntityState.Dirty;
      this.authService.saveUser(this.user)
        .then(x => {
          this.store.sys.notificationService.push(new SimpleNotification('success', 'Saved', '').setTimeout(5000))
        })
        .catch(e => {
          this.store.sys.onError(e);
        })
    }
  }

  onIconPick(event: any) {
    if (event.target.files && event.target.files[0]) {
      const fileReader = new FileReader();
      fileReader.onload = () => {
        this.user.avatarBase64 = fileReader.result;
      };

      fileReader.readAsDataURL(event.target.files[0]);
    }
  }
}
