import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { KalblocksBarComponent } from './cmp.kalblocks-bar';
import { ProjectBarModule } from '../project-bar/mod.project-bar';
import { KalblocksBarIconComponent } from './kalblocks-bar-icon/cmp.kalblocks-bar-icon';
import { KalblocksBarLogoComponent } from './kalblocks-bar-logo/cmp.kalblocks-bar-logo';

@NgModule({
  imports: [
    CommonModule,
    ProjectBarModule,
    RouterModule
  ],
  providers: [

  ],
  declarations: [
    KalblocksBarComponent,
    KalblocksBarIconComponent,
    KalblocksBarLogoComponent
  ],
  entryComponents: [

  ],
  exports: [
    KalblocksBarComponent,
    KalblocksBarIconComponent,
    KalblocksBarLogoComponent
  ]
})
export class KalBlocksBarModule { }
