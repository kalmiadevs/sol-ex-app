import { Component } from '@angular/core';
import { AuthService } from '../../core/auth/svc.auth';
import { I18nService, _ } from '../../core/i18n/svc.i18n';

@Component({
  selector: 'app-kalblocks-bar',
  templateUrl: './tpl.kalblocks-bar.html'
})
export class KalblocksBarComponent {

  links = [
    {label: _('kbBarLinkLabelProfile'), routerLink: ['/user/profile']},
    {label: _('kbBarLinkLabelAccount'), routerLink: ['/user/account']},
    {label: _('kbBarLinkLabelLogs'), routerLink: ['/user/logs']},
  ];

  constructor(private authService: AuthService) {

  }
}
