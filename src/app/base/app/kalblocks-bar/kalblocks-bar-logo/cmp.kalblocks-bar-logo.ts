import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-kalblocks-bar-logo',
  template: `
    <span>
      <div class="project-logo-wrapper">
        <ng-content></ng-content>
      </div>
      
      <span class="projectName">
        <span class="nameBase">{{nameBase}}</span>{{namePostFix}}
      </span>
    </span>
  `,
  styleUrls: ['./cmp.kalblocks-bar-logo.scss']
})
export class KalblocksBarLogoComponent implements OnInit {

  @Input() nameBase: string = 'Kal';
  @Input() namePostFix: string = '';

  constructor() { }

  ngOnInit() {
  }

}
