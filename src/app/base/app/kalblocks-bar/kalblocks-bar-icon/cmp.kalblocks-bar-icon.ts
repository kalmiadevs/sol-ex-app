import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-kalblocks-bar-icon',
  template: `
    <div class="project-icon-wrapper">
      <div class="project-box">
        <div class="icon">
          <ng-content></ng-content>
        </div>
      </div>
    </div>
  `,
  styleUrls: ['./cmp.kalblocks-bar-icon.scss']
})
export class KalblocksBarIconComponent implements OnInit {

  @Input() icon?: string;

  constructor() { }

  ngOnInit() {
  }

}
