import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BaseDashboardComponent } from './dashboard/cmp.dashboard';
import { AuthGuard } from '../core/auth/guard.auth';
import { KalblocksBarComponent } from './kalblocks-bar/cmp.kalblocks-bar';
import { appConf } from '../../conf.app';
import { VersionComponent } from './cmp.version';

export const routes: Routes = [
  { path: 'version', component: VersionComponent },

  ...(appConf.enableKalBlocks ? [
    { path: 'blocks', canActivate: [ AuthGuard ], children: [
      { path: '', component: BaseDashboardComponent },
      {
        path: '',
        outlet: 'project-bar',
        component: KalblocksBarComponent
      }
    ]}
  ] : [])
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class BaseAppRoutingModule { }
