import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BaseDashboardComponent } from './cmp.dashboard';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    CommonModule,
    RouterModule
  ],
  providers: [
  ],
  declarations: [
    BaseDashboardComponent
  ],
  exports: [
    BaseDashboardComponent
  ]
})
export class BaseDashboardModule { }
