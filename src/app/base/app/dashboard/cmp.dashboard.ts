import { Component, forwardRef, Inject, OnInit } from '@angular/core';
import { AuthService } from '../../core/auth/svc.auth';
import { StoreService } from '../../core/svc.store';
import { Router } from '@angular/router';
import { AuthUser } from '../../core/auth/cls.auth-user';
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'app-base-dashboard',
  templateUrl: './tpl.dashboard.html',
  styleUrls: ['./cmp.dashboard.scss']
})
export class BaseDashboardComponent implements OnInit {

  public authService: AuthService;
  public user: AuthUser;

  constructor(
    @Inject(forwardRef(() => StoreService)) sysStore: StoreService,
    private router: Router,
  ) {
    this.authService = sysStore.sys.authService;

    sysStore.sys.authService.user.subscribe((u) => {
      this.user = u;
    });
  }

  ngOnInit() {
  }

  openApp(appId) {
    if (environment.appId === appId) {
      this.router.navigate(['/']);
    } else {
      if (environment.appsLocation[appId]) {
        window.location.href = environment.appsLocation[appId];
      }
    }
  }

  get showDetails(): boolean {
    return this.user.isAdmin();
  }
}
