import { Component, forwardRef, Inject } from '@angular/core';
import { StoreService } from '../../core/svc.store';
import { BaseSystemService } from '../../core/svc.system';
import { AuthService } from '../../core/auth/svc.auth';
import { Router } from '@angular/router';
import { AuthUser } from '../../core/auth/cls.auth-user';
import { environment } from '../../../../environments/environment';
import { appConf } from '../../../conf.app';

@Component({
  selector: 'app-top-bar',
  templateUrl: './tpl.top-bar.html',
  styleUrls: ['./cmp.top-bar.scss']
})
export class TopBarComponent {

  public authService: AuthService;
  public sys: BaseSystemService;
  public user: AuthUser;
  public adminLink = environment.appsLocation.admin;

  adminLinkEnabled = appConf.enableKalBlocks;
  homeLink         = appConf.topBarHomeLink;

  constructor(
    @Inject(forwardRef(() => StoreService)) sysStore: StoreService,
    private router: Router,
  ) {
    this.authService = sysStore.sys.authService;
    this.sys = sysStore.sys;

    sysStore.sys.authService.user.subscribe((u) => {
      this.user = u;
    });
  }

  tmp_sim401() {
    this.sys.onAuthExpired();
  }

  logout() {
    this.sys.logout();
    this.router.navigate(['/user/login']);
  }
}
