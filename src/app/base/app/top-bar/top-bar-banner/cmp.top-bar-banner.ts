import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-top-bar-banner',
  templateUrl: './tpl.top-bar-banner.html',
  styleUrls: ['./cmp.top-bar-banner.scss']
})
export class TopBarBannerComponent implements OnInit {

  @Input() link: string;

  constructor() { }

  ngOnInit() {
  }

}
