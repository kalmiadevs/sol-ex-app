import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TopBarComponent } from './cmp.top-bar';
import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule } from '@angular/router';
import { TopBarBannerComponent } from './top-bar-banner/cmp.top-bar-banner';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    NgbDropdownModule
  ],
  declarations: [
    TopBarComponent,
    TopBarBannerComponent
  ],
  exports: [
    TopBarComponent
  ],
})
export class TopBarModule { }
