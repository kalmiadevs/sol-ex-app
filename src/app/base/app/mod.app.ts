import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { KalBlocksBarModule } from './kalblocks-bar/mod.kalblocks-bar';
import { UserModule } from './user/mod.user';
import { BaseAppRoutingModule } from './rut.app';
import { BaseDashboardModule } from './dashboard/mod.dashboard';
import { NoneComponent } from './cmp.none';
import { VersionComponent } from './cmp.version';
import { NoPublicGuard } from '../core/auth/guard.nopublic';

@NgModule({
  imports: [
    CommonModule,
    KalBlocksBarModule,
    UserModule,
    BaseAppRoutingModule,
    BaseDashboardModule
  ],
  providers: [
    NoPublicGuard,
  ],
  declarations: [
    NoneComponent,
    VersionComponent
  ],
  exports: [
    UserModule
  ]
})
export class AppModule { }
