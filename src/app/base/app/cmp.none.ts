import { Component, OnInit } from '@angular/core';

/**
 * Empty component often used for overwriting kalBlocks outlets.
 */
@Component({
  selector: 'app-none',
  template: ``,
  styles: []
})
export class NoneComponent {}
