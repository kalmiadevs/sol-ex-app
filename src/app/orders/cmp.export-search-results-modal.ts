import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { environment } from '../../environments/environment';
import { HttpAuthService } from '../base/core/http/svc.http-auth';
import { AuthService } from '../base/core/auth/svc.auth';

@Component({
  selector: 'app-export-search-results-modal',
  template: `
    <div class="modal-header">
      <h4 class="modal-title"><i class="fa fa-exclamation-circle"></i>&nbsp;<ng-container i18n>Export search results</ng-container></h4>
      <button type="button" class="close" aria-label="Close" (click)="activeModal.dismiss()">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
      <p i18n>What data would you like to export?</p>
      <div class="form-group">
        <button type="button" class="btn btn-secondary export-button" (click)="activeModal.close(1)">
          <i class="fa fa-download" aria-hidden="true"></i>
          <ng-container i18n>Export only current page</ng-container>
        </button>
        <button type="button" class="btn btn-secondary export-button" (click)="activeModal.close(2)">
          <i class="fa fa-download" aria-hidden="true"></i>
          <ng-container i18n>Export all pages</ng-container>
        </button>
      </div>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-secondary" (click)="activeModal.dismiss()" i18n>Close</button>
    </div>
  `,
  styles: [
    `
      .export-button {
        width: 50%;
        display: block;
        margin: 10px auto;
      }
    `
  ]
})
export class ExportSearchResultsModalComponent {

  constructor(public activeModal: NgbActiveModal) {
  }
}
