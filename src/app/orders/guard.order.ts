import { Injectable } from '@angular/core';
import { CanDeactivate } from '@angular/router';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {OrderComponent} from './order/cmp.order';
import {ActionConfirmationModalComponent} from './cmp.action-confirmation-modal';
import {I18nService} from '../base/core/i18n/svc.i18n';
import {AuthService} from '../base/core/auth/svc.auth';

@Injectable()
export class OrderGuard implements CanDeactivate<OrderComponent> {

  actionConfirmationModalDialog: any = ActionConfirmationModalComponent;

  constructor(private modalService: NgbModal,
              private authService: AuthService,
              private i18n: I18nService) {
  }

  canDeactivate(target: OrderComponent): Promise<boolean> | boolean {
    if (target.edit && !this.authService.getUser().isPublic()) {
      const modalRef = this.modalService.open(this.actionConfirmationModalDialog);
      modalRef.componentInstance.title = this.i18n.get('orderNavigateWarningTitle');
      modalRef.componentInstance.content = this.i18n.get('orderNavigateWarningContent');
      modalRef.componentInstance.confirmationButtonText = this.i18n.get('orderNavigateWarningButtonText');
      return new Promise((resolve, reject) => {
        modalRef.result.then(() => resolve(true)).catch(() => resolve(false));
      })
    } else {
      return true;
    }
  }
}
