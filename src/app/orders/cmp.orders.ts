import { Component, OnInit } from '@angular/core';
import { EntitiesComponent } from '../base/entity/cmp.entities';
import { OrderService } from './svc.order';
import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { Order } from './cls.order';
import { SystemService } from '../core/svc.system';
import { Entities } from 'app/base/entity/itf.entities';
import { debounce as _debounce, mapValues as _mapValues, reduce as _reduce } from 'lodash';
import { environment } from '../../environments/environment';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { PromiseHelper } from '../base/shared/cls.promise-helper';
import { SimpleNotification } from '../base/app/notifications/';
import { AuthService } from '../base/core/auth/svc.auth';
import { ActionConfirmationModalComponent } from './cmp.action-confirmation-modal';
import { I18nService } from '../base/core/i18n/svc.i18n';
import { Observable } from 'rxjs/Observable';
import { fromPromise } from 'rxjs/observable/fromPromise';
import * as moment from 'moment';
import { TransferOwnershipModalComponent } from './cmp.transfer-ownership-modal';
import { HttpError } from '../base/core/cls.error';
import { HttpAuthService } from '../base/core/http/svc.http-auth';
import { getFileNameFromResponseContentDisposition, saveFile } from '../cls.utils';
import { ResponseContentType } from '@angular/http';
import { ExportSearchResultsModalComponent } from './cmp.export-search-results-modal';

@Component({
  selector: 'app-orders',
  templateUrl: './tpl.orders.html',
  styleUrls: ['./cmp.orders.scss']
})
export class OrdersComponent extends EntitiesComponent<Order, OrderService> implements OnInit {

  get orders(): Entities<Order> {
    return this.entities;
  };

  canAdd = false;
  isAdmin = false;
  isSuperAdmin = false;

  searchInput: string;
  loading: boolean;

  env = environment.env;

  paginationEnabled = true;

  transferOwnershipModalDialog: any = TransferOwnershipModalComponent;
  exportSearchResultsModalDialog: any = ExportSearchResultsModalComponent;

  sortBy: string = 'birth';
  reverseSort: boolean = true;

  _aggregateStats: any;
  get aggregateStats(): any {
    return this._aggregateStats;
  }

  _allSalesManagers: string[] = [];
  get allSalesManagers(): string[] {
    return this._allSalesManagers;
  }

  set allSalesManagers(v: string[]) {
    this._allSalesManagers = v;
  }

  get products(): string[] {
    return Object.keys(this.aggregateStats.product);
  }

  actionConfirmationModalDialog: any = ActionConfirmationModalComponent;

  entitiesProvider: BehaviorSubject<Entities<Order>> = new BehaviorSubject<Entities<Order>>({});

  filterDefaultFrom = moment().toObject();
  filterDefaultTo = moment().subtract(90, 'day').toObject();

  filters = {
    number: '',
    'client.name': '',
    'project.name': '',
    'product.pergolaType': '',
    step: '',
    costVal: {
      from: <number>undefined,
      to: <number>undefined
    },
    'project.date': {
      from: <{
        day: number, month: number, year: number
      } | string>undefined,
      to: <{
        day: number, month: number, year: number
      } | string>undefined
    },
    'createdAt': {
      from: <{
        day: number, month: number, year: number
      } | string> {
        day: this.filterDefaultTo.date,
        month: this.filterDefaultTo.months + 1,
        year: this.filterDefaultTo.years
      },
      to: <{
        day: number, month: number, year: number
      } | string> {
        day: this.filterDefaultFrom.date,
        month: this.filterDefaultFrom.months + 1,
        year: this.filterDefaultFrom.years
      }
    },
    distributor: '',
    salesManager: ''
  };

  private typeaheadDistributors;
  private typeaheadSalesManagers;

  _totalEntitiesCount: 0;

  _loadDataDebounced: any;

  getFormattedDateFilterString(filterData: any) {
    return `${ filterData.day }.${ filterData.month }.${ filterData.year }`;
  }

  get totalEntitiesCount(): number {
    return this._totalEntitiesCount;
  }

  get filterStepEnabled(): boolean {
    return !!this.filters.step;
  }

  get filterAmountEnabled(): boolean {
    return !!this.filters['costVal'].from && !!this.filters['costVal'].to;
  }

  get filterDateEnabled(): boolean {
    return !!this.filters['project.date'].to && !!this.filters['project.date'].from;
  }

  get filterCreatedDateEnabled(): boolean {
    return !!this.filters['createdAt'].to && !!this.filters['createdAt'].from;
  }

  constructor(
    protected authService: AuthService,
    protected location: Location,
    protected orderService: OrderService,
    protected route: ActivatedRoute,
    protected sys: SystemService,
    protected router: Router,
    protected modalService: NgbModal,
    protected http: HttpAuthService,
    private i18n: I18nService) {
    super(location, route, router, sys, orderService, modalService);
    this.init();
  }

  subscribeToEntities() {
    super.subscribeToEntities(this.entitiesProvider.asObservable());
  }

  ngOnInit() {
    // FE load all (don't want that)
    // this.orderService.sync();

    this.loadData();
    this._loadDataDebounced = _debounce(this.loadData, 250);

    this.isAdmin = this.authService.getJwtPayload().grp === 'admin';
    this.isSuperAdmin = this.authService.getJwtPayload().grp === 'superadmin';
    // this.canAdd = !this.isSuperAdmin || environment.env === 'local';
    this.canAdd = true;

    if (this.isSuperAdmin) {
      this.loadAllSalesManagers();
    }
  }

  trySelectEntityOnCtrl(event, id) {
    // do nothing
  }

  loadData() {
    // this.displayedEntities = [];
    this.loading = true;

    const options = this.buildListFilterOptions();

    this.orderService.loadEntitiesFromRemote(options)
      .then(x => {
        this.loading = false;
        this.displayedEntities = [];

        this._totalEntitiesCount = x.data.total || 0;
        this.entitiesProvider.next(x.entities);
        this.allEntities = x.keys;
        this.displayedEntities = x.keys;

        this._aggregateStats = x.data.data.stats;
      })
      .catch(e => {
        this.displayedEntities = [];
        this.loading = false;
        this.sys.tryHandleHttpError(e);
      });
  }

  buildListFilterOptions() {
    const options: any = {
      pagination: {
        start: this.selectedPageIndex * this.pageSize,
        items: this.pageSize
      },
      aggregate: true
    };

    // console.log(this.filters.date);

    const filter: any = _reduce(this.filters, (a, v, k) => v ? {...a, [k]: v} : a, {});

    if (!this.filterDateEnabled) {
      delete filter['project.date'];
    } else {
      filter['project.date'] = {
        from: new Date(filter['project.date'].from.year, filter['project.date'].from.month - 1, filter['project.date'].from.day),
        to: new Date(filter['project.date'].to.year, filter['project.date'].to.month - 1, filter['project.date'].to.day, 23, 59)
      };
    }

    if (!this.filterCreatedDateEnabled) {
      delete filter['createdAt'];
    } else {
      filter['createdAt'] = {
        from: new Date(filter['createdAt'].from.year, filter['createdAt'].from.month - 1, filter['createdAt'].from.day),
        to: new Date(filter['createdAt'].to.year, filter['createdAt'].to.month - 1, filter['createdAt'].to.day, 23, 59)
      };
    }

    if (!this.filterAmountEnabled) {
      delete filter['costVal'];
    } else {
      filter['costVal'] = {
        from: <number>filter['costVal'].from,
        to: <number>filter['costVal'].to
      };
    }

    if (Object.keys(filter).length) {
      options.filter = filter;
    }

    if (this.sortBy) {
      options.sort = {
        by: this.sortBy,
        order: this.reverseSort && 'desc' || 'asc'
      };
    } else {
      options.sort = {
        by: 'birth',
        order: 'desc'
      };
    }

    if (this.searchInput) {
      options.search = _mapValues(this.filters, x => this.searchInput);

      delete options.search['project.date'];
      delete options.search['createdAt'];
      delete options.search.costVal;
      delete options.search.number;
    }

    return options;
  }

  onEntitiesChange(entities: Entities<Order>) {
    this.entities = entities;
    this.allEntities = Object.keys(entities);
    this.displayedEntities = this.allEntities;
  }

  setPageSize(size: number) {
    this.pageSize = size;
    this.loadData();
  }

  setSort(name: string, reverse: boolean = this.reverseSort) {
    if (name === this.sortBy && reverse === this.reverseSort) {
      // reset
      this.reverseSort = true;
      this.sortBy = 'birth';
    } else {
      this.reverseSort = reverse;
      this.sortBy = name;
    }
    this.sort();
  }

  onSelectedPageChange() {
    this.loadData();
  }

  search() {
    this._loadDataDebounced();
  }

  sort() {
    // FE sort
    // this.allEntities = _sortBy(this.allEntities, this.sortBy);
    // if (this.reverseSort) {
    //   this.allEntities = this.allEntities.reverse();
    // }

    // BE filter
    this.loadData();
  }


  filter() {
    // FE filter
    // if (this.search) {
    //   this.allEntities = Object.keys(this.entities);
    //   this.allEntities = this.allEntities.filter((id) => {
    //     const o = this.entities[id];
    //     return (o.step || '').indexOf(this.search) > -1;
    //   });
    //   this.updateDisplayedEntities();
    // }

    // BE filter
    this._loadDataDebounced();
  }

  confirmSelected() {
    const p = Array.from(this.selectedEntities).map(id => {
      return this.entityService.confirm(id)
        .then(x => {
          this.entities[id] = this.entityService.getEntity(id);
        });
    });
    PromiseHelper.allSettled(p)
      .then(x => {
        // const failed = 0;
        // const ok = 0;
        // x.forEach(n => n.error ? failed++ : ok ++);
        // this.sys.notificationService.push(new SimpleNotification('info', 'Failed 0', 'Confirmed 0.').setTimeout(5000))
      })
  }

  transferSelected() {
    const modalRef = this.modalService.open(this.transferOwnershipModalDialog);
    modalRef.result
      .then(modalRes => {
        const p = Array.from(this.selectedEntities).map(id => {
          return this.entityService.transferOwnership(id, modalRes, true)
            .then(x => {
              this.entities[id] = this.entityService.getEntity(id);
            });
        });
        PromiseHelper.allSettled(p)
          .then(x => {
            this.filter(); // refresh listing
            this.deselectAll();
            this.sys.notificationService.push(new SimpleNotification('success', this.i18n.get('orderTransferNotificationTitle'), this.i18n.get('orderTransferBulkNotificationSuccess')).setTimeout(5000));
          });
      })
      .catch(e => {
      });
  }

  exportSearchResults() {
    const modalRef = this.modalService.open(this.exportSearchResultsModalDialog);
    modalRef.result
      .then(modalRes => {
        const options = this.buildListFilterOptions();
        options.aggregate = false;

        if (modalRes === 2) {
          // Export all pages
          delete options.pagination;
        }

        this.loading = true;

        const params = this.entityService.hookProcessLoadEntitiesParams(options);

        this.http.get(`${ environment.apiBaseProject }/orders/exportSearch`, {
          search: params,
          responseType: ResponseContentType.Blob
        }).subscribe(res => {
          const fileName = getFileNameFromResponseContentDisposition(res);
          saveFile(res.blob(), fileName);

          this.loading = false;
          this.sys.notificationService.push(new SimpleNotification('success', this.i18n.get('searchExportNotificationTitle'), this.i18n.get('searchExportNotificationSuccess')).setTimeout(5000));

          }, e => {
          this.sys.onError(new HttpError(e));
          this.loading = false;
        });
      })
      .catch(e => {
      });
  }

  confirm(id: string): Promise<any> {
    const modalRef = this.modalService.open(this.actionConfirmationModalDialog);
    modalRef.componentInstance.title = this.i18n.get('orderConfirmModalTitle', [this.entities[id].number]);
    modalRef.componentInstance.content = this.i18n.get('orderConfirmModalContent');
    modalRef.componentInstance.confirmationButtonText = this.i18n.get('orderConfirmModalButtonText');
    return modalRef.result
      .then(modalRes => {
        this.loading = true;
        this.entityService.confirm(id)
          .then(x => {
            this.entities[id] = this.entityService.getEntity(id);
            this.sys.notificationService.push(new SimpleNotification('success', this.entities[id].number, this.i18n.get('orderConfirmNotificationSuccess')).setTimeout(5000));
            this.loading = false;
          })
          .catch(x => {
            this.loading = false;
          })
      })
      .catch(e => {
      });
  }

  cancel(id: string): Promise<any> {
    const modalRef = this.modalService.open(this.actionConfirmationModalDialog);
    modalRef.componentInstance.title = this.i18n.get('orderCancelModalTitle', [String(this.entities[id].number)]);
    modalRef.componentInstance.content = this.i18n.get('orderCancelModalContent');
    modalRef.componentInstance.confirmationButtonText = this.i18n.get('orderCancelModalButtonText');
    return modalRef.result
      .then(modalRes => {
        this.loading = true;
        this.entityService.cancel(id)
          .then(x => {
            this.entities[id] = this.entityService.getEntity(id);
            this.sys.notificationService.push(new SimpleNotification('success', this.entities[id].number, this.i18n.get('orderCancelNotificationSuccess')).setTimeout(5000));
            this.entitiesProvider.next(this.entities); // fix order state not updating on cancel after confirm.
            this.loading = false;
          })
          .catch(x => {
            this.loading = false;
          })
      })
      .catch(e => {
      });
  }

  genDemoOrders(n) {
    this.orderService.generateAndSaveDemoOrders(n);
    alert('Refresh page after logs in console end :)');
  }

  clearDateFilter() {
    this.filters['project.date'].to = undefined;
    this.filters['project.date'].from = undefined;
  }

  clearAmountFrom() {
    this.filters['costVal'].from = undefined;
    this.filter();
  }

  clearAmountTo() {
    this.filters['costVal'].to = undefined;
    this.filter();
  }

  clearCreatedDateFilter() {
    this.filters['createdAt'].to = undefined;
    this.filters['createdAt'].from = undefined;
  }

  public hook_filterRemoveSelected(x) {
    return x.filter(id => {
      const e = this.entities[id] || this.entityService.getEntitySoft(id);
      return e.$canCancel;
    });
  }

  suggestDistributors = (text: Observable<string>) => {
    let searchTerm;
    return text
      .debounceTime(250)
      //.distinctUntilChanged()
      .do(query => {
        searchTerm = query;
      })
      .switchMap(query => this.typeaheadDistributors || fromPromise(this.orderService.suggestDistributors()))
      .map(res => {
        if (!this.typeaheadDistributors) {
          this.typeaheadDistributors = (res['status'] && res['status'] === 200) ? res['data'].distributors : [];
        }

        return searchTerm.length < 1 ?
          this.typeaheadDistributors.filter(v => v.status === 'B2B').map(x => x.name) :
          this.typeaheadDistributors.filter(v => v.name.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1).map(x => x.name);
      })
  };

  suggestSalesManagers = (text: Observable<string>) => {
    let searchTerm;
    return text
      .debounceTime(250)
      .distinctUntilChanged()
      .do(query => {
        searchTerm = query;
      })
      .switchMap(query => this.typeaheadSalesManagers || fromPromise(this.orderService.suggestSalesManagers()))
      .map(res => {
        if (!this.typeaheadSalesManagers) {
          this.typeaheadSalesManagers = (res['status'] && res['status'] === 200) ? res['data'].salesManagers : [];
        }

        return searchTerm.length < 2 ? [] : this.typeaheadSalesManagers.filter(v => v.name.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1).map(x => x.name);
      })
  };

  private loadAllSalesManagers() {
    this.orderService.suggestSalesManagers()
      .catch()
      .then((x: any) => {
        this.allSalesManagers = (x.data.salesManagers || []).map((v) => v.name);
      })
  }

  private clearSalesManagerFilter() {
    this.filters['salesManager'] = '';
    this.filter();
  }

}
