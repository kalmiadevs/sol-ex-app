import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OrdersComponent } from './cmp.orders';
import { OrderService } from './svc.order';
import { OrderComponent } from './order/cmp.order';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule } from '@angular/router';
import { EntityModule } from '../base/entity/mod.entity';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BaseSharedModule } from '../base/shared/mod.shared';
import { ValidationModule } from '../base/shared/form/validation/mod.validation';
import { EntityInputModule } from '../base/entity/entity-input/mod.entity-input';
import { ClientInformationComponent } from './order/client-information/cmp.client-information';
import { ProductInformationComponent } from './order/product-information/cmp.product-information';
import { ProductSpecificationsComponent} from './order/product-specifications/cmp.product-specifications';
import { ProductOptionsComponent } from './order/product-options/cmp.product-options';
import { ProductOptionsZipBlindsComponent } from './order/product-options/zip-blinds/cmp.zip-blinds';
import { ProductOptionsSlidingPanelsComponent } from './order/product-options/sliding-panels/cmp.sliding-panels';
import { ProductOptionsGlassPanelsComponent } from './order/product-options/glass-panels/cmp.glass-panels';
import { ProductAccessoriesComponent } from './order/product-accessories/cmp.product-accessories';
import { OrderCommentsComponent } from './order/comments/cmp.comments';
import { OrderTermsComponent } from './order/terms/cmp.terms';
import { FileManagerModule } from '../base/entity/file-system/file-manager/mod.file-manager';
import { FilePickerModule } from '../base/entity/file-system/file-picker/mod.file-picker';
import { InlineSVGModule } from 'ng-inline-svg';
import { MediaModule } from './media/mod.media';
import { OrderProgressComponent } from './order/progress/cmp.progress';
import { AccessoryComponent } from './order/product-accessories/accessory/cmp.accessory';
import { ScrollingTextLoaderComponent } from './order/scrolling-text-loader/cmp.scrolling-text-loader';
import { ActionConfirmationModalComponent } from './cmp.action-confirmation-modal';
import { OrderGuard } from './guard.order';
import { ProductInformationPrintComponent } from './order/product-information/print/cmp.product-information-print';
import { ClientInformationPrintComponent } from './order/client-information/print/cmp.client-information-print';
import { ProductSpecificationsPrintComponent } from './order/product-specifications/print/cmp.product-specifications-print';
import { ProductOptionsZipBlindsPrintComponent } from './order/product-options/zip-blinds/print/cmp.zip-blinds-print';
import { ProductOptionsSlidingPanelsPrintComponent } from './order/product-options/sliding-panels/print/cmp.sliding-panels-print';
import { ProductOptionsGlassPanelsPrintComponent } from './order/product-options/glass-panels/print/cmp.glass-panels-print';
import { ProductAccessoriesPrintComponent } from './order/product-accessories/print/cmp.product-accessories-print';
import { OrderTermsPrintComponent } from './order/terms/print/cmp.terms-print';
import { OrderCommentsPrintComponent } from './order/comments/print/cmp.comments-print';
import { HttpClientModule } from '@angular/common/http';
import { TransferOwnershipModalComponent } from './cmp.transfer-ownership-modal';
import { OrderFooterPrintComponent } from './order/footer-print/cmp.order-footer-print';
import { ExportSearchResultsModalComponent } from './cmp.export-search-results-modal';

@NgModule({
  imports: [
    CommonModule,
    NgbModule,
    RouterModule,
    EntityModule,
    FormsModule,
    BaseSharedModule,
    ReactiveFormsModule,
    ValidationModule,
    EntityInputModule,
    FileManagerModule,
    FilePickerModule,
    MediaModule,
    HttpClientModule,
    InlineSVGModule
  ],
  providers: [
    OrderService,
    OrderGuard,
  ],
  entryComponents: [
    ActionConfirmationModalComponent,
    TransferOwnershipModalComponent,
    ExportSearchResultsModalComponent,
  ],
  declarations: [
    OrdersComponent,
    OrderComponent,
    ScrollingTextLoaderComponent,
    ClientInformationComponent,
    ProductInformationComponent,
    ProductSpecificationsComponent,
    ProductOptionsComponent,
    ProductOptionsZipBlindsComponent,
    ProductOptionsSlidingPanelsComponent,
    ProductOptionsGlassPanelsComponent,
    ProductAccessoriesComponent,
    AccessoryComponent,
    OrderCommentsComponent,
    OrderTermsComponent,
    OrderProgressComponent,
    ActionConfirmationModalComponent,
    TransferOwnershipModalComponent,
    ExportSearchResultsModalComponent,
    ClientInformationPrintComponent,
    ProductInformationPrintComponent,
    ProductSpecificationsPrintComponent,
    ProductOptionsZipBlindsPrintComponent,
    ProductOptionsSlidingPanelsPrintComponent,
    ProductOptionsGlassPanelsPrintComponent,
    ProductAccessoriesPrintComponent,
    OrderCommentsPrintComponent,
    OrderTermsPrintComponent,
    OrderFooterPrintComponent
  ]
})
export class OrdersModule { }
