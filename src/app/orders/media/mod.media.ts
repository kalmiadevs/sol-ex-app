import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MediaComponent } from './cmp.media';
import { MediaFsService } from './svc.media-fs';
import { FileManagerModule } from '../../base/entity/file-system/file-manager/mod.file-manager';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { BaseSharedModule } from '../../base/shared/mod.shared';
import { ValidationModule } from '../../base/shared/form/validation/mod.validation';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FileManagerModule,
    NgbModule,
    FormsModule,
    BaseSharedModule,
    ValidationModule
  ],
  providers: [
    MediaFsService
  ],
  declarations: [
    MediaComponent
  ],
  exports: [
    MediaComponent
  ]
})
export class MediaModule { }
