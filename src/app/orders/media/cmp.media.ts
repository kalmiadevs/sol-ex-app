import { Component, OnInit } from '@angular/core';
import { MediaFsService } from './svc.media-fs';
import { FilePickerService } from '../../base/entity/file-system/file-picker/svc.file-picker';
import { SystemService } from '../../core/svc.system';
import { OrderService } from '../svc.order';
import { ActivatedRoute, Router } from '@angular/router';
import { Order } from '../cls.order';
import { EntityComponent } from '../../base/entity/cmp.entity';
import { Location } from '@angular/common';
import { FsDir } from '../../base/entity/file-system/cls.fs-node';
import { AuthService } from '../../base/core/auth/svc.auth';
import { HttpAuthService } from '../../base/core/http/svc.http-auth';
import { environment } from '../../../environments/environment';
import { ResponseJson } from '../../base/core/http/cls.response-json';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SimpleNotification } from '../../base/app/notifications/notification/wrappers/cmp.simple-notification';
import { I18nService, _ } from '../../base/core/i18n/svc.i18n';

@Component({
  selector: 'app-media',
  templateUrl: './tpl.media.html',
  styleUrls: ['./cmp.media.scss']
})
export class MediaComponent extends EntityComponent<Order, OrderService> implements OnInit {
  entityDataResolver = 'order';
  entityRouteName    = 'order';

  pickedFiles: FsDir = FsDir.createNew();

  email: string = '';
  subject: string = 'Promotional material';
  body: string = '';

  loading: boolean;

  get order(): Order { return this.entity; }

  get attachments(): string [] { return Object.keys(this.pickedFiles.children); }

  constructor(
    protected location: Location,
    protected route: ActivatedRoute,
    protected router: Router,
    protected orderService: OrderService,
    protected sys: SystemService,
    public fs: MediaFsService,
    public filePickerService: FilePickerService,
    private authService: AuthService,
    protected http: HttpAuthService,
    protected modalService: NgbModal,
    private i18n: I18nService
  ) {
    super(location, route, router, sys, orderService, modalService);
  }

  ngOnInit() {
    super.onInit();
  }

  createNewEntity(): Order {
    return Order.createNew();
  }

  send() {
    this.loading = true;

    this.http.post(`${ environment.apiBaseProject }/mail`, {
      email: this.email,
      subject: this.subject,
      body: this.body,

      attachments: Object.keys(this.pickedFiles.children)
    })
      .subscribe((res: ResponseJson) => {
        this.loading = false;
        this.sys.notificationService.push(new SimpleNotification('success', this.i18n.get('mediaEmailSendNotificationSuccessTitle'), this.i18n.get('mediaEmailSendNotificationSuccessContent')).setTimeout(5000));
        this.router.navigate(['order/' + this.order.id]);
      }, e => {
        this.sys.notificationService.push(new SimpleNotification('danger', this.i18n.get('mediaEmailSendNotificationFailTitle'), this.i18n.get('mediaEmailSendNotificationFailContent')).setTimeout(5000));
        this.loading = false;
      });
  }

  pick() {
    this.filePickerService.pick(this.fs, [ this.authService.getUser().language || 'en' ])
      .then(x => {
        if (x) {
          const f = x.clone();
          f.id = x.path;
          this.pickedFiles.addChild(f);
        }
      })
  }
}
