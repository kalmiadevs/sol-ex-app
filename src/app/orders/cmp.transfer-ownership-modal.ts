import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { environment } from '../../environments/environment';
import { HttpAuthService } from '../base/core/http/svc.http-auth';
import { AuthService } from '../base/core/auth/svc.auth';

@Component({
  selector: 'app-transfer-ownership-modal',
  template: `
    <div class="modal-header">
      <h4 class="modal-title"><i class="fa fa-exclamation-circle"></i>&nbsp;<ng-container i18n>Transfer order
      </ng-container>
      </h4>
      <button type="button" class="close" aria-label="Close" (click)="activeModal.dismiss()">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
      <ng-container [ngSwitch]="transferType">
        <!-- Sales manager -->
        <div *ngSwitchCase="'salesManager'" class="transfer-wrapper sales-manager">
          <h6 class="pb-2 text-center" i18n>Select a sales manager you want to transfer the order to:</h6>

          <div class="loading-spinner" *ngIf="!salesManagers; else smDropdown">
            <i class="fa fa-cog fa-2x fa-spin"></i>
          </div>

          <ng-template #smDropdown>
            <div class="form-group row">
              <div class="col-12">
                <select class="custom-select form-control" [(ngModel)]="selectedSalesManagerId">
                  <option *ngFor="let sm of salesManagers" [value]="sm.id">{{ sm.firstName }} {{ sm.lastName }}</option>
                </select>
              </div>

              <p class="pt-3" *ngIf="!isSuperAdmin">
                <i class="fa fa-fw fa-exclamation-triangle"></i>
                <ng-container i18n>By transferring the order to another sales manager you will lose all access to it.
                  This action can only be undone by a portal administrator.
                </ng-container>
              </p>
            </div>
          </ng-template>
        </div>

        <!-- Distributor -->
        <div *ngSwitchCase="'distributor'" class="transfer-wrapper distributor">
          <h6 class="pb-2 text-center" i18n>Select a distributor you want to transfer the order to:</h6>

          <div class="loading-spinner" *ngIf="!distributors; else dDropdown">
            <i class="fa fa-cog fa-2x fa-spin"></i>
          </div>

          <ng-template #dDropdown>
            <div class="form-group row">
              <div class="col-12">
                <select class="custom-select form-control" [(ngModel)]="selectedDistributorId">
                  <option *ngFor="let d of distributors" [value]="d.id">{{ d.firstName }}</option>
                </select>
              </div>

              <p class="pt-3" *ngIf="isSuperAdmin">
                <i class="fa fa-fw fa-exclamation-triangle"></i>
                <ng-container i18n>By transferring the order to another distributor you are also changing the assigned sales manager, unless the
                  sales manager for selected distributor is the same, or the order was manually transferred to another sales manager.
                </ng-container>
              </p>
            </div>
          </ng-template>
        </div>

        <!-- Selector -->
        <div *ngSwitchDefault class="transfer-wrapper selector">
          <h6 class="pb-2 text-center" i18n>Who would you like to transfer the order to?</h6>

          <div class="form-group row">
            <div class="col-6 text-center">
              <button class="btn btn-secondary w-100" type="button" (click)="setDistributorTransferType()">
                <img src="/assets/img/transfer-distributor.png">
                <span class="py-1 d-inline-block" i18n>Distributor</span>
              </button>
            </div>
            <div class="col-6">
              <button class="btn btn-secondary w-100" type="button" (click)="setSalesManagerTransferType()">
                <img src="/assets/img/transfer-sales-manager.png">
                <span class="py-1 d-inline-block" i18n>Sales Manager</span>
              </button>
            </div>
          </div>
        </div>
      </ng-container>

    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-secondary" (click)="dismissModal()" i18n>Close</button>
      <button type="button" class="btn btn-info" [disabled]="!selectedSalesManagerId && !selectedDistributorId" (click)="closeModal()" i18n>
        Transfer
      </button>
    </div>
  `,
  styles: [
      `
      .loading-spinner {
        text-align: center;
        color: grey;
        margin: 25px;
      }

      .transfer-wrapper.selector img {
        display: block;
        height: 100px;
        margin: auto;
      }

      .transfer-wrapper .form-group {
        width: 80%;
        margin: auto;
      }
    `
  ]
})
export class TransferOwnershipModalComponent {

  transferType: 'salesManager' | 'distributor';

  salesManagers: any[];
  distributors: any[];
  selectedDistributorId: string;
  selectedSalesManagerId: string;

  get isSuperAdmin() {
    return this.authService.getJwtPayload().grp === 'superadmin';
  }

  constructor(public activeModal: NgbActiveModal,
              protected http: HttpAuthService,
              protected authService: AuthService) {

    // Fetch list of available sales managers
    http.get(`${environment.apiBaseProject}/orders/listSalesManagers`, {})
      .subscribe((x) => {
        this.salesManagers = (x as any).data.salesManagers || x;
        this.salesManagers = this.salesManagers.filter(v => v.id !== this.authService.getUser().id)
      }, (e) => {
        this.salesManagers = e;
      });

    // Fetch list of available distributors
    http.get(`${environment.apiBaseProject}/orders/listDistributors`, {})
      .subscribe((x) => {
        this.distributors = (x as any).data.distributors || x;
        this.distributors = this.distributors.filter(v => v.id !== this.authService.getUser().id)
      }, (e) => {
        this.distributors = e;
      });
  }

  dismissModal() {
    this.activeModal.dismiss();
  }

  closeModal() {
    this.activeModal.close({
      type: this.transferType,
      userId: this.transferType === 'salesManager' ? this.selectedSalesManagerId : this.selectedDistributorId
    })
  }

  setSalesManagerTransferType = () => this.transferType = 'salesManager';
  setDistributorTransferType = () => this.transferType = 'distributor';
}
