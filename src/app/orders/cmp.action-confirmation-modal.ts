import { Component, Input } from '@angular/core';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-action-confirmation-modal',
  template: `
    <div class="modal-header">
      <h4 class="modal-title"><i class="fa fa-exclamation-circle"></i>&nbsp;{{ title }}</h4>
      <button type="button" class="close" aria-label="Close" (click)="activeModal.dismiss()">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
      {{ content }}
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-secondary" (click)="activeModal.dismiss()" i18n>Close</button>
      <button type="button" class="btn btn-primary" (click)="activeModal.close()">{{ confirmationButtonText }}</button>
    </div>
  `
})
export class ActionConfirmationModalComponent {
  @Input() title: string;
  @Input() content: string;
  @Input() confirmationButtonText: string = 'Confirm';

  constructor(public activeModal: NgbActiveModal) {

  }
}
