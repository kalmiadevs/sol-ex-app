import { FormGroup } from '@angular/forms';
/* tslint:disable:no-null-keyword */

export const pergolaProperties = {
  'SL 160/28': {  maxWidth: 4000,
                  maxHeight: 3000,
                  length: [
                    329, 511, 694, 876, 1059, 1242, 1424, 1607, 1789, 1972, 2155, 2337, 2520, 2702, 2885, 3068, 3250, 3433, 3615, 3798, 3981, 4163, 4346, 4528, 4711, 4894, 5076, 5259, 5441, 5624, 5807, 5989
                  ],
                  poleConfigurations: [
                    4, 2, 1, 2, 1, 4
                  ]
  },

  'SL 170/28': {  maxWidth: 4000,
                  maxHeight: 3000,
                  length: [
                    329, 511, 694, 876, 1059, 1242, 1424, 1607, 1789, 1972, 2155, 2337, 2520, 2702, 2885, 3068, 3250, 3433, 3615, 3798, 3981, 4163, 4346, 4528, 4711, 4894, 5076, 5259, 5441, 5624, 5807, 5989, 6172, 6354, 6537, 6720, 6902
                  ],
                  poleConfigurations: [
                    4, 2, 1, 2, 1, 4
                  ]
  },

  'SL 170/36': {  maxWidth: 4500,
                  maxHeight: 3000,
                  length: [
                    329, 511, 694, 876, 1059, 1242, 1424, 1607, 1789, 1972, 2155, 2337, 2520, 2702, 2885, 3068, 3250, 3433, 3615, 3798, 3981, 4163, 4346, 4528, 4711, 4894, 5076, 5259, 5441, 5624, 5807, 5989, 6172, 6354, 6537, 6720, 6902
                  ],
                  poleConfigurations: [
                    4, 2, 1, 2, 1, 4
                  ]
  }
};

export const ledLengths = [500, 1000, 1500];

export const pergolaLengthReductionForPanels = 240;
export const glassPanelsLengths = { min: 330, max: 1200 };
export const slidingPanelLengths = { min : 330, max: 1600 };
export const slidingPanelGuideQuantityMap = {
  /* panelCount => [ guideCount, ... ] */
  0: [0], // only here to prevent errors, panels section is hidden in this case anyway.
  1: [1],
  2: [1, 2],
  3: [1, 2],
  4: [1, 2],
  5: [1, 2],
  6: [1, 2],
};

export const guideQuantityMap = {
  /* panelCount => [ guideCount, ... ] */
  0: [0], // only here to prevent errors, panels section is hidden in this case anyway.
  1: [1],
  2: [2],
  3: [2, 3],
  4: [2, 4],
  5: [3, 4],
  6: [3, 6],
};

export const slidingPanelPlacementDefaults = {
  /* panelCount => guideCount -> [ panelPlacement ] */
  0: { 0: [0], 1: [0] }, // only here to prevent errors, panels section is hidden in this case anyway.
  1: {
    1: [1], 2: [1]
  },
  2: {
    1: [1, 1], 2: [2, 1],
  },
  3: {
    1: [1, 1, 1], 2: [2, 1, 2],
  },
  4: {
    1: [1, 1, 1, 1], 2: [2, 1, 1, 2],
  },
  5: {
    1: [1, 1, 1, 1, 1], 2: [2, 1, 2, 1, 2],
  },
  6: {
    1: [1, 1, 1, 1, 1, 1], 2: [2, 1, 2, 1, 2, 1],
  }
};

export const glassPanelPlacementDefaults = {
  /* panelCount => guideCount => [ panelPlacement ] */
  0: { 0: [0], 1: [0] }, // only here to prevent errors, panels section is hidden in this case anyway.
  1: {
    1: [1],
  },
  2: {
    2: [1, 2],
  },
  3: {
    2: [2, 1, 2], 3: [3, 2, 1]
  },
  4: {
    2: [2, 1, 1, 2], 4: [4, 3, 2, 1]
  },
  5: {
    3: [3, 2, 1, 2, 3], 4: [4, 3, 1, 2, 3]
  },
  6: {
    3: [3, 2, 1, 1, 2, 3], 6: [6, 5, 4, 3, 2, 1]
  }
};

//
// This file should contain all custom validators.
//

export function productInformationValidator(g: FormGroup): {[key: string]: any} | null {
  const res: any = {};

  if (g.root.disabled) {
    return res;
  }

  const length = g.get('length').value;
  const ledLength = (g.get('specifications') as FormGroup).get('ledLength').value;

  if (length < ledLength && length >= ledLengths[0]) {
    res.ledLengthInvalid = true;
    g.get('specifications').get('ledLength').setErrors({ledLengthInvalid: true});
  }

  const maxWidth = pergolaProperties[g.get('pergolaType').value].maxWidth;
  const maxHeight = pergolaProperties[g.get('pergolaType').value].maxHeight;

  const width = g.get('width').value;
  const height = g.get('height').value;
  if (width > maxWidth) {
    res.widthInvalid = true;
    g.get('width').setErrors({widthInvalid: true});
  } else {
    if (width && width >= 1500) {
      g.get('width').setErrors(null); // fix error not disappearing when switching to pergola with largest max width
    }
  }
  if (height > maxHeight) {
    res.heightInvalid = true;
    g.get('height').setErrors({heightInvalid: true});
  }

  // ZIP blinds
  if (g.get('options').get('blindsPosition').get('p1p2').value ||
      g.get('options').get('blindsPosition').get('p3p4').value ||
      g.get('options').get('blindsPosition').get('p1p3').value ||
      g.get('options').get('blindsPosition').get('p2p4').value) {
    if (height < 2000) {
      g.get('options').get('blindsPosition').setErrors({blindsHeightInvalid: true});
    } else {
      g.get('options').get('blindsPosition').setErrors(null);
    }
  }

  // Primary poles
  const remainingPoles = g.get('numberOfPoles').value - ((g.get('poleData').get('p1').value ? 1 : 0) +
    (g.get('poleData').get('p2').value ? 1 : 0) +
    (g.get('poleData').get('p3').value ? 1 : 0) +
    (g.get('poleData').get('p4').value ? 1 : 0));

  if (remainingPoles) {
    res.polesRemaining = true;
    g.get('poleData').setErrors({polesRemaining: true});
  }

  /*
  if (g.get('installationType').value === 6) {
    for (let i = 1; i < 5; i++) {
      if (g.get('poleData').get('p' + i + 'y').value < 300 || g.get('poleData').get('p' + i + 'y').value > g.get('length').value / 4) {

        switch (i) {
          case 1:
          case 3:
            if (!g.get('poleData').get('p2y').value && !g.get('poleData').get('p4y').value) {
              g.get('poleData').get('p' + i + 'y').setErrors({valueNotInRange: true});
            } else {
              g.get('poleData').get('p' + i + 'y').setErrors(null);
            }
            break;
          case 2:
          case 4:
            if (!g.get('poleData').get('p1y').value && !g.get('poleData').get('p3y').value) {
              g.get('poleData').get('p' + i + 'y').setErrors({valueNotInRange: true});
            } else {
              g.get('poleData').get('p' + i + 'y').setErrors(null);
            }
            break;
        }
      }
    }
  } else {
    for (let i = 1; i < 5; i++) {
      g.get('poleData').get('p' + i + 'y').setErrors(null);
    }
  }
  */
  if (g.get('installationType').value === 6) {
    for (let i = 1; i < 5; i++) {
      if (g.get('poleData').get('p' + i + 'y').value === 0) {
        g.get('poleData').get('p' + i + 'y').setErrors(null);

      } else if (g.get('poleData').get('p' + i + 'y').value < 330 || g.get('poleData').get('p' + i + 'y').value > g.get('length').value / 4) {
        g.get('poleData').get('p' + i + 'y').setErrors({valueNotInRange: true});

      } else {
        g.get('poleData').get('p' + i + 'y').setErrors(null);
      }
    }
  } else {
    for (let i = 1; i < 5; i++) {
      g.get('poleData').get('p' + i + 'y').setErrors(null);
    }
  }

  return Object.keys(res).length ? res : null;
}


export function specValidator(g: FormGroup) {
  const errors = {};

  if (g.root.disabled) {
    return errors;
  }

  // Motor position
  if (!g.get('motorPos').value) {
    errors['motorPositionMissing'] = true;
  }

  // Disable motor-water same side check for SL 160/28
  if (g.parent && g.parent.get('pergolaType')) {
    if (g.parent.get('pergolaType').value !== 'SL 160/28') {
      for (let i = 1; i <= 2; i++) {
        if (g.get('waterExit').value['a' + i] || g.get('waterExit').value['b' + i]) {
          if (g.get('motorPos').value === 'b' && !g.get('waterExit').get('custom').value) {
            errors['motorPositionInvalid'] = true;
            break;
          }
        }
      }

      for (let i = 3; i <= 4; i++) {
        if (g.get('waterExit').value['a' + i] || g.get('waterExit').value['b' + i]) {
          if (g.get('motorPos').value === 'a' && !g.get('waterExit').get('custom').value) {
            errors['motorPositionInvalid'] = true;
            break;
          }
        }
      }
    }
  }

  // Water exit
  let waterExitCount = 0;
  for (let i = 1; i <= 4; i++) {
    if (g.get('waterExit').value['a' + i]) {
      if (g.parent && g.parent.get('poleData')) {
        if (!g.parent.get('poleData').get('p' + i).value && !g.get('waterExit').get('custom').value) {
          errors['waterExitMissingPole'] = true;
          break;
        }
      }
      if (g.get('powerSupply').value['p' + i] && !g.get('waterExit').get('custom').value) {
        errors['waterExitPositionUnavailable'] = true;
        break;
      }
      waterExitCount++;
    }
    if (g.get('waterExit').value['b' + i]) {
      if (g.parent && g.parent.get('poleData')) {
        if (!g.parent.get('poleData').get('p' + i).value && !g.get('waterExit').get('custom').value) {
          errors['waterExitMissingPole'] = true;
          break;
        }
      }
      if (g.get('powerSupply').value['p' + i] && !g.get('waterExit').get('custom').value) {
        errors['waterExitPositionUnavailable'] = true;
        break;
      }
      waterExitCount++;
    }
  }
  if (waterExitCount < 1 && !errors['waterExitPositionUnavailable'] && !errors['waterExitMissingPole'] && !g.get('waterExit').get('custom').value) {
    errors['waterExitInvalid'] = true;
  }

  // Power supply
  if (!g.get('powerSupply').value.p1 &&
      !g.get('powerSupply').value.p2 &&
      !g.get('powerSupply').value.p3 &&
      !g.get('powerSupply').value.p4) {
        errors['powerSupplyInvalid'] = true;
  }

  return Object.keys(errors).length ? errors : null;
}


export function optionsValidator(g: FormGroup) {
  const errors = {};

  if (g.root.disabled) {
    return errors;
  }

  // Glass panels
  if (g.get('glassPanelsPosition').value.p1p2 || g.get('glassPanelsPosition').value.p3p4) {
    if (g.get('glassPanelsOnLength') && g.get('glassPanelsOnLength').value) {
      const glassPanelsQuantity = (g.get('glassPanelsOnLength').get('panelQuantity').value as number);
      let placedGlassPanels = 0;
      for (const x of g.get('glassPanelsOnLength').get('placement').value) {
        if (x) {
          placedGlassPanels++;
        }
      }
      if (placedGlassPanels !== Number(glassPanelsQuantity)) {
        errors['glassPanelsNotPlacedOnLength'] = true;
      }

      if (g.root && g.root !== g) {
        if (glassPanelsQuantity * (g.get('glassPanelsOnLength').get('size').value as number) > g.root.get('product').get('length').value - 240) {
          g.get('glassPanelsOnLength').get('size').setErrors({glassPanelsSizeTooLarge: g.root.get('product').get('length').value - 240});
        } else if (g.get('glassPanelsOnLength').get('size').errors && g.get('glassPanelsOnLength').get('size').errors.glassPanelsSizeTooLarge) {
          g.get('glassPanelsOnLength').get('size').setErrors(null);
        }
      }
    }
  } else {
    g.get('glassPanelsOnLength').get('size').setErrors(null);
  }

  if (g.get('glassPanelsPosition').value.p1p3 || g.get('glassPanelsPosition').value.p2p4) {
    if (g.get('glassPanelsOnWidth') && g.get('glassPanelsOnWidth').value) {
      const glassPanelsQuantity = (g.get('glassPanelsOnWidth').get('panelQuantity').value as number);
      let placedGlassPanels = 0;
      for (const x of g.get('glassPanelsOnWidth').get('placement').value) {
        if (x) {
          placedGlassPanels++;
        }
      }
      if (placedGlassPanels !== Number(glassPanelsQuantity)) {
        errors['glassPanelsNotPlacedOnWidth'] = true;
      }

      if (g.root && g.root !== g) {
        if (glassPanelsQuantity * (g.get('glassPanelsOnWidth').get('size').value as number) > g.root.get('product').get('width').value - 240) {
          g.get('glassPanelsOnWidth').get('size').setErrors({glassPanelsSizeTooLarge: g.root.get('product').get('width').value - 240});
        } else if (g.get('glassPanelsOnWidth').get('size').errors && g.get('glassPanelsOnWidth').get('size').errors.glassPanelsSizeTooLarge) {
          g.get('glassPanelsOnWidth').get('size').setErrors(null);
        }
      }
    }
  } else {
    g.get('glassPanelsOnWidth').get('size').setErrors(null);
  }

  // Sliding panels
  if (g.get('slidingPanelPosition').value.p1p2 || g.get('slidingPanelPosition').value.p3p4) {
    if (g.get('slidingPanelOnLength') && g.get('slidingPanelOnLength').value) {
      const slidingPanelQuantity = (g.get('slidingPanelOnLength').get('panelQuantity').value as number);
      let placedSlidingPanels = 0;
      for (const x of g.get('slidingPanelOnLength').get('placement').value) {
        if (x) {
          placedSlidingPanels++;
        }
      }
      if (placedSlidingPanels !== Number(slidingPanelQuantity)) {
        errors['slidingPanelsNotPlacedOnLength'] = true;
      }

      if (g.root && g.root !== g) {
        if (slidingPanelQuantity * (g.get('slidingPanelOnLength').get('size').value as number) > g.root.get('product').get('length').value - 240) {
          g.get('slidingPanelOnLength').get('size').setErrors({slidingPanelSizeTooLarge: g.root.get('product').get('length').value - 240});
        } else if (g.get('slidingPanelOnLength').get('size').errors && g.get('slidingPanelOnLength').get('size').errors.slidingPanelSizeTooLarge) {
          g.get('slidingPanelOnLength').get('size').setErrors(null);
        }
      }
    }
  } else {
    g.get('slidingPanelOnLength').get('size').setErrors(null);
  }

  if (g.get('slidingPanelPosition').value.p1p3 || g.get('slidingPanelPosition').value.p2p4) {
    if (g.get('slidingPanelOnWidth') && g.get('slidingPanelOnWidth').value) {
      const slidingPanelQuantity = (g.get('slidingPanelOnWidth').get('panelQuantity').value as number);
      let placedSlidingPanels = 0;
      for (const x of g.get('slidingPanelOnWidth').get('placement').value) {
        if (x) {
          placedSlidingPanels++;
        }
      }
      if (placedSlidingPanels !== Number(slidingPanelQuantity)) {
        errors['slidingPanelsNotPlacedOnWidth'] = true;
      }

      if (g.root && g.root !== g) {
        if (slidingPanelQuantity * (g.get('slidingPanelOnWidth').get('size').value as number) > g.root.get('product').get('width').value - 240) {
          g.get('slidingPanelOnWidth').get('size').setErrors({slidingPanelSizeTooLarge: g.root.get('product').get('width').value - 240});
        } else if (g.get('slidingPanelOnWidth').get('size').errors && g.get('slidingPanelOnWidth').get('size').errors.slidingPanelSizeTooLarge) {
          g.get('slidingPanelOnWidth').get('size').setErrors(null);
        }
      }
    }
  } else {
    g.get('slidingPanelOnWidth').get('size').setErrors(null);
  }

  return Object.keys(errors).length ? errors : null;
}
