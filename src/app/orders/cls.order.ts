import { BaseNode } from '../base/entity/node/cls.node';
import { EntityState } from '../base/entity/cls.entity-state';
import { OrderApiResponse } from './itf.order-api-response';
import { deserialize, JsonProperty, serialize } from 'json-typescript-mapper';
import { classToForm, FormArrayClass, FormClass, FormProperty, formToClass } from './ng-reactive-form-mapper';
import { FormGroup, Validators } from '@angular/forms';
import {
  glassPanelPlacementDefaults,
  ledLengths,
  optionsValidator,
  pergolaProperties,
  productInformationValidator,
  slidingPanelPlacementDefaults,
  specValidator
} from 'app/orders/cls.order-validators';
import { _ } from '../base/core/i18n/svc.i18n';

export class ClientInformation {
  @FormProperty(Validators.required)
  @JsonProperty('name')
  name: string = void 0;

  @FormProperty(Validators.required)
  @JsonProperty('address')
  address: string = void 0;

  @FormProperty(Validators.required)
  @JsonProperty('phone')
  phone: string = void 0;

  @FormProperty([Validators.required, Validators.email])
  @JsonProperty('email')
  email: string = void 0;

  @FormProperty()
  @JsonProperty('vat')
  vat: string = void 0;

  @FormProperty()
  @JsonProperty('countryCode')
  countryCode: string = void 0;
}

export class DeliveryInformation {
  @FormProperty(Validators.required)
  @JsonProperty('name')
  name: string = void 0;

  @FormProperty(Validators.required)
  @JsonProperty('address')
  address: string = void 0;

  @FormProperty(Validators.required)
  @JsonProperty('phone')
  phone: string = void 0;

  @FormProperty([Validators.required, Validators.email])
  @JsonProperty('email')
  email: string = void 0;
}

export class MotorPositions {
  @FormProperty()
  @JsonProperty('a')
  a: boolean = true;

  @FormProperty()
  @JsonProperty('b')
  b: boolean = false;
}

export class WaterExits {
  @FormProperty()
  @JsonProperty('a1')
  a1: boolean = true;

  @FormProperty()
  @JsonProperty('b1')
  b1: boolean = false;

  @FormProperty()
  @JsonProperty('a2')
  a2: boolean = true;

  @FormProperty()
  @JsonProperty('b2')
  b2: boolean = false;

  @FormProperty()
  @JsonProperty('a3')
  a3: boolean = false;

  @FormProperty()
  @JsonProperty('b3')
  b3: boolean = false;

  @FormProperty()
  @JsonProperty('a4')
  a4: boolean = false;

  @FormProperty()
  @JsonProperty('b4')
  b4: boolean = false;

  @FormProperty()
  @JsonProperty('custom')
  custom: boolean = false;
}

class PowerSupplies {
  @FormProperty()
  @JsonProperty('p1')
  p1: boolean = false;

  @FormProperty()
  @JsonProperty('p2')
  p2: boolean = false;

  @FormProperty()
  @JsonProperty('p3')
  p3: boolean = true;

  @FormProperty()
  @JsonProperty('p4')
  p4: boolean = false;
}


class WallFixations {
  @FormProperty()
  @JsonProperty('p1p2')
  p1p2: boolean = false;

  @FormProperty()
  @JsonProperty('p3p4')
  p3p4: boolean = false;

  @FormProperty()
  @JsonProperty('p1p3')
  p1p3: boolean = false;

  @FormProperty()
  @JsonProperty('p2p4')
  p2p4: boolean = false;
}

export class Specifications {
  // @FormClass(MotorPositions)
  // @JsonProperty({clazz: MotorPositions, name: 'motorPosition'})
  // motorPosition: MotorPositions = new MotorPositions();

  @FormProperty()
  @JsonProperty('motorPos')
  motorPos: string = void 0;

  @FormClass(WaterExits)
  @JsonProperty({clazz: WaterExits, name: 'waterExit'})
  waterExit: WaterExits = new WaterExits();

  @FormClass(PowerSupplies)
  @JsonProperty({clazz: PowerSupplies, name: 'powerSupply'})
  powerSupply: PowerSupplies = new PowerSupplies();

  @FormProperty([Validators.required])
  @JsonProperty('mountingWallType')
  mountingWallType: string = 'Standard';

  @FormClass(WallFixations)
  @JsonProperty({clazz: WallFixations, name: 'wallFixations'})
  wallFixations: WallFixations = new WallFixations();

  @FormProperty([Validators.required])
  @JsonProperty('mountingGroundType')
  mountingGroundType: string = 'Standard';

  @FormProperty([Validators.required])
  @JsonProperty('mountingGroundQuantity')
  mountingGroundQuantity: number = 0;

  @FormProperty([Validators.required])
  @JsonProperty('mountingGroundColorStandard')
  mountingGroundColorStandard: boolean = true;


  @FormProperty([Validators.required])
  @JsonProperty('bladesOpeningDirection')
  bladesOpeningDirection: string = 'CW';

  @FormProperty([Validators.required])
  @JsonProperty('ledLength')
  ledLength: number = ledLengths[0];

  @FormProperty([Validators.required])
  @JsonProperty('ledType')
  ledType: 'white-natural' | 'white-warm' | 'rgb' = 'white-natural';

  @FormProperty()
  @JsonProperty('ledPositions')
  ledPositions: number[] = [];

  @FormProperty()
  @JsonProperty('customLedPositionEnabled')
  customLedPositionEnabled: boolean = false;

  @FormProperty([Validators.required, Validators.min(0)])
  @JsonProperty('customLedPosition')
  customLedPosition: number = 0;

  static get validator() {
    return specValidator;
  }
}

class Positions {
  @FormProperty(Validators.required)
  @JsonProperty('p1p2')
  p1p2: boolean = false;
  @FormProperty(Validators.required)
  @JsonProperty('p3p4')
  p3p4: boolean = false;
  @FormProperty(Validators.required)
  @JsonProperty('p1p3')
  p1p3: boolean = false;
  @FormProperty(Validators.required)
  @JsonProperty('p2p4')
  p2p4: boolean = false;
}

class SlidingPanelGroup {
  @FormProperty(Validators.required)
  @JsonProperty('type')
  type: 'A' | 'B' = 'A';
  @FormProperty([Validators.required, Validators.min(330), Validators.max(1200)])
  @JsonProperty('size')
  size: number = 330;
  @FormProperty([Validators.required, Validators.min(0)])
  @JsonProperty('panelQuantity')
  panelQuantity: number = 1;
  @FormProperty()
  @JsonProperty('placement')
  placement: number[] = Object.assign([], slidingPanelPlacementDefaults[1][1]);
  @FormProperty(Validators.required)
  @JsonProperty('arrowLeft')
  arrowLeft: string = 'left';
  @FormProperty(Validators.required)
  @JsonProperty('arrowRight')
  arrowRight: string = 'right';
}

class GlassPanelGroup {
  @FormProperty(Validators.required)
  @JsonProperty('guides')
  guides: number = 2;
  @FormProperty([Validators.required, Validators.min(330), Validators.max(1200)])
  @JsonProperty('size')
  size: number = 330;
  @FormProperty([Validators.required, Validators.min(0)])
  @JsonProperty('panelQuantity')
  panelQuantity: number = 2;
  @FormProperty()
  @JsonProperty('placement')
  placement: number[] = Object.assign([], glassPanelPlacementDefaults[2][2]);
  @FormProperty(Validators.required)
  @JsonProperty('arrowLeft')
  arrowLeft: string = 'left';
  @FormProperty(Validators.required)
  @JsonProperty('arrowRight')
  arrowRight: string = 'right';
  @FormProperty()
  @JsonProperty('locker')
  locker: boolean = false;
}

export class Options {
  @FormClass(Positions)
  @JsonProperty({clazz: Positions, name: 'blindsPosition'})
  blindsPosition: Positions = new Positions();
  @FormProperty(Validators.required)
  @JsonProperty('blindsFabric')
  blindsFabric: string = '108101';

  @FormProperty(Validators.required)
  @JsonProperty('blindsCustomFabric')
  blindsCustomFabric: boolean = false;
  @FormProperty(Validators.required)
  @JsonProperty('blindsCustomFabricID')
  blindsCustomFabricID: string = '108101';

  @FormProperty(Validators.required)
  @JsonProperty('blindsFabricSoltis')
  blindsFabricSoltis: boolean = false;
  @FormProperty()
  @JsonProperty('blindsFabricSoltisCode')
  blindsFabricSoltisCode: string = '';
  @FormProperty(Validators.required)
  @JsonProperty('blindsFullScreen')
  blindsFullScreen: boolean = false;
  @FormProperty(Validators.required)
  @JsonProperty('blindsGuideColorMatchesFrame')
  blindsGuideColorMatchesFrame: boolean = true;
  @FormProperty(Validators.required)
  @JsonProperty('blindsGuideColorCustomID')
  blindsGuideColorCustomID: number = 7016;
  @FormProperty(Validators.required)
  @JsonProperty('blindsFinishType')
  blindsFinishType: string = 'Matt';
  @FormProperty(Validators.required)
  @JsonProperty('blindsGuideType')
  blindsGuideType: string = 'Standard';
  @FormProperty(Validators.required)
  @JsonProperty('blindsMotorType')
  blindsMotorType: string = 'Standard';
  @FormProperty([Validators.required, Validators.min(0)])
  @JsonProperty('blindsPanelSize1')
  blindsPanelSize1: number = 0;
  @FormProperty([Validators.required, Validators.min(0)])
  @JsonProperty('blindsPanelSize2')
  blindsPanelSize2: number = 0;

  @FormClass(Positions)
  @JsonProperty({clazz: Positions, name: 'slidingPanelPosition'})
  slidingPanelPosition: Positions = new Positions();
  @FormClass(SlidingPanelGroup)
  @JsonProperty({clazz: SlidingPanelGroup, name: 'slidingPanelOnLength'})
  slidingPanelOnLength: SlidingPanelGroup = new SlidingPanelGroup();
  @FormClass(SlidingPanelGroup)
  @JsonProperty({clazz: SlidingPanelGroup, name: 'slidingPanelOnWidth'})
  slidingPanelOnWidth: SlidingPanelGroup = new SlidingPanelGroup();
  @FormProperty(Validators.required)
  @JsonProperty('slidingPanelMaterial')
  slidingPanelMaterial: string = 'Wood';
  @FormProperty(Validators.required)
  @JsonProperty('slidingPanelColor')
  slidingPanelColor: number = 7016;
  @FormProperty(Validators.required)
  @JsonProperty('slidingPanelCustomColor')
  slidingPanelCustomColor: boolean = false;
  @FormProperty(Validators.required)
  @JsonProperty('slidingPanelCustomColorID')
  slidingPanelCustomColorID: number = 7016;
  @FormProperty(Validators.required)
  @JsonProperty('slidingPanelFinishType')
  slidingPanelFinishType: string = 'Matt';

  @FormClass(Positions)
  @JsonProperty({clazz: Positions, name: 'glassPanelsPosition'})
  glassPanelsPosition: Positions = new Positions();
  @FormClass(GlassPanelGroup)
  @JsonProperty({clazz: GlassPanelGroup, name: 'glassPanelsOnLength'})
  glassPanelsOnLength: GlassPanelGroup = new GlassPanelGroup();
  @FormClass(GlassPanelGroup)
  @JsonProperty({clazz: GlassPanelGroup, name: 'glassPanelsOnWidth'})
  glassPanelsOnWidth: GlassPanelGroup = new GlassPanelGroup();

  static get validator() {
    return optionsValidator;
  }
}

export class Accessories {
  // LED Lights
  @FormProperty([Validators.required, Validators.min(0)])
  @JsonProperty('betaQuantity')
  betaQuantity: number = 0;
  @FormProperty([Validators.required, Validators.min(0), Validators.max(1)])
  @JsonProperty('ledStrip')
  ledStrip: number = 0;

  // Remote Controller
  @FormProperty([Validators.required, Validators.min(0)])
  @JsonProperty('additionalRemoteControl')
  additionalRemoteControl: number = 0;

  // Heaters
  @FormProperty([Validators.required, Validators.min(0)])
  @JsonProperty('heaterQuantity')
  heaterQuantity: number = 0;
  @FormProperty([Validators.required, Validators.min(0)])
  @JsonProperty('heaterAnthraciteQuantity')
  heaterAnthraciteQuantity: number = 0;

  // Audio
  @FormProperty([Validators.required, Validators.min(0)])
  @JsonProperty('bluetoothSystem')
  bluetoothSystem: number = 0;
  @FormProperty([Validators.required, Validators.min(0)])
  @JsonProperty('speakers')
  speakers: number = 0;
  @FormProperty([Validators.required, Validators.min(0)])
  @JsonProperty('rcAudioSystem')
  rcAudioSystem: number = 0;


  // Sensors
  @FormProperty([Validators.required, Validators.min(0)])
  @JsonProperty('windSensorQuantity')
  windSensorQuantity: number = 0;
  @FormProperty([Validators.required, Validators.min(0)])
  @JsonProperty('rainSensorQuantity')
  rainSensorQuantity: number = 0;
  @FormProperty([Validators.required, Validators.min(0)])
  @JsonProperty('temperatureSensorQuantity')
  temperatureSensorQuantity: number = 0;
  @FormProperty([Validators.required, Validators.min(0)])
  @JsonProperty('presenceSensorQuantity')
  presenceSensorQuantity: number = 0;
  @FormProperty([Validators.required, Validators.min(0)])
  @JsonProperty('externalAntenna')
  externalAntenna: number = 0;

  // Somfy
  @FormProperty([Validators.required, Validators.min(0)])
  @JsonProperty('somfyMotorQuantity')
  somfyMotorQuantity: number = 0;

  @FormProperty([Validators.required, Validators.min(0)])
  @JsonProperty('somfyRTST1TBQuantity')
  somfyRTST1TBQuantity: number = 0;
  @FormProperty([Validators.required, Validators.min(0)])
  @JsonProperty('somfyRTST4TBQuantity')
  somfyRTST4TBQuantity: number = 0;
  @FormProperty([Validators.required, Validators.min(0)])
  @JsonProperty('somfyRTST16TBQuantity')
  somfyRTST16TBQuantity: number = 0;

  @FormProperty([Validators.required, Validators.min(0)])
  @JsonProperty('somfyBridge1Quantity')
  somfyBridge1Quantity: number = 0;
  @FormProperty([Validators.required, Validators.min(0)])
  @JsonProperty('somfyBridge7Quantity')
  somfyBridge7Quantity: number = 0;

  @FormProperty([Validators.required, Validators.min(0)])
  @JsonProperty('somfyRTST1Quantity')
  somfyRTST1Quantity: number = 0;
  @FormProperty([Validators.required, Validators.min(0)])
  @JsonProperty('somfyRTST4Quantity')
  somfyRTST4Quantity: number = 0;
  @FormProperty([Validators.required, Validators.min(0)])
  @JsonProperty('somfyRTST16Quantity')
  somfyRTST16Quantity: number = 0;

  @FormProperty([Validators.required, Validators.min(0)])
  @JsonProperty('doubleMotor')
  doubleMotor: number = 0;

  @FormProperty([Validators.required, Validators.min(0)])
  @JsonProperty('transportBox1')
  transportBox1: number = 0;
  @FormProperty([Validators.required, Validators.min(0)])
  @JsonProperty('transportBox2')
  transportBox2: number = 0;
  @FormProperty([Validators.required, Validators.min(0)])
  @JsonProperty('transportBox3')
  transportBox3: number = 0;

  @FormProperty([Validators.required, Validators.min(0)])
  @JsonProperty('sparePartsKitSL')
  sparePartsKitSL: number = 0;
  @FormProperty([Validators.required, Validators.min(0)])
  @JsonProperty('merbenitGlue')
  merbenitGlue: number = 0;
  @FormProperty([Validators.required, Validators.min(0)])
  @JsonProperty('butylTapeRoll')
  butylTapeRoll: number = 0;
}

export class PoleData {
  @FormProperty()
  @JsonProperty('p1')
  p1: boolean = true;

  @FormProperty()
  @JsonProperty('p2')
  p2: boolean = true;

  @FormProperty()
  @JsonProperty('p3')
  p3: boolean = true;

  @FormProperty()
  @JsonProperty('p4')
  p4: boolean = true;


  @FormProperty([Validators.required])
  @JsonProperty('p1y')
  p1y: number = 0;

  @FormProperty([Validators.required])
  @JsonProperty('p2y')
  p2y: number = 0;

  @FormProperty([Validators.required])
  @JsonProperty('p3y')
  p3y: number = 0;

  @FormProperty([Validators.required])
  @JsonProperty('p4y')
  p4y: number = 0;


  @FormProperty([Validators.required, Validators.min(0)])
  @JsonProperty('p1height')
  p1height: number = 0;

  @FormProperty([Validators.required, Validators.min(0)])
  @JsonProperty('p2height')
  p2height: number = 0;

  @FormProperty([Validators.required, Validators.min(0)])
  @JsonProperty('p3height')
  p3height: number = 0;

  @FormProperty([Validators.required, Validators.min(0)])
  @JsonProperty('p4height')
  p4height: number = 0;
}

export class AdditionalPole {
  @FormProperty([Validators.required])
  @JsonProperty('position')
  position: 'p1p2' | 'p3p4' | 'p1p3' | 'p2p4' = 'p1p2';

  @FormProperty([Validators.required, Validators.min(1)])
  @JsonProperty('x')
  x: number = 0;

  @FormProperty([Validators.required, Validators.min(0)])
  @JsonProperty('height')
  height: number = 0;
}

export class ProjectInformation {
  @FormProperty(Validators.required)
  @JsonProperty('name')
  name: string = void 0;

  @FormProperty()
  @JsonProperty('description')
  description: string = '';

  @JsonProperty('date')
  date: string = void 0;

  @JsonProperty('requestedDeliveryDate')
  requestedDeliveryDate: Date = void 0;
}

export class ProductInformation {
  @FormProperty()
  @JsonProperty('pergolaType')
  pergolaType: string = 'SL 160/28';
  @FormProperty([Validators.required])
  @JsonProperty('installationType')
  installationType: number = 1;

  @FormProperty([Validators.required, Validators.min(1)])
  @JsonProperty('pergolaQuantity')
  pergolaQuantity: number = 1;

  @FormProperty([Validators.required, Validators.min(1500)])
  @JsonProperty('width')
  width: number = 1500;
  @FormProperty(Validators.required)
  @JsonProperty('length')
  length: number = 3068;
  @FormProperty([Validators.required, Validators.min(0)])
  @JsonProperty('height')
  height: number = 0;

  @FormProperty([Validators.required])
  @JsonProperty('numberOfPoles')
  numberOfPoles: number = pergolaProperties[this.pergolaType].poleConfigurations[this.installationType - 1];

  @FormProperty([Validators.required, Validators.min(0)])
  @JsonProperty('numberOfAdditionalPoles')
  numberOfAdditionalPoles: number = 0;

  @FormClass(PoleData)
  @JsonProperty('poleData')
  poleData: PoleData = new PoleData();

  @FormProperty()
  @JsonProperty('customPoleHeight')
  customPoleHeight: boolean = false;

  @FormArrayClass(AdditionalPole) // Example usage: @FormArrayClass([AdditionalPole, arrayValidator, itemValidator])
  @JsonProperty({clazz: AdditionalPole, name: 'additionalPoleData'})
  additionalPoleData: AdditionalPole[] = [];

  @FormProperty()
  @JsonProperty('nonStandardColorEnabled')
  _nonStandardColorEnabled: boolean = false;

  get nonStandardColorEnabled(): boolean {
    return this._nonStandardColorEnabled;
  }

  set nonStandardColorEnabled(value: boolean) {
    if (!value) {
      this.structureColor = 7016;
      this.bladesColor = 7016;
      this.structureFinishType = 'Matt';
      this.bladesFinishType = 'Matt';
    }
    this._nonStandardColorEnabled = value;
  }

  @FormProperty([Validators.required])
  @JsonProperty('structureColor')
  structureColor: number = 7016;
  @FormProperty([Validators.required])
  @JsonProperty('bladesColor')
  bladesColor: number = 7016;

  @FormProperty([Validators.required])
  @JsonProperty('structureFinishType')
  structureFinishType: string = 'Matt';
  @FormProperty([Validators.required])
  @JsonProperty('bladesFinishType')
  bladesFinishType: string = 'Matt';

  @FormClass([Specifications, Specifications.validator])
  @JsonProperty({clazz: Specifications, name: 'specifications'})
  specifications: Specifications = new Specifications();

  @FormClass([Options, Options.validator])
  @JsonProperty({clazz: Options, name: 'options'})
  options: Options = new Options();

  @FormClass(Accessories)
  @JsonProperty({clazz: Accessories, name: 'accessories'})
  accessories: Accessories = new Accessories();

  static get validator() {
    return productInformationValidator;
  }
}

export class Terms {
  @JsonProperty('terms')
  terms: string = 'Order terms';

  @FormProperty([Validators.required, Validators.requiredTrue])
  @JsonProperty('agree')
  agree: boolean = true;
}

export enum OrderStep { S0, S1, S2, S3, S4 }

export enum StepStateS3 {
  order,
  accepted,
  inProcess_cuttingAndCnc,
  inProcess_painting,
  inProcess_assembly,
  ready,
}

export class Order extends BaseNode {

  @JsonProperty('id')
  id: string = void 0;
  @JsonProperty('owner')
  owner: string = void 0;
  @JsonProperty('_originalOwnerId')
  _originalOwnerId: string = void 0;
  @JsonProperty('_originalOwnerName')
  _originalOwnerName: string = void 0;

  @JsonProperty('_state')
  _state: 'in_progress' | 'ok' | 'failed' = void 0;
  @JsonProperty('_salesManagerOverrideId')
  _salesManagerOverrideId: string = void 0;
  @JsonProperty('_originalSalesManagerName')
  _originalSalesManagerName: string = void 0;
  @JsonProperty('_originalSalesManagerId')
  _originalSalesManagerId: string = void 0;
  @JsonProperty('_public')
  _public: string = void 0;

  @JsonProperty('spId')
  spId: string = void 0;
  @JsonProperty('birth')
  birth: Date = void 0;
  @JsonProperty('spSyncTimestamp')
  spSyncTimestamp: number = -1;
  @JsonProperty('spModified')
  spModified: string = void 0;
  @JsonProperty('spOrigin')
  spOrigin: boolean = false;
  @JsonProperty('spDueDate')
  spDueDate: string = void 0;
  @JsonProperty('spProjectID')
  spProjectID: string = void 0;
  @JsonProperty('spFiles')
  spFiles: { file: string, name: string, modified: string }[] = [];

  @JsonProperty('documents')
  documents: { meta: any, path: string, type: string, name: string, noPublic: boolean }[] = [];

  @JsonProperty('number')
  number: string = void 0;
  @JsonProperty('projectNumber')
  projectNumber: string = void 0;

  @JsonProperty('cost')
  cost: string = void 0;

  @JsonProperty('step')
  step: 'S0' | 'S1' | 'S2' | 'S3' | 'S4' = 'S1';

  @JsonProperty('status')
  status: string = void 0;

  @FormClass(ClientInformation)
  @JsonProperty({clazz: ClientInformation, name: 'client'})
  client: ClientInformation = new ClientInformation();

  @JsonProperty('deliverySameAsClient')
  deliverySameAsClient: boolean = true;

  @FormClass(DeliveryInformation)
  @JsonProperty({clazz: DeliveryInformation, name: 'delivery'})
  delivery?: DeliveryInformation = new DeliveryInformation();

  @FormClass(ProjectInformation)
  @JsonProperty({clazz: ProjectInformation, name: 'project'})
  project: ProjectInformation = new ProjectInformation();

  @FormClass([ProductInformation, ProductInformation.validator])
  @JsonProperty({clazz: ProductInformation, name: 'product'})
  product: ProductInformation = new ProductInformation();

  // @FormProperty()
  @JsonProperty('comment')
  comment: string = '';

  @FormClass(Terms)
  @JsonProperty({clazz: Terms, name: 'terms'})
  terms: Terms = new Terms();

  @JsonProperty('distributor')
  distributor: string = void 0;

  @JsonProperty('salesManager')
  salesManager: string = void 0;

  @JsonProperty('languageOverride')
  languageOverride: string;


  get $isInProgress(): boolean {
    return this._state === 'in_progress' || this._state === 'failed';
  }

  get $step(): OrderStep {
    return {
      'S0': OrderStep.S0,
      'S1': OrderStep.S1,
      'S2': OrderStep.S2,
      'S3': OrderStep.S3,
      'S4': OrderStep.S4
    }[this.step];
  }

  get $stepNumber(): number {
    return {
      'S0': 0,
      'S1': 1,
      'S2': 2,
      'S3': 3,
      'S4': 4,
      undefined: -1
    }[this.step];
  }

  get $stepStateNumber(): number {
    if (this.$stepNumber < 3) {
      return -1;
    }
    if (this.$step === OrderStep.S3) {
      return {
        'ORDER ACCEPTED': 1,
        'IN PROCESS / CUTTING&amp;CNC': 2,
        'IN PROCESS / CUTTING&CNC': 2,
        'IN PROCESS / CUTTING & CNC': 2,
        'IN PROCESS / PAINTING': 3,
        'IN PROCESS / ASSEMBLING': 4,
        'READY FOR DISPATCHED': 5,
        undefined: 0
      }[this.status];
    }
    return 999;
  }

  get $stepDescription(): string {
    if (this.$step === OrderStep.S3) {
      return {
        'ORDER ACCEPTED': _('substepOrderAccepted'),
        'IN PROCESS / CUTTING&amp;CNC': _('substepOrderCuttingAndCNC'),
        'IN PROCESS / CUTTING&CNC': _('substepOrderCuttingAndCNC'),
        'IN PROCESS / CUTTING & CNC': _('substepOrderCuttingAndCNC'),
        'IN PROCESS / PAINTING': _('substepOrderPainting'),
        'IN PROCESS / ASSEMBLING': _('substepOrderAssembly'),
        'READY FOR DISPATCHED': _('substepOrderReadyForDispatch'),
        undefined: _('substepOrderDefault')
      }[this.status] || _('substepOrderDefault');
    }

    return {
      'S0': _('stepOrderCancelled'),
      'S1': _('stepOrderOffer'),
      'S2': _('stepOrderOrder'),
      'S4': _('stepOrderCompleted'),
      undefined: ''
    }[this.step];
  }

  static createNew(): Order {
    const x = new Order();
    x.$state = EntityState.New;
    x.product.specifications.motorPos = 'a';
    // x.product.pergolaType = 'SL 160/28';
    return x;
  }

  static createFromApi(json: OrderApiResponse | any): Order {
    // auto corrections
    json.step = json.step && json.step.toUpperCase();

    const order = deserialize(Order, json);
    if (order.$stepNumber === -1) {
      order.setError(new Error('Unknown step'));
    }

    order.client = order.client || new ClientInformation();
    order.delivery = order.delivery || new DeliveryInformation();
    order.project = order.project || new ProjectInformation();
    order.product = order.product || new ProductInformation();
    order.version = 0;

    // cancel out default val
    order.product.pergolaType = json.product && json.product.pergolaType || '';

    if (order.spOrigin) {
      order.$locked = true;
    }

    // if (json._state === 'failed') {
    //   order.addWarning(new SimpleWarning(_('orderFailedWarning')))
    // }

    return order;
  }

  static stringToImgCode(x: string | number = '') {
    x = (x + '').replace(/[\\/ .]/g, '_');
    return x.replace(/[^a-zA-Z0-9_\-]/g, '');
  }

  static genPreviewImgCode(pergolaType: string, installationType: string | number): string {
    return pergolaType && installationType
      ?
      `${ Order.stringToImgCode(pergolaType) }.${ Order.stringToImgCode(installationType) }`
      :
      undefined;
  }

  static updateZIPBlinds(orderForm) {
    const pole = orderForm.get('product').get('additionalPoleData').value.find((x) => x.position === 'p1p2');
    if (pole) {
      orderForm.get('product').get('options').get('blindsPanelSize1').setValue(pole.x);
      orderForm.get('product').get('options').get('blindsPanelSize2').setValue(orderForm.get('product').get('length').value - pole.x - (orderForm.get('product').get('poleData').get('p1y').value || orderForm.get('product').get('poleData').get('p2y').value) - (3 * 120));
    } else if (orderForm.get('product').get('length').value > 6000) {
      orderForm.get('product').get('options').get('blindsPanelSize1').setValue(orderForm.get('product').get('length').value / 2);
      orderForm.get('product').get('options').get('blindsPanelSize2').setValue(orderForm.get('product').get('length').value / 2);
    } else {
      orderForm.get('product').get('options').get('blindsPanelSize1').setValue(orderForm.get('product').get('length').value - (orderForm.get('product').get('poleData').get('p1y').value || orderForm.get('product').get('poleData').get('p2y').value) - (2 * 120));
      orderForm.get('product').get('options').get('blindsPanelSize2').setValue(0);
    }
  }

  constructor() {
    super();
  }

  get $canEdit(): boolean {
    return (this.$stepNumber === 1 || this.$stepNumber === 2) && !this.spOrigin;
  }

  get $canCancel(): boolean {
    return (this.$stepNumber === 1 || this.$stepNumber === 2) && !this.spOrigin;
  }

  get $canConfirm(): boolean {
    return (this.$stepNumber === 1) && !this.spOrigin;
  }

  get previewImgCode(): string {
    return Order.genPreviewImgCode(this.product.pergolaType, this.product.installationType);
  }

  confirm() {
    if (this.$step === OrderStep.S1) {
      this.step = 'S2';
    } else {
      throw new Error('Order ' + this.id + ' already confirmed or canceled.');
    }
  }

  cancel() {
    if (this.$step === OrderStep.S1 || this.$step === OrderStep.S2 || this.$step === OrderStep.S3) {
      this.step = 'S0';
    } else {
      throw new Error('Order ' + this.id + ' can not be canceled.');
    }
  }

  public getSaveObj(): any {
    const x = serialize(this);
    if (this.deliverySameAsClient) {
      delete x.delivery;
    }
    delete x.owner;
    return x;
  };

  public niceName(): string {
    return `# ${ this.number }`;
  }

  static get validator() {
    return undefined;
  }

  toForm() {
    const x = classToForm(this, Order.validator);

    // you can add extra stuff here if needed

    return x;
  }

  toClass(form: FormGroup): Order {
    const x = formToClass(form, this);
    x.comment = this.comment;

    // TODO check for any other non reactive elements!

    return x;
  }
}
