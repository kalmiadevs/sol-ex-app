export const TabNameMap = {
  0: 'Client Information',
  1: 'Product Information',
  2: 'Specifications',
  3: 'Options',
  4: 'Accessories',
  5: 'Comments',
  6: 'Terms'
};

export const TabNameMapPublic = {
  0: 'Product Information',
  1: 'Specifications',
  2: 'Options',
  3: 'Accessories',
  4: 'Comments',
  5: 'Terms'
};

export const AnalyticsEvents = {
  ConfiguratorTabChanged: 'Configurator Tab'
};
