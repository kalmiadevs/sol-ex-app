import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OrderCompleteComponent } from './order-complete/order-complete.component';

@NgModule({
  imports: [
    CommonModule,
  ],
  providers: [],
  declarations: [
    OrderCompleteComponent
  ],
  exports: [
    OrderCompleteComponent
  ]
})
export class ExternalOrderModule {}
