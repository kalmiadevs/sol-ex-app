import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BaseGroupService } from '../base/entity/group/svc.group';
import { ProjectBarComponent } from '../project-bar/cmp.project-bar';
import { OrderComponent } from './order/cmp.order';
import { OrderService } from './svc.order';
import { MediaComponent } from './media/cmp.media';
import {OrderGuard} from './guard.order';

export const routes: Routes = [
  { path: '', component: OrderComponent, canDeactivate: [ OrderGuard ] },
  {
    path: '',
    outlet: 'sidebar',
    component: ProjectBarComponent
  },
  {
    path: ':id',
    component: OrderComponent,
    canActivate: [  ],
    resolve: {
      order: OrderService,
    }
  },
  {
    path: ':id/media',
    component: MediaComponent,
    canActivate: [  ],
    resolve: {
      order: OrderService,
    }
  },
  {
    path: ':id/print',
    component: OrderComponent,
    canActivate: [  ],
    resolve: {
      order: OrderService
    },
    data: {
      print: true
    }
  },
  {
    path: ':id/edit',
    component: OrderComponent,
    canActivate: [  ],
    resolve: {
      order: OrderService
    },
    data: {
      edit: true
    }
  },
  {
    path: ':id/edit/:step',
    component: OrderComponent,
    canActivate: [  ],
    resolve: {
      order: OrderService
    },
    data: {
      edit: true
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class OrderRoutingModule { }
