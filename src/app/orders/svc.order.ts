import { Injectable } from '@angular/core';
import { BaseNodeService } from '../base/entity/node/svc.node';
import { Order } from './cls.order';
import { Entities } from '../base/entity/itf.entities';
import { HttpAuthService } from '../base/core/http/svc.http-auth';
import { BaseSystemService } from '../base/core/svc.system';
import { SystemService } from '../core/svc.system';
import { environment } from '../../environments/environment';
import { chain as _chain, map as _map } from 'lodash';
import { EntityState } from '../base/entity/cls.entity-state';
import { HttpError } from '../base/core/cls.error';
import { EntityApplyRemoteResponse } from '../base/entity/itf.entity-remote-response';
import { Angulartics2 } from 'angulartics2';

@Injectable()
export class OrderService extends BaseNodeService<Order>  {

  protected apiEntityName      = `orders`;
  protected apiEntitiesName    = `orders`;
  protected apiBase            = `${ environment.apiBaseProject }`;

  constructor(
    protected http: HttpAuthService,
    protected analytics: Angulartics2
  ) {
    super(http);
  }

  public init(sys: SystemService): any {
    super.init(sys);
  }

  newNodeInstance(): Order {
    return Order.createNew();
  }

  protected convertRemoteResToEntities(data: any): Promise<Order[]> {
    return new Promise((resolve, reject) => {
      resolve(this.objToOrders(data.orders));
    });
  }

  protected convertRemoteResToEntity(data: any): Promise<Order> {
    return new Promise((resolve, reject) => {
      resolve(Order.createFromApi(data.order));
    });
  }

  /**
   * Convert remote object into Order
   * @param data
   */
  private objToOrders(data: any): Order[] {
    return _chain(data)
      .map(x => Order.createFromApi(x))
      .value();
  }

  confirm(id: string): Promise<boolean> {
    return new Promise((resolve, reject) => {
      this.http.put(`${ environment.apiBaseProject }/orders/${ id }/confirm`, {})
        .subscribe(res => {
          this.store.entities[id].confirm();
          this.notifyEntitiesChange();
          resolve(true);
        }, e => {
          this.sys.onError(new HttpError(e));
          reject(e);
        });
    });
  }

  cancel(id: string): Promise<boolean> {
    return new Promise((resolve, reject) => {
      this.http.put(`${ environment.apiBaseProject }/orders/${ id }/cancel`, {})
        .subscribe(res => {
          this.store.entities[id].cancel();
          this.notifyEntitiesChange();
          resolve(true);
        }, e => {
          this.sys.onError(new HttpError(e));
          reject(e);
        });
    });
  }

  suggestClients(): Promise<object> {
    return new Promise((resolve, reject) => {
      this.http.get(`${ environment.apiBaseProject }/orders/suggestions/clients`, {})
        .subscribe(res => {
          resolve(res);
        }, e => {
          reject(e);
        });
    });
  }

  suggestDistributors(): Promise<object> {
    return new Promise((resolve, reject) => {
      this.http.get(`${ environment.apiBaseProject }/orders/suggestions/distributors`, {})
        .subscribe(res => {
          resolve(res);
        }, e => {
          reject(e);
        });
    });
  }

  suggestSalesManagers(): Promise<object> {
    return new Promise((resolve, reject) => {
      this.http.get(`${ environment.apiBaseProject }/orders/suggestions/salesManagers`, {})
        .subscribe(res => {
          resolve(res);
        }, e => {
          reject(e);
        });
    });
  }

  generateAndSaveDemoOrders(x: number) {
    for (let i = 0; i < x; i++) {
      const o                = Order.createNew();
      o.id                   = (<any>[1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, c => (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)); // tslint:disable-line
      o.step                 = <any>`S${ Math.floor(Math.random() * 5) }`;
      o.step                 = 'S1';
      o.comment              = `No comment.`;
      o.deliverySameAsClient = true;
      o.client.name          = `KALMIA DEV`;
      o.client.address       = `KALMIA DEV Mestni trg 33`;
      o.client.phone         = `1234567890`;
      o.client.email         = `tilen.tomakic@kalmia.si`;
      o.project.name         = `KALMIA DEV TEST PROJECT #${ i }`;
      o.product.pergolaType  = 'SL 160/28';
      o.product.pergolaType  = 'SL 170/36';
      o.product.pergolaType  = 'SL 170/28';
      o.project.date         = ''; // new Date(new Date(2012, 0, 1).getTime() + Math.random() * (new Date().getTime() - new Date(2012, 0, 1).getTime()));
      o.product.width = 1000;
      o.product.height = 2500;
      o.product.length = 2500;
      // o.product.customPoleHeight = 10;
      o.product.specifications.mountingGroundQuantity = 11;
      o.product.specifications.ledLength = 11;
      o.product.specifications.motorPos = 'a';
      o.product.specifications.ledType = 'rgb';
      o.terms.agree = true;
      this.save(o).then(console.log).catch(console.error);

      // o.$state = EntityState.Pristine;
      // this.addEntity(o);
    }
  }

  public remove(id: string): Promise<EntityApplyRemoteResponse<Order>> {
    // Mark as removed. So it will be detected by sync operation and removed from remote.
    this.store.entities[id].$state = EntityState.Removed;
    this.store.entities[id].$locked = false;

    this.notifyEntitiesChange();
    return this.applyToRemote([this.store.entities[id]])[0];
  }

  transferOwnership(id: string, newOwnerData: { type: 'salesManager' | 'distributor', userId: string }, silent = false): Promise<boolean> {
    return new Promise((resolve, reject) => {
      this.http.put(`${ environment.apiBaseProject }/orders/${ id }/transfer`, newOwnerData)
        .subscribe(res => {
          this.notifyEntitiesChange();
          resolve(true);
        }, e => {
          if (!silent) {
            this.sys.onError(new HttpError(e));
          }
          reject(e);
        });
    });
  }

  public emitAnalyticsEvent(action: string, value: string) {
    this.analytics.eventTrack.next({
      action: value,
      properties: {
        category: action,
        value,
        label: value
      }
    });
  }
}
