import 'reflect-metadata';
import { FormControl, FormGroup, FormArray, ValidatorFn } from '@angular/forms';
import { isArray } from 'lodash';

const META_DATA_KEY  = 'FormProperty';

/*
*
* ReactiveFormMapper
*
* class A {
*   @RfBuilder([optionalValidator])
*   name: string;
* }
*
* # from class to reactive form
* reactiveFormObj = ReactiveFormMapper.toForm(A)
* -- or --
* const a = new A();
* reactiveFormObj = ReactiveFormMapper.toForm(a)
*
* # from reactive form back to class
* a = ReactiveFormMapper.toClass(reactiveFormObj)
*
* */

export function formToNewClass<T>(form: FormGroup, Cls: {new(): T}): T {
  return formToClass(form, new Cls());
}


export function formToClass<T>(form: FormGroup, instance: T, options = {}): T {
  Object.keys(instance).forEach(key => {
    const val = Reflect.getMetadata(META_DATA_KEY, instance, key);
    if (val) {
      if (val.cls) {
        if (val.isArray) {
          for (let i = 0; i < form.controls[key].value.length; i++) {
            formToClass((form.controls[key] as FormArray).controls[i] as FormGroup, instance[key][i], { isArrayItem: true });
          }
        } else {
          formToClass(form.controls[key] as FormGroup, instance[key]);
        }
      } else {
        instance[key] = form.controls[key].value === null ? undefined : form.controls[key].value;
      }
    }
  });

  return instance;
}

export function classToForm(instance: Object, validator?: ValidatorFn): FormGroup {
  const obj: any = {};

  if (instance) {
    Object.keys(instance).forEach(key => {
      const val = Reflect.getMetadata(META_DATA_KEY, instance, key);
      if (val) {
        // console.log(key, val);
        if (val.cls) {
          if (val.isArray) {
            obj[key] = new FormArray((instance[key] || []).map(x =>
              classToForm(x, val.itemValidator)
            ), val.clsValidator);
          } else {
            obj[key] = classToForm(instance[key], val.validator);
          }
        } else {
          if (val.isArray) {
            obj[key] = new FormArray((instance[key] || []).map(x =>
              new FormControl(x, val.itemValidators)
            ), val.clsValidator);
          } else {
            obj[key] = new FormControl(instance[key], val.validators);
          }
        }
      }
    });
  }
  return new FormGroup(obj, validator);
}

export function FormClass<T>(opt: any): (target: Object, targetKey: string | symbol) => void {
  let cls;
  let validator;
  if (isArray(opt)) {
    validator = opt[1];
    cls = opt[0];
  } else {
    cls = opt || true;
  }
  return Reflect.metadata(META_DATA_KEY, { cls, validator });
}

export function FormProperty<T>(validators?: any | any[]): (target: Object, targetKey: string | symbol) => void {
  if (validators && !isArray(validators)) {
    validators = [ validators ];
  }
  return Reflect.metadata(META_DATA_KEY, { validators: validators });
}

export function FormPropertyArray<T>(validators?: any | any[]): (target: Object, targetKey: string | symbol) => void {
  let itemValidators;
  let clsValidator;

  if (validators && !isArray(validators)) {
    validators = [undefined, [validators]];
  }

  clsValidator   = validators[0];
  itemValidators = validators[1];

  return Reflect.metadata(META_DATA_KEY, { isArray: true, itemValidators, clsValidator });
}

export function FormArrayClass<T>(opt: any): (target: Object, targetKey: string | symbol) => void {
  let cls;
  let itemValidator;
  let clsValidator;
  if (isArray(opt)) {
    cls = opt[0];
    clsValidator = opt[1];
    itemValidator = opt[2];
  } else {
    cls = opt || true;
  }
  return Reflect.metadata(META_DATA_KEY, { isArray: true, cls, clsValidator, itemValidator });
}
