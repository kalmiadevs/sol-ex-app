import {Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';
import {BaseNodeService} from '../../base/entity/node/svc.node';
import {SystemService} from '../../core/svc.system';
import {HttpAuthService} from '../../base/core/http/svc.http-auth';
import {Article} from './cls.article';
import {chain as _chain} from 'lodash';

@Injectable()
export class ArticleService extends BaseNodeService<Article>  {

  protected apiEntityName      = `articles`;
  protected apiEntitiesName    = `articles`;
  protected apiBase            = `${ environment.apiBaseProject }`;

  constructor(
    protected http: HttpAuthService
  ) {
    super(http);
  }

  public init(sys: SystemService): any {
    super.init(sys);
  }

  newNodeInstance(): Article {
    return Article.createNew();
  }

  protected convertRemoteResToEntities(data: any): Promise<Article[]> {
    return new Promise((resolve, reject) => {
      resolve(this.objToArticles(data.articles));
    });
  }

  protected convertRemoteResToEntity(data: any): Promise<Article> {
    return new Promise((resolve, reject) => {
      resolve(Article.createFromApi(data.article));
    });
  }

  /**
   * Convert remote object into Articles
   * @param data
   */
  private objToArticles(data: any): Article[] {
    return _chain(data)
      .map(x => Article.createFromApi(x))
      .value();
  }
}
