import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {NewsArticleComponent} from './article/cmp.news-article';
import {ArticleService} from './svc.article';
import {NewsListComponent} from './cmp.news';

export const routes: Routes = [
  { path: '', component: NewsListComponent },
  { path: 'article', component: NewsArticleComponent },
  {
    path: 'article/:id',
    component: NewsArticleComponent,
    canActivate: [  ],
    resolve: {
      article: ArticleService,
    }
  },
  {
    path: 'article/:id/edit',
    component: NewsArticleComponent,
    canActivate: [  ],
    resolve: {
      article: ArticleService
    },
    data: {
      edit: true
    }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class NewsRoutingModule { }
