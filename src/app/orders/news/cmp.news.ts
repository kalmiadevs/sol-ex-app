import { Component, OnInit } from '@angular/core';
import { I18nService, _ } from '../../base/core/i18n/svc.i18n';
import {ArticleService} from './svc.article';
import {EntitiesComponent} from '../../base/entity/cmp.entities';
import {Article} from './cls.article';
import {Entities} from '../../base/entity/itf.entities';
import {environment} from '../../../environments/environment';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {AuthService} from '../../base/core/auth/svc.auth';
import {ActivatedRoute, Router} from '@angular/router';
import {SystemService} from '../../core/svc.system';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {Location} from '@angular/common';
import {BaseSystemService} from '../../base/core/svc.system';
import {OrderService} from '../svc.order';
import {mapValues as _mapValues, debounce as _debounce, reduce as _reduce} from 'lodash';

@Component({
  selector: 'app-news-list',
  templateUrl: './tpl.news.html',
  styleUrls: ['./cmp.news.scss']
})
export class NewsListComponent extends EntitiesComponent<Article, ArticleService> implements OnInit {

  get articles(): Entities<Article> {
    return this.entities;
  };

  isAdmin = false;
  isSuperAdmin = false;

  searchInput: string;
  loading: boolean;

  env = environment.env;

  paginationEnabled = true;

  sortBy: string = 'birth';
  reverseSort: boolean = true;

  filters = {
    title: '',
    content: '',
  };

  entitiesProvider: BehaviorSubject<Entities<Article>> = new BehaviorSubject<Entities<Article>>({});

  _totalEntitiesCount: 0;

  _loadDataDebounced: any;

  get totalEntitiesCount(): number {
    return this._totalEntitiesCount;
  }

  constructor(
    protected authService: AuthService,
    protected location: Location,
    protected articleService: ArticleService,
    protected route: ActivatedRoute,
    protected sys: SystemService,
    protected router: Router,
    protected modalService: NgbModal,
    private i18n: I18nService) {
    super(location, route, router, sys, articleService, modalService);
    this.init();
  }

  subscribeToEntities() {
    super.subscribeToEntities(this.entitiesProvider.asObservable());
  }

  ngOnInit() {
    this.loadData();
    this._loadDataDebounced = _debounce(this.loadData, 250);

    this.isAdmin = this.authService.getJwtPayload().grp === 'admin';
    this.isSuperAdmin = this.authService.getJwtPayload().grp === 'superadmin';
  }

  trySelectEntityOnCtrl(event, id) {
    // do nothing
  }

  loadData() {
    this.loading = true;

    const options: any = {
      pagination: {
        start: this.selectedPageIndex * this.pageSize,
        items: this.pageSize
      },
      aggregate: false,
    };

    const filter: any = _reduce(this.filters, (a, v, k) => v ? {...a, [k]: v} : a, {});

    if (Object.keys(filter).length) {
      options.filter = filter;
    }

    if (this.sortBy) {
      options.sort = {
        by: this.sortBy,
        order: this.reverseSort && 'desc' || 'asc'
      };
    } else {
      options.sort = {
        by: 'birth',
        order: 'desc'
      };
    }

    if (this.searchInput) {
      options.search = _mapValues(this.filters, x => this.searchInput);
    }

    this.articleService.loadEntitiesFromRemote(options)
      .then(x => {
        this.loading = false;
        this.displayedEntities = [];

        this._totalEntitiesCount = x.data.total || 0;
        this.entitiesProvider.next(x.entities);
        this.allEntities       = x.keys;
        this.displayedEntities = x.keys;
      })
      .catch(e => {
        this.displayedEntities = [];
        this.loading = false;
        this.sys.tryHandleHttpError(e);
      });
  }

  onEntitiesChange(entities: Entities<Article>) {
    this.entities    = entities;
    this.allEntities = Object.keys(entities);
    this.displayedEntities = this.allEntities;
  }

  setPageSize(size: number) {
    this.pageSize = size;
    this.loadData();
  }

  setSort(name: string, reverse: boolean = this.reverseSort) {
    if (name === this.sortBy && reverse === this.reverseSort) {
      // reset
      this.reverseSort = true;
      this.sortBy      = 'birth';
    } else {
      this.reverseSort = reverse;
      this.sortBy      = name;
    }
    this.sort();
  }

  onSelectedPageChange() {
    this.loadData();
  }

  search() {
    this._loadDataDebounced();
  }

  sort() {
    this.loadData();
  }

  filter() {
    this._loadDataDebounced();
  }
}
