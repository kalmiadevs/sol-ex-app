import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {BaseSharedModule} from '../../base/shared/mod.shared';
import {ValidationModule} from '../../base/shared/form/validation/mod.validation';
import {NewsDropdownComponent} from './dropdown/cmp.news-dropdown';
import {NewsArticleComponent} from './article/cmp.news-article';
import {NewsListComponent} from './cmp.news';
import {ArticleService} from './svc.article';
import {EntityModule} from '../../base/entity/mod.entity';
import {ReactiveFormsModule} from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    NgbModule,
    BaseSharedModule,
    ValidationModule,
    EntityModule,
    ReactiveFormsModule,
  ],
  providers: [
    ArticleService,
  ],
  declarations: [
    NewsListComponent,
    NewsArticleComponent,
    NewsDropdownComponent
  ],
  exports: [
    NewsDropdownComponent
  ]
})
export class NewsModule {
}
