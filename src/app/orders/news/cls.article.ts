import {FormGroup, Validators} from '@angular/forms';
import {OrderApiResponse} from '../itf.order-api-response';
import {classToForm, FormClass, FormProperty, formToClass} from '../ng-reactive-form-mapper';
import {_} from '../../base/core/i18n/svc.i18n';
import {EntityState} from '../../base/entity/cls.entity-state';
import {BaseNode} from '../../base/entity/node/cls.node';
import {deserialize, JsonProperty, serialize} from 'json-typescript-mapper';

export class Article extends BaseNode {

  @JsonProperty('id')
  id: string = void 0;
  @JsonProperty('owner')
  owner: string = void 0;

  @JsonProperty('birth')
  birth: Date = void 0;

  @JsonProperty('title')
  @FormProperty([Validators.required])
  title: string = void 0;
  @JsonProperty('content')
  @FormProperty([Validators.required])
  content: string = void 0;

  static createNew(): Article {
    const x = new Article();
    x.$state = EntityState.New;
    return x;
  }

  static createFromApi(json: any): Article {
    return deserialize(Article, json);
  }

  constructor() {
    super();
  }

  public getSaveObj(): any {
    const x = serialize(this);
    return x;
  };

  public niceName(): string { return `${ this.title }`; }

  static get validator() { return undefined; }

  toForm() {
    return classToForm(this, Article.validator);
  }

  toClass(form: FormGroup): Article {
    return formToClass(form, this);
  }
}
