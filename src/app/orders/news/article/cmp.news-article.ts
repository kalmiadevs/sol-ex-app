import {Component, OnDestroy, OnInit} from '@angular/core';
import { I18nService, _ } from '../../../base/core/i18n/svc.i18n';
import {FormGroup} from '@angular/forms';
import {OrderService} from '../../svc.order';
import {EntityComponent} from '../../../base/entity/cmp.entity';
import {Order} from '../../cls.order';
import {Article} from '../cls.article';
import {ArticleService} from '../svc.article';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {SystemService} from '../../../core/svc.system';
import {AuthService} from '../../../base/core/auth/svc.auth';
import {Location} from '@angular/common';
import {ActivatedRoute, Router} from '@angular/router';
import {HttpAuthService} from '../../../base/core/http/svc.http-auth';
import {HttpError} from '../../../base/core/cls.error';
import {SimpleNotification} from '../../../base/app/notifications';
import {EntityState} from '../../../base/entity/cls.entity-state';
import {FsDir, FsFile} from '../../../base/entity/file-system/cls.fs-node';
import {ActionConfirmationModalComponent} from '../../cmp.action-confirmation-modal';

@Component({
  selector: 'app-news-article',
  templateUrl: './tpl.news-article.html',
  styleUrls: ['./cmp.news-article.scss']
})
export class NewsArticleComponent extends EntityComponent<Article, ArticleService> implements OnInit, OnDestroy {

  entityDataResolver = 'article';
  entityRouteName    = 'news/article';

  articleForm: FormGroup;

  isSuperAdmin = false;

  updating: boolean = false;

  actionConfirmationModalDialog: any = ActionConfirmationModalComponent;

  get article(): Article { return this.entity; }

  constructor(
    protected location: Location,
    protected route: ActivatedRoute,
    protected router: Router,
    protected articleService: ArticleService,
    protected sys: SystemService,
    protected modalService: NgbModal,
    protected http: HttpAuthService,
    protected authService: AuthService,
    private i18n: I18nService,
  ) {
    super(location, route, router, sys, articleService, modalService);
  }

  ngOnInit() {
    super.onInit();
    this.isSuperAdmin = this.authService.getJwtPayload().grp === 'superadmin';
  }

  ngOnDestroy() {
    super.onDestroy();
  }

  onEntityChange() {
    super.onEntityChange();
    this.articleForm = this.article.toForm();
  }

  destroy() {
    super.remove();
  }

  save(f) {
    this.entity = this.entity.toClass(this.articleForm);
    super.save(f);
  }

  back() {
    this.router.navigate(['/news']);
  }

  onSave() {
    super.onSave();
    this.updating = true;

    this.articleService.loadEntityFromRemote(this.article.id)
      .then(x => {
        this.setEntity(x.entity);
        this.sys.notificationService.push(new SimpleNotification('success', this.i18n.get('articleSaveNotificationSuccessTitle', ), this.i18n.get('articleSaveNotificationSuccessContent')).setTimeout(7500));
        this.updating = false;
      })
      .catch(x => {
        this.updating = false;
      })
  }

  onSaveFail(e) {
    if (e.error instanceof HttpError && e.error.httpResponse.status === 409) {
      this.saving = false;
      this.sys.notificationService.push(new SimpleNotification('danger', this.i18n.get('articleSaveNotificationErrorTitle', ), this.i18n.get('articleSaveNotificationErrorContent')).setTimeout(7500));
    } else {
      super.onSaveFail(e);
    }
  }

  /* impl */
  createNewEntity(): Article {
    return Article.createNew();
  }

}
