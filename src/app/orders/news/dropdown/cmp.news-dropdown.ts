import { Component, OnInit } from '@angular/core';
import { I18nService, _ } from '../../../base/core/i18n/svc.i18n';
import {ArticleService} from '../svc.article';
import {SystemService} from '../../../core/svc.system';
import {AuthService} from '../../../base/core/auth/svc.auth';
import {AuthUser} from '../../../base/core/auth/cls.auth-user';

@Component({
  selector: 'app-news-dropdown',
  templateUrl: './tpl.news-dropdown.html',
  styleUrls: ['./cmp.news-dropdown.scss']
})
export class NewsDropdownComponent implements OnInit {

  private newsItems = 3;

  latestArticles = [];

  constructor(
    protected sys: SystemService,
    protected articleService: ArticleService,
    protected authService: AuthService
  ) {}

  ngOnInit() {
    this.authService.user.subscribe((user: AuthUser) => {
      if (!!user) {
        if (!user.isPublic()) {
          // Fetch recent news
          const options: any = {
            pagination: {
              start: 0,
              items: this.newsItems,
            },
            sort: {
              by: 'birth',
              order: 'desc',
            }
          };

          this.articleService.loadEntitiesFromRemote(options)
            .then(x => {
              this.latestArticles = x.data.articles;
            })
            .catch(e => {
              this.sys.tryHandleHttpError(e);
            });
        }
      } else {
        // clear latest articles
        this.latestArticles = [];
      }
    });
  }

}
