export class SVG {
  static setComponentStyle(component: any, id: string, property: string = 'fill', value: string | number = 'black') {
    if (component) {
      const $el = component.getElementById(id);
      const $g = $el && $el.children.length === 1 ? $el.children[0] : $el;

      if ($g) {
        $g.style[property] = value;
      }
    }
  }

  static getComponentStyle(component: any, id: string, property: string) {
    if (component) {
      const $el = component.getElementById(id);
      const $g = $el && $el.children.length === 1 ? $el.children[0] : $el;

      if ($g) {
        return $g.style[property];
      }
    }
  }

  static setComponentAttribute(component: any, id: string, attr: string = 'x', value: string | number = 0) {
    if (component) {
      const $el = component.getElementById(id);
      const $g = $el && $el.children.length === 1 ? $el.children[0] : $el;

      if ($g) {
        $g.setAttribute(attr, value);
      }
    }
  }

  static getComponentAttribute(component: any, id: string, attr: string) {
    if (component) {
      const $el = component.getElementById(id);
      const $g = $el && $el.children.length === 1 ? $el.children[0] : $el;

      if ($g) {
        return $g.getAttribute(attr);
      }
    }
  }
}
