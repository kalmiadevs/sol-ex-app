import { AfterViewChecked, AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { EntityComponent } from '../../base/entity/cmp.entity';
import { Order, OrderStep } from '../cls.order';
import { OrderService } from '../svc.order';
import { ActivatedRoute, Router } from '@angular/router';
import { SystemService } from '../../core/svc.system';
import { Location } from '@angular/common';
import { HttpError } from '../../base/core/cls.error';
import { NgbModal, NgbTabset } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, Validators } from '@angular/forms';
import { FsDir, FsFile } from '../../base/entity/file-system/cls.fs-node';
import { EntityState } from '../../base/entity/cls.entity-state';
import { environment } from '../../../environments/environment';
import { HttpAuthService } from '../../base/core/http/svc.http-auth';
import { AuthService } from '../../base/core/auth/svc.auth';
import { SimpleNotification } from '../../base/app/notifications/';
import { ActionConfirmationModalComponent } from '../cmp.action-confirmation-modal';
import { I18nService } from '../../base/core/i18n/svc.i18n';
import { getCookie } from '../../base/shared/tools/cookie';
import { AnalyticsEvents, TabNameMap, TabNameMapPublic } from '../cls.order-analytics';
import { TransferOwnershipModalComponent } from '../cmp.transfer-ownership-modal';

@Component({
  selector: 'app-order',
  templateUrl: './tpl.order.html',
  styleUrls: ['./cmp.order.scss']
})
export class OrderComponent extends EntityComponent<Order, OrderService> implements OnInit, AfterViewInit, OnDestroy, AfterViewChecked {

  entityDataResolver = 'order';
  entityRouteName = 'order';

  @ViewChild('tabSet') tabSet: NgbTabset;

  docs: FsDir = FsDir.createNew();

  selectedTab: number = 0;

  updating: boolean = false;

  printMode: boolean = true;

  _publicFormShowAll: boolean = false;

  get publicFormShowAll(): boolean {
    return this._publicFormShowAll;
  }

  set publicFormShowAll(x: boolean) {
    this._publicFormShowAll = x;
    this.tabSelect(0);
  }

  actionConfirmationModalDialog: any = ActionConfirmationModalComponent;
  transferOwnershipModalDialog: any = TransferOwnershipModalComponent;
  /**
   * We are using hybrid between template forms and reactive forms.
   * Since template forms can't validate if not rendered. But reactive forms
   * don't change directly state of order.
   *
   * For full transition to reactive forms proposed solution is state
   * update on tab change.
   */
  orderForm: FormGroup;

  isAdmin = false;
  isSuperAdmin = false;

  get editEnabled(): boolean {
    return !this.order.$isInProgress || this.isSuperAdmin
  }

  get order(): Order {
    return this.entity;
  }

  get canEdit(): boolean {
    return this.order.$canEdit;
  }

  get canRemove(): boolean {
    return !this.entity.$new && this.order.$canCancel;
  }

  constructor(
    protected location: Location,
    protected route: ActivatedRoute,
    protected router: Router,
    protected orderService: OrderService,
    protected sys: SystemService,
    protected modalService: NgbModal,
    protected http: HttpAuthService,
    protected authService: AuthService,
    private i18n: I18nService
  ) {
    super(location, route, router, sys, orderService, modalService);
  }

  isPublic(): boolean {
    return this.authService.getUser().isPublic();
  }

  ngOnInit() {
    super.onInit();
    this.isAdmin = this.authService.getJwtPayload().grp === 'admin';
    this.isSuperAdmin = this.authService.getJwtPayload().grp === 'superadmin';
    this.printMode = !!this.route.snapshot.data['print'];

    if (this.entity.$new) {
      if (this.authService.getJwtPayload().grp === 'user') {
        this.orderForm.get('terms').get('agree').setValue(false);
      }
    }

    if (!!this.orderForm && !!this.orderForm.get('client')) {
      this.orderForm.get('client').get('phone').clearValidators();
      // if (!this.isPublic()) {
      this.orderForm.get('client').get('phone').setValidators([Validators.required]);
      // }
    }

    this.onEditToggle.subscribe(state => {
      if (state) {
        this.selectedTab = 0;
        this.tabEmitAnalytics();
      }
    });
  }

  ngAfterViewChecked() {
    if (typeof (window as any).callPhantom === 'function') {
      (window as any).callPhantom({component: 'OrderComponent', type: 'ngAfterViewChecked'});
    }
  }

  ngOnDestroy() {
    super.onDestroy();
  }

  ngAfterViewInit(): void {
    // dev only
    // this.tabSet.select('ngb-tab-' + 2);

    if (typeof (window as any).callPhantom === 'function') {
      (window as any).callPhantom({component: 'OrderComponent', type: 'ngAfterViewInit'});
    }
  }

  onEntityChange() {
    super.onEntityChange();

    this.docs = FsDir.createNew();

    // SP MOCKUP
    (this.order.spFiles || []).forEach(x => {
      if (x.name) {
        const file = FsFile.createNew(x.name);
        file.$state = EntityState.Pristine;
        file.meta = x;
        file.size = undefined;
        this.docs.addChild(file);
      } else {
        console.warn('Bad file in order: ' + this.order.id, x);
      }
    });

    const isAdmin = ['superadmin'/*, 'admin'*/].indexOf(this.authService.getJwtPayload().grp) > -1;
    (this.order.documents || []).forEach(x => {
      if (!isAdmin && ['excel_orig'].indexOf(x.type) > -1) {
        return;
      }
      const file = FsFile.createNew(x.name);
      file.$state = EntityState.Pristine;
      file.meta = {local: true, path: x.path};
      file.size = undefined;
      this.docs.addChild(file);
    });

    if (!this.order.spOrigin) {
      this.orderForm = this.order.toForm();
      // this.updateOrderFormDisabledState();
      if (this.order.deliverySameAsClient) {
        this.orderForm.controls.delivery.disable();
      }
    } else {
      this.orderForm = undefined;
    }
  }

  updateOrderFormDisabledState() {
    if (this.order.$step === OrderStep.S3 ||
      this.order.$step === OrderStep.S4 ||
      this.order.$step === OrderStep.S0) {
      this.orderForm.disable();
    } else {
      this.orderForm.enable();
    }
    this.orderForm.updateValueAndValidity();
  }

  onFileClick(x: FsFile) {
    if (x.meta.local) {
      this.http.downloadFile(
        `${ environment.apiBaseProject }/orders/${ this.order.id }/documents?file=${ encodeURIComponent(x.meta.path) }`,
        x.name
      );
    } else {
      // window.open(x.meta.file);
      this.http.downloadFile(
        `${ environment.apiBaseProject }/orders/${ this.order.id }/spDocuments?file=${ encodeURIComponent(`${ x.meta.id }/${ x.name }`) }`,
        x.name
      );
    }
  }

  isClientInformationValid(): boolean {
    if (!this.orderForm.controls.client.touched && !this.orderForm.get('project').get('name').touched) {
      return true;
    }
    return this.orderForm.controls.client.valid &&
      (this.orderForm.controls.delivery.valid || this.order.deliverySameAsClient) &&
      (this.orderForm.controls.project as FormGroup).controls.name.valid;
  }

  isProductInformationValid(): boolean {
    if (!this.orderForm.controls.product.touched) {
      return true;
    }
    for (const x of Object.keys((this.orderForm.controls.product as FormGroup).controls)) {
      if (x !== 'specifications' &&
        x !== 'options' &&
        x !== 'accessories' &&
        x !== 'poleData' &&
        x !== 'additionalPoleData') {
        if (!(this.orderForm.controls.product as FormGroup).get(x).valid) {
          return false;
        }
      }
    }
    return true;
  }

  customPoleCountSelected() {
    if (this.isPublic()) {
      this.publicFormShowAll = true;
      this.orderForm.get('product').get('specifications').markAsTouched();
      this.orderForm.updateValueAndValidity();
    }
  }

  isProductSpecificationsValid(): boolean {
    if (!(this.orderForm.controls.product as FormGroup).controls.specifications.touched && !this.orderForm.controls.product.touched) {
      return true;
    }
    return ((this.orderForm.controls.product as FormGroup).controls.specifications as FormGroup).valid &&
      ((this.orderForm.controls.product as FormGroup).controls.poleData as FormGroup).valid &&
      ((this.orderForm.controls.product as FormGroup).controls.additionalPoleData as FormGroup).valid;
  }

  isProductOptionsValid(): boolean {
    return ((this.orderForm.controls.product as FormGroup).controls.options as FormGroup).valid;
  }

  isProductAccessoriesValid(): boolean {
    return ((this.orderForm.controls.product as FormGroup).controls.accessories as FormGroup).valid;
  }

  isOrderCommentsValid(): boolean {
    return true;
  }

  isOrderTermsValid(): boolean {
    if (!this.orderForm.controls.terms.touched) {
      return true;
    }
    return ((this.orderForm.controls.terms as FormGroup).controls.agree as FormGroup).valid;
  }

  remove() {
    this.entityService.cancel(this.entity.id)
      .then(x => {
        this.entity = this.entityService.getEntity(this.order.id);
        this.sys.notificationService.push(new SimpleNotification('success', this.order.number, this.i18n.get('orderCancelNotificationSuccess')).setTimeout(5000));
        this.cancelEdit();
      })
  }

  removeWithConfirmDialog() {
    const modalRef = this.modalService.open(this.actionConfirmationModalDialog);
    modalRef.componentInstance.title = this.i18n.get('orderCancelModalTitle', [this.order.number]);
    modalRef.componentInstance.content = this.i18n.get('orderCancelModalContent');
    modalRef.componentInstance.confirmationButtonText = this.i18n.get('orderCancelModalButtonText');
    modalRef.result
      .then(modalRes => {
        this.updating = true;

        this.entityService.cancel(this.entity.id)
          .then(x => {
            this.entity = this.entityService.getEntity(this.order.id);
            this.sys.notificationService.push(new SimpleNotification('success', this.order.number, this.i18n.get('orderCancelNotificationSuccess')).setTimeout(5000));
            this.cancelEdit();
            this.updating = false;
          })
          .catch(x => {
            this.updating = false;
          })
      })
      .catch(e => {
      });
  }

  destroy() {
    super.remove();
  }

  save(f) {
    const isNew = this.entity.$new;

    this.entity = this.entity.toClass(this.orderForm);

    if (isNew && this.authService.getUser().isPublic()) {
      const lang = getCookie('locale');
      if (lang) {
        this.entity.languageOverride = lang;
      }
    }
    super.save(f);
  }

  onSave() {
    super.onSave();

    if (!this.isPublic()) {
      this.updating = true;

      this.orderService.loadEntityFromRemote(this.order.id)
        .then(x => {
          this.setEntity(x.entity);
          this.sys.notificationService.push(new SimpleNotification('success', this.i18n.get('orderSaveNotificationSuccessTitle', [this.order.number]), this.i18n.get('orderSaveNotificationSuccessContent')).setTimeout(7500));
          this.updating = false;
        })
        .catch(x => {
          this.updating = false;
        })
    } else {
      this.sys.notificationService.push(new SimpleNotification('success', this.i18n.get('orderPublicSaveNotificationSuccessTitle'), this.i18n.get('orderPublicSaveNotificationSuccessContent')).setTimeout(7500));
      this.router.navigate(['/order-complete'])
        .then(() => {
          this.sys.logout();
        });
    }
  }

  onSaveFail(e) {
    if (e.error instanceof HttpError && e.error.httpResponse.status === 409) {
      this.saving = false;
      this.sys.notificationService.push(new SimpleNotification('danger', this.i18n.get('orderSaveNotificationConflictTitle', [this.order.number]), this.i18n.get('orderSaveNotificationConflictContent')).setTimeout(7500));
    } else {
      super.onSaveFail(e);
    }
  }

  tabEmitAnalytics() {
    if (!this.printMode) {
      const tabName = this.isPublic() ? TabNameMapPublic[this.selectedTab] : TabNameMap[this.selectedTab];
      this.orderService.emitAnalyticsEvent(AnalyticsEvents.ConfiguratorTabChanged, tabName);
    }
  }

  tabChange(e) {
    this.selectedTab = +e.nextId.split('-')[2];
    this.tabEmitAnalytics();
  }

  tabBack() {
    this.tabSelect(this.selectedTab - 1);
  }

  tabNext() {
    this.tabSelect(this.selectedTab + 1);
  }

  tabSelect(tab: number) {
    if (this.tabSet) {
      this.tabSet.select('ngb-tab-' + (tab));
    }
  }

  createNewEntity(): Order {
    return Order.createNew();
  }

  confirm() {
    const modalRef = this.modalService.open(this.actionConfirmationModalDialog);
    modalRef.componentInstance.title = this.i18n.get('orderConfirmModalTitle', [this.order.number]);
    modalRef.componentInstance.content = this.i18n.get('orderConfirmModalContent');
    modalRef.componentInstance.confirmationButtonText = this.i18n.get('orderConfirmModalButtonText');
    modalRef.result
      .then(modalRes => {
        this.updating = true;

        this.orderService.confirm(this.order.id)
          .then(x => {
            this.entity = this.entityService.getEntity(this.order.id);
            this.sys.notificationService.push(new SimpleNotification('success', this.order.number, this.i18n.get('orderConfirmNotificationSuccess')).setTimeout(5000));
            this.updating = false;
          })
          .catch(x => {
            this.updating = false;
          })
      })
      .catch(e => {
      });
  }

  transferOwnership() {
    const modalRef = this.modalService.open(this.transferOwnershipModalDialog);
    modalRef.result
      .then(modalRes => {
        this.updating = true;

        this.entityService.transferOwnership(this.entity.id, modalRes)
          .then(x => {
            this.entity = this.entityService.getEntity(this.order.id);
            this.sys.notificationService.push(new SimpleNotification('success', this.order.number, this.i18n.get('orderTransferNotificationSuccess')).setTimeout(5000));
            this.cancelEdit();
            this.back();
            this.updating = false;
          })
          .catch(x => {
            this.sys.notificationService.push(new SimpleNotification('danger', this.order.number, this.i18n.get('orderTransferNotificationError')).setTimeout(5000));
            this.updating = false;
          });
      })
      .catch(e => {
      });
  }

  refresh() {
    this.updating = true;

    this.orderService.loadEntityFromRemote(this.order.id)
      .then(x => {
        this.setEntity(x.entity);
        this.updating = false;
      })
      .catch(e => {
        this.updating = false;
      });
  }
}
