import {Component, Input, OnInit, ViewChild} from '@angular/core';
import { Order } from '../../cls.order';
import { FormGroup } from '@angular/forms';
import {AuthService} from '../../../base/core/auth/svc.auth';
import {SafeDatePipe} from '../../../base/shared/pip.date';
import {DatePipe} from '@angular/common';

@Component({
  selector: 'app-order-terms',
  templateUrl: './tpl.terms.html',
  styleUrls: ['./cmp.terms.scss']
})
export class OrderTermsComponent implements OnInit {

  @Input() order: Order;
  @Input() orderForm: FormGroup;

  get clientF(): FormGroup {
    return this.orderForm.get('client') as FormGroup;
  }

  get projectF(): FormGroup {
    return this.orderForm.get('project') as FormGroup;
  }

  get tF(): FormGroup {
    return this.orderForm.controls.terms as FormGroup;
  }

  @ViewChild('termsForm') form;

  constructor(protected authService: AuthService) { }

  isPublic(): boolean {
    return this.authService.getUser().isPublic();
  }

  ngOnInit() {
  }

  controlValid(control: string) {
    if (this.tF.controls[control]) {
      return !(this.tF.controls[control].errors && this.tF.controls[control].touched);
    }
    return false;
  }

  updateInformation() {
    this.projectF.get('name').setValue('Pergola Agava (' + this.clientF.get('email').value + ') - ' + new DatePipe('en-us').transform(new Date(), 'd.M.y'));
  }
}
