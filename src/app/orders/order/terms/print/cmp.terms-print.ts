import { Component, Input, OnInit } from '@angular/core';
import { Order } from '../../../cls.order';
import { RalToHex } from '../../cls.ral-colors';
import { FormGroup } from '@angular/forms';
import { OrderTabComponent } from '../../cmp.order-tab';

@Component({
  selector: 'app-order-terms-print',
  templateUrl: './tpl.terms-print.html',
  styleUrls: ['./cmp.terms-print.scss']
})
export class OrderTermsPrintComponent extends OrderTabComponent implements OnInit {

  @Input() order: Order;
  @Input() orderForm: FormGroup;

  ralToHex = RalToHex.hex;

  get form(): FormGroup {
    return this.orderForm.controls.product as FormGroup;
  }

  constructor() {
    super();
  }

  ngOnInit() {
  }
}
