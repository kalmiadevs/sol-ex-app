import { Component, Input, OnInit } from '@angular/core';
import { Order } from '../../../cls.order';
import { RalToHex } from '../../cls.ral-colors';
import { FormGroup } from '@angular/forms';
import { OrderTabComponent } from '../../cmp.order-tab';

@Component({
  selector: 'app-product-information-print',
  templateUrl: './tpl.product-information-print.html',
  styleUrls: ['./cmp.product-information-print.scss']
})
export class ProductInformationPrintComponent extends OrderTabComponent implements OnInit {

  @Input() order: Order;
  @Input() orderForm: FormGroup;

  ralToHex = RalToHex.hex;

  get form(): FormGroup {
    return this.orderForm as FormGroup;
  }

  constructor() {
    super();
  }

  ngOnInit() {
  }

  getControlValue(path: string) {
    return (this.form.get(path) ? this.form.get(path).value : undefined);
  }
}
