import { AfterViewChecked, AfterViewInit, Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { AdditionalPole, Order} from '../../cls.order';
import { RalToHex } from '../cls.ral-colors';
import { FormArray, FormGroup } from '@angular/forms';
import { OrderTabComponent } from '../cmp.order-tab';
import { range as _range, max as _max } from 'lodash';
import {
  glassPanelsLengths, guideQuantityMap, pergolaProperties, glassPanelPlacementDefaults,
  pergolaLengthReductionForPanels, slidingPanelLengths, slidingPanelPlacementDefaults
} from '../../cls.order-validators';
import { classToForm } from '../../ng-reactive-form-mapper';

@Component({
  selector: 'app-product-information',
  templateUrl: './tpl.product-information.html',
  styleUrls: ['./cmp.product-information.scss']
})
export class ProductInformationComponent extends OrderTabComponent implements OnInit, AfterViewInit, AfterViewChecked {

  @Input() order: Order;
  @Input() orderForm: FormGroup;

  @Output() customPoleCountSelected = new EventEmitter<any>();

  @ViewChild('productInformationForm') form;

  ralToHex = RalToHex.hex;

  get fp(): FormGroup {
    return this.orderForm.controls.product as FormGroup;
  }

  get pergolaProperties() {
    return pergolaProperties[this.fp.get('pergolaType').value];
  }

  constructor() {
    super();
  }

  ngOnInit() {
  }

  ngAfterViewChecked() {
    if (typeof (window as any).callPhantom === 'function') {
      (window as any).callPhantom({ component: 'ProductInformationComponent', type: 'ngAfterViewChecked' });
    }
  }

  ngAfterViewInit() {
    if (typeof (window as any).callPhantom === 'function') {
      (window as any).callPhantom({ component: 'ProductInformationComponent', type: 'ngAfterViewInit' });
    }
  }

  get height() {
    return this.fp.get('height').value;
  }

  get pergolaLengths() {
    return this.pergolaProperties.length.filter(x => this.fp.get('installationType').value < 6 || (this.fp.get('installationType').value === 6 && x / 4 >= 300));
  }

  pergolaTypeChange() {
    this.fp.get('installationType').setValue(1); // Set installation type to 1
    this.fp.get('specifications').get('ledPositions').setValue([]); // Reset LED positions (fixes SOL-33)
    this.pergolaInstallationChange();
    this.fp.updateValueAndValidity();
  }

  pergolaInstallationChange() {
    this.fp.get('numberOfPoles').setValue(this.pergolaProperties.poleConfigurations[this.fp.get('installationType').value - 1]);
    this.fp.get('poleData').get('p1y').setValue(0);
    this.fp.get('poleData').get('p2y').setValue(0);
    this.fp.get('poleData').get('p3y').setValue(0);
    this.fp.get('poleData').get('p4y').setValue(0);
    this.fp.get('length').setValue(this.pergolaLengths[0]);
    this.updatePoleCount();
  }

  updateLength() {
    this.fp.get('specifications').get('ledPositions').setValue([]);
    this.updateGlassPanels();
    this.updateSlidingPanels();
    Order.updateZIPBlinds(this.orderForm);

    const length = this.fp.get('length').value;
    if (length > 4100) {
      this.fp.get('accessories').get('transportBox1').setValue(0);
    }
    if (length > 6100) {
      this.fp.get('accessories').get('transportBox2').setValue(0);
    }
  }

  updateWidth() {
    this.updateGlassPanels();
    this.updateSlidingPanels();
  }

  get pergolaLength(): number {
    return (this.fp.get('length').value - pergolaLengthReductionForPanels);
  }

  get guidesPerPanelCount(): number[] { return guideQuantityMap[this.fp.get('options').get('glassPanelsQuantity').value]; }

  updateGlassPanels() {
    // Quantity
    this.fp.get('options').get('glassPanelsOnLength').get('guides').setValue(2);
    this.fp.get('options').get('glassPanelsOnLength').get('size').setValue(330);
    this.fp.get('options').get('glassPanelsOnLength').get('panelQuantity').setValue(2);

    this.fp.get('options').get('glassPanelsOnWidth').get('guides').setValue(2);
    this.fp.get('options').get('glassPanelsOnWidth').get('size').setValue(330);
    this.fp.get('options').get('glassPanelsOnWidth').get('panelQuantity').setValue(2);

    // Choose default position
    this.fp.get('options').get('glassPanelsOnLength').get('placement').setValue(glassPanelPlacementDefaults[this.fp.get('options').get('glassPanelsOnLength').get('panelQuantity').value][this.fp.get('options').get('glassPanelsOnLength').get('guides').value].slice());
    this.fp.get('options').get('glassPanelsOnWidth').get('placement').setValue(glassPanelPlacementDefaults[this.fp.get('options').get('glassPanelsOnWidth').get('panelQuantity').value][this.fp.get('options').get('glassPanelsOnWidth').get('guides').value].slice());
  }

  updateSlidingPanels() {
    // Quantity
    this.fp.get('options').get('slidingPanelOnLength').get('type').setValue('A');
    this.fp.get('options').get('slidingPanelOnLength').get('size').setValue(330);
    this.fp.get('options').get('slidingPanelOnLength').get('panelQuantity').setValue(1);

    this.fp.get('options').get('slidingPanelOnWidth').get('type').setValue('A');
    this.fp.get('options').get('slidingPanelOnWidth').get('size').setValue(330);
    this.fp.get('options').get('slidingPanelOnWidth').get('panelQuantity').setValue(1);

    // Choose default position
    this.fp.get('options').get('slidingPanelOnLength').get('placement').setValue(slidingPanelPlacementDefaults[this.fp.get('options').get('slidingPanelOnLength').get('panelQuantity').value][this.fp.get('options').get('slidingPanelOnLength').get('type').value === 'A' ? 1 : 2].slice());
    this.fp.get('options').get('slidingPanelOnWidth').get('placement').setValue(slidingPanelPlacementDefaults[this.fp.get('options').get('slidingPanelOnWidth').get('panelQuantity').value][this.fp.get('options').get('slidingPanelOnWidth').get('type').value === 'A' ? 1 : 2].slice());
  }

  updateAdditionalPoleCount() {
    if (this.fp.get('numberOfAdditionalPoles').valid) {
      const apdF = this.fp.get('additionalPoleData') as FormArray;

      // create new form array
      this.order.product.additionalPoleData = _range(
        this.fp.get('numberOfAdditionalPoles').value
      ).map(x => new AdditionalPole());
      const newApdF = classToForm(this.order.product).get('additionalPoleData') as FormArray;

      console.log(this.fp.get('numberOfAdditionalPoles').value);

      // populate existing form array (keep already existing poles, remove excess)
      let m = _max([newApdF.length, apdF.length]);
      _range(m).reverse().map(i => {
        if (i >= newApdF.length) { // handle overflow
          apdF.removeAt(i);
        }
      });

      m = _max([newApdF.length, apdF.length]);
      _range(m).map(i => {
        if (i >= apdF.length) { // add new poles
          apdF.push(newApdF.at(i));
        }
      })

      // console.log(this.fp.get('additionalPoleData')['controls']);
    }
  }


  updatePoleCount() {
    const poles = this.fp.get('numberOfPoles').value;
    if (Number(poles) === 4) {
      this.fp.get('poleData').get('p1').setValue(true);
      this.fp.get('poleData').get('p2').setValue(true);
      this.fp.get('poleData').get('p3').setValue(true);
      this.fp.get('poleData').get('p4').setValue(true);
    } else {
      this.fp.get('poleData').get('p1').setValue(false);
      this.fp.get('poleData').get('p2').setValue(false);
      this.fp.get('poleData').get('p3').setValue(false);
      this.fp.get('poleData').get('p4').setValue(false);

      this.customPoleCountSelected.emit();
    }

    this.fp.get('specifications').get('waterExit').get('custom').setValue(!Number(poles));

    this.fp.get('specifications').updateValueAndValidity();
  }

  controlValid(control: string): boolean {
    if (this.fp.controls[control]) {
      return !(this.fp.controls[control].errors /*&& this.fp.controls[control].touched*/);
    }
    return false;
  }

  get nonStandardPoleQuantity(): boolean {
    const poleQuantity = Number(this.fp.get('numberOfPoles').value);

    // tslint:disable:curly
    switch (this.fp.get('installationType').value) {
      case 1:
      case 6:
        if (poleQuantity !== 4) {
          return true;
        }
        break;
      case 2:
      case 4:
        if (poleQuantity !== 2) {
          return true;
        }
        break;
      case 3:
      case 5:
        if (poleQuantity !== 1) {
          return true;
        }
        break;
    }

    return false;
  }


  genCodeForType(x) {
    return Order.genPreviewImgCode(this.order.product.pergolaType, x);
  }
}
