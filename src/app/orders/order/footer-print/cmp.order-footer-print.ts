import { Component, Input, OnInit } from '@angular/core';
import { Order } from '../../cls.order';
import { RalToHex } from '../cls.ral-colors';
import { FormGroup } from '@angular/forms';
import { OrderTabComponent } from '../cmp.order-tab';
import * as moment from 'moment';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-order-footer-print',
  templateUrl: './tpl.order-footer-print.html',
  styleUrls: ['./cmp.order-footer-print.scss']
})
export class OrderFooterPrintComponent extends OrderTabComponent implements OnInit {

  @Input() order: Order;
  @Input() orderForm: FormGroup;

  ralToHex = RalToHex.hex;

  currentDate: string = moment().format('D.M.YYYY, kk:mm:ss');
  author: string;

  get form(): FormGroup {
    return this.orderForm as FormGroup;
  }

  constructor(protected activatedRoute: ActivatedRoute) {
    super();
  }

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe(params => {
      this.author = params['author']
    });
  }

  getControlValue(path: string) {
    return (this.form.get(path) ? this.form.get(path).value : undefined);
  }
}
