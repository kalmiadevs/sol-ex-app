import { Component, Input, OnInit } from '@angular/core';
import { Order } from '../../cls.order';
import { ActivatedRoute } from '@angular/router';
import { I18nService } from '../../../base/core/i18n/svc.i18n';

@Component({
  selector: 'app-order-comments',
  templateUrl: './tpl.comments.html',
  styleUrls: ['./cmp.comments.scss']
})
export class OrderCommentsComponent implements OnInit {

  @Input() order: Order;

  printMode: boolean;

  constructor(
    protected route: ActivatedRoute,
    private i18n: I18nService
  ) { }

  ngOnInit() {
    this.printMode = !!this.route.snapshot.data['print'];
  }

}
