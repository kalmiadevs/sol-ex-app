import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { Order } from '../../../cls.order';
import { RalToHex } from '../../cls.ral-colors';
import { FormGroup } from '@angular/forms';
import { SVG } from '../../cls.svg';

@Component({
  selector: 'app-product-options-zip-blinds',
  templateUrl: './tpl.zip-blinds.html',
  styleUrls: ['./cmp.zip-blinds.scss']
})
export class ProductOptionsZipBlindsComponent implements OnInit {

  @Input() order: Order;
  @Input() orderForm: FormGroup;

  @ViewChild('zipBlindsForm') form;

  ralToHex = RalToHex.hex;
  pergolaView: any;

  get fp(): FormGroup {
    return (this.orderForm.controls.product as FormGroup).controls.options as FormGroup;
  }

  get projectF(): FormGroup {
    return (this.orderForm.controls.product as FormGroup);
  }

  constructor() { }

  ngOnInit() { }

  controlValid(control: string) {
    if (this.fp.controls[control]) {
      return !(this.fp.controls[control].errors && this.fp.controls[control].touched);
    }
    return false;
  }

  isPergolaSideAvailable(side: string) {
    return !this.fp.get('glassPanelsPosition').get(side).value && !this.fp.get('slidingPanelPosition').get(side).value;
  }

  get showBlindsDimensions(): boolean {
    return this.fp.get('blindsPanelSize1').value && this.fp.get('blindsPanelSize2').value;
  }

  getPanelSplitData(position: string): any {
    if (this.fp.get('blindsPosition').get(position).value) {
      const len = this.projectF.get('length').value;
      const pole = this.projectF.get('additionalPoleData').value.find((x) => x.position === position);
      if (pole && pole.x) {
        return [pole.x, len - pole.x - (3 * 120)]
      } else if (position === 'p1p2' || position === 'p3p4') {
        if (len > 6000) {
          return [len / 2, len / 2];
        }
      }
    }

    return false;
  }

  refreshPergolaView() {
    if (this.fp) {
      // Disable other options on the same sides
      if (this.fp.get('blindsPosition').get('p1p2').value) {
        this.fp.get('slidingPanelPosition').get('p1p2').setValue(false);
        this.fp.get('glassPanelsPosition').get('p1p2').setValue(false);
      }
      if (this.fp.get('blindsPosition').get('p3p4').value) {
        this.fp.get('slidingPanelPosition').get('p3p4').setValue(false);
        this.fp.get('glassPanelsPosition').get('p3p4').setValue(false);
      }
      if (this.fp.get('blindsPosition').get('p1p3').value) {
        this.fp.get('slidingPanelPosition').get('p1p3').setValue(false);
        this.fp.get('glassPanelsPosition').get('p1p3').setValue(false);
      }
      if (this.fp.get('blindsPosition').get('p2p4').value) {
        this.fp.get('slidingPanelPosition').get('p2p4').setValue(false);
        this.fp.get('glassPanelsPosition').get('p2p4').setValue(false);
      }

      // Update SVG components
      SVG.setComponentStyle(this.pergolaView, 'side-p1p2', 'fill', this.fp.get('blindsPosition').get('p1p2').value ? '#1b4871' : 'none');
      SVG.setComponentStyle(this.pergolaView, 'side-p3p4', 'fill', this.fp.get('blindsPosition').get('p3p4').value ? '#1b4871' : 'none');
      SVG.setComponentStyle(this.pergolaView, 'side-p1p3', 'fill', this.fp.get('blindsPosition').get('p1p3').value ? '#1b4871' : 'none');
      SVG.setComponentStyle(this.pergolaView, 'side-p2p4', 'fill', this.fp.get('blindsPosition').get('p2p4').value ? '#1b4871' : 'none');

      SVG.setComponentStyle(this.pergolaView, 'p1', 'opacity', this.projectF.get('poleData').get('p1').value ? 1 : 0.15);
      SVG.setComponentStyle(this.pergolaView, 'p2', 'opacity', this.projectF.get('poleData').get('p2').value ? 1 : 0.15);
      SVG.setComponentStyle(this.pergolaView, 'p3', 'opacity', this.projectF.get('poleData').get('p3').value ? 1 : 0.15);
      SVG.setComponentStyle(this.pergolaView, 'p4', 'opacity', this.projectF.get('poleData').get('p4').value ? 1 : 0.15);

      this.fp.updateValueAndValidity();
    }
  }

  get isType6Pergola(): boolean {
    return this.orderForm.get('product').get('installationType').value === 6;
  }

  onSvgLoad(e: SVGElement) {
    this.pergolaView = e;
    this.refreshPergolaView();
  }
}
