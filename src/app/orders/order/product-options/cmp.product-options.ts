import { Component, Input, OnInit } from '@angular/core';
import { Order } from '../../cls.order';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-product-options',
  templateUrl: './tpl.product-options.html',
  styleUrls: ['./cmp.product-options.scss']
})
export class ProductOptionsComponent implements OnInit {

  @Input() order: Order;
  @Input() orderForm: FormGroup;

  constructor() { }

  ngOnInit() {
  }

}
