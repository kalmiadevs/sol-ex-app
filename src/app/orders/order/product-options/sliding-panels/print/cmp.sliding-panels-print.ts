import { Component, Input, OnInit } from '@angular/core';
import { Order } from '../../../../cls.order';
import { RalToHex } from '../../../cls.ral-colors';
import { FormGroup } from '@angular/forms';
import { OrderTabComponent } from '../../../cmp.order-tab';
import { SVG } from '../../../cls.svg';
import { pergolaLengthReductionForPanels } from '../../../../cls.order-validators';
import { range as _range } from 'lodash';


@Component({
  selector: 'app-product-options-sliding-panels-print',
  templateUrl: './tpl.sliding-panels-print.html',
  styleUrls: ['./cmp.sliding-panels-print.scss']
})
export class ProductOptionsSlidingPanelsPrintComponent extends OrderTabComponent implements OnInit {

  @Input() order: Order;
  @Input() orderForm: FormGroup;

  ralToHex = RalToHex.hex;
  pergolaView: any;

  get form(): FormGroup {
    return this.orderForm as FormGroup;
  }

  get fProject(): FormGroup {
    return (this.orderForm.controls.product as FormGroup);
  }

  get fp(): FormGroup {
    return this.fProject.controls.options as FormGroup;
  }

  get pergolaLength(): number {
    return (this.fProject.get('length').value - pergolaLengthReductionForPanels);
  }

  get panelsOnLength(): boolean {
    return this.fp.get('slidingPanelPosition').get('p1p2').value || this.fp.get('slidingPanelPosition').get('p3p4').value;
  }

  get panelsOnWidth(): boolean {
    return this.fp.get('slidingPanelPosition').get('p1p3').value || this.fp.get('slidingPanelPosition').get('p2p4').value;
  }

  constructor() {
    super();
  }

  ngOnInit() {
  }

  getControlValue(path: string) {
    return (this.form.get(path) ? this.form.get(path).value : undefined);
  }

  _guides(x: any): number[] {
    return this.fp.get('slidingPanelOn' + x).get('type').value === 'A' ? _range(1) : _range(2)
  }

  _panels(x: any): number[] {
    return _range(this.fp.get('slidingPanelOn' + x).get('panelQuantity').value);
  }

  getPanelPlacement(x: any) {
    return this.fp.get('slidingPanelOn' + x).get('placement').value;
  }

  arrowLeft(x: any): string {
    return this.fp.get('slidingPanelOn' + x).get('arrowLeft').value
  };

  arrowRight(x: any): string {
    return this.fp.get('slidingPanelOn' + x).get('arrowRight').value
  };

  refreshPergolaView() {
    if (this.fp) {
      // Disable other options on the same sides
      if (this.fp.get('slidingPanelPosition').get('p1p2').value) {
        this.fp.get('blindsPosition').get('p1p2').setValue(false);
        this.fp.get('glassPanelsPosition').get('p1p2').setValue(false);
      }
      if (this.fp.get('slidingPanelPosition').get('p3p4').value) {
        this.fp.get('blindsPosition').get('p3p4').setValue(false);
        this.fp.get('glassPanelsPosition').get('p3p4').setValue(false);
      }
      if (this.fp.get('slidingPanelPosition').get('p1p3').value) {
        this.fp.get('blindsPosition').get('p1p3').setValue(false);
        this.fp.get('glassPanelsPosition').get('p1p3').setValue(false);
      }
      if (this.fp.get('slidingPanelPosition').get('p2p4').value) {
        this.fp.get('blindsPosition').get('p2p4').setValue(false);
        this.fp.get('glassPanelsPosition').get('p2p4').setValue(false);
      }

      // Update SVG components
      SVG.setComponentStyle(this.pergolaView, 'side-p1p2', 'fill', this.fp.get('slidingPanelPosition').get('p1p2').value ? '#1b4871' : 'none');
      SVG.setComponentStyle(this.pergolaView, 'side-p3p4', 'fill', this.fp.get('slidingPanelPosition').get('p3p4').value ? '#1b4871' : 'none');
      SVG.setComponentStyle(this.pergolaView, 'side-p1p3', 'fill', this.fp.get('slidingPanelPosition').get('p1p3').value ? '#1b4871' : 'none');
      SVG.setComponentStyle(this.pergolaView, 'side-p2p4', 'fill', this.fp.get('slidingPanelPosition').get('p2p4').value ? '#1b4871' : 'none');

      SVG.setComponentStyle(this.pergolaView, 'p1', 'opacity', this.fProject.get('poleData').get('p1').value ? 1 : 0.15);
      SVG.setComponentStyle(this.pergolaView, 'p2', 'opacity', this.fProject.get('poleData').get('p2').value ? 1 : 0.15);
      SVG.setComponentStyle(this.pergolaView, 'p3', 'opacity', this.fProject.get('poleData').get('p3').value ? 1 : 0.15);
      SVG.setComponentStyle(this.pergolaView, 'p4', 'opacity', this.fProject.get('poleData').get('p4').value ? 1 : 0.15);
    }
  }

  onSvgLoad(e: SVGElement) {
    this.pergolaView = e;
    this.refreshPergolaView();
  }
}
