import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {Order} from '../../../cls.order';
import {RalToHex} from '../../cls.ral-colors';
import {FormGroup} from '@angular/forms';
import {SVG} from '../../cls.svg';
import {
  pergolaLengthReductionForPanels,
  slidingPanelGuideQuantityMap,
  slidingPanelLengths,
  slidingPanelPlacementDefaults
} from '../../../cls.order-validators';
import {range as _range} from 'lodash';

@Component({
  selector: 'app-product-options-sliding-panels',
  templateUrl: './tpl.sliding-panels.html',
  styleUrls: ['./cmp.sliding-panels.scss']
})
export class ProductOptionsSlidingPanelsComponent implements OnInit {

  @Input() order: Order;
  @Input() orderForm: FormGroup;

  @ViewChild('slidingPanelsForm') form;

  ralToHex = RalToHex.hex;

  pergolaView: any;

  get fProject(): FormGroup {
    return (this.orderForm.controls.product as FormGroup);
  }

  get fp(): FormGroup {
    return this.fProject.controls.options as FormGroup;
  }

  get pergolaLength(): number { return (this.fProject.get('length').value - pergolaLengthReductionForPanels); }

  get panelsOnLength(): boolean { return this.fp.get('slidingPanelPosition').get('p1p2').value || this.fp.get('slidingPanelPosition').get('p3p4').value; }
  get panelsOnWidth(): boolean { return this.fp.get('slidingPanelPosition').get('p1p3').value || this.fp.get('slidingPanelPosition').get('p2p4').value; }

  constructor() { }

  ngOnInit() {
  }

  _guides(x: any): number[] { return this.fp.get('slidingPanelOn' + x).get('type').value === 'A' ? _range(1) : _range(2) }
  _panels(x: any): number[] { return _range(this.fp.get('slidingPanelOn' + x).get('panelQuantity').value); }

  getPanelPlacement(x: any) {
    return this.fp.get('slidingPanelOn' + x).get('placement').value;
  }

  togglePanelPosition(x: any, panelNumber: number, guideNumber: number) {
    const placement = this.getPanelPlacement(x);
    placement[panelNumber] = !!placement[panelNumber] ? undefined : guideNumber;
    this.fp.get('slidingPanelOn' + x).get('placement').setValue(placement);
  }

  updateType(x: any) {
    // this.fp.get('slidingPanelOn' + x).get('panelQuantity').setValue(1);
    // this.fp.get('slidingPanelOn' + x).get('size').setValue(330);
    this.resetPanelPositions(x);
  }

  updateSize(x: any) {
    // nothing for now
  }

  updatePanelQuantity(x: any) {
    this.resetPanelPositions(x);
  }

  resetPanelPositions(x: any) {
    this.fp.get('slidingPanelOn' + x).get('placement').setValue(slidingPanelPlacementDefaults[this.fp.get('slidingPanelOn' + x).get('panelQuantity').value][this.fp.get('slidingPanelOn' + x).get('type').value === 'A' ? 1 : 2].slice());
  }

  arrowLeft(x: any): string { return this.fp.get('slidingPanelOn' + x).get('arrowLeft').value };
  arrowRight(x: any): string { return this.fp.get('slidingPanelOn' + x).get('arrowRight').value };
  toggleLeftArrow(x: any) { this.fp.get('slidingPanelOn' + x).get('arrowLeft').setValue(this.arrowLeft(x) === 'left' ? 'right' : 'left'); }
  toggleRightArrow(x: any) { this.fp.get('slidingPanelOn' + x).get('arrowRight').setValue(this.arrowRight(x) === 'right' ? 'left' : 'right'); }

  controlValid(control: string) {
    if (this.fp.controls[control]) {
      return !(this.fp.controls[control].errors && this.fp.controls[control].touched);
    }
    return false;
  }

  isPergolaSideAvailable(side: string) {
    return !this.fp.get('blindsPosition').get(side).value && !this.fp.get('glassPanelsPosition').get(side).value;
  }

  refreshPergolaView() {
    if (this.fp) {
      // Disable other options on the same sides
      if (this.fp.get('slidingPanelPosition').get('p1p2').value) {
        this.fp.get('blindsPosition').get('p1p2').setValue(false);
        this.fp.get('glassPanelsPosition').get('p1p2').setValue(false);
      }
      if (this.fp.get('slidingPanelPosition').get('p3p4').value) {
        this.fp.get('blindsPosition').get('p3p4').setValue(false);
        this.fp.get('glassPanelsPosition').get('p3p4').setValue(false);
      }
      if (this.fp.get('slidingPanelPosition').get('p1p3').value) {
        this.fp.get('blindsPosition').get('p1p3').setValue(false);
        this.fp.get('glassPanelsPosition').get('p1p3').setValue(false);
      }
      if (this.fp.get('slidingPanelPosition').get('p2p4').value) {
        this.fp.get('blindsPosition').get('p2p4').setValue(false);
        this.fp.get('glassPanelsPosition').get('p2p4').setValue(false);
      }

      // Update SVG components
      SVG.setComponentStyle(this.pergolaView, 'side-p1p2', 'fill', this.fp.get('slidingPanelPosition').get('p1p2').value ? '#1b4871' : 'none');
      SVG.setComponentStyle(this.pergolaView, 'side-p3p4', 'fill', this.fp.get('slidingPanelPosition').get('p3p4').value ? '#1b4871' : 'none');
      SVG.setComponentStyle(this.pergolaView, 'side-p1p3', 'fill', this.fp.get('slidingPanelPosition').get('p1p3').value ? '#1b4871' : 'none');
      SVG.setComponentStyle(this.pergolaView, 'side-p2p4', 'fill', this.fp.get('slidingPanelPosition').get('p2p4').value ? '#1b4871' : 'none');

      SVG.setComponentStyle(this.pergolaView, 'p1', 'opacity', this.fProject.get('poleData').get('p1').value ? 1 : 0.15);
      SVG.setComponentStyle(this.pergolaView, 'p2', 'opacity', this.fProject.get('poleData').get('p2').value ? 1 : 0.15);
      SVG.setComponentStyle(this.pergolaView, 'p3', 'opacity', this.fProject.get('poleData').get('p3').value ? 1 : 0.15);
      SVG.setComponentStyle(this.pergolaView, 'p4', 'opacity', this.fProject.get('poleData').get('p4').value ? 1 : 0.15);
    }
  }

  onSvgLoad(e: SVGElement) {
    this.pergolaView = e;
    this.refreshPergolaView();
  }
}
