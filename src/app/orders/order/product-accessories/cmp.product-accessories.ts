import {Component, Input, OnInit, ViewChild} from '@angular/core';
import { Order } from '../../cls.order';
import { FormGroup } from '@angular/forms';
import { I18nService } from '../../../base/core/i18n/svc.i18n';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-product-accessories',
  templateUrl: './tpl.product-accessories.html',
  styleUrls: ['./cmp.product-accessories.scss']
})
export class ProductAccessoriesComponent implements OnInit {

  @Input() order: Order;
  @Input() orderForm: FormGroup;

  @ViewChild('accessoriesForm') form;

  printMode: boolean;

  get fp(): FormGroup {
    return (this.orderForm.get('product').get('accessories') as FormGroup);
  }

  get pergolaLength(): number {
    return this.orderForm.get('product').get('length').value;
  }

  constructor(
    protected route: ActivatedRoute,
    private i18n: I18nService
  ) { }

  ngOnInit() {
    this.printMode = !!this.route.snapshot.data['print'];
  }

  controlValid(control: string) {
    if (this.form.controls[control]) {
      return !(this.form.controls[control].errors && this.form.controls[control].touched);
    }
    return false;
  }


  remoteControlQuantityChange(value: any) {
    this.fp.get('somfyBridgeQuantity').setValue(value);
  }

  bridgeQuantityChange(value: any) {
    this.fp.get('somfyRemoteControlQuantity').setValue(value);
  }
}
