import { Component, Input, OnInit } from '@angular/core';
import { Order } from '../../../cls.order';
import { RalToHex } from '../../cls.ral-colors';
import { FormGroup } from '@angular/forms';
import { OrderTabComponent } from '../../cmp.order-tab';
import { I18nService } from '../../../../base/core/i18n/svc.i18n';

@Component({
  selector: 'app-product-accessories-print',
  templateUrl: './tpl.product-accessories-print.html',
  styleUrls: ['./cmp.product-accessories-print.scss']
})
export class ProductAccessoriesPrintComponent extends OrderTabComponent implements OnInit {

  @Input() order: Order;
  @Input() orderForm: FormGroup;

  ralToHex = RalToHex.hex;

  get form(): FormGroup {
    return this.orderForm as FormGroup;
  }

  get fp(): FormGroup {
    return this.form.get('product.accessories') as FormGroup;
  }

  constructor(private i18n: I18nService) {
    super();
  }

  ngOnInit() {
  }

  getControlValue(path: string) {
    return (this.form.get(path) ? this.form.get(path).value : undefined);
  }
}
