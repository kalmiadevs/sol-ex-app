import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { I18nService } from '../../../../base/core/i18n/svc.i18n';

@Component({
  selector: 'app-accessory',
  templateUrl: './tpl.accessory.html',
  styleUrls: ['./cmp.accessory.scss']
})
export class AccessoryComponent implements OnInit {

  @Input() orderForm: FormGroup;
  @Input() control: string;
  @Input() name: string;
  @Input() imageSrc: string;
  @Input() price?: number;
  @Input() description?: string;
  @Input() max?: number;

  @Output() valueChange: EventEmitter<any> = new EventEmitter();

  printMode: boolean;

  get form(): FormGroup {
    return (this.orderForm as FormGroup);
  }

  get quantity() {
    return this.form && this.form.get(this.control).value;
  }

  constructor(
    protected route: ActivatedRoute,
    private i18n: I18nService
  ) { }

  controlValid(control: string) {
    if (this.form.controls[control]) {
      return !(this.form.get(control).errors && this.form.get(control).touched);
    }
    return false;
  }

  ngOnInit() {
    this.printMode = !!this.route.snapshot.data['print'];
  }

  add() {
    if (this.max) {
      this.form.get(this.control).setValue(Math.min(this.max, this.form.get(this.control).value + 1));
    } else {
      this.form.get(this.control).setValue(this.form.get(this.control).value + 1);
    }
    this.onValueChange();
  }

  subtract() {
    this.form.get(this.control).setValue(Math.max(this.form.get(this.control).value - 1, 0));
    this.onValueChange();
  }

  onValueChange(e?: any) {
    this.valueChange.emit(this.form.get(this.control).value);
  }
}
