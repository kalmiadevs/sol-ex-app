// LEGEND:
// bladesDisabledZones
// length <-- for which pergola we are looking
// startOnBlade  <-- on which blade disabled blades MIGHT start
// disableZoneLen  <-- for how long from startOnBlade is disabled zone

// TIP: check excel file  other/BLADES.xlsx  for help since in following code i use colors from excel as variable names so it's easier to understand

const bladesDisabledZones160 =
[
  { length: 511	, i:  2, startOnBlade: -2, disableZoneLen: 2 },
  { length: 694	, i:  3, startOnBlade: -2, disableZoneLen: 2 },
  { length: 876	, i:  4, startOnBlade: -1, disableZoneLen: 3 },
  { length: 1059, i:  5, startOnBlade: -1, disableZoneLen: 6 },
  { length: 1242, i:  6, startOnBlade:  0, disableZoneLen: 6 },
  { length: 1424, i:  7, startOnBlade:  0, disableZoneLen: 6 },
  { length: 1607, i:  8, startOnBlade:  1, disableZoneLen: 6 },
  { length: 1789, i:  9, startOnBlade:  1, disableZoneLen: 6 },
  { length: 1972, i: 10, startOnBlade:  2, disableZoneLen: 6 },
  { length: 2155, i: 11, startOnBlade:  2, disableZoneLen: 6 },
  { length: 2337, i: 12, startOnBlade:  3, disableZoneLen: 6 },
  { length: 2520, i: 13, startOnBlade:  3, disableZoneLen: 6 },
  { length: 2702, i: 14, startOnBlade:  4, disableZoneLen: 6 },
  { length: 2885, i: 15, startOnBlade:  4, disableZoneLen: 6 },
  { length: 3068, i: 16, startOnBlade:  5, disableZoneLen: 6 },
  { length: 3250, i: 17, startOnBlade:  5, disableZoneLen: 6 },
  { length: 3433, i: 18, startOnBlade:  6, disableZoneLen: 6 },
  { length: 3615, i: 19, startOnBlade:  6, disableZoneLen: 6 },
  { length: 3798, i: 20, startOnBlade:  7, disableZoneLen: 6 },
  { length: 3981, i: 21, startOnBlade:  7, disableZoneLen: 6 },
  { length: 4163, i: 22, startOnBlade:  8, disableZoneLen: 6 },
  { length: 4346, i: 23, startOnBlade:  8, disableZoneLen: 6 },
  { length: 4528, i: 24, startOnBlade:  9, disableZoneLen: 6 },
  { length: 4711, i: 25, startOnBlade:  9, disableZoneLen: 6 },
  { length: 4894, i: 26, startOnBlade: 10, disableZoneLen: 6 },
  { length: 5076, i: 27, startOnBlade: 10, disableZoneLen: 6 },
  { length: 5259, i: 28, startOnBlade: 11, disableZoneLen: 6 },
  { length: 5441, i: 29, startOnBlade: 11, disableZoneLen: 6 },
  { length: 5624, i: 30, startOnBlade: 12, disableZoneLen: 6 },
  { length: 5807, i: 31, startOnBlade: 12, disableZoneLen: 6 },
  { length: 5989, i: 32, startOnBlade: 13, disableZoneLen: 6 },

  // double
  { length: 6172, i: 33, startOnBlade:  5, disableZoneLen: 6 },
  { length: 6172, i: 33, startOnBlade: 22, disableZoneLen: 6 },

  { length: 6354, i: 34, startOnBlade:  5, disableZoneLen: 6 },
  { length: 6354, i: 34, startOnBlade: 22, disableZoneLen: 6 },

  { length: 6537, i: 35, startOnBlade:  6, disableZoneLen: 6 },
  { length: 6537, i: 35, startOnBlade: 23, disableZoneLen: 6 },

  { length: 6720, i: 36, startOnBlade:  6, disableZoneLen: 6 },
  { length: 6720, i: 36, startOnBlade: 24, disableZoneLen: 6 },

  { length: 6902, i: 37, startOnBlade:   6, disableZoneLen: 6 },
  { length: 6902, i: 37, startOnBlade:  25, disableZoneLen: 6 },
];

const bladesDisabledZones170 =
[
 { length: 511	, i:  2, startOnBlade: -3, disableZoneLen: 5 },
 { length: 694	, i:  3, startOnBlade: -3, disableZoneLen: 5 },
 { length: 876	, i:  4, startOnBlade: -2, disableZoneLen: 5 },
 { length: 1059 , i:  5, startOnBlade: -2, disableZoneLen: 7 },
 { length: 1242 , i:  6, startOnBlade:  -1, disableZoneLen: 7 },
 { length: 1424 , i:  7, startOnBlade:  -1, disableZoneLen: 8 },
 { length: 1607 , i:  8, startOnBlade:  0, disableZoneLen: 8 },
 { length: 1789 , i:  9, startOnBlade:  0, disableZoneLen: 8 },
 { length: 1972 , i: 10, startOnBlade:  1, disableZoneLen: 8 },
 { length: 2155 , i: 11, startOnBlade:  1, disableZoneLen: 8 },
 { length: 2337 , i: 12, startOnBlade:  2, disableZoneLen: 8 },
 { length: 2520 , i: 13, startOnBlade:  2, disableZoneLen: 8 },
 { length: 2702 , i: 14, startOnBlade:  3, disableZoneLen: 8 },
 { length: 2885 , i: 15, startOnBlade:  3, disableZoneLen: 8 },
 { length: 3068 , i: 16, startOnBlade:  4, disableZoneLen: 8 },
 { length: 3250 , i: 17, startOnBlade:  4, disableZoneLen: 8 },
 { length: 3433 , i: 18, startOnBlade:  5, disableZoneLen: 8 },
 { length: 3615 , i: 19, startOnBlade:  5, disableZoneLen: 8 },
 { length: 3798 , i: 20, startOnBlade:  6, disableZoneLen: 8 },
 { length: 3981 , i: 21, startOnBlade:  6, disableZoneLen: 8 },
 { length: 4163 , i: 22, startOnBlade:  7, disableZoneLen: 8 },
 { length: 4346 , i: 23, startOnBlade:  7, disableZoneLen: 8 },
 { length: 4528 , i: 24, startOnBlade:  8, disableZoneLen: 8 },
 { length: 4711 , i: 25, startOnBlade:  8, disableZoneLen: 8 },
 { length: 4894 , i: 26, startOnBlade:  9, disableZoneLen: 8 },
 { length: 5076 , i: 27, startOnBlade:  9, disableZoneLen: 8 },
 { length: 5259 , i: 28, startOnBlade: 10, disableZoneLen: 8 },
 { length: 5441 , i: 29, startOnBlade: 10, disableZoneLen: 8 },
 { length: 5624 , i: 30, startOnBlade: 11, disableZoneLen: 8 },
 { length: 5807 , i: 31, startOnBlade: 11, disableZoneLen: 8 },
 { length: 5989 , i: 32, startOnBlade: 12, disableZoneLen: 8 },

 // double
 { length: 6172, i: 33, startOnBlade:  4, disableZoneLen: 8 },
 { length: 6172, i: 33, startOnBlade: 21, disableZoneLen: 8 },

 { length: 6354, i: 34, startOnBlade:  4, disableZoneLen: 8 },
 { length: 6354, i: 34, startOnBlade: 21, disableZoneLen: 8 },

 { length: 6537, i: 35, startOnBlade:  5, disableZoneLen: 8 },
 { length: 6537, i: 35, startOnBlade: 22, disableZoneLen: 8 },

 { length: 6720, i: 36, startOnBlade:  5, disableZoneLen: 8 },
 { length: 6720, i: 36, startOnBlade: 23, disableZoneLen: 8 },

 { length: 6902, i: 37, startOnBlade:   5, disableZoneLen: 8 },
 { length: 6902, i: 37, startOnBlade:  24, disableZoneLen: 8 },
];

export function isBladeInDisabledZone(
  i: number,
  type:  'SL 160/28' | 'SL 170/28' | 'SL 170/36',
  pergolaLength: number,
  bladesOpeningDirection: 'CW' | 'CCW',
  motorPos: 'a' | 'b'
  ): boolean {

  const opt: {motorSize: number, motorPadding: number, zone: { length: number, i: number, startOnBlade: number, disableZoneLen: number }[]} = {
    'SL 160/28': { zone: bladesDisabledZones160, motorSize: 2, motorPadding: 2 },
    'SL 170/28': { zone: bladesDisabledZones170, motorSize: 2, motorPadding: 3 },
    'SL 170/36': { zone: bladesDisabledZones170, motorSize: 2, motorPadding: 3 }
  }[type];

  for (let k = 0; k < opt.zone.length; k++) {
    const item = opt.zone[k];
    if (item.length !== pergolaLength) {
      continue;
    }
    if (i >= item.startOnBlade && i < item.startOnBlade + item.disableZoneLen) {
      // we are in teh colored zone now let's check motor position

      const checkOrange = (motorPos === 'a' && bladesOpeningDirection === 'CCW') || (motorPos === 'b' && bladesOpeningDirection === 'CW');
      const checkBrown = (motorPos === 'a' && bladesOpeningDirection === 'CW') || (motorPos === 'b' && bladesOpeningDirection === 'CCW');

      // ORANGE (by excel definition)
      const orangeEnd = item.startOnBlade + opt.motorPadding;
      if (checkOrange && i >= item.startOnBlade && i < orangeEnd) { return true; }

      // YELLOW (by excel definition)
      const yellowEnd = orangeEnd + opt.motorSize;
      if (i >= orangeEnd && i < yellowEnd ) { return true; }

      // BROWN (by excel definition)
      const brownEnd = yellowEnd + opt.motorPadding;
      if (checkBrown && i >= yellowEnd && i < brownEnd) { return true; }
    }
  }
  return false;
}









































