import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { Order } from '../../../cls.order';
import { RalToHex } from '../../cls.ral-colors';
import { FormGroup } from '@angular/forms';
import { OrderTabComponent } from '../../cmp.order-tab';
import { SVG } from '../../cls.svg';
import { isBladeInDisabledZone } from '../bladesMap';
import { range as _range } from 'lodash';
import { ledLengths, pergolaProperties } from '../../../cls.order-validators';

@Component({
  selector: 'app-product-specifications-print',
  templateUrl: './tpl.product-specifications-print.html',
  styleUrls: ['./cmp.product-specifications-print.scss']
})
export class ProductSpecificationsPrintComponent extends OrderTabComponent implements OnInit {

  @Input() order: Order;
  @Input() orderForm: FormGroup;

  ralToHex = RalToHex.hex;

  topSpecView: any;
  legendView: any;
  bladeIndicatorSpacing = 10;

  svgAlphaFull = 1.0;
  svgAlphaHalf = 0.15;
  svgAlphaNone = 0;

  get form(): FormGroup {
    return this.orderForm as FormGroup;
  }


  get projectSpecF(): FormGroup {
    return (this.projectF.controls.specifications as FormGroup);
  }

  get projectF(): FormGroup {
    return (this.orderForm.controls.product as FormGroup);
  }

  get pergolaProperties() {
    return pergolaProperties[this.projectF.get('pergolaType').value];
  }

  get totalPoles(): number {
    return Number(this.projectF.get('numberOfPoles').value);
  }

  get placedPoles(): number {
    return (this.projectF.get('poleData').get('p1').value ? 1 : 0) +
      (this.projectF.get('poleData').get('p2').value ? 1 : 0) +
      (this.projectF.get('poleData').get('p3').value ? 1 : 0) +
      (this.projectF.get('poleData').get('p4').value ? 1 : 0);
  }

  get remainingFreePoles(): number {
    return this.totalPoles - this.placedPoles;
  }

  get blades(): number[] {
    return _range(1, this.pergolaProperties.length.indexOf(this.projectF.get('length').value) + 2);
  }

  get leds(): number[] {
    return this.projectSpecF.get('ledPositions').value;
  }

  get powerSupply1IsPoleText(): boolean {
    return !this.projectF.get('poleData').get('p1').value
  }

  get powerSupply2IsPoleText(): boolean {
    return !this.projectF.get('poleData').get('p2').value
  }

  get powerSupply3IsPoleText(): boolean {
    return !this.projectF.get('poleData').get('p3').value
  }

  get powerSupply4IsPoleText(): boolean {
    return !this.projectF.get('poleData').get('p4').value
  }

  isBladeDisabled(i: number) {
    const pergolaLength          = this.projectF.get('length').value;
    const type                   = this.projectF.get('pergolaType').value;
    const bladesOpeningDirection = this.projectSpecF.get('bladesOpeningDirection').value;
    const motorPos               = this.projectSpecF.get('motorPos').value;
    return isBladeInDisabledZone(i, type, pergolaLength, bladesOpeningDirection, motorPos);
  }

  isBladeLEDEnabled(blade) {
    return this.leds.indexOf(blade) > -1;
  }

  constructor(protected cd: ChangeDetectorRef) {
    super();
  }

  ngOnInit() {
    this.mwpRefresh();
  }

  getControlValue(path: string) {
    return (this.form.get(path) ? this.form.get(path).value : undefined);
  }

  mwpRefresh() {
    if (this.projectSpecF) {
      // walls
      this.wallsRefresh();

      // wall fixations
      // const hasWalls = [1, 6].indexOf(this.projectF.get('installationType').value) === -1;
      const hasWalls = true;
      SVG.setComponentStyle(this.topSpecView, 'wallfix-p1p2', 'opacity', hasWalls && this.projectSpecF.get('wallFixations').get('p1p2').value ? this.svgAlphaFull : this.svgAlphaNone);
      SVG.setComponentStyle(this.topSpecView, 'wallfix-p3p4', 'opacity', hasWalls && this.projectSpecF.get('wallFixations').get('p3p4').value ? this.svgAlphaFull : this.svgAlphaNone);
      SVG.setComponentStyle(this.topSpecView, 'wallfix-p1p3', 'opacity', hasWalls && this.projectSpecF.get('wallFixations').get('p1p3').value ? this.svgAlphaFull : this.svgAlphaNone);
      SVG.setComponentStyle(this.topSpecView, 'wallfix-p2p4', 'opacity', hasWalls && this.projectSpecF.get('wallFixations').get('p2p4').value ? this.svgAlphaFull : this.svgAlphaNone);

      // motors
      SVG.setComponentStyle(this.topSpecView, 'motor-a', 'opacity', this.projectSpecF.get('motorPos').value === 'a' ? this.svgAlphaFull : this.svgAlphaHalf);
      SVG.setComponentStyle(this.topSpecView, 'motor-b', 'opacity', this.projectSpecF.get('motorPos').value === 'b' ? this.svgAlphaFull : this.svgAlphaHalf);

      // water & power
      for (let i = 1; i < 5; i++) {
        let alphaHalf = ((!this.remainingFreePoles && !this.projectF.get('poleData').get('p' + i).value) || // hide if not selected and no remaining poles available
          !this.projectF.get('poleData').get('p' + i).value) // hide if pole not selected at all
          ? this.svgAlphaNone : this.svgAlphaHalf;

        const alphaFull = (!this.projectF.get('poleData').get('p' + i).value) ? this.svgAlphaNone : this.svgAlphaFull;

        SVG.setComponentStyle(this.topSpecView, 'water-a' + i, 'opacity', this.projectSpecF.get('waterExit').get('a' + i).value ? alphaFull : alphaHalf);
        SVG.setComponentStyle(this.topSpecView, 'water-b' + i, 'opacity', this.projectSpecF.get('waterExit').get('b' + i).value ? alphaFull : alphaHalf);
        SVG.setComponentStyle(this.topSpecView, 'water-a' + i + 't', 'opacity', this.projectSpecF.get('waterExit').get('a' + i).value ? alphaFull : alphaHalf);
        SVG.setComponentStyle(this.topSpecView, 'water-b' + i + 't', 'opacity', this.projectSpecF.get('waterExit').get('b' + i).value ? alphaFull : alphaHalf);

        SVG.setComponentStyle(this.topSpecView, 'power-p' + i, 'opacity', this.projectSpecF.get('powerSupply').get('p' + i).value ? this.svgAlphaFull : alphaHalf);

        alphaHalf = this.remainingFreePoles ? this.svgAlphaHalf : this.svgAlphaNone;
        SVG.setComponentStyle(this.topSpecView, 'p' + i, 'opacity', this.projectF.get('poleData').get('p' + i).value ? this.svgAlphaFull : alphaHalf);
      }

      // poles
      SVG.setComponentStyle(this.topSpecView, 'p1p2', 'opacity', this.svgAlphaNone);
      SVG.setComponentStyle(this.topSpecView, 'p3p4', 'opacity', this.svgAlphaNone);
      SVG.setComponentStyle(this.topSpecView, 'p1p3', 'opacity', this.svgAlphaNone);
      SVG.setComponentStyle(this.topSpecView, 'p2p4', 'opacity', this.svgAlphaNone);
      for (const additionalPole of this.projectF.get('additionalPoleData').value) {
        SVG.setComponentStyle(this.topSpecView, additionalPole.position, 'opacity', this.svgAlphaFull);
      }

      const offsetP1 = this.projectF.get('poleData').get('p1y').value;
      const offsetP2 = this.projectF.get('poleData').get('p2y').value;
      const offsetP3 = this.projectF.get('poleData').get('p3y').value;
      const offsetP4 = this.projectF.get('poleData').get('p4y').value;


      if (offsetP1) {
        SVG.setComponentStyle(this.topSpecView, 'p1p2', 'transform',
          SVG.getComponentAttribute(this.topSpecView, 'p1p2', 'transform') + ' translateX(23.5px)');
      }

      if (offsetP3) {
        SVG.setComponentStyle(this.topSpecView, 'p3p4', 'transform',
          SVG.getComponentAttribute(this.topSpecView, 'p3p4', 'transform') + ' translateX(23.5px)');
      }

      SVG.setComponentStyle(this.topSpecView, 'p1-y', 'opacity', offsetP1 ? 1 : 0);
      SVG.setComponentStyle(this.topSpecView, 'p3-y', 'opacity', offsetP3 ? 1 : 0);
      SVG.setComponentStyle(this.topSpecView, 'p2-y', 'opacity', offsetP2 ? 1 : 0);
      SVG.setComponentStyle(this.topSpecView, 'p4-y', 'opacity', offsetP4 ? 1 : 0);

      SVG.setComponentAttribute(this.topSpecView, 'p1', 'dx', offsetP1 ? 30 : 0);
      SVG.setComponentAttribute(this.topSpecView, 'p3', 'dx', offsetP3 ? 30 : 0);
      SVG.setComponentStyle(this.topSpecView, 'power-p1', 'transform',
        SVG.getComponentAttribute(this.topSpecView, 'power-p1', 'transform') + (offsetP1 ? ' translateX(1100px)' : ''));
      SVG.setComponentStyle(this.topSpecView, 'power-p3', 'transform',
        SVG.getComponentAttribute(this.topSpecView, 'power-p3', 'transform') + (offsetP3 ? ' translateX(1100px)' : ''));
      SVG.setComponentStyle(this.topSpecView, 'water-a1', 'transform',
        SVG.getComponentAttribute(this.topSpecView, 'water-a1', 'transform') + (offsetP1 ? ' translateX(30px)' : ''));
      SVG.setComponentStyle(this.topSpecView, 'water-a3', 'transform',
        SVG.getComponentAttribute(this.topSpecView, 'water-a3', 'transform') + (offsetP3 ? ' translateX(30px)' : ''));
      SVG.setComponentAttribute(this.topSpecView, 'water-a1t', 'dx', offsetP1 ? 30 : 0);
      SVG.setComponentAttribute(this.topSpecView, 'water-a3t', 'dx', offsetP3 ? 30 : 0);

      SVG.setComponentAttribute(this.topSpecView, 'p2', 'dx', offsetP2 ? -35 : 0);
      SVG.setComponentAttribute(this.topSpecView, 'p4', 'dx', offsetP4 ? -35 : 0);
      SVG.setComponentStyle(this.topSpecView, 'power-p2', 'transform',
        SVG.getComponentAttribute(this.topSpecView, 'power-p2', 'transform') + (offsetP2 ? ' translateX(-1200px)' : ''));
      SVG.setComponentStyle(this.topSpecView, 'power-p4', 'transform',
        SVG.getComponentAttribute(this.topSpecView, 'power-p4', 'transform') + (offsetP4 ? ' translateX(-1200px)' : ''));
      SVG.setComponentStyle(this.topSpecView, 'water-a2', 'transform',
        SVG.getComponentAttribute(this.topSpecView, 'water-a2', 'transform') + (offsetP2 ? ' translateX(-35px)' : ''));
      SVG.setComponentStyle(this.topSpecView, 'water-a4', 'transform',
        SVG.getComponentAttribute(this.topSpecView, 'water-a4', 'transform') + (offsetP4 ? ' translateX(-35px)' : ''));
      SVG.setComponentAttribute(this.topSpecView, 'water-a2t', 'dx', offsetP2 ? -35 : 0);
      SVG.setComponentAttribute(this.topSpecView, 'water-a4t', 'dx', offsetP4 ? -35 : 0);
    }

    this.cd.detectChanges();
  }

  wallsRefresh() {
    SVG.setComponentStyle(this.topSpecView, 'wall-p1p2', 'opacity', this.svgAlphaNone);
    SVG.setComponentStyle(this.topSpecView, 'wall-p2p4', 'opacity', this.svgAlphaNone);
    SVG.setComponentStyle(this.topSpecView, 'wall-p1p3', 'opacity', this.svgAlphaNone);
    SVG.setComponentStyle(this.topSpecView, 'wall-p3p4', 'opacity', this.svgAlphaNone);

    if (this.projectSpecF.get('wallFixations').get('p1p2').value) {
      SVG.setComponentStyle(this.topSpecView, 'wall-p1p2', 'opacity', this.svgAlphaFull);
    }

    if (this.projectSpecF.get('wallFixations').get('p3p4').value) {
      SVG.setComponentStyle(this.topSpecView, 'wall-p3p4', 'opacity', this.svgAlphaFull);
    }

    if (this.projectSpecF.get('wallFixations').get('p1p3').value) {
      SVG.setComponentStyle(this.topSpecView, 'wall-p1p3', 'opacity', this.svgAlphaFull);
    }

    if (this.projectSpecF.get('wallFixations').get('p2p4').value) {
      SVG.setComponentStyle(this.topSpecView, 'wall-p2p4', 'opacity', this.svgAlphaFull);
    }

    /*
    switch (this.projectF.get('installationType').value) {
      case 2:
      case 4:
        if (this.projectF.get('poleData').get('p1').value && this.projectF.get('poleData').get('p2').value) {
          SVG.setComponentStyle(this.topSpecView, 'wall-p3p4', 'opacity', this.svgAlphaFull);
        }
        if (this.projectF.get('poleData').get('p1').value && this.projectF.get('poleData').get('p3').value) {
          SVG.setComponentStyle(this.topSpecView, 'wall-p2p4', 'opacity', this.svgAlphaFull);
        }
        if (this.projectF.get('poleData').get('p3').value && this.projectF.get('poleData').get('p4').value) {
          SVG.setComponentStyle(this.topSpecView, 'wall-p1p2', 'opacity', this.svgAlphaFull);
        }
        if (this.projectF.get('poleData').get('p2').value && this.projectF.get('poleData').get('p4').value) {
          SVG.setComponentStyle(this.topSpecView, 'wall-p1p3', 'opacity', this.svgAlphaFull);
        }
        break;
      case 3:
      case 5:
        if (this.projectF.get('poleData').get('p1').value || this.projectF.get('poleData').get('p2').value) {
          SVG.setComponentStyle(this.topSpecView, 'wall-p3p4', 'opacity', this.svgAlphaFull);
        }
        if (this.projectF.get('poleData').get('p3').value || this.projectF.get('poleData').get('p4').value) {
          SVG.setComponentStyle(this.topSpecView, 'wall-p1p2', 'opacity', this.svgAlphaFull);
        }

        if (this.projectF.get('poleData').get('p1').value || this.projectF.get('poleData').get('p3').value) {
          SVG.setComponentStyle(this.topSpecView, 'wall-p2p4', 'opacity', this.svgAlphaFull);
        }
        if (this.projectF.get('poleData').get('p2').value || this.projectF.get('poleData').get('p4').value) {
          SVG.setComponentStyle(this.topSpecView, 'wall-p1p3', 'opacity', this.svgAlphaFull);
        }
        break;
    }
    */
  }

  legendRefresh() {
    for (let i = 1; i < 7; i++) {
      SVG.setComponentStyle(this.legendView, 'pergola' + i, 'opacity', this.projectF.get('installationType').value === i ? this.svgAlphaFull : this.svgAlphaNone);
    }
  }

  onPergolaSvgLoad(e: SVGElement) {
    this.topSpecView = e;
    this.mwpRefresh();
  }

  onLegendSvgLoad(e: SVGElement) {
    this.legendView = e;
    this.legendRefresh();
  }

}
