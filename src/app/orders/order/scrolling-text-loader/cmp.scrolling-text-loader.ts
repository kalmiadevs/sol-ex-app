import {Component, Input, OnDestroy, OnInit} from '@angular/core';

@Component({
  selector: 'app-scrolling-text-loader',
  templateUrl: './tpl.scrolling-text-loader.html',
  styleUrls: ['./cmp.scrolling-text-loader.scss']
})
export class ScrollingTextLoaderComponent implements OnInit, OnDestroy {

  private _lines: string[];
  private linesInternal: any[];

  @Input() delay: number = 2200;
  @Input()
  set lines(value: string[]) {
    if (value.length < 3) {
      throw new Error('ScrollingTextLoaderComponent requires at least 3 lines');
    }
    this._lines = value;
    this.linesInternal = this.lines.concat(this.lines);
    this.linesInternal = this.linesInternal.map((x, i) => {
      return { text: x, offset: i * 30, checkIndex: i }
    });
    this.currentIndex = 0;
  }

  get lines(): string[] {
    return this._lines;
  }

  private spinInterval: any;
  private currentIndex: number = 0;

  ngOnInit() {
    this.start();
  }

  ngOnDestroy() {
    clearInterval(this.spinInterval);
  }

  private calcOpacity(offset: number) {
    return 1 - Math.min(60, Math.abs(offset - 30)) / 45;
  }

  private start() {
    this.spinInterval = setInterval(() => {
      this.currentIndex++;

      for (const x of this.linesInternal) {
          x.offset -= 30;
          if (x.offset < -30) {
            x.offset = 30 * this.lines.length - x.offset;
            x.checkIndex = this.currentIndex + this.lines.length + 2;
          }
      }
    }, this.delay);
  }
}
