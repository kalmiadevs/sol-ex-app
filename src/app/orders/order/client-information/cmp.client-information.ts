import { Component, Input, OnInit } from '@angular/core';
import { Order } from '../../cls.order';
import { FormGroup } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import { fromPromise } from 'rxjs/observable/fromPromise';
import { OrderService } from '../../svc.order';

@Component({
  selector: 'app-client-information',
  templateUrl: './tpl.client-information.html',
  styleUrls: ['./cmp.client-information.scss']
})
export class ClientInformationComponent implements OnInit {

  @Input() order: Order;
  @Input() orderForm: FormGroup;
  @Input() orderService: OrderService;

  get clientF(): FormGroup {
    return this.orderForm.controls.client as FormGroup;
  }

  get deliveryInformationForm(): FormGroup {
    return this.orderForm.controls.delivery as FormGroup;
  }

  get projectInformationForm(): FormGroup {
    return this.orderForm.controls.project as FormGroup;
  }

  private typeaheadClients;

  constructor() { }

  ngOnInit() {
    this.orderService
        .suggestClients()
        .then((data: any) => {
          this.typeaheadClients = data.data.clients;
        })
  }

  suggestClients = (text: Observable<string>) => {
    // let searchTerm;
    return text
            .debounceTime(250)
            .distinctUntilChanged()
            .map(term => term.length < 2 ? []
              : this.typeaheadClients.filter(v => (v.name || '').toLowerCase().indexOf(term.toLowerCase()) > -1).map(x => x.name));

      // .do(query => { searchTerm = query; })
      // .switchMap(query => this.typeaheadClients || fromPromise(this.orderService.suggestClients()))
      // // .switchMap(query => this.typeaheadClients)
      // .map(res => {
      //   if (!this.typeaheadClients) {
      //     this.typeaheadClients = (res['status'] && res['status'] === 200) ? res['data'].clients : [];
      //   }

      //   // return searchTerm.length < 2 ? [] : this.typeaheadClients.filter(v => (v.name || '').toLowerCase().indexOf(searchTerm.toLowerCase()) > -1).map(x => x.spTitle);
      //   return searchTerm.length < 2 ? [] : this.typeaheadClients.filter(v => (v.name || '').toLowerCase().indexOf(searchTerm.toLowerCase()) > -1).map(x => x.name);
      // })
  };

  onTypeaheadSelect(selectItem: any) {
    const clientData = this.typeaheadClients.find(x => { return x.name === selectItem.item });
    this.clientF.get('name').setValue(clientData.spTitle);
    this.clientF.get('phone').setValue(clientData.phone);
    this.clientF.get('email').setValue(clientData.email);
    this.clientF.get('vat').setValue(clientData.vat);
    let addressString = clientData.address + '\n';
    if (clientData.postNumber) {
      addressString = addressString + clientData.postNumber + '\n';
    }
    if (clientData.city) {
      addressString = addressString + clientData.city + '\n';
    }
    if (clientData.country) {
      addressString = addressString + clientData.country;
    }

    this.clientF.get('address').setValue(addressString.trim());
  }

  deliverySameAsClientChange(e) {
    if (e.target.checked) {
      this.orderForm.controls.delivery.disable();

      /*
      (this.orderForm.controls.delivery as FormGroup).controls.name.setValue('');
      (this.orderForm.controls.delivery as FormGroup).controls.address.setValue('');
      (this.orderForm.controls.delivery as FormGroup).controls.phone.setValue('');
      (this.orderForm.controls.delivery as FormGroup).controls.email.setValue('');
      */
    } else {
      this.orderForm.controls.delivery.enable();


      /*
      (this.orderForm.controls.delivery as FormGroup).controls.name.setValue(
        (this.orderForm.controls.client as FormGroup).controls.name.value
      );
      (this.orderForm.controls.delivery as FormGroup).controls.address.setValue(
        (this.orderForm.controls.client as FormGroup).controls.address.value
      );
      (this.orderForm.controls.delivery as FormGroup).controls.phone.setValue(
        (this.orderForm.controls.client as FormGroup).controls.phone.value
      );
      (this.orderForm.controls.delivery as FormGroup).controls.email.setValue(
        (this.orderForm.controls.client as FormGroup).controls.email.value
      );
      */
    }
  }
}
