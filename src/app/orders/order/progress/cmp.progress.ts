import { Component, Input, OnInit } from '@angular/core';
import { Order } from '../../cls.order';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-order-progress',
  templateUrl: './tpl.progress.html',
  styleUrls: ['./cmp.progress.scss']
})
export class OrderProgressComponent implements OnInit {

  @Input() order: Order;

  constructor() { }

  ngOnInit() {

  }
}
