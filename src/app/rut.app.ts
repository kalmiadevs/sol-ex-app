import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './base/core/auth/guard.auth';
import { routes as orderRoutes } from './orders/rut.order';
import { routes as newsRoutes } from './orders/news/rut.news';
import { ProjectBarComponent } from './project-bar/cmp.project-bar';
import { OrdersComponent } from './orders/cmp.orders';
import { LogsComponent } from './logs/cmp.logs';
import { UserTreeComponent } from './user-tree/cmp.user-tree';
import { AdminGuard } from './base/core/auth/guard.admin';
import {ExternalPublicAuthComponent} from './auth/external-public-auth/external-public-auth.component';
import {NoPublicGuard} from './base/core/auth/guard.nopublic';
import {OrderCompleteComponent} from './orders/external/order-complete/order-complete.component';

export const routes: Routes = [

  { path: '', redirectTo: '/orders', pathMatch: 'full' },

  { path: '', children: [

  ]},

  { path: 'external/order', component: ExternalPublicAuthComponent },

  {
    path: '',
    canActivate: [AuthGuard],
    resolve: {
      vault: AuthGuard
    },
    children: [
      {canActivate: [AdminGuard], path: 'logs', component: LogsComponent},
      {canActivate: [AdminGuard], path: 'utree', component: UserTreeComponent},
      {path: 'order', canActivate: [AuthGuard], children: orderRoutes},
      {path: 'order-complete', canActivate: [AuthGuard], component: OrderCompleteComponent },
      {
        path: 'orders', canActivate: [NoPublicGuard], children: [
          {path: '', component: OrdersComponent},
          {
            path: '',
            outlet: 'sidebar',
            component: ProjectBarComponent
          },
        ]
      },
      {path: 'news', canActivate: [NoPublicGuard], children: newsRoutes},
    ],
  },
  { path: '**', redirectTo: '/orders' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: []
})
export class AppRoutingModule { }
