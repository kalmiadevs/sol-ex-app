import { baseAppConf } from './base/app/conf.app';
import { NoneComponent } from './base/app/cmp.none';
import { UserBarComponent } from './user-bar/cmp.user-bar';
import {ProfileComponent} from './user/profile/cmp.profile';

/**
 * Custom config specific to this project.
 * @type {{}}
 */
export const appConf = {
  ...baseAppConf,
  userAllowRegistration: false,
  userPublicProjectBar: NoneComponent,
  userAuthProjectBar: UserBarComponent,
  userLoginHeader: undefined,
  userProfileComponent: ProfileComponent,

  enableKalBlocks: false,
  topBarHomeLink: '',

  customLoginFields: ['vat number'],

  footerText: 'Copyright &copy; 2017 Soltec Partner Portal by Kalmia LTD',
};
