import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ExternalPublicAuthComponent } from './external-public-auth/external-public-auth.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [ExternalPublicAuthComponent]
})
export class AuthModule { }
