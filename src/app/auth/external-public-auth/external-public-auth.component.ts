import { Component, OnInit } from '@angular/core';
import { HttpService } from '../../base/core/http/svc.http';
import { environment } from '../../../environments/environment';
import { SystemService } from '../../core/svc.system';
import { SimpleNotification } from '../../base/app/notifications';
import { I18nService } from '../../base/core/i18n/svc.i18n';
import { AuthService } from '../../base/core/auth/svc.auth';
import { ActivatedRoute, Router } from '@angular/router';
import * as bowser from 'bowser';

@Component({
  selector: 'app-external-public-auth',
  templateUrl: './external-public-auth.component.html',
  styleUrls: ['./external-public-auth.component.scss']
})
export class ExternalPublicAuthComponent implements OnInit {

  private deviceNotSupported;

  constructor(protected http: HttpService,
              protected sys: SystemService,
              protected i18n: I18nService,
              protected authService: AuthService,
              protected router: Router,
              protected activatedRoute: ActivatedRoute) {
  }

  ngOnInit() {
    if (bowser.mobile || bowser.tablet) {
      this.deviceNotSupported = true;
      return;
    }

    this.activatedRoute.queryParams.subscribe(params => {
      const countryParam = params['country'] || 'si'; // Default to si
      const sourceParam = params['src'];

      const requestBody = {
        meta: {
          country: countryParam,
          source: sourceParam,
        }
      };

      this.http.request(`${ environment.apiBaseProject }/public/login`, { method: 'POST', body: requestBody }).subscribe((data: any) => {
        if (!data || !data.token) {
          return this.sys.notificationService.push(new SimpleNotification('error', this.i18n.get('externalServiceErrorTitle'), this.i18n.get('externalServiceErrorContent')).setTimeout(5000));
        } else {
          this.authService.setToken(data.token);
          this.router.navigate(['order']);
        }
      });
    })
  }

}
