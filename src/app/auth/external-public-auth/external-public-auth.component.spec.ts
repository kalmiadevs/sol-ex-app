import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExternalPublicAuthComponent } from './external-public-auth.component';

describe('ExternalPublicAuthComponent', () => {
  let component: ExternalPublicAuthComponent;
  let fixture: ComponentFixture<ExternalPublicAuthComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExternalPublicAuthComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExternalPublicAuthComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
