import { Component, OnInit } from '@angular/core';
import { AuthService } from '../base/core/auth/svc.auth';

@Component({
  selector: 'app-project-bar',
  templateUrl: './tpl.project-bar.html',
  styleUrls: ['./cmp.project-bar.scss']
})
export class ProjectBarComponent implements OnInit {

  links = [
   // {label: 'Orders', routerLink: ['/orders']},
   // {label: 'Settings', routerLink: ['/settings']},
  ];

  constructor(private authService: AuthService) { }

  ngOnInit() {

  }

}
