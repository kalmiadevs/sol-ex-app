import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ProfileComponent } from './profile/cmp.profile';
import {ValidationModule} from '../base/shared/form/validation/mod.validation';
import {BaseSharedModule} from '../base/shared/mod.shared';
import {UserRoutingModule} from '../base/app/user/rut.user';

@NgModule({
  imports: [
    CommonModule,
    UserRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    ValidationModule,
    BaseSharedModule
  ],
  providers: [

  ],
  declarations: [
    ProfileComponent
  ],
  entryComponents: [

  ],
  exports: [
    UserRoutingModule,
    ProfileComponent,
  ]
})
export class UserModule { }
