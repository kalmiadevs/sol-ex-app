import { baseAppConfNoDep } from './base/app/conf.app-no-dep';
export const appConfNoDep = {
  ...baseAppConfNoDep,

  userCertEnabled: false,

  userLogsEnabled: false
};

