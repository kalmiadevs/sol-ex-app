import { Component, OnInit } from '@angular/core';
import { I18nService, _ } from '../base/core/i18n/svc.i18n';

@Component({
  selector: 'app-user-bar',
  templateUrl: './tpl.user-bar.html',
  styleUrls: ['./cmp.user-bar.scss']
})
export class UserBarComponent implements OnInit {

  links = [
    {label: _('userBarLinkLabelProfile'), routerLink: ['/user/profile']},
    {label: _('userBarLinkLabelAcount'), routerLink: ['/user/account']},
    // {label: 'Logs', routerLink: ['/user/logs']},
  ];

  constructor() {}

  ngOnInit() {
  }

}
