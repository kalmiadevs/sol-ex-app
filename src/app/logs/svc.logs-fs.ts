import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { AuthService as Ng2UiAuthService } from 'ng2-ui-auth';
import { BaseFileSystemService } from '../base/entity/file-system/svc.file-system';
import { HttpAuthService } from '../base/core/http/svc.http-auth';

@Injectable()
export class LogsFsService extends BaseFileSystemService {

  constructor(
    protected http: HttpAuthService,
    protected ng2UiAuthService: Ng2UiAuthService,
  ) {
    super(http, ng2UiAuthService);
    this.bucket = 'logs';
  }

}
