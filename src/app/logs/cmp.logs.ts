import { Component, OnInit } from '@angular/core';
import { LogsFsService } from './svc.logs-fs';
import { union, without } from 'lodash';
import { FilePickerService } from '../base/entity/file-system/file-picker/svc.file-picker';
import { FsItem } from '../base/entity/file-system/cls.fs-node';
import { environment } from '../../environments/environment';
import { HttpAuthService } from '../base/core/http/svc.http-auth';
import { BaseUserService } from '../base/entity/user/svc.user';

@Component({
  selector: 'app-logs',
  templateUrl: './tpl.logs.html',
  styleUrls: ['./cmp.logs.scss']
})
export class LogsComponent implements OnInit {

  apiBase = environment.apiBaseProject;
  loaded      = false;
  syncRes;
  pwResetRes;
  spAction;
  spData;

  path: string;
  alerts: any[]  = [];
  table: any[]   = [];
  keys: string[] = [];

  activeUsers: any[] = [];

  detail: any;

  constructor(
    public fs: LogsFsService,
    private us: BaseUserService,
    public filePickerService: FilePickerService,
    protected http: HttpAuthService
  ) { }

  ngOnInit() {
    this.us.loadEntitiesFromRemote()
      .then(x => {
        this.activeUsers = x.data.users.filter(user => user.active);
      });
  }

  load(text) {
    this.alerts = [];
    this.table  = [];
    this.keys   = [];
    this.detail = undefined;

    text.split('\n').forEach(x => {
      if (!x) {
        return;
      }

      try {
        const n = JSON.parse(x);
        if (n.__alert) {
          const timestamp = n.__timestamp;
          const type = n.__type;
          delete n.__alert;
          delete n.__type;
          delete n.__timestamp;
          this.alerts.push({
            val      : n,
            timestamp: timestamp,
            type     : type,
          });
        } else {
          this.keys = union(this.keys, Object.keys(n));
          this.table.push(n);
        }
      } catch (e) {
        console.log(x);
        console.error(e);
      }
    });

    this.keys = without(this.keys, '__alert', '__timestamp', '__type');
    this.keys.unshift('__timestamp');
  }

  open(i: FsItem) {
    this.alerts = [];
    this.table  = [];
    this.keys   = [];
    this.detail = undefined;

    if (i.isFile()) {
      this.path = i.path;
      this.fs.downloadText(i)
        .then(x => {
          this.load(x);
          this.loaded = true;
        })
        .catch(console.error)
    }
  }

  syncAll() {
    this.syncRes = 'please wait ...';
    this.http.get(`${ this.apiBase }/sync`)
      .subscribe((x) => {
        this.syncRes = (x as any).data || x;
      }, (e) => {
        this.syncRes = e;
      });
  }

  sync() {
    this.http.get(`${ this.apiBase }/sync/${ this.spAction }${ this.spData ? ('/' + this.spData) : '' }`)
      .subscribe((x) => {
        this.syncRes = x;
      }, (e) => {
        this.syncRes = e;
      });
  }

  resetPassword(userId) {
    this.http.post(`${ this.apiBase }/admin/reset-user-password`, { id: userId })
      .subscribe((x) => {
        this.pwResetRes = x;
      }, (e) => {
        this.pwResetRes = e;
      });
  }
}
