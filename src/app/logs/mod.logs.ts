import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LogsComponent } from './cmp.logs';
import { FilePickerModule } from '../base/entity/file-system/file-picker/mod.file-picker';
import { FileManagerModule } from '../base/entity/file-system/file-manager/mod.file-manager';
import { LogsFsService } from './svc.logs-fs';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    FileManagerModule,
    FilePickerModule
  ],
  declarations: [
    LogsComponent
  ],
  providers: [
    LogsFsService
  ],
  exports: [
    LogsComponent
  ]
})
export class LogsModule { }
