import { Component, OnInit, AfterViewInit, Inject, ElementRef } from '@angular/core';
import { BaseUserService } from '../base/entity/user/svc.user';
import { DOCUMENT } from '@angular/common';
import { forEach } from 'lodash';
import { OrderService } from '../orders/svc.order';

/* tslint:disable:no-null-keyword */

@Component({
  selector: 'app-user-tree',
  templateUrl: './tpl.user-tree.html',
  styleUrls: ['./cmp.user-tree.scss']
})
export class UserTreeComponent implements OnInit, AfterViewInit {

  nodes: any[] = [];
  edges: any[] = [];

  constructor(
    private us: BaseUserService,
    private os: OrderService,
    @Inject(DOCUMENT) private document: any,
    private elementRef: ElementRef,
  ) { }

  ngOnInit() {

  }

  ngAfterViewInit() {
    this.us.loadEntitiesFromRemote()
      .then(x => {
        const sa = [];
        const aa = [];

        forEach(x.entities, (v, k) => {
          const level = {
            'superadmin': 0,
            'admin': 5,
            'user': 15,
          }[v.role];

          if (v.role === 'admin') {
            sa.push(v.id);
          }

          if (v.role === 'superadmin') {
            aa.push(v.id);
          }

          const node = { id: v.id, label: v.niceName(), level, color: '#466d98', font: { color: 'white' }  };
          this.nodes.push(node);

          if (v.meta) {
            if (v.meta.parent) {
              this.edges.push({from: v.id, to: v.meta.parent});
            }

            // if (v.meta.children) {
            //   v.meta.children.map(c => {
            //     this.edges.push({from: v.id, to: c});
            //   })
            // }
          }
        });

        aa.forEach(a => {
          sa.forEach(xx => {
            this.edges.push({from: a, to: xx});
          });
        });

        this.os.loadEntitiesFromRemote()
          .then( oRes => {
            forEach(oRes.entities, (v, k) => {
              const node = { id: v.id, label: v.niceName(), level: 25, shape: 'box' };
              this.nodes.push(node);

              this.edges.push({from: v.owner, to: v.id});
            });

            this.drawTree(this.us, this.os);
          });
      });
  }

  drawTree(us, os) {
    const s = this.document.createElement('script');
    s.type  = 'text/javascript';
    s.src   = '/assets/js/vis.min.js';
    this.elementRef.nativeElement.appendChild(s);

    const nodes = this.nodes;
    const edges = this.edges;
    let network = null;

    function destroy() {
      if (network !== null) {
        network.destroy();
        network = null;
      }
    }

    function draw() {
      destroy();

      // create a network
      const container = document.getElementById('mynetwork');
      const data = {
        nodes: nodes,
        edges: edges
      };

      const options = {
        edges: {
          smooth: {
            type: 'cubicBezier',
            forceDirection: 'vertical',
            roundness: 0.4
          }
        },
        layout: {
          hierarchical: {
            direction: 'UD'
          }
        },
        physics: false
      };
      network = new window['vis'].Network(container, data, options);

      // add event listeners
      network.on('select', function (params) {
        if (us.entityExists(params.nodes)) {
          const u = us.getEntity(params.nodes);
          console.log('#####################');
          console.log(`Info for user: ${ u.niceName() }`);
          console.log(u);
          console.log('#####################');
        } else if (os.entityExists(params.nodes)) {
          const o = os.getEntity(params.nodes);
          console.log('#####################');
          console.log(`Info for order: ${ o.niceName() }`);
          console.log(o);
          console.log('#####################');
        }
      });
    }

    s.onload = () => {
      draw();
    };
  }
}
