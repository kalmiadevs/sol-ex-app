import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserTreeComponent } from './cmp.user-tree';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [UserTreeComponent]
})
export class UserTreeModule { }
