import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule, RequestOptions, XHRBackend } from '@angular/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './rut.app';
import { AppComponent } from './cmp.app';
import { environment } from '../environments/environment';
import { BaseModule } from './base/mod.base';

/* Lib */
import { CustomConfig, Ng2UiAuthModule } from 'ng2-ui-auth';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

/* App core */
import { CoreModule } from './core/mod.core';

/* App modules */
import { BaseUserService } from './base/entity/user/svc.user';
import { BaseGroupService } from './base/entity/group/svc.group';
import { BasePermissionService } from './base/entity/permission/svc.permission';
import { ProjectBarComponent } from './project-bar/cmp.project-bar';
import { OrdersModule } from './orders/mod.orders';
import { ProjectBarModule } from 'app/base/app/project-bar/mod.project-bar';
import { UserBarComponent } from './user-bar/cmp.user-bar';
import { BaseErrorHandler } from './base/core/svc.report';
import { LogsModule } from './logs/mod.logs';
import { UserTreeModule } from './user-tree/mod.user-tree';
import { AppI18nHolderComponent } from './i18n/cmp.i18n';
import {UserModule} from './user/mod.user';
import { EntityModule } from './base/entity/mod.entity';
import {NewsModule} from './orders/news/mod.news';
import {AuthModule} from './auth/auth.module';
import {ExternalOrderModule} from './orders/external/external.module';
import { Angulartics2Module } from 'angulartics2';
import { Angulartics2GoogleTagManager } from 'angulartics2/gtm';
import { Angulartics2GoogleAnalytics } from 'angulartics2/ga';

/* Validators */
// --

export class SatellizerConfig extends CustomConfig {
  baseUrl = environment.apiBaseIDMS;
  defaultHeaders = { 'Content-Type': 'application/json' };
  loginUrl = '/login';
  signupUrl = '/auth/signup';
  unlinkUrl = '/auth/unlink/';
  authHeader = 'X-Access-Token';
  authToken = '';
}

@NgModule({
  declarations: [
    AppComponent,
    ProjectBarComponent,
    UserBarComponent,
    AppI18nHolderComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    Ng2UiAuthModule.forRoot(SatellizerConfig),
    BaseModule,
    CoreModule,
    AppRoutingModule,
    NgbModule.forRoot(),
    BrowserAnimationsModule,
    OrdersModule,
    ExternalOrderModule,
    NewsModule,
    LogsModule,
    UserTreeModule,
    ProjectBarModule,
    UserModule,
    EntityModule,
    AuthModule,
    Angulartics2Module.forRoot([Angulartics2GoogleAnalytics], {
      pageTracking: {
        excludedRoutes: [
          // Can't use regex literals: https://github.com/angular/angular/issues/14187
          '/print',
        ],
      }
    }),
  ],
  providers: [
    BaseGroupService,
    BaseUserService,
    BasePermissionService,
    { provide: ErrorHandler, useClass: BaseErrorHandler }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
