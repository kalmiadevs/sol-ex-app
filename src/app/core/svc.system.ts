import { Injectable } from '@angular/core';
import { map as _map } from 'lodash';
import { Router } from '@angular/router';
import { BaseNotificationService } from '../base/app/notifications/svc.notification';
import { AuthService, SignInData } from '../base/core/auth/svc.auth';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { BaseSystemService } from '../base/core/svc.system';
import { Entities } from '../base/entity/itf.entities';
import { EntityState } from '../base/entity/cls.entity-state';
import { StoreService } from '../base/core/svc.store';
import { HttpAuthService } from '../base/core/http/svc.http-auth';
import { AuthUser } from '../base/core/auth/cls.auth-user';
import { BaseUserService } from '../base/entity/user/svc.user';
import { BaseGroupService } from '../base/entity/group/svc.group';
import { BasePermissionService } from '../base/entity/permission/svc.permission';
import { OrderService } from '../orders/svc.order';
import { ArticleService } from '../orders/news/svc.article';

@Injectable()
export class SystemService extends BaseSystemService {

  constructor(
    protected router: Router,
    protected modalService: NgbModal,
    public authService: AuthService,
    public httpAuthService: HttpAuthService,
    public notificationService: BaseNotificationService,
    public userService: BaseUserService,
    public groupService: BaseGroupService,
    public orderService: OrderService,
    public articleService: ArticleService,
    public permissionService: BasePermissionService,
    public storeService: StoreService
  ) {
    super(router, modalService, authService, httpAuthService, notificationService, userService, groupService, permissionService, storeService);
    this.initServices();
  }

  protected initServices() {
    super.initServices(this);

    this.orderService.init(this);
    this.articleService.init(this);
  }

  onAuthExpired() {
    super.onAuthExpired();
  }

  /**
   * Trigger data load needed at start.
   */
  login(u: AuthUser, signInData?: SignInData): Promise<undefined> {
    return new Promise((resolve, reject) => {
      super.login(u, signInData)
        .then(() => {
          // this.groupService.sync();
          // this.userService.sync();
          resolve();
        })
        .catch(reject);
    });
  }

  hasPendingActions(): boolean {
    return false;
  }
}
