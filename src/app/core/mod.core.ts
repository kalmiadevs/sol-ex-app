import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SystemService } from './svc.system';

@NgModule({
  imports: [
    CommonModule
  ],
  providers: [
    SystemService
  ],
  declarations: []
})
export class CoreModule { }
