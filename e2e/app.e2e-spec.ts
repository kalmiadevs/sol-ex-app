import { PassManagerPage } from './app.po';

describe('pass-manager App', () => {
  let page: PassManagerPage;

  beforeEach(() => {
    page = new PassManagerPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!' as any);
  });
});
