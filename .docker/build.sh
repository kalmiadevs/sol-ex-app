#!/bin/sh

# NOTE: this file is copied into app root in docker!
echo "OS info:"
cat /etc/*-release

echo "build.sh location:"
pwd

############
##  PREPARE BAD
############
echo "-------------------------------------------"
echo "INSTALLING MODULES ..."
rm -rf node_modules/

yarn install
if [ $? -eq 0 ]
then
  echo "INSTALL OK"
else
  echo "INSTALL FAILED"
  exit 1
fi

npm run cli:link

echo "-------------------------------------------"
echo "INSERTING BUILD VERSION ..."
./src/app/base/.tools/version.sh SERVER_ENV


############
##  BUILD
############
echo "-------------------------------------------"
echo "BUILDING ..."
ng build --prod --environment=SERVER_ENV --aot=false --sourcemap
if [ $? -eq 0 ]
then
  echo "BUILD OK"
else
  echo "BUILD FAILED"
  exit 1
fi

# upload source maps to sentry & remove them from prod!
echo "-------------------------------------------"
echo "STARTING UPLOAD AND DELETE SOURCE MAPS ..."
./src/app/base/.tools/source-maps.sh


############
##  OUT
############
echo "-------------------------------------------"
echo "COPYING TO DIST ..."

# NOTE:
# build.tar.gz will have root owner!
cp -R /app/dist /dist/


############
##  EXTRA LOG
############
echo "-------------------------------------------"
echo "PACKAGES LIST:"
yarn list
