#!/usr/bin/env bash

# In case developer does not have it installed globally already:
npm install --save-dev node-sass

yarn install
yarn run serve
